#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
*************************************************************************
  1. Title   : My Utils
  2. Writer  : hElLoOnG
  3. Date    : 2017.11.01(Wed)
  4. ETC     :
  5. History
     - 2017.11.01, helloong, First Created
*************************************************************************
"""
import sys
import base64
import hashlib
import errno
import os
import sqlite3
import sys
import csv
import socket
import py7zr
from random import randint, uniform, seed, shuffle, sample
from datetime import *
import threading
import simplejson as json
# pip install python-dateutil
from dateutil.relativedelta import *
# pip install pycryptodomex
import base64
import Cryptodome.Random
from Cryptodome.Cipher import AES
from telebot import util
import csv
from xlsxwriter.workbook import Workbook

import kbpda_logger
from kbpda_query_map import QUERY_MAPS

LOGGER = kbpda_logger.MyLogger('kbpda_main', 'kbpda_util').make_logger('debug')
EXEPATH, EXEFILENAME = os.path.split(os.path.abspath(sys.executable))


class UserError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


def get_trace_this_point(type_gb):
    """
    Add, 2018.01.04, SYB, 호출되어진 포인트의 트레이스 정보를 이쁘게 돌려준다.
    :param type_gb: 0:"FILENAME", 1:"CODELINE", 2:"FUNCNAME", 3:"TEXT", 9:"ALL"
    :return: 0:"FILENAME", 1:"CODELINE", 2:"FUNCNAME", 3:"TEXT", 9:"ALL"
    """
    if 4 < type_gb < 0:
        return ""

    import traceback
    stack = traceback.extract_stack()
    if type_gb == 9:
        return stack[-2]
    else:
        return stack[-2][type_gb]


def force_type_casting(terget_str, encoding="euc-kr"):
    """
    Add, 2017.12.27, SYB, 입력받은 문자열을 강제 인코딩해서 이쁘게 돌려준다.
    :param terget_str:
    :param encoding:
    :return:
    """
    return terget_str if isinstance(terget_str, str) else terget_str.decode(encoding, errors='ignore')


def csv_to_excel(csv_file_name, excel_file_name, encoding='utf-8'):
    """
    Add, 2017.12.27, SYB, 입력받은 CSV파일을 강제 인코딩하여 EXCEL파일로 이쁘게 생성해준다.
    :param csv_file_name:
    :param excel_file_name:
    :param encoding:
    :return:
    """

    workbook = Workbook(excel_file_name)
    worksheet = workbook.add_worksheet()
    with open(csv_file_name, "rt", encoding='utf-8') as f:
        reader = csv.reader(f)
        for r, row in enumerate(reader):
            for c, col in enumerate(row):
                worksheet.write(r, c, force_type_casting(col, encoding))
    workbook.close()


def is_date(str_date):
    """
    Add, 2017.11.22, SYB, 입력받은 문자열이 날자형식 텍스트인지 확인하여 맞으면 True를 이쁘게 돌려준다.
    :param str_date:
    :return:
    """
    from datetime import datetime

    try:
        datetime.strptime(str_date, "%Y%m%d")
        return True
    except ValueError as e:
        LOGGER.debug(get_trace_this_point(9))
        LOGGER.debug(f'Errmsg: {e}')
        LOGGER.error(str_date)
        return False


def get_close_matches(str1, str_list, ret_num=1, cut_off=0.4):
    """
    Add, 2017.11.22, SYB, 입력받은 문자열을 입력받은 리스트에서 최대한 비슷한 값을 찾아 이쁘게 돌려준다
    :param str1:
    :param str_list:
    :param ret_num:
    :param cut_off:
    :return:
    """
    from difflib import get_close_matches as gcm

    LOGGER.debug(str1)
    LOGGER.debug(str_list)

    retval = gcm(str1, str_list, n=ret_num, cutoff=cut_off)

    if len(retval) <= 0:
        LOGGER.debug("근사값 검색 실패!!")
        return "000"
    else:
        LOGGER.debug(get_trace_this_point(9))
        LOGGER.debug(retval)
        return retval[0]


def get_today(format_str="%Y%m%d"):
    """
    Add, 2017.11.22, SYB, 오늘 로컬시스템 데이트 타임을 포멧에 맞춰 이쁘게 돌려준다.
    :param format_str:
    :return:
    """
    return datetime.now().strftime(format_str)


def get_date_add(format_str="%Y%m%d", addday=0):
    """
    Add, 2017.11.22, SYB, 오늘을 기준으로 주어진 addday 만큼의 전, 후 일자를 이쁘게 돌려준다.
    :param format_str:
    :return:
    """
    if addday is None:
        addday == 0
    return (date.today() + timedelta(days=addday)).strftime(format_str)


def get_last_month(format_str="%Y%m"):
    """
    Add, 2017.11.22, SYB, 직전월을 포멧에 맞춰 이쁘게 돌려준다.
    :param format_str:
    :return:
    """
    return (date.today().replace(day=1) - timedelta(days=1)).strftime(format_str)


def get_next_month(format_str="%Y%m"):
    """
    Add, 2017.11.22, SYB, 직전월을 포멧에 맞춰 이쁘게 돌려준다.
    :param format_str:
    :return:
    """
    return (date.today().replace(day=1) + timedelta(days=31)).strftime(format_str)


def get_same_weekday(base_date=datetime.date(datetime.now()), type_gb="LAST", format_str="%Y%m%d"):
    """
    Add, 2017.11.22, SYB, 기준일자를 기준으로 전주/다음주의 동요일 일자를 이쁘게 돌려준다.
    :param type_gb:
    :param base_date:
    :param format_str:
    :return:
    """
    week_days = [MO, TU, WE, TH, FR, SA, SU]

    if type_gb.upper() == "NEXT":
        retval = base_date + relativedelta(weekday=week_days[datetime.date(datetime.now()).weekday()](2))
    else:
        retval = base_date + relativedelta(weekday=week_days[datetime.date(datetime.now()).weekday()](-2))

    return retval.strftime(format_str)


def phone_format(phone_num):
    """
    전화번호를 이쁘게 대시바를 넣어서 이쁘게 돌려준다.
    :param phone_num:
    :return:
    """
    temp_phone = phone_num.replace("-", "")
    if len(temp_phone) == 11:
        return "{}-{}-{}".format(
            temp_phone[0:3],
            temp_phone[3:7],
            temp_phone[7:]
        )
    else:
        return "{}-{}-{}".format(
            temp_phone[0:3],
            temp_phone[3:6],
            temp_phone[6:]
        )


def is_all_ascii(str):
    """
    입력된 문자열이 전부 아스키코드인지 아닌지를 이쁘게 돌려준다.
    :param str:
    :return:
    """
    return all(ord(c) < 128 for c in str)



def str_format(string, width, align="<", fill=" "):
    """
    주어진 문자열을 오와 열을 맞추어 이쁘게 되돌려준다.
    :param string:
    :param width:
    :param align:
    :param fill:
    :return:
    """
    # Add, 2020.09.04, helloong, We can use f string!!
    align_string = f'{string:{fill}{align}{width}}'
    return align_string


def split_files_path_name_ext(return_type, full_file_name):
    """
    풀패스를 받아 리턴 타입별로 분리하여 이쁘게 돌려준다.
    :param return_type:
    :param full_file_name:
    :return:
    """
    import os
    path_only, file_name_ext = os.path.split(full_file_name)
    path_file_name, file_ext = os.path.splitext(full_file_name)
    file_name_only = os.path.splitext(os.path.split(full_file_name)[1])

    if str(return_type).upper() == "PATH":
        return path_only
    elif str(return_type).upper() == "FILENAMEEXT":
        return file_name_ext
    elif str(return_type).upper() == "PATHFILENAME":
        return path_file_name
    elif str(return_type).upper() == "FILEEXT":
        return file_ext
    elif str(return_type).upper() == "FILENAMEONLY":
        return file_name_only




def verify_korean(str_word):
    """
    입력된 단어가 전부 한글인지 판단하여 결과를 이쁘게 돌려준다.
    :param str:
    :return:
    """
    import re

    LOGGER.debug(len(str(str_word)))
    LOGGER.debug(len(re.findall("[가-힣]", str_word)))

    if len(str(str_word)) == len(re.findall("[가-힣]", str_word)):
        LOGGER.debug("한글!!")
        return True
    else:
        LOGGER.debug("안한글!!")
        return False


def get_rand_chars(length, type='NUM'):
    """
    랜덤한 문자열을 파라메터 길이만큼 이쁘게 돌려준다.
    :param length:
    :return:
    """
    import random
    import string

    if type.upper() == 'NUM':
        retval = ''.join(random.choice(string.digits) for _ in range(length))
    else:
        retval = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(length))
    LOGGER.debug(retval)

    return retval


def get_cur_date_time(format_str):
    """
    현재 로컬 일자/시간을 사용자 포멧에 맞게 이쁘게 돌려준다.
    :param format_str:
    :return:
    """
    from datetime import datetime

    # datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    # '2009-01-05 22:14:39'
    retval = datetime.now().strftime(format_str)
    # LOGGER.debug(retval)

    return retval


def is_file_exist(file_name):
    """
    해당 파일이 해당 경로에 제대로 존재하는지 여부를 이쁘게 알려준다.
    :param file_name:
    :return:
    """
    import os.path
    return os.path.isfile(file_name)


def check_n_mkdir(dirpath, type='M'):
    """
    dirpath: The name of check or make directory name
    type: C => Check only, M => Check and Make
    Return values:
        if the dir is exists it will be True
        if the dir isn't exists it will be False
    """
    LOGGER.debug(get_trace_this_point(9))
    dir_exist = os.path.isdir(dirpath)
    if type == 'C':
        if dir_exist:
            return True
        else:
            LOGGER.debug(f'{dirpath} 경로 미존재!!')
            return False
    elif type == 'M':
        if dir_exist:
            return True
        else:
            try:
                LOGGER.debug(f'{dirpath} 경로 미존재 생성시도!!')
                os.makedirs(dirpath, exist_ok=True)
                return True
            except OSError as e:
                if e.errno != errno.EEXIST:
                    LOGGER.debug(get_trace_this_point(9))
                    LOGGER.debug(f'Errmsg: {e}')
                    raise
                else:
                    return True


class AES256(object):
    def __init__(self, key="", iv=Cryptodome.Random.new().read(Cryptodome.Cipher.AES.block_size)):
        if type(key) in [bytearray, bytes]:
            self.key = key
        else:
            self.key = key.encode('utf-8')
        if type(iv) in [bytearray, bytes]:
            self.iv = iv
        else:
            self.iv = iv.encode('utf-8')

    def encrypt(self, clear_text):
        key = self.key
        key = key.ljust(32, "\0".encode('utf-8'))
        if len(key) > 32:
            key = key[:32]
        iv = self.iv
        iv = iv.ljust(16, "\0".encode('utf-8'))
        if len(iv) > 16:
            iv = iv[:16]
        pad_len = 16 - len(clear_text.encode('utf-8')) % 16
        padding = chr(pad_len) * pad_len
        clear_text += padding
        cryptor = AES.new(key, AES.MODE_CBC, iv)
        data = cryptor.encrypt(clear_text.encode('utf-8'))
        return base64.b64encode(data).decode('utf-8')

    def decrypt(self, data):
        data_byte = base64.b64decode(data.encode('utf-8'))
        key = self.key
        key = key.ljust(32, "\0".encode('utf-8'))
        if len(key) > 32:
            key = key[:32]
        iv = self.iv
        iv = iv.ljust(16, "\0".encode('utf-8'))
        if len(iv) > 16:
            iv = iv[:16]

        cryptor = AES.new(key, AES.MODE_CBC, iv)
        c_text = cryptor.decrypt(data_byte)
        pad_len = ord(c_text.decode('utf-8')[-1])
        clear_text = c_text.decode('utf-8')[:-pad_len]
        return clear_text


def quote(str):
    import urllib.parse
    return urllib.parse.quote(str)


def dequote(str):
    import urllib.parse
    return urllib.parse.unquote(str)


def clean_db(db_name):
    conn = sqlite3.connect(db_name)
    conn.execute("VACUUM")
    conn.close()


def write_db(db_name, write_query_map_name, check_query_map_name, cond=dict()):
    conn = sqlite3.connect(db_name)
    c = conn.cursor()
    c.execute(QUERY_MAPS[write_query_map_name].format(**cond))
    conn.commit()
    conn.close()

    return read_db(db_name, check_query_map_name, cond)


def read_db(db_name, query_map_name, cond=dict()):
    try:
        conn = sqlite3.connect(db_name)
        conn.row_factory = sqlite3.Row
        c = conn.cursor()
        c.execute(QUERY_MAPS[query_map_name].format(**cond))
        result = c.fetchall()
        result_set = []
        for row in result:
            result_set.append(dict(zip(row.keys(), row)))
        return result_set
    except Exception as e:
        LOGGER.error(e)
    finally:
        c.close()
        conn.close()


def json_dumps(json_data):
    LOGGER.debug(get_trace_this_point(9))
    retval = json.dumps(json_data)
    LOGGER.debug(retval)
    return retval


def rand_delay(startRange=0.3, endRange=2.0):
    randInterval = round(uniform(startRange, endRange), 1)
    LOGGER.debug('Time delay to prevent crawling detection!!'.format(randInterval))
    return randInterval


def rand_sample(list_data):
    if type(list_data) != list:
        return list_data

    try:
        list_data.pop(list_data.index('SNACK'))
        return ['SNACK'] + sample(list_data, len(list_data))
    except ValueError as e:
        return sample(list_data, len(list_data))


def write_csv_from_db(output_name, db_name, query_map_name, cond=dict()):
    with sqlite3.connect(db_name) as conn:
        conn.row_factory = sqlite3.Row
        c = conn.cursor()
        c.execute(QUERY_MAPS[query_map_name].format(**cond))
        data = c.fetchall()
    with open(output_name, 'w', encoding='utf-8', newline='') as f:
        csv_out = csv.writer(f)
        # write header
        csv_out.writerow([d[0] for d in c.description])
        # write data
        for result in data:
            csv_out.writerow(result)


def w_7zip(input_file, output_file, passwd=None):
    if passwd:
        with py7zr.SevenZipFile(output_file, 'w', password=passwd) as archive:
            archive.write(input_file)
    else:
        with py7zr.SevenZipFile(output_file, 'w') as archive:
            archive.write(input_file)


def get_my_ip():
    from requests import get
    try:
        ip = get('https://api.ipify.org').text
        if ip:
            return ip
        else:
            return 'NO_EXT_IP'
    except Exception as e:
        return 'ERR_EXT_IP'


def chk_user_identity():
    pass


def get_my_local_ip():
    try:
        hname = socket.gethostname()
        local_ip = socket.gethostbyname(hname)
        if local_ip:
            return local_ip
        else:
            return 'NO_LOCAL_IP'
    except Exception as e:
        return 'ERR_LOCAL_IP'


def get_executed_path():
    import os
    import sys
    if getattr(sys, 'frozen', False):
        retval = os.path.abspath(sys.executable)
    else:
        retval = os.path.abspath(__file__)
    return retval


if __name__ == '__main__':
    import requests

    data = {
        "tkotScreenId": "Default",
        "tkotRdpCode": "001",
        "tkinRdpCode": "004",
        "tkotDate": "20210323",
        "tkioDvsnCode": "003",
        "tkioQntt": 1,
        "cmdtJoCode": "15",
        "cmdtId": "5800006284916",
        "cmdtPrce": 61600,
        "handlingUser": {},
        "isGridEvent": "false",

        "tkotWrkNum": "00001",
        "boxNum": "70691962"
    }

    json_val = json.dumps(data)
    r = requests.get(f'http://10.101.10.55/store/kbpda/saveRequestTkoutFromPDA.action?params={quote(json_val)}')
    ret_json = json.loads(r.text)
    print(ret_json['key1'])
