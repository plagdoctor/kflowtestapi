#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
*************************************************************************
  1. Title   :
  2. Writer  : hElLoOnG
  3. Date    :
  4. ETC     :
  5. History
     - 20XX.XX.XX, helloong, First Created
     - Add, 2021.01.20, 심윤보, 바로결제API, registerSalePickupQueueTable, 바로드림 매출 생성 대상 이력 생성
     - Add, 2021.01.21, 도지원, 바로결제API, 바로드림 오늘배송 보관완료 상태 처리를 위한 집책완료 상태인 주문 조회
*************************************************************************
"""

QUERY_MAPS = {
    # Add, 2020.05.12, 심윤보, flasktest01.insert_bksh_bks().cmdt_main_detail_bksh_num, SS_BookshelfBooksManagement.findStoreBookshelfBooksInfm 점포별서가도서:상품안내Main
    "findDirectPickupOrderDetailsNew": """
    SELECT T1.ordr_id                                                 AS orderNo
         , T8.cmdt_code                                               AS barcode
         , T8.onln_cmdt_dvsn_code                                     AS bookGb
         , Rtrim(T2.jo_code)                                          AS joCode
         , T9.bookname                                                AS bookNm
         , T9.pubname                                                 AS pubNm
         , T9.author                                                  AS author
         , T9.pubymd                                                  AS pubYmd
         , T2.cmdt_prce_amnt                                          AS junga
         , T2.cmdt_sls_amnt                                           AS orderPrice
         , IsNull(T6.orig_drcs_qntt, 0) - IsNull(T6.ttl_cncl_qntt, 0) AS orderQty
         , 0                                                          AS realStockQty
         , T6.dlvr_prgs_cdtn_code                                     AS stateGb
         , T5.is_balju_tg                                             AS isBaljuTg
         , T5.collect_yn                                              AS collectYn
         , T5.transfer_yn                                             AS transferYn
         , T5.pickup_yn                                               AS pickupYn
         , T5.change_yn                                               AS changeYn
         , T5.pickup_self_yn                                          AS pickupSelfYn
         , T5.return_yn                                               AS returnYn
         , 0                                                          AS leadTime
         , T5.sms_yn                                                  AS smsYn
         , T5.book_state                                              AS bookState
         , T5.is_call_yn                                              AS isCallYn
         , T5.after_call_proc                                         AS afterCallProc
         , T5.ori_balju_type                                          AS oriBaljuType
         , T5.balju_yn                                                AS baljuYn
         , T2.dlvr_dtl_shp_code                                       AS sendMethod
         , T5.ori_barcode                                             AS oriBarcode
         , T5.ori_bookgb                                              AS oriBookGb
         , T1.ordr_date                                               AS orderYmd

         , T4.invn_bran_code                                          AS siteGubun
         , T4.invn_bran_code                                          AS frmrRdpCode
         , T4.rcpt_bran_code                                          AS rcvSite
         , T2.ordr_srmb                                               AS ordrSrmb
         , T2.ordr_cmdt_srmb                                          AS ordrCmdtSrmb
         , T2.ordr_cmdt_pros_srmb                                     AS ordrCmdtProsSrmb
         , T6.dlvr_requ_id                                            AS dlvrRequId
         , T6.dlvr_requ_iem_srmb                                      AS dlvrRequIemSrmb
         , T8.unfy_cmdt_id                                            AS unfyCmdtId
         , T4.ordr_dlpn_id                                            AS ordrDlpnId

         , T2.dlvr_rspb_code                                          AS dlvrRspbCode
         , EBIO.dbo.ufn_code_name('1693', T2.dlvr_rspb_code)          AS dlvrRspbName
         , T2.dlvr_shp_code                                           AS dlvrShpCode
         , EBIO.dbo.ufn_code_name('1692', T2.dlvr_shp_code)           AS dlvrShpName
         , T2.dlvr_dtl_shp_code                                       AS dlvrDtlShpCode
         , EBIO.dbo.ufn_code_name('1731', T2.dlvr_dtl_shp_code)       AS dlvrDtlShpName
         , EBIO.dbo.ufn_code_name('1691', T6.dlvr_prgs_cdtn_code)     AS dlvrPrgsCdtnName

         , IsNull(T5.jibgb, '0')                                      AS jibGb
         
         , IsNull(T1.mmbr_num, '')                                    AS mmbrNum
         , (SELECT S1.rdp_code
              FROM ibims..TM_RDP S1
             WHERE S1.frmr_rdp_code = T4.invn_bran_code
               AND S1.dlt_ysno = 'N')                                 AS rdpCode
         , IsNull((SELECT max(S10.cmdt_id)
                     FROM ibims..TM_CMDT S10
                    WHERE S10.cmdt_code = T9.barcode
                      AND S10.dlt_ysno = 'N'), '')                    AS cmdtId
      FROM EBIO..TM_ORDR          T1
         , EBIO..TD_ORDR_CMDT     T2
         , EBIO..TI_DLVR_REQU     T3
         , EBIO..TM_ORDR_DLPN     T4
         , EBIO..TD_ORDR_PICKUP   T5
         , EBIO..TI_DLVR_REQU_IEM T6
         , EBIO..TM_UNFY_CMDT     T8
         , cti..CTI_CD10          T9
     WHERE T1.ordr_id             = T2.ordr_id
       AND T1.ordr_srmb           = T2.ordr_srmb
       AND T2.ordr_id             = T3.ordr_id
       AND T2.ordr_srmb           = T3.ordr_srmb

       AND T2.dlvr_rspb_code      = T3.dlvr_rspb_code
       AND T2.dlvr_shp_code       = T3.dlvr_shp_code
       AND T2.dlvr_dtl_shp_code   = T3.dlvr_dtl_shp_code

       AND T3.ordr_dlpn_id        = T4.ordr_dlpn_id
       AND T3.dlvr_requ_id        = T6.dlvr_requ_id

       AND T2.ordr_id             = T5.ordr_id
       AND T2.ordr_srmb           = T5.ordr_srmb
       AND T2.ordr_cmdt_srmb      = T5.ordr_cmdt_srmb
       AND T2.ordr_cmdt_pros_srmb = T5.ordr_cmdt_pros_srmb
       AND T5.ordr_id             = T6.ordr_id
       AND T5.ordr_srmb           = T6.ordr_srmb
       AND T5.ordr_cmdt_srmb      = T6.ordr_cmdt_srmb
       AND T5.ordr_cmdt_pros_srmb = T6.ordr_cmdt_pros_srmb
       AND T5.unfy_cmdt_id        = T8.unfy_cmdt_id
       AND T5.ori_bookgb          = T9.bookgb
       AND T8.cmdt_code           = T9.barcode
       AND T1.pros_rslt_code      = '100'
       AND T2.dlvr_rspb_code      = '050'          /* 배송담당코드 050 영업점 */
       AND T2.dlvr_shp_code       = '130'          /* 배송형태코드 130 바로드림 */
       AND T1.ordr_dvsn_code      = '100'
       AND T1.ordr_date          >= '20200301'
       AND T5.collect_yn          = 'N'
       AND T5.transfer_yn         = 'N'
       AND T5.pickup_yn           = 'N'
       AND T5.is_balju_tg         = 'N'
       AND T6.dlvr_prgs_cdtn_code = '101'
       AND T5.ordr_id             = '{ordrNum}'
        AT ISOLATION 0
    """,
    # Add, 2020.05.12, 심윤보, 키오스 바로드림 매출처리 프로시저 호출
    "modifyDirectPickupOrderStateNew": """
        DECLARE @dlvrRequId CHAR(13), @dlvrRequIemSrmb INT, @newStateGb CHAR(3), @returnOutput INT, @returnOutputMsg CHAR(500)
        EXEC EBIO.dbo.usp_dlvr_mdfc_rcms '{dlvrRequId}', {dlvrRequIemSrmb}, '{stateGb}', @returnOutput output, @returnOutputMsg output
        SELECT @returnOutput as retCode, @returnOutputMsg as retMsg
    """,
    # Add, 2020.05.12, 심윤보, 키오스 바로드림 매출처리 프로시저 호출
    "registerSaleProcessProcedures": """
    DECLARE @dlvrRequId CHAR(13), @dlvrRequIemSrmb INT
    EXEC EBIO.dbo.usp_dlvr_sls_pros_snms '{dlvrRequId}', {dlvrRequIemSrmb}
    
    SELECT 0 AS retCode
    """,
    # Add, 2020.05.12, 심윤보, SS_DirectPickup.getDirectPickupSaleChkNew, 바로드림 기 매출처리 여부 체크
    "getDirectPickupSaleChkNew": """
        SELECT COUNT(*) AS existCount
          FROM EBIO..TI_DLVR_BFR_PROS T1
         WHERE T1.dlvr_requ_id = '{dlvrRequId}'
           AND T1.dlvr_requ_iem_srmb = CONVERT(numeric, {dlvrRequIemSrmb})
            AT ISOLATION 0
    """,
    # Add, 2020.05.12, 심윤보, SS_DirectPickup.registerSalePickupNewNew, 바로드림 매출 생성 대상 이력 생성
    "registerSalePickupNewNew": """
    INSERT INTO EBIO..TI_DLVR_BFR_PROS
        ( dlvr_requ_id
        , dlvr_requ_iem_srmb
        , sndg_srmb
        , invc_num
        , dlvr_qntt
        , ordr_dvsn_code
        , pros_rslt_code
        , crtr_id
        , cret_dttm
        , amnr_id
        , amnd_dttm
        , flag
        , dscm_code
        , dlvr_prgs_cdtn_code
        , ordr_id
        , rfnm_mthd_code
        , rfnm_bank_code
        , rfnm_bnac_num
        , rfnm_bnac_name
        , pstg_amnt
        )
    VALUES
        ( '{dlvrRequId}'
        , {dlvrRequIemSrmb}
        , 0
        , NULL
        , {orderQty}
        , '300'
        , '100'
        , 'P{crtrId}'
        , GETDATE()
        , 'P{crtrId}'
        , GETDATE()
        , 'N'
        , NULL
        , '804'
        , NULL
        , NULL
        , NULL
        , NULL
        , NULL
        , NULL
        )
        
    SELECT COUNT(*) AS insertedCnt
      FROM EBIO..TI_DLVR_BFR_PROS T1
     WHERE T1.dlvr_requ_id = '{dlvrRequId}'
       AND T1.dlvr_requ_iem_srmb = CONVERT(numeric, {dlvrRequIemSrmb})
        AT ISOLATION 0
    """,
    # Add, 2020.05.12, 심윤보, SS_DirectPickup.registerSaleProcessFailedLogNew, 바로드림 매출 생성 대상 이력 생성 실패 로그 생성
    "registerSaleProcessFailedLogNew": """
    INSERT INTO EBIO..TD_ORDR_PICKUP_LOG
              ( ordr_id
              , ordr_srmb
              , ordr_cmdt_srmb
              , ordr_cmdt_pros_srmb
              , unfy_cmdt_id
              , gb
              , regdate
              , regsabun
              , flag
              , bigo
              , procdate
              , procsabun
              , pdate
              , psabun
              )
         VALUES
              ( '{orderNo}'
              , CONVERT(numeric, {ordrSrmb})
              , CONVERT(numeric, {ordrCmdtSrmb})
              , CONVERT(numeric, {ordrCmdtProsSrmb})
              , '{unfyCmdtId}'
              , '102'
              , GETDATE()
              , 'P{crtrId}'
              , 'N'
              , '매출처리실패'
              , GETDATE()
              , 'P{crtrId}'
              )
    """,
    # Add, 2020.05.12, 심윤보, wmSeqService.nextTkotWrkNum, 통유 채번룰
    "getSeqNumFromKflow": """
    update IDCM..TM_IDPL_NBR
       set prst_nbr_num = prst_nbr_num + nbr_indc_wrth
         , amnr_id      = 'P{crtrId}'
         , amnd_dttm    = getdate()
     where nbr_trgt_tabl_id = '{tableName}'
       and nbr_trgt_colm_id = '{columnName}'
       and nbr_idnf_num     = '{today}'
       and end_nbr_num     >= prst_nbr_num + nbr_indc_wrth
       
    select prst_nbr_num AS seqNum
      from IDCM..TM_IDPL_NBR
     where nbr_trgt_tabl_id = '{tableName}'
       and nbr_trgt_colm_id = '{columnName}'
       and nbr_idnf_num     = '{today}'
    """,
    # Add, 2020.05.12, 심윤보, registerSeqNumToKflow, 통유 채번룰 없으면 생성
    "registerSeqNumToKflow": """
    INSERT INTO IDCM..TM_IDPL_NBR (nbr_trgt_tabl_id, nbr_trgt_colm_id, nbr_idnf_num, prst_nbr_num, sttg_nbr_num, end_nbr_num, nbr_indc_wrth, crtr_id, cret_dttm, amnr_id, amnd_dttm)
    VALUES ('{tableName}', '{columnName}', '{today}', 0, 1, 9999999999, 1, 'P{crtrId}', getdate(), 'P{crtrId}', getdate())
    
    select COUNT(*) AS cnt
      from IDCM..TM_IDPL_NBR
     where nbr_trgt_tabl_id = '{tableName}'
       and nbr_trgt_colm_id = '{columnName}'
       and nbr_idnf_num     = '{today}'
    """,
    # Add, 2020.05.12, 심윤보, checkExistsTodaySeqNum, 통유 채번룰 있나 없나?
    "checkExistsTodaySeqNum": """
    select COUNT(*) AS cnt
      from IDCM..TM_IDPL_NBR
     where nbr_trgt_tabl_id = '{tableName}'
       and nbr_trgt_colm_id = '{columnName}'
       and nbr_idnf_num     = '{today}'
        AT ISOLATION 0
    """,
    # Add, 2020.05.12, 심윤보, checkExistsTodaySeqNum, 통유 채번룰 있나 없나?
    "registerTakeInOut": """
    insert into IDPL..TM_TKIO(
        tkot_rdp_code,
        tkot_date,
        tkot_wrk_num,
        box_num,
        tkio_dvsn_code,
        tkot_ip_adrs,
        tkin_rdp_code,
        tkin_date,
        tkin_wrk_num,
        tkin_ip_adrs,
        brmv_phds_cnfr_date,
        tkin_cnfr_pers_id,
        tkin_cnfr_dttm,
        trct_box_num,
        brmv_cnfr_pers_id,
        brmv_cnfr_dttm,
        dta_mdfc_dvsn_code,
        snms_ysno,
        crtr_id,
        cret_dttm,
        amnr_id,
        amnd_dttm,
        dlt_ysno
    ) values (
        '{tkotRdpCode}',
        '{tkotDate}',
        '{tkotWrkNum}',
        '{boxNum}',
        '{tkioDvsnCode}',
        '{tkotIpAdrs}',
        '{tkinRdpCode}',
        '{tkinDate}',
        '{tkinWrkNum}',
        '{tkinIpAdrs}',
        '{brmvPhdsCnfrDate}',
        '{tkinCnfrPersId}',
        '{tkinCnfrDttm}',
        '{trctBoxNum}',
        '{brmvCnfrPersId}',
        '{brmvCnfrDttm}',
        '{dtaMdfcDvsnCode}',
        '{snmsYsno}',
        'P{crtrId}',
        getdate(),
        'P{crtrId}',
        getdate(),
        'N'
    )
    
    SELECT COUNT(*) AS insertedCnt
      FROM IDPL..TM_TKIO
     WHERE tkot_rdp_code = '{tkotRdpCode}'
       AND tkot_date     = '{tkotDate}'
       AND tkot_wrk_num  = '{tkotWrkNum}'
       AND box_num       = '{boxNum}'
        AT ISOLATION 0
    """,
    # Add, 2020.05.12, 심윤보, checkExistsTodaySeqNum, 통유 채번룰 있나 없나?
    "registerTakeInOutDetail": """
    DECLARE @tkotWrkSrmb INT
    
    SELECT @tkotWrkSrmb = isnull(max(tkot_wrk_srmb), 0) + 1
      FROM IDPL..TD_TKIO_DTL
     WHERE tkot_rdp_code = '{tkotRdpCode}'
       AND tkot_date     = '{tkotDate}'
       AND tkot_wrk_num  = '{tkotWrkNum}'
       AND box_num       = '{boxNum}'
       AND cmdt_id       = '{cmdtId}'

    insert into IDPL..TD_TKIO_DTL(
        tkot_rdp_code,
        tkot_date,
        tkot_wrk_num,
        box_num,
        cmdt_id,
        tkot_wrk_srmb,
        byng_srmb,
        tkio_qntt,
        cmdt_prce,
        ccbk_drcs_num,
        plor_num,
        plor_rlnc_num,
        rtgd_rsn_code,
        crpn_rtgd_num,
        crpn_rtgd_srmb,
        dta_mdfc_dvsn_code,
        snms_ysno,
        crtr_id,
        cret_dttm,
        amnr_id,
        amnd_dttm,
        dlt_ysno
    ) values (
          '{tkotRdpCode}'
        , '{tkotDate}'
        , '{tkotWrkNum}'
        , '{boxNum}'
        , '{cmdtId}'
        , @tkotWrkSrmb
        , {byngSrmb}
        , {tkioQntt}
        , {cmdtPrce}
        , '{ccbkDrcsNum}'
        , '{plorNum}'
        , '{plorRlncNum}'
        , '{rtgdRsnCode}'
        , '{crpnRtgdNum}'
        , {crpnRtgdSrmb}
        , '{dtaMdfcDvsnCode}'
        , '{snmsYsno}'
        , 'P{crtrId}'
        , getdate()
        , 'P{crtrId}'
        , getdate()
        , 'N'
    )
    
    SELECT COUNT(*) AS insertedCnt
      FROM IDPL..TD_TKIO_DTL
     WHERE tkot_rdp_code = '{tkotRdpCode}'
       AND tkot_date     = '{tkotDate}'
       AND tkot_wrk_num  = '{tkotWrkNum}'
       AND box_num       = '{boxNum}'
       AND tkot_wrk_srmb = @tkotWrkSrmb
       AND cmdt_id       = '{cmdtId}'
        AT ISOLATION 0
    """,

    # Add, 2020.05.12, 심윤보, 핫트랙스 상품여부 조회 1: 핫트랙스, 0:교보문고
    "getIsHotCmdtYsno": """
    /* 핫트랙스 상품여부 조회 SS_DirectPickup.getIsHotCmdtYsno */
    SELECT (CASE WHEN T1.cmdt_dvsn_code LIKE '40%' THEN 1 ELSE 0 END) AS isHotCmdtYsno
      FROM ibims..TM_CMDT T1
     WHERE T1.cmdt_code = '{barcode}'
       AND T1.dlt_ysno = 'N'
        AT ISOLATION 0
    """,

    # Add, 2020.05.12, 심윤보, 수수료율 적용 93조 5프로, 94조 9프로[요청: 핫트랙스 서국영과장님]
    "registerHotInterface": """
    /* 핫트랙스 상품 인터페이스 적재 SS_InventoryManagement.registerHotInterface */
    insert into IDHI..TI_KH_BRDR_RCPT_DSRP
      (
        ordr_id
      , ordr_srmb
      , ordr_cmdt_srmb
      , ordr_cmdt_pros_srmb
      , cmdt_id
      , cmdt_code
      , unfy_cmdt_id
      , rcpt_date
      , rcpt_tme
      , rcpt_wrkr_id
      , requ_qntt
      , cmdt_sls_amnt
      , kbc_fee_amnt
      , mmbr_num
      , rcpt_rdp_code
      , invn_rdp_code
      , pros_ysno
      , crtr_id
      , cret_dttm
      , amnr_id
      , amnd_dttm
      )
   SELECT '{orderNo}'
        , {ordrSrmb}
        , {ordrCmdtSrmb}
        , {ordrCmdtProsSrmb}
        , '{cmdtId}'
        , '{barcode}'
        , '{unfyCmdtId}'
        , CONVERT(CHAR(8), GETDATE(), 112)
        , CONVERT(CHAR(6), str_replace(CONVERT(CHAR(8), GETDATE(), 108), ':', NULL))
        , 'KIOSK'
        , {orderQty}
        , {orderPrice}
        , (CASE WHEN '{joCode}' = '93' THEN ROUND({orderPrice} * 0.05, 0)
                WHEN '{joCode}' = '94' THEN ROUND({orderPrice} * 0.09, 0)
                ELSE 0 END)
        , '{mmbrNum}'
        , '{rcvSite}'
        , '{siteGubun}'
        , 'N'
        , 'P{crtrId}'
        , getdate()
        , 'P{crtrId}'
        , getdate()
        
    SELECT count(*) AS insertedCnt
      FROM IDHI..TI_KH_BRDR_RCPT_DSRP
     WHERE ordr_id             = '{ordrId}'
       AND ordr_srmb           = {ordrSrmb}
       AND ordr_cmdt_srmb      = {ordrCmdtSrmb}
       AND ordr_cmdt_pros_srmb = {ordrCmdtProsSrmb}
        AT ISOLATION 0
    """,

    # Add, 2020.05.12, 심윤보, 수불처 재고 등록/변경
    "modifyRdpInventory": """
    /* 수불처 재고 등록/변경 SS_DirectPickup.modifyRdpInventory */
    DECLARE @existCnt INT, @invnAmndSrmb NUMERIC(8, 0)
    
    SELECT @invnAmndSrmb = IsNull(Max(invn_amnd_srmb), 0) + 1  
      FROM IDSS..TH_RDP_INVN_HSTR              
     WHERE rdp_code = '{rdpCode}'               
       AND cmdt_id  = '{cmdtId}'
    
    INSERT INTO IDSS..TH_RDP_INVN_HSTR (rdp_code, cmdt_id, invn_amnd_srmb, rcds_invn_qntt, real_invn_qntt, str_pren_invn_qntt, amnd_invn_qntt, crtr_id, cret_dttm)
    SELECT '{rdpCode}'                                 AS rdp_code,
           '{cmdtId}'                                  AS cmdt_id,
           @invnAmndSrmb                               AS invn_amnd_srmb,
           (SELECT IsNull(Max(rcds_invn_qntt), 0)      
              FROM IDSS..TM_RDP_INVN                   
             WHERE rdp_code = '{rdpCode}'
               AND cmdt_id  = '{cmdtId}')                AS rcds_invn_qntt,
           (SELECT IsNull(Max(real_invn_qntt), 0)      
              FROM IDSS..TM_RDP_INVN                   
             WHERE rdp_code = '{rdpCode}'
               AND cmdt_id  = '{cmdtId}')                AS real_invn_qntt,
           (SELECT IsNull(Max(str_pren_invn_qntt), 0)  
              FROM IDSS..TM_RDP_INVN                   
             WHERE rdp_code = '{rdpCode}'
               AND cmdt_id  = '{cmdtId}')                AS str_pren_invn_qntt, 
           {realInvnQntt}                              AS amnd_invn_qntt,
           'P{crtrId}'                                  AS crtr_id,
           getdate()                                   AS cret_dttm
    
    SELECT @existCnt = COUNT(*)
      FROM IDSS..TM_RDP_INVN
     WHERE rdp_code = '{rdpCode}'
       AND cmdt_id  = '{cmdtId}'
    
    IF (@existCnt = 0)
        BEGIN
            INSERT INTO IDSS..TM_RDP_INVN
            ( rdp_code
            , cmdt_id
            , rcds_invn_qntt
            , real_invn_qntt
            , str_pren_invn_qntt
            , crtr_id
            , cret_dttm
            , amnr_id
            , amnd_dttm
            , dlt_ysno
            )
            VALUES 
            ( '{rdpCode}'
            , '{cmdtId}'
            , {rcdsInvnQntt}
            , {realInvnQntt}
            , {strPrenInvnQntt}
            , 'P{crtrId}'
            , GETDATE()
            , 'P{crtrId}'
            , GETDATE()
            , 'N'
            )
        END
    ELSE
        BEGIN
            UPDATE IDSS..TM_RDP_INVN
               SET rcds_invn_qntt     = rcds_invn_qntt + {rcdsInvnQntt}
                 , real_invn_qntt     = real_invn_qntt + {realInvnQntt}
                 , str_pren_invn_qntt = str_pren_invn_qntt + {strPrenInvnQntt}
                 , amnr_id            = 'P{crtrId}'
                 , amnd_dttm          = GETDATE()
                 , dlt_ysno           = 'N'
             WHERE rdp_code           = '{rdpCode}'
               AND cmdt_id            = '{cmdtId}'
        END

    SELECT rdp_code                     AS rdpCode
         , cmdt_id                      AS cmdtId
         , invn_amnd_srmb               AS invnAmndSrmb
         , rcds_invn_qntt               AS rcdsInvnQntt
         , real_invn_qntt               AS realInvnQntt
         , str_pren_invn_qntt           AS strPrenInvnQntt
         , amnd_invn_qntt               AS amndInvnQntt
         , crtr_id                      AS crtrId
         , cret_dttm                    AS cretDttm
      FROM IDSS..TH_RDP_INVN_HSTR
     WHERE rdp_code       = '{rdpCode}'
       AND cmdt_id        = '{cmdtId}'
       AND invn_amnd_srmb = @invnAmndSrmb
        AT ISOLATION 0
    """,

    # Add, 2020.05.12, 심윤보, 점포 수불처 재고 변경 EAI 호출
    "registerEaiParamsInvn": """
    /* 점포 수불처 재고 변경 EAI 호출 SS_DirectPickup.registerEaiParamsInvn */
    exec IDSS..spe_TI_ISS_RLTM_KFLW 'TM_RDP_INVN','U','00','{rdpCode}','{cmdtId}', NULL, NULL, NULL
    
    SELECT 0 AS retCode
    """,

    # Add, 2020.05.12, 심윤보, 바로드림 수령 완료 처리
    "modifyDirectPickupActionYmdForPickupNew": """
    /* 점포 수불처 재고 변경 EAI 호출 SS_DirectPickup.modifyDirectPickupActionYmdForPickupNew */
    UPDATE EBIO..TD_ORDR_PICKUP
       SET collect_yn        = 'Y'
         , collect_ymd       = CONVERT(CHAR(8), GETDATE(), 112)
         , collect_time      = str_replace(convert(char, getdate(), 108), ':', null)
         , collect_userid    = 'KIOSK'
         , set_usestock_yn   = 'Y'
         , transfer_yn       = 'Y'
         , transfer_ymd      = convert(char(8), getdate(), 112)
         , transfer_time     = str_replace(convert(char, getdate(), 108), ':', null)
         , transfer_userid   = 'KIOSK'
         , pickup_yn         = 'Y'
         , pickup_ymd        = convert(char(8), getdate(), 112)
         , pickup_time       = str_replace(convert(char, getdate(), 108), ':', null)
         , pickup_userid     = 'KIOSK'
         , pickup_self_yn    = 'Y'
         , pickup_nm         = 'KIOSK'
         , pickup_id         = 'KIOSK'
         , pickup_tel        = 'KIOSK'
         , change_yn         = 'N'
         , etc_userid        = 'KIOSK'
         , sale_proc_yn      = 'Y'
         , free_usestock_yn  = 'Y'
         , real_stock_qty_yn = 'Y'
         , comp_proc_step    = '3'
         , pro_date          = getdate()
         , pro_sabun         = 'P{crtrId}'
     WHERE ordr_id             = '{orderNo}'
       AND ordr_srmb           = CONVERT(SMALLINT, {ordrSrmb})
       AND ordr_cmdt_srmb      = CONVERT(SMALLINT, {ordrCmdtSrmb})
       AND ordr_cmdt_pros_srmb = CONVERT(SMALLINT, {ordrCmdtProsSrmb})
       
    SELECT COUNT(*) AS updatedCnt
      FROM EBIO..TD_ORDR_PICKUP
     WHERE ordr_id             = '{orderNo}'
       AND ordr_srmb           = CONVERT(SMALLINT, {ordrSrmb})
       AND ordr_cmdt_srmb      = CONVERT(SMALLINT, {ordrCmdtSrmb})
       AND ordr_cmdt_pros_srmb = CONVERT(SMALLINT, {ordrCmdtProsSrmb})
       AND pickup_yn           = 'Y'
        AT ISOLATION 0
    """,
    # Add, 2020.05.12, 심윤보, SS_DirectPickup.getDirectPickupSaleChkNew, 바로드림 기 매출처리 여부 체크
    "findAuthIP": """
        SELECT count(*) AS existCnt
          FROM IDSS..TC_STR_CODE
         WHERE code_id        = '0012'
           AND code_wrth_name = '{ip}'
            AT ISOLATION 0
    """,

    "updateAuthIP": """
        UPDATE IDSS..TC_STR_CODE
           set code_id_dscr = '도매매출_계산서발행권한_{cnt}'
         WHERE code_id = '0011'
           AND code_wrth = '001'
           AND seq = 1
           
        select count(*) as existCnt
          from IDSS..TC_STR_CODE
         where code_id = '0011'
           and code_wrth = '001'
            AT ISOLATION 0
    """,
    "checkAlreadyExistTkio": """
        SELECT count(*) AS existCnt
          FROM IDPL..TM_TKIO T1, IDPL..TD_TKIO_DTL T2
         WHERE T1.tkot_rdp_code     = T2.tkot_rdp_code
           AND T1.tkot_date         = T2.tkot_date
           AND T1.tkot_wrk_num      = T2.tkot_wrk_num
           AND T1.box_num           = T2.box_num
           AND T1.tkot_rdp_code     = '{rdpCode}'
           AND T1.tkot_date         = '{tkotDate}'
           AND T1.box_num           = 'ONEDAYBX'
           AND T1.tkin_cnfr_pers_id = '{orderNo}'
           AND T1.crtr_id           = 'P{crtrId}'
           AND T2.cmdt_id           = '{cmdtId}'
            AT ISOLATION 0
    """,
    # Add, 2021.01.20, 심윤보, registerSalePickupQueueTable, 바로드림 매출 생성 대상 이력 생성
    "registerSalePickupQueueTable": """
        INSERT INTO EBIO..TI_KIOSK_SLS_PROS_REQU (ordr_id, sls_pros_ysno, crtr_id, cret_dttm, amnr_id, amnd_dttm)
             VALUES ('{orderNo}', 'N', 'P{crtrId}', getdate(), 'P{crtrId}', getdate())
        
        SELECT COUNT(*) AS insertedCnt
          FROM EBIO..TI_KIOSK_SLS_PROS_REQU
         WHERE ordr_id = '{orderNo}'
            AT ISOLATION 0
    """,
    # 바로드림 오늘배송 보관완료 상태 처리를 위한 집책완료 상태인 주문 조회
    "findDirectPickupOrderDetailsNewCcbk": """
        SELECT T1.ordr_id                                                 AS orderNo
            , T8.cmdt_code                                               AS barcode
            , T8.onln_cmdt_dvsn_code                                     AS bookGb
            , Rtrim(T2.jo_code)                                          AS joCode
            , T9.bookname                                                AS bookNm
            , T9.pubname                                                 AS pubNm
            , T9.author                                                  AS author
            , T9.pubymd                                                  AS pubYmd
            , T2.cmdt_prce_amnt                                          AS junga
            , T2.cmdt_sls_amnt                                           AS orderPrice
            , IsNull(T6.orig_drcs_qntt, 0) - IsNull(T6.ttl_cncl_qntt, 0) AS orderQty
            , 0                                                          AS realStockQty
            , T6.dlvr_prgs_cdtn_code                                     AS stateGb
            , T5.is_balju_tg                                             AS isBaljuTg
            , T5.collect_yn                                              AS collectYn
            , T5.transfer_yn                                             AS transferYn
            , T5.pickup_yn                                               AS pickupYn
            , T5.change_yn                                               AS changeYn
            , T5.pickup_self_yn                                          AS pickupSelfYn
            , T5.return_yn                                               AS returnYn
            , 0                                                          AS leadTime
            , T5.sms_yn                                                  AS smsYn
            , T5.book_state                                              AS bookState
            , T5.is_call_yn                                              AS isCallYn
            , T5.after_call_proc                                         AS afterCallProc
            , T5.ori_balju_type                                          AS oriBaljuType
            , T5.balju_yn                                                AS baljuYn
            , T2.dlvr_dtl_shp_code                                       AS sendMethod
            , T5.ori_barcode                                             AS oriBarcode
            , T5.ori_bookgb                                              AS oriBookGb
            , T1.ordr_date                                               AS orderYmd

            , T4.invn_bran_code                                          AS siteGubun
            , T4.invn_bran_code                                          AS frmrRdpCode
            , T4.rcpt_bran_code                                          AS rcvSite
            , T2.ordr_srmb                                               AS ordrSrmb
            , T2.ordr_cmdt_srmb                                          AS ordrCmdtSrmb
            , T2.ordr_cmdt_pros_srmb                                     AS ordrCmdtProsSrmb
            , T6.dlvr_requ_id                                            AS dlvrRequId
            , T6.dlvr_requ_iem_srmb                                      AS dlvrRequIemSrmb
            , T8.unfy_cmdt_id                                            AS unfyCmdtId
            , T4.ordr_dlpn_id                                            AS ordrDlpnId

            , T2.dlvr_rspb_code                                          AS dlvrRspbCode
            , EBIO.dbo.ufn_code_name('1693', T2.dlvr_rspb_code)          AS dlvrRspbName
            , T2.dlvr_shp_code                                           AS dlvrShpCode
            , EBIO.dbo.ufn_code_name('1692', T2.dlvr_shp_code)           AS dlvrShpName
            , T2.dlvr_dtl_shp_code                                       AS dlvrDtlShpCode
            , EBIO.dbo.ufn_code_name('1731', T2.dlvr_dtl_shp_code)       AS dlvrDtlShpName
            , EBIO.dbo.ufn_code_name('1691', T6.dlvr_prgs_cdtn_code)     AS dlvrPrgsCdtnName

            , IsNull(T5.jibgb, '0')                                      AS jibGb

            , IsNull(T1.mmbr_num, '')                                    AS mmbrNum
            , (SELECT S1.rdp_code
                  FROM ibims..TM_RDP S1
                WHERE S1.frmr_rdp_code = T4.invn_bran_code
                  AND S1.dlt_ysno = 'N')                                 AS rdpCode
            , IsNull((SELECT max(S10.cmdt_id)
                        FROM ibims..TM_CMDT S10
                        WHERE S10.cmdt_code = T9.barcode
                          AND S10.dlt_ysno = 'N'), '')                    AS cmdtId
         FROM EBIO..TM_ORDR          T1
            , EBIO..TD_ORDR_CMDT     T2
            , EBIO..TI_DLVR_REQU     T3
            , EBIO..TM_ORDR_DLPN     T4
            , EBIO..TD_ORDR_PICKUP   T5
            , EBIO..TI_DLVR_REQU_IEM T6
            , EBIO..TM_UNFY_CMDT     T8
            , cti..CTI_CD10          T9
        WHERE T1.ordr_id             = T2.ordr_id
          AND T1.ordr_srmb           = T2.ordr_srmb
          AND T2.ordr_id             = T3.ordr_id
          AND T2.ordr_srmb           = T3.ordr_srmb

          AND T2.dlvr_rspb_code      = T3.dlvr_rspb_code
          AND T2.dlvr_shp_code       = T3.dlvr_shp_code
          AND T2.dlvr_dtl_shp_code   = T3.dlvr_dtl_shp_code

          AND T3.ordr_dlpn_id        = T4.ordr_dlpn_id

          AND T2.dlvr_rspb_code      = T3.dlvr_rspb_code
          AND T2.dlvr_shp_code       = T3.dlvr_shp_code
          AND T2.dlvr_dtl_shp_code   = T3.dlvr_dtl_shp_code

          AND T3.ordr_dlpn_id        = T4.ordr_dlpn_id
          AND T3.dlvr_requ_id        = T6.dlvr_requ_id

          AND T2.ordr_id             = T5.ordr_id
          AND T2.ordr_srmb           = T5.ordr_srmb
          AND T2.ordr_cmdt_srmb      = T5.ordr_cmdt_srmb
          AND T2.ordr_cmdt_pros_srmb = T5.ordr_cmdt_pros_srmb
          AND T5.ordr_id             = T6.ordr_id
          AND T5.ordr_srmb           = T6.ordr_srmb
          AND T5.ordr_cmdt_srmb      = T6.ordr_cmdt_srmb
          AND T5.ordr_cmdt_pros_srmb = T6.ordr_cmdt_pros_srmb
          AND T5.unfy_cmdt_id        = T8.unfy_cmdt_id
          AND T5.ori_bookgb          = T9.bookgb
          AND T8.cmdt_code           = T9.barcode
          AND T1.pros_rslt_code      = '100'
          AND T2.dlvr_rspb_code      = '050'          /* 배송담당코드 050 영업점 */
          AND T2.dlvr_shp_code       = '130'          /* 배송형태코드 130 바로드림 */
          AND T2.dlvr_dtl_shp_code   = '027'
          AND T1.ordr_dvsn_code      = '100'
          AND T1.ordr_date          >= '20210101'
          AND T5.collect_yn          = 'Y'
          AND T5.transfer_yn         = 'N'
          AND T5.pickup_yn           = 'N'
          AND T5.is_balju_tg         = 'N'
          AND T6.dlvr_prgs_cdtn_code = '817'
          AND T5.ordr_id             = '{ordrNum}'
            AT ISOLATION 0
    """,

    # 바로드림 오늘배송 EBIO..TD_ORDR_PICKUP 테이블 유무 확인
    "getNowOrderDetailCountsNew": """
        SELECT COUNT(*)
          FROM EBIO..TD_ORDR_PICKUP
         WHERE ordr_id             = '{orderNo}'
           AND ordr_srmb           = CONVERT(numeric, '{ordrSrmb}')
           AND ordr_cmdt_srmb      = CONVERT(numeric, '{ordrCmdtSrmb}')
           AND ordr_cmdt_pros_srmb = CONVERT(numeric, '{ordrCmdtProsSrmb}')
    """,

    # 바로드림 오늘배송 EBIO..TD_ORDR_PICKUP 보관완료 처리
    # 저장 프로시저가 항상 SET NOCOUNT ON(DML 문(UPDATE, DELETE 등)으로 시작되도록 하고 저장 프로시저가 SELECT 문에서 행을 반환하도록 하는 것이 가장 좋은 방법입니다.
    "modifyDirectPickupActionYmdForTransferNew": """
        UPDATE EBIO..TD_ORDR_PICKUP
           SET transfer_yn     = 'Y'
             , transfer_ymd    = convert(char(8), getdate(), 112)
             , transfer_time   = str_replace(convert(char, getdate(), 108), ':', null)
             , transfer_userid = 'P{crtrId}'
             , comp_proc_step  = '2'
             , etc_userid      = 'P{crtrId}'
             , pro_date        = getdate()
             , pro_sabun       = 'bt_ss'
         WHERE ordr_id             = '{orderNo}'
           AND ordr_srmb           = CONVERT(numeric, '{ordrSrmb}')
           AND ordr_cmdt_srmb      = CONVERT(numeric, '{ordrCmdtSrmb}')
           AND ordr_cmdt_pros_srmb = CONVERT(numeric, '{ordrCmdtProsSrmb}')

        SELECT COUNT(*) AS updatedCnt
          FROM EBIO..TD_ORDR_PICKUP
         WHERE ordr_id             = '{orderNo}'
           AND ordr_srmb           = CONVERT(SMALLINT, {ordrSrmb})
           AND ordr_cmdt_srmb      = CONVERT(SMALLINT, {ordrCmdtSrmb})
           AND ordr_cmdt_pros_srmb = CONVERT(SMALLINT, {ordrCmdtProsSrmb})
           AND pickup_yn           = 'Y'
           AT ISOLATION 0   
    """,

    "findRdpCode": """
        SELECT rdp_code          AS rdpCode
             , rdp_name          AS rdpName
             , rdp_adrs          AS rdpAdrs
             , rdp_tlnm          AS rdpTlnm
             , rdp_fax_num       AS rdpFaxNum
             , rdp_dvsn_code     AS rdpDvsnCode
             , by_warh_rdp_code  AS byWarhRdpCode
             , phds_wrk_grp_code AS phdsWrkGrpCode
             , frmr_rdp_code     AS frmrRdpCode
             , stdvr_ysno        AS stdvrYsno
             , plor_grp_code     AS plorGrpCode
             , str_ysno          AS strYsno
             , dprt_code         AS dprtCode
             , crtr_id           AS crtrId
             , cret_dttm         AS cretDttm
             , amnr_id           AS amnrId
             , amnd_dttm         AS amndDttm
             , dlt_ysno          AS dltYsno
             , bstm_dvsn_code    AS bstmDvsnCode
        FROM IDCM..TM_RDP
        WHERE rdp_code = '{rdpCode}'
    """,


    "getstorecd": """
        SELECT CODE||CODENAME as 'STOREINFO' from IDPS..MSTCOMMON WHERE CDTYPE ='C03' AND USEYN ='Y' AND DELYN ='0'
            AT ISOLATION 0
    """,


    "getstorecdinclall": """
        SELECT '000전체' AS 'STOREINFO'
         UNION all
        SELECT CODE||CODENAME as 'STOREINFO' from IDPS..MSTCOMMON WHERE CDTYPE ='C03' AND USEYN ='Y' AND DELYN ='0'
            AT ISOLATION 0
    """,

    "getposnofromstorecd": """
        SELECT POSNO||':'||POSNAME as 'POSNAME',POSNO
          from IDPS..MSTPOSTM 
         WHERE DELYN ='0' 
           AND STORECD = '{storecd}'
           AND POSNO <> '0000'
            AT ISOLATION 0
    """,



    "getinfoforinvninvs": """

        SELECT TT2.cmdt_code                                                                                                              AS cmdtCode      
              , IDSS.dbo.uf_removelf(TT2.cmdt_name)                                                                                        AS cmdtName      
              , (SELECT S1.code_wrth_name FROM IDCM..TC_KFLW_CODE_DTL S1 WHERE S1.code_id = '3002' AND S1.code_wrth = TT2.jo_code)         AS jo_name       
              , IDSS.dbo.uf_removelf((SELECT S2.pbcm_name FROM IDCM..TM_PBCM S2 WHERE S2.pbcm_code = TT2.pbcm_code AND S2.dlt_ysno = 'N')) AS pbcm_name     
              , TT1.rcds_invn_qntt                                                                                                         AS rcdsInvnQntt  
              , TT1.real_invn_qntt                                                                                                         AS realInvnQntt  
              , IsNull(SUM((SELECT S1.qntt                                                                                                                  
                              FROM IDTM..TT_RDP_INVN_INVS_SKIP S1                                                                                           
                            WHERE S1.rdp_code = TT1.rdp_code                                                                                               
                              AND S1.cmdt_id  = TT1.cmdt_id)), 0)                                                                          AS qntt         
              , (CASE WHEN (SELECT COUNT(*)                                                                                                                 
                              FROM IDTM..TT_RDP_INVN_INVS_EXCEPTION  S1                                                                                     
                            WHERE S1.rdp_code = TT1.rdp_code                                                                                               
                              AND S1.cmdt_id  = TT1.cmdt_id) = 0 THEN 'N'                                                                                  
                      ELSE                                            'Y'                                                                                   
                END)                                                                                                                      AS exceptYsno    
          FROM (                                                                                                                                           
                SELECT T1.rdp_code, T1.cmdt_id, T1.rcds_invn_qntt, T1.real_invn_qntt, T1.str_pren_invn_qntt                                                
                  FROM IDSS..TM_RDP_INVN T1                                                                                                                
                  WHERE T1.rdp_code = {storecd}                                                                                                 
                UNION ALL                                                                                                                                  
                SELECT {storecd}, T2.cmdt_id, 0, 0, 0                                                                                          
                  FROM IDCM..TM_CMDT T2                                                                                                                    
                  WHERE T2.cret_dttm >= convert(CHAR(8), dateadd(mm, -3, getdate()), 112)                                                                   
                    AND NOT EXISTS (SELECT 'x'                                                                                                              
                                      FROM IDSS..TM_RDP_INVN S1                                                                                             
                                    WHERE S1.rdp_code = {storecd}                                                                             
                                      AND S1.cmdt_id = T2.cmdt_id)                                                                                         
                    AND T2.dlt_ysno = 'N'                                                                                                                   
                UNION ALL                                                                                                                                  
                SELECT T1.rdp_code, T1.cmdt_id, 0, 0, 0                                                                                                    
                  FROM IDSS..TH_RDP_INVN_HSTR T1                                                                                                           
                  WHERE T1.cret_dttm >= convert(CHAR(8), dateadd(mm, -12, getdate()), 112)                                                                  
                    AND T1.rdp_code = {storecd}                                                                                                 
                    AND NOT EXISTS (SELECT 'x'                                                                                                              
                                    FROM IDSS..TM_RDP_INVN S1                                                                                              
                                    WHERE S1.rdp_code = T1.rdp_code                                                                                         
                                      AND S1.cmdt_id = T1.cmdt_id)                                                                                          
                  GROUP BY T1.rdp_code, T1.cmdt_id                                                                                                          
        ) TT1, IDCM..TM_CMDT TT2                                                                                                                           
        WHERE TT1.cmdt_id = TT2.cmdt_id                                                                                                                    
          AND TT2.jo_code <= '45'                                                                                                                          
          AND TT2.dlt_ysno = 'N'                                                                                                                           
        GROUP BY TT1.rdp_code                                                                                                                              
                , TT1.cmdt_id                                                                                                                               
                , TT2.cmdt_code                                                                                                                             
                , TT2.cmdt_name                                                                                                                             
                , TT2.jo_code                                                                                                                               
                , TT2.pbcm_code                                                                                                                             
                , TT1.rcds_invn_qntt                                                                                                                        
                , TT1.real_invn_qntt                                                                                                                        
                , TT1.str_pren_invn_qntt                                                                                                                    
        ORDER BY 1                                                                                                                                         
        AT ISOLATION 0                                                                                                                                     


    """,
    "getstocknorollingbyall": """
  SELECT (SELECT S1.rdp_name
            FROM IDCM..TM_RDP S1
           WHERE S1.rdp_code = T1.dept_cd
             AND S1.dlt_ysno = 'N')                                      AS rdpName
       , T4.cmdt_code                                                    AS cmdtCode
       , (SELECT code_wrth_name
            FROM IDCM..TC_KFLW_CODE_DTL S1
           WHERE 1 = 1
             AND S1.code_id = '1257'
             AND S1.code_wrth = T4.cmdt_cdtn_code
             AND S1.dlt_ysno = 'N')                                      AS cmdtCdtnName
       , T4.jo_code                                                      AS joCode
       , (SELECT code_wrth_name
            FROM IDCM..TC_KFLW_CODE_DTL S1
           WHERE 1 = 1
             AND S1.code_id = '3002'
             AND S1.code_wrth = T4.jo_code
             AND S1.dlt_ysno = 'N')                                      AS joName
       , T4.cmdt_name                                                    AS cmdtName
       , T4.pbcm_code                                                    AS pbcmCode
       , (SELECT IsNull(pbcm_name, '')
            FROM IDCM..TM_PBCM
           WHERE pbcm_code = T4.pbcm_code)                               AS pbcmName
       , (SELECT IsNull(autr_name1, '')
            FROM IDCM..TD_CMDT_AUTR
           WHERE cmdt_id = T4.cmdt_id)                                   AS autrName1
       , T4.rlse_date                                                    AS rlseDate
       , IsNull(min(T5.bksh_num + '-' + T5.bksh_prgp_num), '')           AS minBkshNum
       , IsNull(max(T5.bksh_num + '-' + T5.bksh_prgp_num), '')           AS maxBkshNum
       , T1.gubun                                                        AS saleMnth
       , T2.real_invn_qntt - T2.str_pren_invn_qntt                       AS usableInvnQntt
       , 0                                                               AS saleQty
       , T2.real_invn_qntt - T2.str_pren_invn_qntt                       AS overQty
       , T4.vndr_code AS vndrCode 
    FROM IDSS..TM_NOT_TURN_OVER_CMDT T1, IDSS..TM_RDP_INVN T2, IDSS..TM_STR_ATMT_TKOT_STNR T3, IDCM..TM_CMDT T4
    , IDSS..TM_STR_BKSH_BKS T5
   WHERE T1.dept_cd = T2.rdp_code
     AND T4.cmdt_id = T2.cmdt_id
     AND T1.dept_cd *= T3.rdp_code
     AND T4.jo_code *= T3.jo_code
     AND T1.gubun = '{monthdiv}'
     AND T4.jo_code in ('35','36')
     AND T1.isbn = T4.cmdt_code
     AND T2.rdp_code = T5.str_rdp_code
     AND T2.cmdt_id  = T5.cmdt_id
     AND T1.wrk_ymd        = convert(CHAR(8), dateadd(dd, -1, getdate()), 112) 
     AND T4.dlt_ysno       = 'N'
     AND T4.cmdt_code      NOT LIKE ('4910%')
     
     AND NOT EXISTS ( SELECT 'x'
	                  FROM IDSS..TM_STR_INVN_RTGD_TRGT
	                 WHERE cmdt_id       = T2.cmdt_id
	                   AND rtgd_impb_rsn = 'N'
	              )
	              
   GROUP BY T1.dept_cd
       , T4.cmdt_id
       , T4.cmdt_code
       , T4.jo_code
       , T4.cmdt_name
       , T4.cmdt_cdtn_code
       , T4.pbcm_code
       , T4.rlse_date
       , T1.gubun
       , T2.real_invn_qntt
       , T2.str_pren_invn_qntt
       , T4.vndr_code
  HAVING (T2.real_invn_qntt - T2.str_pren_invn_qntt) > 0
   ORDER BY (T2.real_invn_qntt - T2.str_pren_invn_qntt) DESC
      AT ISOLATION 0
    """,


    "getstocknorolling": """
  SELECT (SELECT S1.rdp_name
            FROM IDCM..TM_RDP S1
           WHERE S1.rdp_code = T1.dept_cd
             AND S1.dlt_ysno = 'N')                                      AS rdpName
       , T4.cmdt_code                                                    AS cmdtCode
       , (SELECT code_wrth_name
            FROM IDCM..TC_KFLW_CODE_DTL S1
           WHERE 1 = 1
             AND S1.code_id = '1257'
             AND S1.code_wrth = T4.cmdt_cdtn_code
             AND S1.dlt_ysno = 'N')                                      AS cmdtCdtnName
       , T4.jo_code                                                      AS joCode
       , (SELECT code_wrth_name
            FROM IDCM..TC_KFLW_CODE_DTL S1
           WHERE 1 = 1
             AND S1.code_id = '3002'
             AND S1.code_wrth = T4.jo_code
             AND S1.dlt_ysno = 'N')                                      AS joName
       , T4.cmdt_name                                                    AS cmdtName
       , T4.pbcm_code                                                    AS pbcmCode
       , (SELECT IsNull(pbcm_name, '')
            FROM IDCM..TM_PBCM
           WHERE pbcm_code = T4.pbcm_code)                               AS pbcmName
       , (SELECT IsNull(autr_name1, '')
            FROM IDCM..TD_CMDT_AUTR
           WHERE cmdt_id = T4.cmdt_id)                                   AS autrName1
       , T4.rlse_date                                                    AS rlseDate
       , IsNull(min(T5.bksh_num + '-' + T5.bksh_prgp_num), '')           AS minBkshNum
       , IsNull(max(T5.bksh_num + '-' + T5.bksh_prgp_num), '')           AS maxBkshNum
       , T1.gubun                                                        AS saleMnth
       , T2.real_invn_qntt - T2.str_pren_invn_qntt                       AS usableInvnQntt
       , 0                                                               AS saleQty
       , T2.real_invn_qntt - T2.str_pren_invn_qntt                       AS overQty
       , T4.vndr_code AS vndrCode 
    FROM IDSS..TM_NOT_TURN_OVER_CMDT T1, IDSS..TM_RDP_INVN T2, IDSS..TM_STR_ATMT_TKOT_STNR T3, IDCM..TM_CMDT T4
    , IDSS..TM_STR_BKSH_BKS T5
   WHERE T1.dept_cd = T2.rdp_code
     AND T4.cmdt_id = T2.cmdt_id
     AND T1.dept_cd *= T3.rdp_code
     AND T4.jo_code *= T3.jo_code
     AND T1.dept_cd = '{storecd}'
     AND T1.gubun = '{monthdiv}'
     AND T1.isbn = T4.cmdt_code
     AND T2.rdp_code = T5.str_rdp_code
     AND T2.cmdt_id  = T5.cmdt_id
     AND T1.wrk_ymd        = convert(CHAR(8), dateadd(dd, -1, getdate()), 112) 
     AND T4.dlt_ysno       = 'N'
     AND T4.cmdt_code      NOT LIKE ('4910%')
     
     AND NOT EXISTS ( SELECT 'x'
	                  FROM IDSS..TM_STR_INVN_RTGD_TRGT
	                 WHERE cmdt_id       = T2.cmdt_id
	                   AND rtgd_impb_rsn = 'N'
	              )
	              
   GROUP BY T1.dept_cd
       , T4.cmdt_id
       , T4.cmdt_code
       , T4.jo_code
       , T4.cmdt_name
       , T4.cmdt_cdtn_code
       , T4.pbcm_code
       , T4.rlse_date
       , T1.gubun
       , T2.real_invn_qntt
       , T2.str_pren_invn_qntt
       , T4.vndr_code
  HAVING (T2.real_invn_qntt - T2.str_pren_invn_qntt) > 0
   ORDER BY (T2.real_invn_qntt - T2.str_pren_invn_qntt) DESC
      AT ISOLATION 0
    """,

    "getoffsaleamtbymem": """
        SELECT (CASE WHEN COUNT(MEMNO) >= 1 THEN 'Y' ELSE 'N' END ) AS SALEBYMEMNO
          from IDPS..TRNSALHDR 
         WHERE SALEDATE =  CONVERT(CHAR(8), GETDATE(), 112)
           AND TRANTYPE ='00'
           AND MEMNO ='{memno}'
            AT ISOLATION 0
    """,

    "findCmdtMain": """
        SELECT TT2.rdp_code                                                                               AS rdp_code
             , TT2.rdp_name                                                                               AS rdp_name
             , TT1.cmdt_code                                                                              AS cmdt_code
             , IDSS.dbo.uf_removelf(TT1.cmdt_name)                                                        AS cmdt_name
             , TT1.jo_code                                                                                AS jo_code
             , (SELECT code_wrth_name                                                                     
                  FROM IDCM..TC_KFLW_CODE_DTL                                                             
                 WHERE code_id = '3002'                                                                   
                   AND code_wrth = TT1.jo_code)                                                           AS jo_name
             , TT1.pbcm_code                                                                              AS pbcm_code
             , IDSS.dbo.uf_removelf(IsNull((SELECT pbcm_name                                              
                                              FROM IDCM..TM_PBCM                                          
                                             WHERE pbcm_code = TT1.pbcm_code), ''))                       AS pbcm_name
             , IDSS.dbo.uf_removelf(IsNull((SELECT autr_name1                                             
                                              FROM IDCM..TD_CMDT_AUTR                                     
                                             WHERE cmdt_id = TT1.cmdt_id), ''))                           AS autr_name1
             , TT1.rlse_date                                                                              AS rlse_date
             , TT1.vndr_code                                                                              AS vndr_code
             , IDSS.dbo.uf_removelf((SELECT S1.vndr_name                                                  
                                       FROM IDCM..TM_VNDR S1                                              
                                      WHERE S1.vndr_code = TT1.vndr_code))                                AS vndr_name
             , IsNull((SELECT S1.UNITPRICE                                                                
                         FROM IDPS..MSTGOODS S1                                                           
                        WHERE S1.PLUBARCD = TT1.cmdt_code                                                 
                          AND S1.STORECD  = TT2.rdp_code), 0)                                             AS UNITPRICE
             , IsNull((SELECT wncr_prce                                                                   
                         FROM IDCM..TD_CMDT_PRCE                                                                 
                        WHERE cmdt_id  = TT1.cmdt_id                                                             
                          AND apl_end_dttm = '99991231235959'), 0)                                        AS wncr_prce
             , IsNull((SELECT bks_prce                                                                    
                         FROM IDCM..TD_BKS_BSC_PRCE                                                              
                        WHERE cmdt_id  = TT1.cmdt_id                                                             
                          AND rdp_code = TT2.rdp_code), 0)                                                AS bks_prce
             , IsNull((SELECT ISNULL(MAX(byng_rate), 0.00)                                                
                         FROM IDCM..TD_CMDT_BYPR                                                          
                        WHERE cmdt_id = TT1.cmdt_id                                                       
                          AND use_ysno = 'Y'                                                              
                          AND CONVERT(FLOAT, byng_rate) <= 100), 0)                                       AS byng_rate
             , IsNull((SELECT max(S3.plor_rlnc_date)
                         FROM IDPL..TM_PLOR_RLNC S3
                        WHERE S3.rdp_code = TT2.rdp_code
                          AND S3.cmdt_id  = TT1.cmdt_id
                          AND S3.plor_rlnc_date >= convert(CHAR(8), dateadd(dd, -30, getdate()), 112)
                          AND S3.plor_rlnc_cdtn_code IN ('006', '007', '008')), '')                       AS plor_rlnc_date
             , IsNull((SELECT max(T1.tkin_date)
                         FROM IDPL..TM_TKIO T1, IDPL..TD_TKIO_DTL T2
                        WHERE T1.tkot_rdp_code = T2.tkot_rdp_code
                          AND T1.tkot_date     = T2.tkot_date
                          AND T1.tkot_wrk_num  = T2.tkot_wrk_num
                          AND T1.box_num       = T2.box_num
                          AND T1.tkot_date    >= convert(CHAR(8), dateadd(dd, -30, getdate()), 112)
                          AND T2.cmdt_id       = TT1.cmdt_id
                          AND T1.tkin_rdp_code = TT2.rdp_code
                          AND T1.dlt_ysno = 'N'
                          AND T2.dlt_ysno = 'N'), '')                                                     AS tkin_date
             , IsNull((SELECT max(T1.rcvd_date)
                         FROM IDPL..TM_RCVD T1, IDPL..TD_RCVD_DTL T2
                        WHERE T1.rcvd_num = T2.rcvd_num
                          AND T1.rdp_code = TT2.rdp_code
                          AND T1.rcvd_date >= convert(CHAR(8), dateadd(dd, -30, getdate()), 112)
                          AND T2.cmdt_id = TT1.cmdt_id
                          AND T1.dlt_ysno = 'N'
                          AND T2.dlt_ysno = 'N'), '')                                                     AS rcvd_date
             , IsNull((SELECT S1.real_invn_qntt - S1.str_pren_invn_qntt
                         FROM IDSS..TM_RDP_INVN S1
                        WHERE S1.rdp_code = TT2.rdp_code
                          AND S1.cmdt_id  = TT1.cmdt_id), 0)                                              AS avlbQntt
             , TT1.cmdt_dvsn_code                                                                         AS cmdt_dvsn_code
             , IsNull((SELECT code_wrth_name                                                              
                         FROM IDCM..TC_KFLW_CODE_DTL                                                    
                        WHERE code_id = '3001'                                                          
                          AND code_wrth = TT1.cmdt_dvsn_code), '')                                        AS cmdt_dvsn_name
             , TT1.cmdt_cdtn_code                                                                         AS cmdt_cdtn_code
             , IsNull((SELECT code_wrth_name                                                              
                         FROM IDCM..TC_KFLW_CODE_DTL                                                    
                        WHERE code_id = '1257'                                                          
                          AND code_wrth = TT1.cmdt_cdtn_code), '')                                        AS cmdt_cdtn_name
             , IsNull((SELECT sum(S1.sale_qntt)                                                           
                         FROM IDSS..TS_CMDT_SLS_SUMY S1                                                   
                        WHERE S1.sale_date >= convert(CHAR(8), dateadd(dd, -30, getdate()), 112)          
                          AND S1.str_rdp_code = TT2.rdp_code                                              
                          AND S1.cmdt_id = TT1.cmdt_id), 0)                                               AS sale_qntt
             , (CASE WHEN (SELECT count(*)
                             FROM IDCM..TM_APIC_AREA_ECSL_VNDR S1                                                    
                            WHERE S1.rdp_code = TT2.rdp_code                                                         
                              AND S1.cmdt_id  = TT1.cmdt_id
                              AND S1.dlt_ysno = 'N') > 0 THEN 'Y' ELSE 'N' END)                           AS area_vndr_code
          FROM IDCM..TM_CMDT TT1, IDCM..TM_RDP TT2
         WHERE 1=1
           AND TT2.rdp_code      LIKE '{rdpCode}%'
           AND TT1.cmdt_code     = '{cmdtCode}'
           AND TT2.rdp_dvsn_code = '002'
           AND TT2.dlt_ysno      = 'N'
         ORDER BY 1
            AT ISOLATION 0
    """,


    "getcampaign": """
				
      SELECT TOP 99 a.mmbr_nmbr as MEMNO,
             IsNull(rtrim(a.pr_nmbr), '') as PRCD,
             '540' as MEMLEVEL,
             '801' as JOINTYPE
        FROM cti..TM_POS_OFFER a
       WHERE a.mmbr_nmbr = RTRIM('{memNo}')
         AND a.strt_date <= CONVERT(CHAR(8), GETDATE(), 112)
         AND a.clse_date >= CONVERT(CHAR(8), GETDATE(), 112)
         AND a.use_yn = 'Y'
       ORDER BY a.pr_nmbr DESC

    """,

    "getcampaignfromidps": """
				
      SELECT TOP 99 a.mmbr_nmbr as MEMNO,
             IsNull(rtrim(a.pr_nmbr), '') as PRCD,
             '540' as MEMLEVEL,
             '801' as JOINTYPE
        FROM IDPS..TM_POS_OFFER a
       WHERE a.mmbr_nmbr = RTRIM('{memNo}')
         AND a.strt_date <= CONVERT(CHAR(8), GETDATE(), 112)
         AND a.clse_date >= CONVERT(CHAR(8), GETDATE(), 112)
         AND a.use_yn = 'Y'
       ORDER BY a.pr_nmbr DESC

    """,

    "getempsale": """
				
		SELECT EMPNO,
			     EMPNAME,
		       IsNull(SUM(TT.KB_SALEAMT), 0) AS KBTOTSALEAMT,
		       IsNull(SUM(TT.KH_SALEAMT), 0) AS KHTOTSALEAMT
			FROM (
			SELECT  B.EMPNO,
              B.EMPNAME,
              isnull(sum((CASE WHEN A.TRANTYPE = '00' THEN B.SALEAMTM ELSE -B.SALEAMTM END)), 0) AS 'KB_SALEAMT',
				      isnull(sum((CASE WHEN A.TRANTYPE = '00' THEN B.SALEAMTH ELSE -B.SALEAMTH END)), 0) AS 'KH_SALEAMT'
			  FROM IDPS..TRNSALHDR A, IDPS..TRNDEDPAY B
			 WHERE A.SALEDATE  = B.SALEDATE
			   AND A.STORECD   = B.STORECD
			   AND A.POSNO     = B.POSNO
			   AND A.TRANNO    = B.TRANNO
			   AND B.SALEDATE >= SUBSTRING(CONVERT(CHAR(8), getdate(),112), 1, 6) + '01'
			   AND B.SALEDATE <= SUBSTRING(CONVERT(CHAR(8), getdate(),112), 1, 6) + '31'
			   AND B.STORECD  NOT IN ('013', '023', '063')
			   AND B.EMPNO     = '{empNo}'
			   AND B.GRPTYPE   =  '{grpType}'
			   AND A.TRANTYPE IN ('00', '01')
			   GROUP BY B.EMPNO,
			       B.EMPNAME
			UNION ALL 
			SELECT  B.EMPNO,
              B.EMPNAME, 
              isnull(sum((CASE WHEN A.TRANTYPE = '00' THEN B.SALEAMT ELSE -B.SALEAMT END)), 0) AS 'KB_SALEAMT',
				      0 AS 'KH_SALEAMT'
			  FROM IDPS..TRNSALHDR A, IDPS..TRNDEDPAY B
			 WHERE A.SALEDATE  = B.SALEDATE
			   AND A.STORECD   = B.STORECD
			   AND A.POSNO     = B.POSNO
			   AND A.TRANNO    = B.TRANNO
			   AND B.SALEDATE >= SUBSTRING(CONVERT(CHAR(8), getdate(),112), 1, 6) + '01'
			   AND B.SALEDATE <= SUBSTRING(CONVERT(CHAR(8), getdate(),112), 1, 6) + '31'
			   AND B.STORECD   IN ( '013', '023')
			   AND B.EMPNO     = '{empNo}'
			   AND B.GRPTYPE   =  '{grpType}'
			   AND A.TRANTYPE IN ('00', '01')
			   GROUP BY B.EMPNO,
			       B.EMPNAME
			) TT
			GROUP BY TT.EMPNO,
			   TT.EMPNAME      
    """,


    "getKbGiftcardPay": """
        SELECT 
            T.SALEDATE
          , T.STORECD
          , T.POSNO
          , T.TRANNO
          , T.SEQ
          , T.SALETIME
          , T.MEMNO
          , T.CARDNO
          , T.SALEAMT
          , T.TRANTYPE
          , T.AFTSAVEDATE
        FROM (
        SELECT A.SALETIME
          , ISNULL(A.MEMNO,'') AS 'MEMNO'
          , B.SALEAMT AS 'SALEAMT'
          , B.CARDNO AS 'CARDNO'	
          , (CASE WHEN A.TRANTYPE = '00' THEN '001' ELSE '002' END) AS 'TRANTYPE'	
          , '' AS 'AFTSAVEDATE'
          , A.SALEDATE AS 'SALEDATE'
          , A.STORECD AS 'STORECD'
          , A.POSNO AS 'POSNO'
          , A.TRANNO AS 'TRANNO'
          , B.SEQ AS 'SEQ'
          FROM IDPS..TRNSALHDR A, IDPS..TRNKBGVPAY B
        WHERE A.SALEDATE = B.SALEDATE
          AND A.STORECD = B.STORECD
          AND A.POSNO = B.POSNO
          AND A.TRANNO = B.TRANNO
          AND A.SALEDATE = '{saleDate}'
          AND A.SALETIME >= '{startSaleTime}' AND A.SALETIME <= '{endSaleTime}'
          AND A.TRANTYPE IN ('00', '01')
          AND A.MEMNO IS NOT NULL
          AND A.AFTSAVEDATE IS NULL 	  
        UNION ALL  	  
        SELECT A.SALETIME
          , ISNULL(A.MEMNO,'') AS 'MEMNO'
          , B.SALEAMT AS 'SALEAMT'
          , B.CARDNO AS 'CARDNO'	
          , (CASE WHEN A.TRANTYPE = '00' THEN '001' ELSE '002' END) AS 'TRANTYPE'	
          , A.AFTSAVEDATE AS 'AFTSAVEDATE'
          , A.SALEDATE AS 'SALEDATE'
          , A.STORECD AS 'STORECD'
          , A.POSNO AS 'POSNO'
          , A.TRANNO AS 'TRANNO'
          , B.SEQ AS 'SEQ'
          FROM IDPS..TRNSALHDR A, IDPS..TRNKBGVPAY B
        WHERE A.SALEDATE = B.SALEDATE
          AND A.STORECD = B.STORECD
          AND A.POSNO = B.POSNO
          AND A.TRANNO = B.TRANNO
          AND A.AFTSAVEDATE IS NOT NULL
          AND A.MEMNO IS NOT NULL
          AND A.AFTSAVEDATE = '{saleDate}'
          AND A.SALETIME >= '{startSaleTime}' AND A.SALETIME <= '{endSaleTime}'
          AND A.TRANTYPE IN ('00', '01')
          AND A.MEMNO IS NOT NULL
        ) T
        AT ISOLATION 0
    """,

    "getSmartReceipt": """
        SELECT TOP 1000 t1.MEMNO, t1.SALEDATE, t1.STORECD, t1.POSNO, t1.TRANNO, t1.SALETIME, t1.COMPANY, t2.JRDATA
        FROM IDPS..TRNSALHDR t1
        , IDPS..TRNJRNLOG t2
        WHERE t1.SALEDATE = t2.SALEDATE
        AND t1.STORECD  = t2.STORECD
        AND t1.POSNO    = t2.POSNO
        AND t1.TRANNO   = t2.TRANNO
        AND t1.SALEDATE = '{saleDate}'
        AND t1.SALETIME >= '{startSaleTime}' AND t1.SALETIME <= '{endSaleTime}'
        AND t1.TRANTYPE IN ('00', '01')
        AND t1.MEMNO = '{memNo}'
        AT ISOLATION 0
    """,

    "getofflinesale": """
        SELECT t1.MEMNO, t1.SALEDATE, t1.STORECD, t1.POSNO, t1.TRANNO, t1.SALETIME, t1.COMPANY, t2.JRDATA
        FROM IDPS..TRNSALHDR t1
        , IDPS..TRNJRNLOG t2
        WHERE t1.SALEDATE = t2.SALEDATE
        AND t1.STORECD  = t2.STORECD
        AND t1.POSNO    = t2.POSNO
        AND t1.TRANNO   = t2.TRANNO
        AND t1.SALEDATE = '{saleDate}'
        AND t1.SALETIME >= '{startSaleTime}' AND t1.SALETIME <= '{endSaleTime}'
        AND t1.TRANTYPE IN ('00', '01')
        AND t1.STORECD = '{storeCd}'
        AT ISOLATION 0
    """,

    "getempdreamcardno": """
        SELECT  (CASE WHEN A.GRPTYPE ='1' THEN '교보문고' ELSE '핫트랙스' END) AS 'GRPNAME' ,
       			    A.EMPNAME,
                A.EMPNO, 
        	      (CASE WHEN A.DRCARDNO IS NULL THEN 'no card' else SUBSTRING(A.DRCARDNO,1,16) END) AS 'DRCARDNO',
        	      A.DRCARDPIN
          FROM IDPS..MSTEMPLOYEE A 
         WHERE A.GRPTYPE ='{comCd}'
           AND A.USEYN ='Y' 
           AND A.EMPNO ='{empNo}'
        AT ISOLATION 0
    """,

    "calcaftersavepoint": """
        DECLARE 
        @os_cmpy_dvcd                CHAR(1)     , -- 회사코드 (적립금을 지급하는 회사 코드)
        @os_stor_dvcd                CHAR(3)     , -- 영업점코드
        @os_sls_date                 CHAR(8)     , -- 판매일자
        @os_pos_nmbr                 CHAR(4)     , -- POS번호
        @ol_recp_nmbr                INT         , -- 영수증번호
        @ol_sls_amt                  INT         , -- 총 판매금액
        @ol_bsc_acml_amt             INT         , -- 구매적립 금액
        @ol_addt_acml_amt            INT         , -- 추가적립 금액
        @ol_result                   INT         , -- 처리코드
        @os_result_msg               VARCHAR(200)  -- 처리결과
                
                
        EXEC IDPS..usp_cmn_receipt_point_acml
                  '{memNo}',
                  '{inputType}',
                  '{receiptNo}',
                  NULL,
                  NULL,
                  NULL,
                  NULL,                
                  0,
                  {saleAmt},
                  '{kbGrade}',
                  '{khGrade}',
                  '{joinPath}',
                  {additionalSaveRate},
                  {additionalDcRate},
                  '{exeId}', 
        			    @os_cmpy_dvcd       OUTPUT ,
        			    @os_stor_dvcd       OUTPUT ,
        			    @os_sls_date        OUTPUT ,
        			    @os_pos_nmbr        OUTPUT ,
        			    @ol_recp_nmbr       OUTPUT ,
        			    @ol_sls_amt         OUTPUT ,
        			    @ol_bsc_acml_amt    OUTPUT ,
        			    @ol_addt_acml_amt   OUTPUT ,
        			    @ol_result          OUTPUT ,
        			    @os_result_msg      OUTPUT 
        
        SELECT  @os_cmpy_dvcd AS 'COMCD', @os_stor_dvcd  AS 'STORECD', @os_sls_date AS 'SALEDATE', @os_pos_nmbr  AS 'POSNO', @ol_recp_nmbr AS 'TRANNO',
                @ol_sls_amt AS 'SALEAMT'       ,@ol_bsc_acml_amt AS 'SAVEPOINTAMT'   ,@ol_addt_acml_amt AS 'ADDSAVEPOINTAMT'  ,@ol_result AS 'RETCD', @os_result_msg AS 'RETMSG'    
           
    """, 

    "getoffsaldtl": """
        SELECT A.SALEDATE,
              A.STORECD, 
              A.POSNO, 
              A.TRANNO, 
              ISNULL(A.MEMNO,'') AS 'MEMNO',
              A.SALETIME AS 'SALETIME',
              '1' AS 'SALETYPE',
              (CASE WHEN B.PLUGB = '7' THEN 'KB' ELSE 'KH' END) AS 'PLUGB', 
              A.TRANTYPE,
              B.PLUBARCD AS 'PLUBARCD',
              B.JOCD,       
              sum(B.QTY) AS 'QTY', 
              sum(B.SALEAMT) AS 'SALEAMT'
        FROM IDPS..TRNSALHDR A, 
              IDPS..TRNSALDTL B
        WHERE A.SALEDATE = B.SALEDATE
          AND A.STORECD  = B.STORECD
          AND A.POSNO    = B.POSNO
          AND A.TRANNO   = B.TRANNO
          AND A.TRANTYPE IN ('00','01')
          AND B.CANCELGB ='0'  
          AND A.SALEDATE =  '{saleDate}'
          AND A.SALETIME >= '{startSaleTime}'
          AND A.SALETIME <= '{endSaleTime}'
          AND A.STORECD = '{storeCd}'
          AND (A.SALEAMTM <> 0 OR A.STORECD = '013' OR A.STORECD ='023')
        GROUP BY A.SALEDATE,
              A.STORECD, 
              A.POSNO, 
              A.TRANNO, 
              ISNULL(A.MEMNO,''),
              A.SALETIME ,        
              B.PLUGB, 
              A.TRANTYPE ,
              B.PLUBARCD ,
              B.JOCD
        UNION ALL  
        SELECT  A.SALEDATE,
              A.STORECD, 
              A.POSNO, 
              A.TRANNO, 
              ISNULL(A.MEMNO,'') AS 'MEMNO',
              A.SALETIME AS 'SALETIME',
              '2' AS 'SALETYPE',
              (CASE WHEN B.PLUGB = '7' THEN 'KB' ELSE 'KH' END) AS 'PLUGB', 
              A.TRANTYPE AS 'TRANTYPE',
              B.PLUBARCD AS 'PLUBARCD',
              B.JOCD,       
              sum(B.QTY) AS 'QTY', 
              sum(B.SALEAMT) AS 'SALEAMT'  
        FROM IDPS..TRNSALHDR A, 
              IDPS..TRNSALDTL B
        WHERE A.SALEDATE = B.SALEDATE
          AND A.STORECD  = B.STORECD
          AND A.POSNO    = B.POSNO
          AND A.TRANNO   = B.TRANNO
          AND A.TRANTYPE IN ('00','01')
          AND B.CANCELGB ='0'
          AND A.SALEDATE =  '{saleDate}'
          AND A.SALETIME >= '{startSaleTime}'
          AND A.SALETIME <= '{endSaleTime}'
          AND A.STORECD = '{storeCd}'
          AND A.STORECD  NOT in ('013', '023')
          AND (A.SALEAMTH <> 0 OR A.STORECD = '064')
        GROUP BY A.SALEDATE,
              A.STORECD, 
              A.POSNO, 
              A.TRANNO, 
              ISNULL(A.MEMNO,''),
              A.SALETIME ,        
              B.PLUGB, 
              A.TRANTYPE ,
              B.PLUBARCD ,
              B.JOCD
        ORDER BY 1,2,3,4
        AT ISOLATION 0                            

    """,    

    "getcommissionsale_aftermerge": """
        
        SELECT TT1.SALEDATE                                                                                                             AS 'SALEDATE',                                                                    
               (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C03' AND CODE = TT1.STORECD AND DELYN = '0') AS 'STORECD',           

               (CASE WHEN TT1.PLUGB = '1' THEN '교보문고' ELSE '핫트랙스' END)                                                          AS 'PLUGB',                                                                       
               TT1.TENDERTYPE2 + ':' + (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E03' AND CODE = TT1.TENDERTYPE2 AND DELYN = '0') AS 'TENDERTYPE2',                                                                 
               SUM((CASE WHEN TT1.COMPANY = '1' THEN TT1.SALEAMT ELSE 0           END))                                                 AS 'KB_SALEAMT',                              
               SUM((CASE WHEN TT1.COMPANY = '1' THEN 0           ELSE TT1.SALEAMT END))                                                 AS 'HT_SALEAMT',                              
               SUM(TT1.SALEAMT)                                                                                                         AS 'TOT_SALEAMT'                              
          FROM (                                                                                                                                                                                                                   
                SELECT '일반매출금권(상품권판매/상품권지불/선결제 제외)'                                                       AS 'TYPE',                                                                                           
                       a.SALEDATE                                                                                              AS 'SALEDATE',                                                                                       
                       a.STORECD                                                                                               AS 'STORECD',                                                                                        
                       a.POSNO                                                                                                 AS 'POSNO',                                                                                          
                       a.COMPANY                                                                                               AS 'COMPANY',                                                                                        
                       a.TRANNO                                                                                                AS 'TRANNO',                                                                                         
                       (CASE WHEN b.SALEAMTM <> 0 THEN '1' ELSE '2' END)                                                       AS 'PLUGB',                                                                                          
                       b.TENDERTYPE                                                                                            AS 'TENDERTYPE',                                                                                     
                       (SELECT IsNull(ETC1NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E02' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2',                                                                                    
                       (SELECT IsNull(ETC2NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E02' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2NM',                                                                                  
                       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)                                        AS 'SALEAMT'                                                                                         
                  FROM IDPS..TRNSALHDR a, IDPS..TRNSALPAY b                                                                                                                                                                                   
                 WHERE a.SALEDATE   = b.SALEDATE                                                                                                                                                                                    
                   AND a.STORECD    = b.STORECD                                                                                                                                                                                     
                   AND a.POSNO      = b.POSNO                                                                                                                                                                                       
                   AND a.TRANNO     = b.TRANNO                                                                                                                                                                                      
                   AND a.TRANTYPE   IN ('00', '01')                                                                                                                                                                                 
                   AND a.TRANKIND   NOT IN ('10','11','13','14','15', '12', '17')                                                                                                                                                          
                   AND b.TENDERTYPE NOT IN ('03', '12')                                                                                                                                                                         
                AND a.POSNO    NOT LIKE '__[49]_'  
                AND a.SALEDATE='{saleDate}'     
                       AND a.STORECD  = '{storeCd}'  

                UNION ALL                                                                                                                                                                                                 
                SELECT '상품권판매매출금권(상품권지불/선결제 제외)'                                                            AS 'TYPE',                                                                                         
                       a.SALEDATE                                                                                              AS 'SALEDATE',                                                                                       
                       a.STORECD                                                                                               AS 'STORECD',                                                                                        
                       a.POSNO                                                                                                 AS 'POSNO',                                                                                          
                       a.COMPANY                                                                                               AS 'COMPANY',                                                                                        
                       a.TRANNO                                                                                                AS 'TRANNO',                                                                                         
                       (CASE WHEN b.SALEAMTM <> 0 THEN '1' ELSE '2' END)                                                       AS 'PLUGB',                                                                                          
                       b.TENDERTYPE                                                                                            AS 'TENDERTYPE',                                                                                     
                       (SELECT IsNull(ETC1NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E02' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2',                                                                                    
                       (SELECT IsNull(ETC2NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E02' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2NM',                                                                                  
                       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)                                        AS 'SALEAMT'                                                                                         
                  FROM IDPS..TRNSALHDR a, IDPS..TRNSALPAY b                                                                                                                                                                                   
                 WHERE a.SALEDATE   = b.SALEDATE                                                                                                                                                                                    
                   AND a.STORECD    = b.STORECD                                                                                                                                                                                     
                   AND a.POSNO      = b.POSNO                                                                                                                                                                                       
                   AND a.TRANNO     = b.TRANNO                                                                                                                                                                                      
                   AND a.TRANTYPE   IN ('00', '01')                                                                                                                                                                                 
                   AND a.TRANKIND   IN ('13','14', '17')                                                                                                                                                                                
                   AND b.TENDERTYPE NOT IN ('03', '12')                                                                                                                                                                         
                AND a.POSNO    NOT LIKE '__[49]_'  
                AND a.SALEDATE='{saleDate}'     
                       AND a.STORECD  = '{storeCd}'  

                UNION ALL                                                                                                                                                                                                 
                SELECT '선결제완료매출금권(상품권지불제외)'                                                                    AS 'TYPE',                                                                                      
                       a.CLOSEDATE                                                                                             AS 'SALEDATE',                                                                                       
                       a.STORECD                                                                                               AS 'STORECD',                                                                                        
                       a.POSNO                                                                                                 AS 'POSNO',                                                                                          
                       a.COMPANY                                                                                               AS 'COMPANY',                                                                                        
                       a.TRANNO                                                                                                AS 'TRANNO',                                                                                         
                       (CASE WHEN b.SALEAMTM <> 0 THEN '1' ELSE '2' END)                                                       AS 'PLUGB',                                                                                          
                       b.TENDERTYPE                                                                                            AS 'TENDERTYPE',                                                                                     
                       (SELECT IsNull(ETC1NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E02' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2',                                                                                    
                       (SELECT IsNull(ETC2NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E02' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2NM',                                                                                  
                       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)                                        AS 'SALEAMT'                                                                                         
                  FROM 
                  
                  (	select c.FNSHDATE AS CLOSEDATE, a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO, b.SEQ
								        , a.COMPANY, a.TRANTYPE, a.TRANKIND
										  , (case when a.STORECD IN ('013','023') then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end) SALCOMCD
								        , '1' SALEFLAG
								        , '0' PAYFLAG, b.PLUGB
								        , max(c.RESTYPE) RESTYPE
									  from IDPS..TRNRESERVATION c, IDPS..TRNSALHDR a (INDEX PK_TRNSALHDR), IDPS..TRNSALDTL b (INDEX PK_TRNSALDTL)
									 where c.SALEDATE = a.SALEDATE   and c.STORECD  = a.STORECD    and c.POSNO    = a.POSNO      and c.TRANNO   = a.TRANNO
										and a.SALEDATE = b.SALEDATE   and a.STORECD  = b.STORECD    and a.POSNO    = b.POSNO      and a.TRANNO   = b.TRANNO
								      and a.TRANTYPE in ('00','01')
										and a.TRANKIND = '15'
										AND a.POSNO NOT LIKE '99%'
										AND a.DELYN = '0'
										and b.CANCELGB = '0'
								        and (c.FNSHDATE <> '' and IsNull(c.FNSHDATE, '') <> '' )
								 group by c.SALEDATE, c.STORECD, c.POSNO, c.TRANNO
								        , a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO
								        , b.SALEDATE, b.STORECD, b.POSNO, b.TRANNO, b.SEQ
								        , a.COMPANY, a.TRANTYPE, a.TRANKIND, c.FNSHDATE, b.PLUGB
				   						, (case when a.STORECD  IN ('013','023')  then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end) ) a
                  
                  
                  , IDPS..TRNSALPAY b                                                                                                                                                                              
                 WHERE a.SALEDATE   = b.SALEDATE                                                                                                                                                                                    
                   AND a.STORECD    = b.STORECD                                                                                                                                                                                     
                   AND a.POSNO      = b.POSNO                                                                                                                                                                                       
                   AND a.TRANNO     = b.TRANNO                                                                                                                                                                            
                   AND a.SEQ        = 1                                                                                                                                                                                              
                   AND a.TRANTYPE   IN ('00', '01')                                                                                                                                                                                 
                   AND a.TRANKIND  = '15' AND a.SALEFLAG  = '1' AND a.PAYFLAG   = '0'                                                                                                                                             
                   AND b.TENDERTYPE NOT IN ('03', '12')                                                                                                                                                                         
                AND a.POSNO    NOT LIKE '__[49]_'  
                AND a.CLOSEDATE='{saleDate}'     
                       AND a.STORECD  = '{storeCd}'                                                                                                      


                UNION ALL                                                                                                                                                                                                 
                SELECT '상품권지불금권(일반상품판매, 선결제, 상품권판매 제외)'                                                 AS 'TYPE',                                                                                      
                       a.SALEDATE                                                                                              AS 'SALEDATE',                                                                                       
                       a.STORECD                                                                                               AS 'STORECD',                                                                                        
                       a.POSNO                                                                                                 AS 'POSNO',                                                                                          
                       a.COMPANY                                                                                               AS 'COMPANY',                                                                                        
                       a.TRANNO                                                                                                AS 'TRANNO',                                                                                         
                       (CASE WHEN b.SALEAMTM <> 0 THEN '1' ELSE '2' END)                                                       AS 'PLUGB',                                                                                          
                       '03'                                                                                                    AS 'TENDERTYPE',                                                                                     
                       (SELECT IsNull(ETC1NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E01' AND CODE = b.GVTYPE AND DELYN = '0')     AS 'TENDERTYPE2',                                                                                    
                       (SELECT IsNull(ETC2NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E01' AND CODE = b.GVTYPE AND DELYN = '0')     AS 'TENDERTYPE2NM',                                                                                  
                       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)                                        AS 'SALEAMT'                                                                                         
                  FROM IDPS..TRNSALHDR a, IDPS..TRNGVPAY b                                                                                                                                                                                      
                 WHERE a.SALEDATE   = b.SALEDATE                                                                                                                                                                                    
                   AND a.STORECD    = b.STORECD                                                                                                                                                                                     
                   AND a.POSNO      = b.POSNO                                                                                                                                                                                       
                   AND a.TRANNO     = b.TRANNO                                                                                                                                                                                      
                   AND a.TRANTYPE   IN ('00', '01')                                                                                                                                                                                 
                   AND a.TRANKIND   NOT IN ('10', '11','13','14', '15', '12', '17')                                                                                                                                                             
                AND a.POSNO    NOT LIKE '__[49]_'  
                AND a.SALEDATE='{saleDate}'     
                       AND a.STORECD  = '{storeCd}'                                                                                                      

                                                                                                      


                UNION ALL                                                                                                                                                                                                 
                SELECT '상품권지불금권(선결제완료매출금권)'                                                                    AS 'TYPE',                                                                                      
                       a.CLOSEDATE                                                                                             AS 'SALEDATE',                                                                                       
                       a.STORECD                                                                                               AS 'STORECD',                                                                                        
                       a.POSNO                                                                                                 AS 'POSNO',                                                                                          
                       a.COMPANY                                                                                               AS 'COMPANY',                                                                                        
                       a.TRANNO                                                                                                AS 'TRANNO',                                                                                         
                       (CASE WHEN b.SALEAMTM <> 0 THEN '1' ELSE '2' END)                                                       AS 'PLUGB',                                                                                          
                       '03'                                                                                                    AS 'TENDERTYPE',                                                                                     
                       (SELECT IsNull(ETC1NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E01' AND CODE = b.GVTYPE AND DELYN = '0')     AS 'TENDERTYPE2',                                                                                    
                       (SELECT IsNull(ETC2NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E01' AND CODE = b.GVTYPE AND DELYN = '0')     AS 'TENDERTYPE2NM',                                                                                  
                       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)                                        AS 'SALEAMT'                                                                                         
                  FROM 			                  	
                  
                  (	select c.FNSHDATE AS CLOSEDATE, a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO, b.SEQ
								        , a.COMPANY, a.TRANTYPE, a.TRANKIND
										  , (case when a.STORECD IN ('013','023') then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end) SALCOMCD
								        , '1' SALEFLAG
								        , '0' PAYFLAG, b.PLUGB
								        , max(c.RESTYPE) RESTYPE
									  from IDPS..TRNRESERVATION c, IDPS..TRNSALHDR a (INDEX PK_TRNSALHDR), IDPS..TRNSALDTL b (INDEX PK_TRNSALDTL)
									 where c.SALEDATE = a.SALEDATE   and c.STORECD  = a.STORECD    and c.POSNO    = a.POSNO      and c.TRANNO   = a.TRANNO
										and a.SALEDATE = b.SALEDATE   and a.STORECD  = b.STORECD    and a.POSNO    = b.POSNO      and a.TRANNO   = b.TRANNO
								      and a.TRANTYPE in ('00','01')
										and a.TRANKIND = '15'
										AND a.POSNO NOT LIKE '99%'
										AND a.DELYN = '0'
										and b.CANCELGB = '0'
								        and (c.FNSHDATE <> '' and IsNull(c.FNSHDATE, '') <> '' )
								 group by c.SALEDATE, c.STORECD, c.POSNO, c.TRANNO
								        , a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO
								        , b.SALEDATE, b.STORECD, b.POSNO, b.TRANNO, b.SEQ
								        , a.COMPANY, a.TRANTYPE, a.TRANKIND, c.FNSHDATE, b.PLUGB
				   						, (case when a.STORECD  IN ('013','023')  then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end) ) a, IDPS..TRNGVPAY b                                                                                                                                                                            
                 WHERE a.SALEDATE   = b.SALEDATE                                                                                                                                                                                    
                   AND a.STORECD    = b.STORECD                                                                                                                                                                                     
                   AND a.POSNO      = b.POSNO                                                                                                                                                                                       
                   AND a.TRANNO     = b.TRANNO                                                                                                                                                                            
                   AND a.SEQ        = 1                                                                                                                                                                                   
                   AND a.TRANTYPE   IN ('00', '01')                                                                                                                                                                                 
                   AND a.TRANKIND  = '15' AND a.SALEFLAG  = '1' AND a.PAYFLAG   = '0'                                                                                                                                             
                AND a.POSNO    NOT LIKE '__[49]_'  
                AND a.CLOSEDATE='{saleDate}'     
                       AND a.STORECD  = '{storeCd}'                                                                                                      



        ) TT1                                                                                                                                                                                                             
        GROUP BY TT1.SALEDATE, TT1.STORECD, TT1.PLUGB, TT1.TENDERTYPE2, TT1.COMPANY                                                                                                                                         
        ORDER BY 1,2,3,4,5                                                                                                                                                                                                

        AT ISOLATION 0                               
   

    """,    


    "getcommissionsaledata": """
        
        SELECT TT1.SALEDATE                                                                                                             AS 'SALEDATE',                                                                    
               (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C03' AND CODE = TT1.STORECD AND DELYN = '0') AS 'STORECD',           

               (CASE WHEN TT1.PLUGB = '1' THEN '교보문고' ELSE '핫트랙스' END)                                                          AS 'PLUGB',                                                                       
               TT1.TENDERTYPE2 + ':' + (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E03' AND CODE = TT1.TENDERTYPE2 AND DELYN = '0') AS 'TENDERTYPE2',                                                                 
               SUM((CASE WHEN TT1.COMPANY = '1' THEN TT1.SALEAMT ELSE 0           END))                                                 AS 'KB_SALEAMT',                              
               SUM((CASE WHEN TT1.COMPANY = '1' THEN 0           ELSE TT1.SALEAMT END))                                                 AS 'HT_SALEAMT',                              
               SUM(TT1.SALEAMT)                                                                                                         AS 'TOT_SALEAMT'                              
          FROM (                                                                                                                                                                                                                   
                SELECT '일반매출금권(상품권판매/상품권지불/선결제 제외)'                                                       AS 'TYPE',                                                                                           
                       a.SALEDATE                                                                                              AS 'SALEDATE',                                                                                       
                       a.STORECD                                                                                               AS 'STORECD',                                                                                        
                       a.POSNO                                                                                                 AS 'POSNO',                                                                                          
                       a.COMPANY                                                                                               AS 'COMPANY',                                                                                        
                       a.TRANNO                                                                                                AS 'TRANNO',                                                                                         
                       (CASE WHEN b.SALEAMTM <> 0 THEN '1' ELSE '2' END)                                                       AS 'PLUGB',                                                                                          
                       b.TENDERTYPE                                                                                            AS 'TENDERTYPE',                                                                                     
                       (SELECT IsNull(ETC1NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E02' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2',                                                                                    
                       (SELECT IsNull(ETC2NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E02' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2NM',                                                                                  
                       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)                                        AS 'SALEAMT'                                                                                         
                  FROM IDPS..TRNSALHDR a, IDPS..TRNSALPAY b                                                                                                                                                                                   
                 WHERE a.SALEDATE   = b.SALEDATE                                                                                                                                                                                    
                   AND a.STORECD    = b.STORECD                                                                                                                                                                                     
                   AND a.POSNO      = b.POSNO                                                                                                                                                                                       
                   AND a.TRANNO     = b.TRANNO                                                                                                                                                                                      
                   AND a.TRANTYPE   IN ('00', '01')                                                                                                                                                                                 
                   AND a.TRANKIND   NOT IN ('10','11','13','14','15', '12', '17')                                                                                                                                                          
                   AND b.TENDERTYPE NOT IN ('03', '12')                                                                                                                                                                         
                                                                                                            
                AND a.POSNO    NOT LIKE '__[49]_'    
                AND a.SALEDATE='{saleDate}'     
                       AND a.STORECD  = '{storeCd}'  

                UNION ALL                                                                                                                                                                                                 
                SELECT '상품권판매매출금권(상품권지불/선결제 제외)'                                                            AS 'TYPE',                                                                                         
                       a.SALEDATE                                                                                              AS 'SALEDATE',                                                                                       
                       a.STORECD                                                                                               AS 'STORECD',                                                                                        
                       a.POSNO                                                                                                 AS 'POSNO',                                                                                          
                       a.COMPANY                                                                                               AS 'COMPANY',                                                                                        
                       a.TRANNO                                                                                                AS 'TRANNO',                                                                                         
                       (CASE WHEN b.SALEAMTM <> 0 THEN '1' ELSE '2' END)                                                       AS 'PLUGB',                                                                                          
                       b.TENDERTYPE                                                                                            AS 'TENDERTYPE',                                                                                     
                       (SELECT IsNull(ETC1NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E02' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2',                                                                                    
                       (SELECT IsNull(ETC2NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E02' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2NM',                                                                                  
                       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)                                        AS 'SALEAMT'                                                                                         
                  FROM IDPS..TRNSALHDR a, IDPS..TRNSALPAY b                                                                                                                                                                                   
                 WHERE a.SALEDATE   = b.SALEDATE                                                                                                                                                                                    
                   AND a.STORECD    = b.STORECD                                                                                                                                                                                     
                   AND a.POSNO      = b.POSNO                                                                                                                                                                                       
                   AND a.TRANNO     = b.TRANNO                                                                                                                                                                                      
                   AND a.TRANTYPE   IN ('00', '01')                                                                                                                                                                                 
                   AND a.TRANKIND   IN ('13','14', '17')                                                                                                                                                                                
                   AND b.TENDERTYPE NOT IN ('03', '12')                                                                                                                                                                         

                   AND a.POSNO    NOT LIKE '__[49]_'  
                   AND a.SALEDATE='{saleDate}'     
                   AND a.STORECD  = '{storeCd}'  
                                                                
                UNION ALL                                                                                                                                                                                                 
                SELECT '선결제완료매출금권(상품권지불제외)'                                                                    AS 'TYPE',                                                                                      
                       a.CLOSEDATE                                                                                             AS 'SALEDATE',                                                                                       
                       a.STORECD                                                                                               AS 'STORECD',                                                                                        
                       a.POSNO                                                                                                 AS 'POSNO',                                                                                          
                       a.COMPANY                                                                                               AS 'COMPANY',                                                                                        
                       a.TRANNO                                                                                                AS 'TRANNO',                                                                                         
                       (CASE WHEN b.SALEAMTM <> 0 THEN '1' ELSE '2' END)                                                       AS 'PLUGB',                                                                                          
                       b.TENDERTYPE                                                                                            AS 'TENDERTYPE',                                                                                     
                       (SELECT IsNull(ETC1NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E02' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2',                                                                                    
                       (SELECT IsNull(ETC2NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E02' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2NM',                                                                                  
                       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)                                        AS 'SALEAMT'                                                                                         
                  FROM 
                  
                  (	select c.FNSHDATE AS CLOSEDATE, a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO, b.SEQ
								        , a.COMPANY, a.TRANTYPE, a.TRANKIND
										  , (case when a.STORECD IN ('013','023') then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end) SALCOMCD
								        , '1' SALEFLAG
								        , '0' PAYFLAG, b.PLUGB
								        , max(c.RESTYPE) RESTYPE
									  from IDPS..TRNRESERVATION c, IDPS..TRNSALHDR a (INDEX PK_TRNSALHDR), IDPS..TRNSALDTL b (INDEX PK_TRNSALDTL)
									 where c.SALEDATE = a.SALEDATE   and c.STORECD  = a.STORECD    and c.POSNO    = a.POSNO      and c.TRANNO   = a.TRANNO
										and a.SALEDATE = b.SALEDATE   and a.STORECD  = b.STORECD    and a.POSNO    = b.POSNO      and a.TRANNO   = b.TRANNO
								      and a.TRANTYPE in ('00','01')
										and a.TRANKIND = '15'
										AND a.POSNO NOT LIKE '99%'
										AND a.DELYN = '0'
										and b.CANCELGB = '0'
								        and (c.FNSHDATE <> '' and IsNull(c.FNSHDATE, '') <> '' )
								 group by c.SALEDATE, c.STORECD, c.POSNO, c.TRANNO
								        , a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO
								        , b.SALEDATE, b.STORECD, b.POSNO, b.TRANNO, b.SEQ
								        , a.COMPANY, a.TRANTYPE, a.TRANKIND, c.FNSHDATE, b.PLUGB
				   						, (case when a.STORECD  IN ('013','023')  then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end) ) a
                  
                  
                  , IDPS..TRNSALPAY b                                                                                                                                                                              
                 WHERE a.SALEDATE   = b.SALEDATE                                                                                                                                                                                    
                   AND a.STORECD    = b.STORECD                                                                                                                                                                                     
                   AND a.POSNO      = b.POSNO                                                                                                                                                                                       
                   AND a.TRANNO     = b.TRANNO                                                                                                                                                                            
                   AND a.SEQ        = 1                                                                                                                                                                                              
                   AND a.TRANTYPE   IN ('00', '01')                                                                                                                                                                                 
                   AND a.TRANKIND  = '15' AND a.SALEFLAG  = '1' AND a.PAYFLAG   = '0'                                                                                                                                             
                   AND b.TENDERTYPE NOT IN ('03', '12')                                                                                                                                                                         

                   AND a.POSNO    NOT LIKE '__[49]_'  
                   AND a.CLOSEDATE='{saleDate}'     
                   AND a.STORECD  = '{storeCd}'                                                                                                      


                UNION ALL                                                                                                                                                                                                 
                SELECT '상품권지불금권(일반상품판매, 선결제, 상품권판매 제외)'                                                 AS 'TYPE',                                                                                      
                       a.SALEDATE                                                                                              AS 'SALEDATE',                                                                                       
                       a.STORECD                                                                                               AS 'STORECD',                                                                                        
                       a.POSNO                                                                                                 AS 'POSNO',                                                                                          
                       a.COMPANY                                                                                               AS 'COMPANY',                                                                                        
                       a.TRANNO                                                                                                AS 'TRANNO',                                                                                         
                       (CASE WHEN b.SALEAMTM <> 0 THEN '1' ELSE '2' END)                                                       AS 'PLUGB',                                                                                          
                       '03'                                                                                                    AS 'TENDERTYPE',                                                                                     
                       (SELECT IsNull(ETC1NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E01' AND CODE = b.GVTYPE AND DELYN = '0')     AS 'TENDERTYPE2',                                                                                    
                       (SELECT IsNull(ETC2NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E01' AND CODE = b.GVTYPE AND DELYN = '0')     AS 'TENDERTYPE2NM',                                                                                  
                       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)                                        AS 'SALEAMT'                                                                                         
                  FROM IDPS..TRNSALHDR a, IDPS..TRNGVPAY b                                                                                                                                                                                      
                 WHERE a.SALEDATE   = b.SALEDATE                                                                                                                                                                                    
                   AND a.STORECD    = b.STORECD                                                                                                                                                                                     
                   AND a.POSNO      = b.POSNO                                                                                                                                                                                       
                   AND a.TRANNO     = b.TRANNO                                                                                                                                                                                      
                   AND a.TRANTYPE   IN ('00', '01')                                                                                                                                                                                 
                   AND a.TRANKIND   NOT IN ('10', '11','13','14', '15', '12', '17')                                                                                                                                                             

                   AND a.POSNO    NOT LIKE '__[49]_'  
                   AND a.SALEDATE='{saleDate}'     
                   AND a.STORECD  = '{storeCd}'                                                                                                      

                                                                                                      


                UNION ALL                                                                                                                                                                                                 
                SELECT '상품권지불금권(선결제완료매출금권)'                                                                    AS 'TYPE',                                                                                      
                       a.CLOSEDATE                                                                                             AS 'SALEDATE',                                                                                       
                       a.STORECD                                                                                               AS 'STORECD',                                                                                        
                       a.POSNO                                                                                                 AS 'POSNO',                                                                                          
                       a.COMPANY                                                                                               AS 'COMPANY',                                                                                        
                       a.TRANNO                                                                                                AS 'TRANNO',                                                                                         
                       (CASE WHEN b.SALEAMTM <> 0 THEN '1' ELSE '2' END)                                                       AS 'PLUGB',                                                                                          
                       '03'                                                                                                    AS 'TENDERTYPE',                                                                                     
                       (SELECT IsNull(ETC1NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E01' AND CODE = b.GVTYPE AND DELYN = '0')     AS 'TENDERTYPE2',                                                                                    
                       (SELECT IsNull(ETC2NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E01' AND CODE = b.GVTYPE AND DELYN = '0')     AS 'TENDERTYPE2NM',                                                                                  
                       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)                                        AS 'SALEAMT'                                                                                         
                  FROM 			                  	
                  
                  (	select c.FNSHDATE AS CLOSEDATE, a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO, b.SEQ
								        , a.COMPANY, a.TRANTYPE, a.TRANKIND
										  , (case when a.STORECD IN ('013','023') then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end) SALCOMCD
								        , '1' SALEFLAG
								        , '0' PAYFLAG, b.PLUGB
								        , max(c.RESTYPE) RESTYPE
									  from IDPS..TRNRESERVATION c, IDPS..TRNSALHDR a (INDEX PK_TRNSALHDR), IDPS..TRNSALDTL b (INDEX PK_TRNSALDTL)
									 where c.SALEDATE = a.SALEDATE   and c.STORECD  = a.STORECD    and c.POSNO    = a.POSNO      and c.TRANNO   = a.TRANNO
										and a.SALEDATE = b.SALEDATE   and a.STORECD  = b.STORECD    and a.POSNO    = b.POSNO      and a.TRANNO   = b.TRANNO
								      and a.TRANTYPE in ('00','01')
										and a.TRANKIND = '15'
										AND a.POSNO NOT LIKE '99%'
										AND a.DELYN = '0'
										and b.CANCELGB = '0'
								        and (c.FNSHDATE <> '' and IsNull(c.FNSHDATE, '') <> '' )
								 group by c.SALEDATE, c.STORECD, c.POSNO, c.TRANNO
								        , a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO
								        , b.SALEDATE, b.STORECD, b.POSNO, b.TRANNO, b.SEQ
								        , a.COMPANY, a.TRANTYPE, a.TRANKIND, c.FNSHDATE, b.PLUGB
				   						, (case when a.STORECD  IN ('013','023')  then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end) ) a, IDPS..TRNGVPAY b                                                                                                                                                                            
                 WHERE a.SALEDATE   = b.SALEDATE                                                                                                                                                                                    
                   AND a.STORECD    = b.STORECD                                                                                                                                                                                     
                   AND a.POSNO      = b.POSNO                                                                                                                                                                                       
                   AND a.TRANNO     = b.TRANNO                                                                                                                                                                            
                   AND a.SEQ        = 1                                                                                                                                                                                   
                   AND a.TRANTYPE   IN ('00', '01')                                                                                                                                                                                 
                   AND a.TRANKIND  = '15' AND a.SALEFLAG  = '1' AND a.PAYFLAG   = '0'                                                                                                                                             

                   AND a.POSNO    NOT LIKE '__[49]_'  
                   AND a.CLOSEDATE='{saleDate}'     
                   AND a.STORECD  = '{storeCd}'                                                                                                      



        ) TT1                                                                                                                                                                                                             
        GROUP BY TT1.SALEDATE, TT1.STORECD, TT1.PLUGB, TT1.TENDERTYPE2, TT1.COMPANY                                                                                                                                         
        ORDER BY 1,2,3,4,5                                                                                                                                                                                                

        AT ISOLATION 0                               
   

    """,    


    "getcommissionsaledatafromto": """
        
        SELECT TT1.SALEDATE                                                                                                             AS 'SALEDATE',                                                                    
               (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C03' AND CODE = TT1.STORECD AND DELYN = '0') AS 'STORECD',           

               (CASE WHEN TT1.PLUGB = '1' THEN '교보문고' ELSE '핫트랙스' END)                                                          AS 'PLUGB',                                                                       
               TT1.TENDERTYPE2 + ':' + (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E03' AND CODE = TT1.TENDERTYPE2 AND DELYN = '0') AS 'TENDERTYPE2',                                                                 
               SUM((CASE WHEN TT1.COMPANY = '1' THEN TT1.SALEAMT ELSE 0           END))                                                 AS 'KB_SALEAMT',                              
               SUM((CASE WHEN TT1.COMPANY = '1' THEN 0           ELSE TT1.SALEAMT END))                                                 AS 'HT_SALEAMT',                              
               SUM(TT1.SALEAMT)                                                                                                         AS 'TOT_SALEAMT'                              
          FROM (                                                                                                                                                                                                                   
                SELECT '일반매출금권(상품권판매/상품권지불/선결제 제외)'                                                       AS 'TYPE',                                                                                           
                       a.SALEDATE                                                                                              AS 'SALEDATE',                                                                                       
                       a.STORECD                                                                                               AS 'STORECD',                                                                                        
                       a.POSNO                                                                                                 AS 'POSNO',                                                                                          
                       a.COMPANY                                                                                               AS 'COMPANY',                                                                                        
                       a.TRANNO                                                                                                AS 'TRANNO',                                                                                         
                       (CASE WHEN b.SALEAMTM <> 0 THEN '1' ELSE '2' END)                                                       AS 'PLUGB',                                                                                          
                       b.TENDERTYPE                                                                                            AS 'TENDERTYPE',                                                                                     
                       (SELECT IsNull(ETC1NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E02' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2',                                                                                    
                       (SELECT IsNull(ETC2NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E02' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2NM',                                                                                  
                       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)                                        AS 'SALEAMT'                                                                                         
                  FROM IDPS..TRNSALHDR a, IDPS..TRNSALPAY b                                                                                                                                                                                   
                 WHERE a.SALEDATE   = b.SALEDATE                                                                                                                                                                                    
                   AND a.STORECD    = b.STORECD                                                                                                                                                                                     
                   AND a.POSNO      = b.POSNO                                                                                                                                                                                       
                   AND a.TRANNO     = b.TRANNO                                                                                                                                                                                      
                   AND a.TRANTYPE   IN ('00', '01')                                                                                                                                                                                 
                   AND a.TRANKIND   NOT IN ('10','11','13','14','15', '12', '17')                                                                                                                                                          
                   AND b.TENDERTYPE NOT IN ('03', '12')                                                                                                                                                                         
                                                                                                            
                AND a.POSNO    NOT LIKE '__[49]_'    

                   AND a.SALEDATE  >='{fromDate}'     
                   AND a.SALEDATE  <='{toDate}'   
                       AND a.STORECD  = '{storeCd}'  

                UNION ALL                                                                                                                                                                                                 
                SELECT '상품권판매매출금권(상품권지불/선결제 제외)'                                                            AS 'TYPE',                                                                                         
                       a.SALEDATE                                                                                              AS 'SALEDATE',                                                                                       
                       a.STORECD                                                                                               AS 'STORECD',                                                                                        
                       a.POSNO                                                                                                 AS 'POSNO',                                                                                          
                       a.COMPANY                                                                                               AS 'COMPANY',                                                                                        
                       a.TRANNO                                                                                                AS 'TRANNO',                                                                                         
                       (CASE WHEN b.SALEAMTM <> 0 THEN '1' ELSE '2' END)                                                       AS 'PLUGB',                                                                                          
                       b.TENDERTYPE                                                                                            AS 'TENDERTYPE',                                                                                     
                       (SELECT IsNull(ETC1NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E02' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2',                                                                                    
                       (SELECT IsNull(ETC2NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E02' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2NM',                                                                                  
                       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)                                        AS 'SALEAMT'                                                                                         
                  FROM IDPS..TRNSALHDR a, IDPS..TRNSALPAY b                                                                                                                                                                                   
                 WHERE a.SALEDATE   = b.SALEDATE                                                                                                                                                                                    
                   AND a.STORECD    = b.STORECD                                                                                                                                                                                     
                   AND a.POSNO      = b.POSNO                                                                                                                                                                                       
                   AND a.TRANNO     = b.TRANNO                                                                                                                                                                                      
                   AND a.TRANTYPE   IN ('00', '01')                                                                                                                                                                                 
                   AND a.TRANKIND   IN ('13','14', '17')                                                                                                                                                                                
                   AND b.TENDERTYPE NOT IN ('03', '12')                                                                                                                                                                         

                   AND a.POSNO    NOT LIKE '__[49]_'  

                   AND a.SALEDATE  >='{fromDate}'     
                   AND a.SALEDATE  <='{toDate}'   
                   AND a.STORECD  = '{storeCd}'  
                                                                
                UNION ALL                                                                                                                                                                                                 
                SELECT '선결제완료매출금권(상품권지불제외)'                                                                    AS 'TYPE',                                                                                      
                       a.CLOSEDATE                                                                                             AS 'SALEDATE',                                                                                       
                       a.STORECD                                                                                               AS 'STORECD',                                                                                        
                       a.POSNO                                                                                                 AS 'POSNO',                                                                                          
                       a.COMPANY                                                                                               AS 'COMPANY',                                                                                        
                       a.TRANNO                                                                                                AS 'TRANNO',                                                                                         
                       (CASE WHEN b.SALEAMTM <> 0 THEN '1' ELSE '2' END)                                                       AS 'PLUGB',                                                                                          
                       b.TENDERTYPE                                                                                            AS 'TENDERTYPE',                                                                                     
                       (SELECT IsNull(ETC1NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E02' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2',                                                                                    
                       (SELECT IsNull(ETC2NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E02' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2NM',                                                                                  
                       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)                                        AS 'SALEAMT'                                                                                         
                  FROM 
                  
                  (	select c.FNSHDATE AS CLOSEDATE, a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO, b.SEQ
								        , a.COMPANY, a.TRANTYPE, a.TRANKIND
										  , (case when a.STORECD IN ('013','023') then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end) SALCOMCD
								        , '1' SALEFLAG
								        , '0' PAYFLAG, b.PLUGB
								        , max(c.RESTYPE) RESTYPE
									  from IDPS..TRNRESERVATION c, IDPS..TRNSALHDR a (INDEX PK_TRNSALHDR), IDPS..TRNSALDTL b (INDEX PK_TRNSALDTL)
									 where c.SALEDATE = a.SALEDATE   and c.STORECD  = a.STORECD    and c.POSNO    = a.POSNO      and c.TRANNO   = a.TRANNO
										and a.SALEDATE = b.SALEDATE   and a.STORECD  = b.STORECD    and a.POSNO    = b.POSNO      and a.TRANNO   = b.TRANNO
								      and a.TRANTYPE in ('00','01')
										and a.TRANKIND = '15'
										AND a.POSNO NOT LIKE '99%'
										AND a.DELYN = '0'
										and b.CANCELGB = '0'
								        and (c.FNSHDATE <> '' and IsNull(c.FNSHDATE, '') <> '' )
								 group by c.SALEDATE, c.STORECD, c.POSNO, c.TRANNO
								        , a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO
								        , b.SALEDATE, b.STORECD, b.POSNO, b.TRANNO, b.SEQ
								        , a.COMPANY, a.TRANTYPE, a.TRANKIND, c.FNSHDATE, b.PLUGB
				   						, (case when a.STORECD  IN ('013','023')  then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end) ) a
                  
                  
                  , IDPS..TRNSALPAY b                                                                                                                                                                              
                 WHERE a.SALEDATE   = b.SALEDATE                                                                                                                                                                                    
                   AND a.STORECD    = b.STORECD                                                                                                                                                                                     
                   AND a.POSNO      = b.POSNO                                                                                                                                                                                       
                   AND a.TRANNO     = b.TRANNO                                                                                                                                                                            
                   AND a.SEQ        = 1                                                                                                                                                                                              
                   AND a.TRANTYPE   IN ('00', '01')                                                                                                                                                                                 
                   AND a.TRANKIND  = '15' AND a.SALEFLAG  = '1' AND a.PAYFLAG   = '0'                                                                                                                                             
                   AND b.TENDERTYPE NOT IN ('03', '12')                                                                                                                                                                         

                   AND a.POSNO    NOT LIKE '__[49]_'  
                   AND a.CLOSEDATE  >='{fromDate}'     
                   AND a.CLOSEDATE  <='{toDate}'   
                   AND a.STORECD  = '{storeCd}'                                                                                                      


                UNION ALL                                                                                                                                                                                                 
                SELECT '상품권지불금권(일반상품판매, 선결제, 상품권판매 제외)'                                                 AS 'TYPE',                                                                                      
                       a.SALEDATE                                                                                              AS 'SALEDATE',                                                                                       
                       a.STORECD                                                                                               AS 'STORECD',                                                                                        
                       a.POSNO                                                                                                 AS 'POSNO',                                                                                          
                       a.COMPANY                                                                                               AS 'COMPANY',                                                                                        
                       a.TRANNO                                                                                                AS 'TRANNO',                                                                                         
                       (CASE WHEN b.SALEAMTM <> 0 THEN '1' ELSE '2' END)                                                       AS 'PLUGB',                                                                                          
                       '03'                                                                                                    AS 'TENDERTYPE',                                                                                     
                       (SELECT IsNull(ETC1NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E01' AND CODE = b.GVTYPE AND DELYN = '0')     AS 'TENDERTYPE2',                                                                                    
                       (SELECT IsNull(ETC2NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E01' AND CODE = b.GVTYPE AND DELYN = '0')     AS 'TENDERTYPE2NM',                                                                                  
                       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)                                        AS 'SALEAMT'                                                                                         
                  FROM IDPS..TRNSALHDR a, IDPS..TRNGVPAY b                                                                                                                                                                                      
                 WHERE a.SALEDATE   = b.SALEDATE                                                                                                                                                                                    
                   AND a.STORECD    = b.STORECD                                                                                                                                                                                     
                   AND a.POSNO      = b.POSNO                                                                                                                                                                                       
                   AND a.TRANNO     = b.TRANNO                                                                                                                                                                                      
                   AND a.TRANTYPE   IN ('00', '01')                                                                                                                                                                                 
                   AND a.TRANKIND   NOT IN ('10', '11','13','14', '15', '12', '17')                                                                                                                                                             

                   AND a.POSNO    NOT LIKE '__[49]_'  

                   AND a.SALEDATE  >='{fromDate}'     
                   AND a.SALEDATE  <='{toDate}'   
                   AND a.STORECD  = '{storeCd}'                                                                                                      

                                                                                                      


                UNION ALL                                                                                                                                                                                                 
                SELECT '상품권지불금권(선결제완료매출금권)'                                                                    AS 'TYPE',                                                                                      
                       a.CLOSEDATE                                                                                             AS 'SALEDATE',                                                                                       
                       a.STORECD                                                                                               AS 'STORECD',                                                                                        
                       a.POSNO                                                                                                 AS 'POSNO',                                                                                          
                       a.COMPANY                                                                                               AS 'COMPANY',                                                                                        
                       a.TRANNO                                                                                                AS 'TRANNO',                                                                                         
                       (CASE WHEN b.SALEAMTM <> 0 THEN '1' ELSE '2' END)                                                       AS 'PLUGB',                                                                                          
                       '03'                                                                                                    AS 'TENDERTYPE',                                                                                     
                       (SELECT IsNull(ETC1NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E01' AND CODE = b.GVTYPE AND DELYN = '0')     AS 'TENDERTYPE2',                                                                                    
                       (SELECT IsNull(ETC2NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E01' AND CODE = b.GVTYPE AND DELYN = '0')     AS 'TENDERTYPE2NM',                                                                                  
                       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)                                        AS 'SALEAMT'                                                                                         
                  FROM 			                  	
                  
                  (	select c.FNSHDATE AS CLOSEDATE, a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO, b.SEQ
								        , a.COMPANY, a.TRANTYPE, a.TRANKIND
										  , (case when a.STORECD IN ('013','023') then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end) SALCOMCD
								        , '1' SALEFLAG
								        , '0' PAYFLAG, b.PLUGB
								        , max(c.RESTYPE) RESTYPE
									  from IDPS..TRNRESERVATION c, IDPS..TRNSALHDR a (INDEX PK_TRNSALHDR), IDPS..TRNSALDTL b (INDEX PK_TRNSALDTL)
									 where c.SALEDATE = a.SALEDATE   and c.STORECD  = a.STORECD    and c.POSNO    = a.POSNO      and c.TRANNO   = a.TRANNO
										and a.SALEDATE = b.SALEDATE   and a.STORECD  = b.STORECD    and a.POSNO    = b.POSNO      and a.TRANNO   = b.TRANNO
								      and a.TRANTYPE in ('00','01')
										and a.TRANKIND = '15'
										AND a.POSNO NOT LIKE '99%'
										AND a.DELYN = '0'
										and b.CANCELGB = '0'
								        and (c.FNSHDATE <> '' and IsNull(c.FNSHDATE, '') <> '' )
								 group by c.SALEDATE, c.STORECD, c.POSNO, c.TRANNO
								        , a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO
								        , b.SALEDATE, b.STORECD, b.POSNO, b.TRANNO, b.SEQ
								        , a.COMPANY, a.TRANTYPE, a.TRANKIND, c.FNSHDATE, b.PLUGB
				   						, (case when a.STORECD  IN ('013','023')  then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end) ) a, IDPS..TRNGVPAY b                                                                                                                                                                            
                 WHERE a.SALEDATE   = b.SALEDATE                                                                                                                                                                                    
                   AND a.STORECD    = b.STORECD                                                                                                                                                                                     
                   AND a.POSNO      = b.POSNO                                                                                                                                                                                       
                   AND a.TRANNO     = b.TRANNO                                                                                                                                                                            
                   AND a.SEQ        = 1                                                                                                                                                                                   
                   AND a.TRANTYPE   IN ('00', '01')                                                                                                                                                                                 
                   AND a.TRANKIND  = '15' AND a.SALEFLAG  = '1' AND a.PAYFLAG   = '0'                                                                                                                                             

                   AND a.POSNO    NOT LIKE '__[49]_'  
                   AND a.CLOSEDATE  >='{fromDate}'     
                   AND a.CLOSEDATE  <='{toDate}'   
                   AND a.STORECD  = '{storeCd}'                                                                                                      



        ) TT1                                                                                                                                                                                                             
        GROUP BY TT1.SALEDATE, TT1.STORECD, TT1.PLUGB, TT1.TENDERTYPE2, TT1.COMPANY                                                                                                                                         
        ORDER BY 1,2,3,4,5                                                                                                                                                                                                

        AT ISOLATION 0                               
   

    """,        

    "getnontaxamtbrieflyfromto": """
              
        SELECT TT1.SALEDATE                                                                                                      AS 'SALEDATE',
            TT1.STORECD + ':' + (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C03' AND CODE = TT1.STORECD AND DELYN = '0')  AS 'STORECD',
            (CASE WHEN TT1.PLUGB = '1' THEN '교보문고' ELSE '핫트랙스' END)                                                   AS 'PLUGB',
            SUM((CASE WHEN TT1.TAXGB = '0' THEN TT1.SALEAMT ELSE 0 END))                                                      AS 'TAXED_SALEAMT',
            SUM((CASE WHEN TT1.TAXGB = '1' THEN TT1.SALEAMT ELSE 0 END))                                                      AS 'NONTAXED_SALEAMT',
            SUM((CASE WHEN TT1.TAXGB = '2' THEN TT1.SALEAMT ELSE 0 END))                                                      AS 'GIFTCARD_SALEAMT',
            SUM(TT1.SALEAMT)                                                                                                  AS 'TOT_SALEAMT'
          FROM (                                                                                                                                           		
            SELECT '일반매출'                                                        AS 'TYPE',                                                             		       
                a.SALEDATE                                                        AS 'SALEDATE',                                                         		       
                a.STORECD                                                         AS 'STORECD',                                                          		       
                a.POSNO                                                           AS 'POSNO',                                                                 		 
                a.COMPANY                                                         AS 'COMPANY',                                                          		       
                a.TRANNO                                                          AS 'TRANNO',                                                           		       
                b.SEQ                                                             AS 'SEQ',                                                              		       
                (CASE WHEN b.PLUGB = '7' THEN '1' ELSE '2' END)                   AS 'PLUGB',                                                            		       
                (CASE WHEN b.TAXGB = '0' THEN '0' ELSE '1' END)                   AS 'TAXGB',                                                            		       
                (CASE WHEN a.TRANTYPE = '00' THEN b.QTY ELSE -b.QTY END)          AS 'QTY',                                                              		       
                (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)  AS 'SALEAMT'                                                           		
              FROM IDPS..TRNSALHDR a, IDPS..TRNSALDTL b                                                                                                                   		
            WHERE a.SALEDATE = b.SALEDATE                                                                                                                   		  
              AND a.STORECD  = b.STORECD                                                                                                                    		  
              AND a.POSNO    = b.POSNO                                                                                                                      		  
              AND a.TRANNO   = b.TRANNO                                                                                                                     		  
              AND a.TRANTYPE IN ('00', '01')                                                                                                                		  
              AND a.TRANKIND NOT IN ('10', '11', '13', '14', '15', '12')                                                                                               		  
              AND b.CANCELGB = '0'
              AND a.POSNO    NOT LIKE '__[49]_'   
              AND a.SALEDATE >= '{fromDate}'  AND a.SALEDATE <= '{toDate}'   
              AND a.STORECD  =  '{storeCd}'  
              AND b.PLUGB    = '7'  
            UNION ALL                                                                                                                                       		
            SELECT '선매출'                                                          AS 'TYPE',                                                             		       
                a.CLOSEDATE                                                       AS 'SALEDATE',                                                         		       
                a.STORECD                                                         AS 'STORECD',                                                          		       
                a.POSNO                                                           AS 'POSNO',                                                                 		 
                a.COMPANY                                                         AS 'COMPANY',                                                          		       
                a.TRANNO                                                          AS 'TRANNO',                                                           		       
                b.SEQ                                                             AS 'SEQ',                                                              		       
                (CASE WHEN b.PLUGB = '7' THEN '1' ELSE '2' END)                   AS 'PLUGB',                                                            		       
                (CASE WHEN b.TAXGB = '0' THEN '0' ELSE '1' END)                   AS 'TAXGB',                                                            		       
                (CASE WHEN a.TRANTYPE = '00' THEN b.QTY ELSE -b.QTY END)          AS 'QTY',                                                              		       
                (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)  AS 'SALEAMT'                                                           		
              FROM 
              (select c.FNSHDATE AS CLOSEDATE, a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO, b.SEQ
                                      , a.COMPANY, a.TRANTYPE, a.TRANKIND
                                    , (case when a.STORECD IN ('013','023') then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end) SALCOMCD
                                      , '1' SALEFLAG
                                      , '0' PAYFLAG, b.PLUGB
                                      , max(c.RESTYPE) RESTYPE
                                  from IDPS..TRNRESERVATION c, IDPS..TRNSALHDR a (INDEX PK_TRNSALHDR), IDPS..TRNSALDTL b (INDEX PK_TRNSALDTL)
                                where c.SALEDATE = a.SALEDATE   and c.STORECD  = a.STORECD    and c.POSNO    = a.POSNO      and c.TRANNO   = a.TRANNO
                                  and a.SALEDATE = b.SALEDATE   and a.STORECD  = b.STORECD    and a.POSNO    = b.POSNO      and a.TRANNO   = b.TRANNO
                                    and a.TRANTYPE in ('00','01')
                                  and a.TRANKIND = '15'
                                  AND a.POSNO NOT LIKE '99%'
                                  AND a.DELYN = '0'
                                  and b.CANCELGB = '0'
                                      and (c.FNSHDATE <> '' AND c.FNSHDATE IS NOT NULL)
                              group by c.SALEDATE, c.STORECD, c.POSNO, c.TRANNO
                                      , a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO
                                      , b.SALEDATE, b.STORECD, b.POSNO, b.TRANNO, b.SEQ
                                      , a.COMPANY, a.TRANTYPE, a.TRANKIND, c.FNSHDATE, b.PLUGB
                                    , (case when a.STORECD  IN ('013','023')  then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end)
                              ) a
              
              , IDPS..TRNSALDTL b                                                                                                             		
            WHERE a.SALEDATE  = b.SALEDATE                                                                                                                  		  
              AND a.STORECD   = b.STORECD                                                                                                                   		  
              AND a.POSNO     = b.POSNO                                                                                                                     		  
              AND a.TRANNO    = b.TRANNO                                                                                                                    		  
              AND a.SEQ       = b.SEQ                                                                                                                       		  
              AND a.TRANTYPE  IN ('00', '01')                                                                                                               		  
              AND a.TRANKIND  = '15' AND a.SALEFLAG  = '1' AND a.PAYFLAG   = '0'                                                                            		  
              AND b.CANCELGB  = '0'
              AND a.POSNO    NOT LIKE '__[49]_'                                                                                                                            		  
              AND a.CLOSEDATE >='{fromDate}' AND a.CLOSEDATE <='{toDate}'   
            AND a.STORECD  =  '{storeCd}'  
                  AND b.PLUGB    = '7'  
            UNION ALL                                                                                                                                       		
            SELECT '상품권'                                                          AS 'TYPE',                                                             		       
                a.SALEDATE                                                        AS 'SALEDATE',                                                         		       
                a.STORECD                                                         AS 'STORECD',                                                          		       
                a.POSNO                                                           AS 'POSNO',                                                                 		 
                a.COMPANY                                                         AS 'COMPANY',                                                          		       
                a.TRANNO                                                          AS 'TRANNO',                                                           		       
                b.SEQ                                                             AS 'SEQ',                                                              		       
                (CASE WHEN b.PLUGB = '7' THEN '1' ELSE '2' END)                   AS 'PLUGB',                                                            		       
                '2'                                                               AS 'TAXGB',                                                            		       
                (CASE WHEN a.TRANTYPE = '00' THEN b.QTY ELSE -b.QTY END)          AS 'QTY',                                                              		       
                (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)  AS 'SALEAMT'                                                           		
              FROM IDPS..TRNSALHDR a, IDPS..TRNSALDTL b                                                                                                                   		
            WHERE a.SALEDATE = b.SALEDATE                                                                                                                   		  
              AND a.STORECD  = b.STORECD                                                                                                                    		  
              AND a.POSNO    = b.POSNO                                                                                                                      		  
              AND a.TRANNO   = b.TRANNO                                                                                                                     		  
              AND a.TRANTYPE IN ('00', '01')                                                                                                                		  
              AND a.TRANKIND IN ('13', '14')                                                                                                                		  
              AND b.CANCELGB = '0'                                                                                                                                 
              AND a.POSNO    NOT LIKE '__[49]_'                          
              AND a.SALEDATE >= '{fromDate}'  AND a.SALEDATE <= '{toDate}'   
              AND a.STORECD  =  '{storeCd}'  
              AND b.PLUGB    = '7'                                                                                                          	   
        ) TT1                                                                                                                                            
        GROUP BY TT1.SALEDATE, TT1.STORECD, TT1.PLUGB
        ORDER BY 1,2,3,4,5                                                                                                                             
        AT ISOLATION 0       
   

    """,        
    "getcommissionsaledata_dtl": """

        SELECT '선결제완료매출금권(상품권지불제외)'                                                                    AS 'TYPE',
                       c.FNSHDATE                                                                                             AS 'SALEDATE',
                       a.STORECD                                                                                               AS 'STORECD',
                       a.POSNO                                                                                                 AS 'POSNO',
                       a.COMPANY                                                                                               AS 'COMPANY',
                       a.TRANNO                                                                                                AS 'TRANNO',
                       (CASE WHEN b.SALEAMTM <> 0 THEN '1' ELSE '2' END)                                                       AS 'PLUGB',
                       b.TENDERTYPE                                                                                            AS 'TENDERTYPE',
                       (SELECT IsNull(ETC1NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E02' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2',
                       (SELECT IsNull(ETC2NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E02' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2NM',
                       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)                                        AS 'SALEAMT'
                  FROM IDPS..TRNRESERVATION c, IDPS..TRNSALHDR a, IDPS..TRNSALPAY b                                                                                                                                                                              
                 WHERE a.SALEDATE   = b.SALEDATE                                                                                                                                                                                    
                   AND a.STORECD    = b.STORECD                                                                                                                                                                                     
                   AND a.POSNO      = b.POSNO                                                                                                                                                                                       
                   AND a.TRANNO     = b.TRANNO   
                   AND c.SALEDATE = a.SALEDATE   and c.STORECD  = a.STORECD    and c.POSNO    = a.POSNO      and c.TRANNO   = a.TRANNO                                                                                                                                                                         
                   AND c.SEQ        = 1
                   AND a.TRANTYPE   IN ('00', '01')
                   AND b.TENDERTYPE NOT IN ('03', '12')
                   AND c.FNSHDATE ='{saleDate}'     
                
                       AND a.STORECD  = '{storeCd}'           
                       AND b.TENDERTYPE ='01'
                       AND a.COMPANY ='1'
        AT ISOLATION 0

    """,      

    "getstorenm": """

        SELECT CODE||':'||CODENAME AS 'storeNm'  
          FROM IDPS..MSTCOMMON 
         WHERE CDTYPE='C03' 
           AND USEYN ='Y' 
           AND DELYN ='0'
        AT ISOLATION 0

    """,      

    "newlottebyprod_dvsn_aftermerge": """



  SELECT TT1.SALEDATE                                                                                                     AS 'SALEDATE',           
         TT1.STORECD + ':' + (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C03' AND CODE = TT1.STORECD AND DELYN = '0') AS 'STORECD',
         (CASE WHEN TT1.PLUGB = '1' THEN '교보문고' ELSE '핫트랙스' END)                                                  AS 'PLUGB',              
         (CASE WHEN TT1.TAXGB = '0' THEN '과세'                                                                                                    
               WHEN TT1.TAXGB = '1' THEN '비과세'                                                                                                  
               WHEN TT1.TAXGB = '2' THEN '상품권' END)                                                                    AS 'TAXGB',              
       SUM((CASE WHEN TT1.COMPANY = '1' THEN TT1.QTY     ELSE 0           END))                                         AS 'KB_QTY',           
       SUM((CASE WHEN TT1.COMPANY = '1' THEN 0           ELSE TT1.QTY     END))                                         AS 'HT_QTY',           
       SUM(TT1.QTY)                                                                                                     AS 'TOT_QTY',          
       SUM((CASE WHEN TT1.COMPANY = '1' THEN TT1.SALEAMT ELSE 0           END))                                         AS 'KB_SALEAMT',       
       SUM((CASE WHEN TT1.COMPANY = '1' THEN 0           ELSE TT1.SALEAMT END))                                         AS 'HT_SALEAMT',       
       SUM(TT1.SALEAMT)                                                                                                 AS 'TOT_SALEAMT'       
    FROM (                                                                                                                                         
  		SELECT '일반매출'                                                        AS 'TYPE',                                                           
  		       a.SALEDATE                                                        AS 'SALEDATE',                                                       
  		       a.STORECD                                                         AS 'STORECD',                                                        
  		       a.POSNO                                                           AS 'POSNO',                                                          
       		   a.COMPANY                                                         AS 'COMPANY',                                                        
  		       a.TRANNO                                                          AS 'TRANNO',                                                         
  		       b.SEQ                                                             AS 'SEQ',                                                            
  		       (CASE WHEN substring(b.CMDTDVSNCD,1,2) <> '40' THEN '1' ELSE '2' END)                   AS 'PLUGB',                                                        
  		       (CASE WHEN b.TAXGB = '0' THEN '0' ELSE '1' END)                   AS 'TAXGB',                                                          
  		       (CASE WHEN a.TRANTYPE = '00' THEN b.QTY ELSE -b.QTY END)          AS 'QTY',                                                            
  		       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)  AS 'SALEAMT'                                                         
  		FROM IDPS..TRNSALHDR a, IDPS..TRNSALDTL b                                                                                                                 
  		WHERE a.SALEDATE = b.SALEDATE                                                                                                                 
  		  AND a.STORECD  = b.STORECD                                                                                                                  
  		  AND a.POSNO    = b.POSNO                                                                                                                    
  		  AND a.TRANNO   = b.TRANNO                                                                                                                   
  		  AND a.TRANTYPE IN ('00', '01')                                                                                                              
  		  AND a.TRANKIND NOT IN ('10', '11', '13', '14', '15', '12', '17')                                                                                             
  		  AND b.CANCELGB = '0'                                                                                                                        
  	     	    AND a.POSNO    NOT LIKE '__[49]_'                                                                                                    


  		  AND a.SALEDATE ='{saleDate}'     
        AND a.STORECD  ='{storeCd}'                                                                      

  		UNION ALL                                                                                                                                     
  		SELECT '선매출'                                                          AS 'TYPE',                                                           
  		       a.CLOSEDATE                                                       AS 'SALEDATE',                                                       
  		       a.STORECD                                                         AS 'STORECD',                                                        
  		       a.POSNO                                                           AS 'POSNO',                                                          
       		 a.COMPANY                                                         AS 'COMPANY',                                                        
  		       a.TRANNO                                                          AS 'TRANNO',                                                         
  		       b.SEQ                                                             AS 'SEQ',                                                            
  		       (CASE WHEN substring(b.CMDTDVSNCD,1,2) <> '40' THEN '1' ELSE '2' END)                   AS 'PLUGB',                                                          
  		       (CASE WHEN b.TAXGB = '0' THEN '0' ELSE '1' END)                   AS 'TAXGB',                                                          
  		       (CASE WHEN a.TRANTYPE = '00' THEN b.QTY ELSE -b.QTY END)          AS 'QTY',                                                            
  		       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)  AS 'SALEAMT'                                                         
  		FROM (select c.FNSHDATE AS CLOSEDATE, a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO, b.SEQ
                              , a.COMPANY, a.TRANTYPE, a.TRANKIND
                            , (case when a.STORECD IN ('013','023') then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end) SALCOMCD
                              , '1' SALEFLAG
                              , '0' PAYFLAG, b.PLUGB
                              , max(c.RESTYPE) RESTYPE
                          from IDPS..TRNRESERVATION c, IDPS..TRNSALHDR a (INDEX PK_TRNSALHDR), IDPS..TRNSALDTL b (INDEX PK_TRNSALDTL)
                        where c.SALEDATE = a.SALEDATE   and c.STORECD  = a.STORECD    and c.POSNO    = a.POSNO      and c.TRANNO   = a.TRANNO
                          and a.SALEDATE = b.SALEDATE   and a.STORECD  = b.STORECD    and a.POSNO    = b.POSNO      and a.TRANNO   = b.TRANNO
                            and a.TRANTYPE in ('00','01')
                          and a.TRANKIND = '15'
                          AND a.POSNO NOT LIKE '99%'
                          AND a.DELYN = '0'
                          and b.CANCELGB = '0'
                              and (c.FNSHDATE <> '' AND c.FNSHDATE IS NOT NULL)
                      group by c.SALEDATE, c.STORECD, c.POSNO, c.TRANNO
                              , a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO
                              , b.SALEDATE, b.STORECD, b.POSNO, b.TRANNO, b.SEQ
                              , a.COMPANY, a.TRANTYPE, a.TRANKIND, c.FNSHDATE, b.PLUGB
                            , (case when a.STORECD  IN ('013','023')  then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end)
                      ) a, IDPS..TRNSALDTL b                                                                                                           
  		WHERE a.SALEDATE  = b.SALEDATE                                                                                                                
  		  AND a.STORECD   = b.STORECD                                                                                                                 
  		  AND a.POSNO     = b.POSNO                                                                                                                   
  		  AND a.TRANNO    = b.TRANNO                                                                                                                  
  		  AND a.SEQ       = b.SEQ                                                                                                                     
  		  AND a.TRANTYPE  IN ('00', '01')                                                                                                             
  		  AND a.TRANKIND  = '15' AND a.SALEFLAG  = '1' AND a.PAYFLAG   = '0'                                                                          
  		  AND b.CANCELGB  = '0'                                                                                                                       
  	     	    AND a.POSNO    NOT LIKE '__[49]_'                                                                                                    


  		  AND a.CLOSEDATE ='{saleDate}'     
        AND a.STORECD  ='{storeCd}'                                                                   
    
  		UNION ALL                                                                                                                                     
  		SELECT '상품권'                                                          AS 'TYPE',                                                           
  		       a.SALEDATE                                                        AS 'SALEDATE',                                                       
  		       a.STORECD                                                         AS 'STORECD',                                                        
  		       a.POSNO                                                           AS 'POSNO',                                                          
       		 a.COMPANY                                                         AS 'COMPANY',                                                        
  		       a.TRANNO                                                          AS 'TRANNO',                                                         
  		       b.SEQ                                                             AS 'SEQ',                                                            
  		       (CASE WHEN substring(b.CMDTDVSNCD,1,2) <> '40' THEN '1' ELSE '2' END)                   AS 'PLUGB',                                                        
  		       '2'                                                               AS 'TAXGB',                                                          
  		       (CASE WHEN a.TRANTYPE = '00' THEN b.QTY ELSE -b.QTY END)          AS 'QTY',                                                            
  		       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)  AS 'SALEAMT'                                                         
  		FROM IDPS..TRNSALHDR a, IDPS..TRNSALDTL b                                                                                                                 
  		WHERE a.SALEDATE = b.SALEDATE                                                                                                                 
  		  AND a.STORECD  = b.STORECD                                                                                                                  
  		  AND a.POSNO    = b.POSNO                                                                                                                    
  		  AND a.TRANNO   = b.TRANNO                                                                                                                   
  		  AND a.TRANTYPE IN ('00', '01')                                                                                                              
  		  AND a.TRANKIND IN ('13', '14', '17')                                                                                                              
  		  AND b.CANCELGB = '0'                                                                                                                        
        AND a.POSNO    NOT LIKE '__[49]_'                                                                                                           

        AND a.SALEDATE ='{saleDate}'     
        AND a.STORECD  ='{storeCd}'                                                                   
  	   ) TT1                                                                                                                                        
    GROUP BY TT1.SALEDATE, TT1.STORECD, TT1.PLUGB, TT1.TAXGB, TT1.COMPANY                                                                            
    ORDER BY 1,2,3,4,5                                                                                                                             

        AT ISOLATION 0

    """,      


   "getcommissionbyprod_dvsn_aftermerge":"""
   
       SELECT TT1.SALEDATE                                                                                                     AS 'SALEDATE',          
              (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C03' AND CODE = TT1.STORECD AND DELYN = '0') AS 'STORECD',           
              (CASE WHEN TT1.PLUGB = '1' THEN '교보문고' ELSE '핫트랙스' END)                                                  AS 'PLUGB',             
              (CASE WHEN TT1.TAXGB = '0' THEN '과세'                                                                                                   
                    WHEN TT1.TAXGB = '1' THEN '비과세'                                                                                                 
                    WHEN TT1.TAXGB = '2' THEN '상품권' END)                                                                    AS 'TAXGB',             
            SUM((CASE WHEN TT1.COMPANY = '1' THEN TT1.QTY     ELSE 0           END))                                         AS 'KB_QTY',          
            SUM((CASE WHEN TT1.COMPANY = '1' THEN 0           ELSE TT1.QTY     END))                                         AS 'HT_QTY',          
            SUM(TT1.QTY)                                                                                                     AS 'TOT_QTY',         
            SUM((CASE WHEN TT1.COMPANY = '1' THEN TT1.SALEAMT ELSE 0           END))                                         AS 'KB_SALEAMT',      
            SUM((CASE WHEN TT1.COMPANY = '1' THEN 0           ELSE TT1.SALEAMT END))                                         AS 'HT_SALEAMT',      
            SUM(TT1.SALEAMT)                                                                                                 AS 'TOT_SALEAMT'      
         FROM (                                                                                                                                        
                   SELECT '일반매출'                                                        AS 'TYPE',                                                          
                          a.SALEDATE                                                        AS 'SALEDATE',                                                      
                          a.STORECD                                                         AS 'STORECD',                                                       
                          a.POSNO                                                           AS 'POSNO',                                                         
                           a.COMPANY                                                         AS 'COMPANY',                                                       
                          a.TRANNO                                                          AS 'TRANNO',                                                        
                          b.SEQ                                                             AS 'SEQ',                                                           
                          (CASE WHEN substring(b.CMDTDVSNCD,1,2) <> '40' THEN '1' ELSE '2' END)                   AS 'PLUGB', 
                          (CASE WHEN b.TAXGB = '0' THEN '0' ELSE '1' END)                   AS 'TAXGB',                                                         
                          (CASE WHEN a.TRANTYPE = '00' THEN b.QTY ELSE -b.QTY END)          AS 'QTY',                                                           
                          (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)  AS 'SALEAMT'                                                        
                   FROM IDPS..TRNSALHDR a, IDPS..TRNSALDTL b                                                                                                                
                   WHERE a.SALEDATE = b.SALEDATE                                                                                                                
                     AND a.STORECD  = b.STORECD                                                                                                                 
                     AND a.POSNO    = b.POSNO                                                                                                                   
                     AND a.TRANNO   = b.TRANNO                                                                                                                  
                     AND a.TRANTYPE IN ('00', '01')                                                                                                             
                     AND a.TRANKIND NOT IN ('10', '11', '13', '14', '15', '12', '17')                                                                                            
                     AND b.CANCELGB = '0'                                                                                                                       
              AND a.POSNO    NOT LIKE '__[49]_'                                                                                                          

                     AND a.SALEDATE >='{fromDate}'     
                     AND a.SALEDATE <='{toDate}'     
                     AND a.STORECD  ='{storeCd}'                                                                 


                   UNION ALL                                                                                                                                    
                    SELECT '선매출'                                                          AS 'TYPE',                                                          
                          a.CLOSEDATE                                                       AS 'SALEDATE',                                                      
                          a.STORECD                                                         AS 'STORECD',                                                       
                          a.POSNO                                                           AS 'POSNO',                                                         
                           a.COMPANY                                                         AS 'COMPANY',                                                       
                          a.TRANNO                                                          AS 'TRANNO',                                                        
                          b.SEQ                                                             AS 'SEQ',                                                           
                          (CASE WHEN substring(b.CMDTDVSNCD,1,2) <> '40' THEN '1' ELSE '2' END)                   AS 'PLUGB', 
                          (CASE WHEN b.TAXGB = '0' THEN '0' ELSE '1' END)                   AS 'TAXGB',                                                         
                          (CASE WHEN a.TRANTYPE = '00' THEN b.QTY ELSE -b.QTY END)          AS 'QTY',                                                           
                          (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)  AS 'SALEAMT'         
                      FROM (select c.FNSHDATE AS CLOSEDATE, a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO, b.SEQ
                              , a.COMPANY, a.TRANTYPE, a.TRANKIND
                            , (case when a.STORECD IN ('013','023') then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end) SALCOMCD
                              , '1' SALEFLAG
                              , '0' PAYFLAG, b.PLUGB
                              , max(c.RESTYPE) RESTYPE
                          from IDPS..TRNRESERVATION c, IDPS..TRNSALHDR a (INDEX PK_TRNSALHDR), IDPS..TRNSALDTL b (INDEX PK_TRNSALDTL)
                        where c.SALEDATE = a.SALEDATE   and c.STORECD  = a.STORECD    and c.POSNO    = a.POSNO      and c.TRANNO   = a.TRANNO
                          and a.SALEDATE = b.SALEDATE   and a.STORECD  = b.STORECD    and a.POSNO    = b.POSNO      and a.TRANNO   = b.TRANNO
                            and a.TRANTYPE in ('00','01')
                          and a.TRANKIND = '15'
                          AND a.POSNO NOT LIKE '99%'
                          AND a.DELYN = '0'
                          and b.CANCELGB = '0'
                              and (c.FNSHDATE <> '' AND c.FNSHDATE IS NOT NULL)
                      group by c.SALEDATE, c.STORECD, c.POSNO, c.TRANNO
                              , a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO
                              , b.SALEDATE, b.STORECD, b.POSNO, b.TRANNO, b.SEQ
                              , a.COMPANY, a.TRANTYPE, a.TRANKIND, c.FNSHDATE, b.PLUGB
                            , (case when a.STORECD  IN ('013','023')  then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end)
                      ) a, IDPS..TRNSALDTL b                                                                                                          
                                        WHERE a.SALEDATE  = b.SALEDATE                                                                                                               
                                          AND a.STORECD   = b.STORECD                                                                                                                
                                          AND a.POSNO     = b.POSNO                                                                                                                  
                                          AND a.TRANNO    = b.TRANNO                                                                                                                 
                                          AND a.SEQ       = b.SEQ                                                                                                                    
                                          AND a.TRANTYPE  IN ('00', '01')                                                                                                            
                                          AND a.TRANKIND  = '15' AND a.SALEFLAG  = '1' AND a.PAYFLAG   = '0'                                                                         
                                          AND b.CANCELGB  = '0'                                                                                                                      
                                    AND a.POSNO    NOT LIKE '__[49]_'                                                                                                          
                                                         
                     AND a.CLOSEDATE >='{fromDate}'     
                     AND a.CLOSEDATE <='{toDate}'     
                     AND a.STORECD  = '{storeCd}'                                                         


                   UNION ALL                                                                                                                                    
                   SELECT '상품권'                                                          AS 'TYPE',                                                          
                          a.SALEDATE                                                        AS 'SALEDATE',                                                      
                          a.STORECD                                                         AS 'STORECD',                                                       
                          a.POSNO                                                           AS 'POSNO',                                                         
                           a.COMPANY                                                         AS 'COMPANY',                                                       
                          a.TRANNO                                                          AS 'TRANNO',                                                        
                          b.SEQ                                                             AS 'SEQ',                                                           
                          (CASE WHEN substring(b.CMDTDVSNCD,1,2) <> '40' THEN '1' ELSE '2' END)                   AS 'PLUGB', 
                          '2'                                                               AS 'TAXGB',                                                         
                          (CASE WHEN a.TRANTYPE = '00' THEN b.QTY ELSE -b.QTY END)          AS 'QTY',                                                           
                          (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)  AS 'SALEAMT'                                                        
                   FROM IDPS..TRNSALHDR a, IDPS..TRNSALDTL b                                                                                                                
                   WHERE a.SALEDATE = b.SALEDATE                                                                                                                
                     AND a.STORECD  = b.STORECD                                                                                                                 
                     AND a.POSNO    = b.POSNO                                                                                                                   
                     AND a.TRANNO   = b.TRANNO                                                                                                                  
                     AND a.TRANTYPE IN ('00', '01')                                                                                                             
                     AND a.TRANKIND IN ('13', '14', '17')                                                                                                             
                     AND b.CANCELGB = '0'                                                                                                                       
              AND a.POSNO    NOT LIKE '__[49]_'                                                                                                          

                     AND a.SALEDATE >='{fromDate}'     
                     AND a.SALEDATE <='{toDate}'     
                     AND a.STORECD  ='{storeCd}'       
               ) TT1                                                                                                                                       
         GROUP BY TT1.SALEDATE, TT1.STORECD, TT1.PLUGB, TT1.TAXGB, TT1.COMPANY                                                                           
         ORDER BY 1,2,3,4,5                                                                                                                            
                       
   """,    


    "newlottebyprod_dvsn_aftermerge": """



  SELECT TT1.SALEDATE                                                                                                     AS 'SALEDATE',           
         TT1.STORECD + ':' + (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C03' AND CODE = TT1.STORECD AND DELYN = '0') AS 'STORECD',
         (CASE WHEN TT1.PLUGB = '1' THEN '교보문고' ELSE '핫트랙스' END)                                                  AS 'PLUGB',              
         (CASE WHEN TT1.TAXGB = '0' THEN '과세'                                                                                                    
               WHEN TT1.TAXGB = '1' THEN '비과세'                                                                                                  
               WHEN TT1.TAXGB = '2' THEN '상품권' END)                                                                    AS 'TAXGB',              
       SUM((CASE WHEN TT1.COMPANY = '1' THEN TT1.SALEAMT ELSE 0           END))                                         AS 'KB_SALEAMT',       
       SUM((CASE WHEN TT1.COMPANY = '1' THEN 0           ELSE TT1.SALEAMT END))                                         AS 'HT_SALEAMT',       
       SUM(TT1.SALEAMT)                                                                                                 AS 'TOT_SALEAMT'       
    FROM (                                                                                                                                         
  		SELECT '일반매출'                                                        AS 'TYPE',                                                           
  		       a.SALEDATE                                                        AS 'SALEDATE',                                                       
  		       a.STORECD                                                         AS 'STORECD',                                                        
  		       a.POSNO                                                           AS 'POSNO',                                                          
       		   a.COMPANY                                                         AS 'COMPANY',                                                        
  		       a.TRANNO                                                          AS 'TRANNO',                                                         
  		       b.SEQ                                                             AS 'SEQ',                                                            
  		       (CASE WHEN substring(b.CMDTDVSNCD,1,2) <> '40' THEN '1' ELSE '2' END)                   AS 'PLUGB',                                                        
  		       (CASE WHEN b.TAXGB = '0' THEN '0' ELSE '1' END)                   AS 'TAXGB',                                                          
  		       (CASE WHEN a.TRANTYPE = '00' THEN b.QTY ELSE -b.QTY END)          AS 'QTY',                                                            
  		       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)  AS 'SALEAMT'                                                         
  		FROM IDPS..TRNSALHDR a, IDPS..TRNSALDTL b                                                                                                                 
  		WHERE a.SALEDATE = b.SALEDATE                                                                                                                 
  		  AND a.STORECD  = b.STORECD                                                                                                                  
  		  AND a.POSNO    = b.POSNO                                                                                                                    
  		  AND a.TRANNO   = b.TRANNO                                                                                                                   
  		  AND a.TRANTYPE IN ('00', '01')                                                                                                              
  		  AND a.TRANKIND NOT IN ('10', '11', '13', '14', '15', '12', '17')                                                                                             
  		  AND b.CANCELGB = '0'                                                                                                                        
  	     	    AND a.POSNO    NOT LIKE '__[49]_'                                                                                                    


  		  AND a.SALEDATE ='{saleDate}'     
        AND a.STORECD  ='{storeCd}'                                                                      

  		UNION ALL                                                                                                                                     
  		SELECT '선매출'                                                          AS 'TYPE',                                                           
  		       a.CLOSEDATE                                                       AS 'SALEDATE',                                                       
  		       a.STORECD                                                         AS 'STORECD',                                                        
  		       a.POSNO                                                           AS 'POSNO',                                                          
       		 a.COMPANY                                                         AS 'COMPANY',                                                        
  		       a.TRANNO                                                          AS 'TRANNO',                                                         
  		       b.SEQ                                                             AS 'SEQ',                                                            
  		       (CASE WHEN substring(b.CMDTDVSNCD,1,2) <> '40' THEN '1' ELSE '2' END)                   AS 'PLUGB',                                                          
  		       (CASE WHEN b.TAXGB = '0' THEN '0' ELSE '1' END)                   AS 'TAXGB',                                                          
  		       (CASE WHEN a.TRANTYPE = '00' THEN b.QTY ELSE -b.QTY END)          AS 'QTY',                                                            
  		       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)  AS 'SALEAMT'                                                         
  		FROM (select c.FNSHDATE AS CLOSEDATE, a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO, b.SEQ
                              , a.COMPANY, a.TRANTYPE, a.TRANKIND
                            , (case when a.STORECD IN ('013','023') then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end) SALCOMCD
                              , '1' SALEFLAG
                              , '0' PAYFLAG, b.PLUGB
                              , max(c.RESTYPE) RESTYPE
                          from IDPS..TRNRESERVATION c, IDPS..TRNSALHDR a (INDEX PK_TRNSALHDR), IDPS..TRNSALDTL b (INDEX PK_TRNSALDTL)
                        where c.SALEDATE = a.SALEDATE   and c.STORECD  = a.STORECD    and c.POSNO    = a.POSNO      and c.TRANNO   = a.TRANNO
                          and a.SALEDATE = b.SALEDATE   and a.STORECD  = b.STORECD    and a.POSNO    = b.POSNO      and a.TRANNO   = b.TRANNO
                            and a.TRANTYPE in ('00','01')
                          and a.TRANKIND = '15'
                          AND a.POSNO NOT LIKE '99%'
                          AND a.DELYN = '0'
                          and b.CANCELGB = '0'
                              and (c.FNSHDATE <> '' AND c.FNSHDATE IS NOT NULL)
                      group by c.SALEDATE, c.STORECD, c.POSNO, c.TRANNO
                              , a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO
                              , b.SALEDATE, b.STORECD, b.POSNO, b.TRANNO, b.SEQ
                              , a.COMPANY, a.TRANTYPE, a.TRANKIND, c.FNSHDATE, b.PLUGB
                            , (case when a.STORECD  IN ('013','023')  then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end)
                      ) a, IDPS..TRNSALDTL b                                                                                                           
  		WHERE a.SALEDATE  = b.SALEDATE                                                                                                                
  		  AND a.STORECD   = b.STORECD                                                                                                                 
  		  AND a.POSNO     = b.POSNO                                                                                                                   
  		  AND a.TRANNO    = b.TRANNO                                                                                                                  
  		  AND a.SEQ       = b.SEQ                                                                                                                     
  		  AND a.TRANTYPE  IN ('00', '01')                                                                                                             
  		  AND a.TRANKIND  = '15' AND a.SALEFLAG  = '1' AND a.PAYFLAG   = '0'                                                                          
  		  AND b.CANCELGB  = '0'                                                                                                                       
  	     	    AND a.POSNO    NOT LIKE '__[49]_'                                                                                                    


  		  AND a.CLOSEDATE ='{saleDate}'     
        AND a.STORECD  ='{storeCd}'                                                                   
    
  		UNION ALL                                                                                                                                     
  		SELECT '상품권'                                                          AS 'TYPE',                                                           
  		       a.SALEDATE                                                        AS 'SALEDATE',                                                       
  		       a.STORECD                                                         AS 'STORECD',                                                        
  		       a.POSNO                                                           AS 'POSNO',                                                          
       		 a.COMPANY                                                         AS 'COMPANY',                                                        
  		       a.TRANNO                                                          AS 'TRANNO',                                                         
  		       b.SEQ                                                             AS 'SEQ',                                                            
  		       (CASE WHEN substring(b.CMDTDVSNCD,1,2) <> '40' THEN '1' ELSE '2' END)                   AS 'PLUGB',                                                        
  		       '2'                                                               AS 'TAXGB',                                                          
  		       (CASE WHEN a.TRANTYPE = '00' THEN b.QTY ELSE -b.QTY END)          AS 'QTY',                                                            
  		       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)  AS 'SALEAMT'                                                         
  		FROM IDPS..TRNSALHDR a, IDPS..TRNSALDTL b                                                                                                                 
  		WHERE a.SALEDATE = b.SALEDATE                                                                                                                 
  		  AND a.STORECD  = b.STORECD                                                                                                                  
  		  AND a.POSNO    = b.POSNO                                                                                                                    
  		  AND a.TRANNO   = b.TRANNO                                                                                                                   
  		  AND a.TRANTYPE IN ('00', '01')                                                                                                              
  		  AND a.TRANKIND IN ('13', '14', '17')                                                                                                              
  		  AND b.CANCELGB = '0'                                                                                                                        
        AND a.POSNO    NOT LIKE '__[49]_'                                                                                                           

        AND a.SALEDATE ='{saleDate}'     
        AND a.STORECD  ='{storeCd}'                                                                   
  	   ) TT1                                                                                                                                        
    GROUP BY TT1.SALEDATE, TT1.STORECD, TT1.PLUGB, TT1.TAXGB, TT1.COMPANY                                                                            
    ORDER BY 1,2,3,4,5                                                                                                                             

        AT ISOLATION 0

    """,      


   "newlotte_tender_aftermerge":"""

        SELECT TT1.SALEDATE                                                                                                             AS 'SALEDATE',                                                                    
               TT1.STORECD + ':' + (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C03' AND CODE = TT1.STORECD AND DELYN = '0')         AS 'STORECD',                                                                         
               (CASE WHEN TT1.PLUGB = '1' THEN '교보문고' WHEN TT1.PLUGB = '2' THEN '핫트랙스' WHEN TT1.PLUGB = '3' THEN '문고상품권' WHEN TT1.PLUGB = '4' THEN '핫트랙스상품권' ELSE '' END) AS 'PLUGB',            
               TT1.TENDERTYPE2 + ':' + (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E16' AND CODE = TT1.TENDERTYPE2 AND DELYN = '0') AS 'TENDERTYPE2',                                                                 
               SUM((CASE WHEN TT1.COMPANY = '1' THEN TT1.SALEAMT ELSE 0           END))                                                 AS 'KB_SALEAMT',                              
               SUM((CASE WHEN TT1.COMPANY = '1' THEN 0           ELSE TT1.SALEAMT END))                                                 AS 'HT_SALEAMT',                              
               SUM(TT1.SALEAMT)                                                                                                         AS 'TOT_SALEAMT'                              
          FROM (                                                                                                                                                          		                                             
                SELECT '일반매출금권(상품권판매/상품권지불/선결제 제외)'                                                       AS 'TYPE',                                      		                                       
                       a.SALEDATE                                                                                              AS 'SALEDATE',                                  		                                       
                       a.STORECD                                                                                               AS 'STORECD',                                   		                                       
                       a.POSNO                                                                                                 AS 'POSNO',                                     		                                       
                       a.COMPANY                                                                                               AS 'COMPANY',                                     		                                       
                       a.TRANNO                                                                                                AS 'TRANNO',                                    		                                       
                       (CASE WHEN b.SALEAMTM <> 0 THEN '1' ELSE '2' END)                                                       AS 'PLUGB',                                     		                                       
                       b.TENDERTYPE                                                                                            AS 'TENDERTYPE',                                		                                       
                       (SELECT IsNull(ETC1NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E15' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2',                               		                                       
                       (SELECT IsNull(ETC2NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E15' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2NM',                             		                                       
                       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)                                        AS 'SALEAMT'                                    		                                       
                  FROM IDPS..TRNSALHDR a, IDPS..TRNSALPAY b                                                                                                                                  		                                     
                 WHERE a.SALEDATE   = b.SALEDATE                                                                                                                                		                                       
                   AND a.STORECD    = b.STORECD                                                                                                                                 		                                       
                   AND a.POSNO      = b.POSNO                                                                                                                                   		                                       
                   AND a.TRANNO     = b.TRANNO                                                                                                                                  		                                       
                   AND a.TRANTYPE   IN ('00', '01')                                                                                                                             		                                       
                   AND a.TRANKIND   NOT IN ('10','11','13','14','15', '12', '17')                                                                                                                         		                         
                   AND b.TENDERTYPE NOT IN ('03', '12')                                                                                                                             		                                   
  	     	    AND a.POSNO    NOT LIKE '__[49]_'                                                                                                    


  		  AND a.SALEDATE  ='{saleDate}'     
          AND a.STORECD  = '{storeCd}'  

                UNION ALL                                                                                                                                                                                                 
                SELECT '상품권판매매출금권(상품권지불/선결제 제외)'                                                            AS 'TYPE',                                      		                                       
                       a.SALEDATE                                                                                              AS 'SALEDATE',                                  		                                       
                       a.STORECD                                                                                               AS 'STORECD',                                   		                                       
                       a.POSNO                                                                                                 AS 'POSNO',                                     		                                       
                       a.COMPANY                                                                                               AS 'COMPANY',                                     		                                       
                       a.TRANNO                                                                                                AS 'TRANNO',                                    		                                       
                       (CASE WHEN b.SALEAMTM <> 0 THEN '3' ELSE '4' END)                                                       AS 'PLUGB',                                     		                                       
                       b.TENDERTYPE                                                                                            AS 'TENDERTYPE',                                		                                       
                       (SELECT IsNull(ETC1NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E15' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2',                               		                                       
                       (SELECT IsNull(ETC2NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E15' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2NM',                             		                                       
                       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)                                        AS 'SALEAMT'                                    		                                       
                  FROM IDPS..TRNSALHDR a, IDPS..TRNSALPAY b                                                                                                                                  		                                     
                 WHERE a.SALEDATE   = b.SALEDATE                                                                                                                                		                                       
                   AND a.STORECD    = b.STORECD                                                                                                                                 		                                       
                   AND a.POSNO      = b.POSNO                                                                                                                                   		                                       
                   AND a.TRANNO     = b.TRANNO                                                                                                                                  		                                       
                   AND a.TRANTYPE   IN ('00', '01')                                                                                                                             		                                       
                   AND a.TRANKIND   IN ('13','14', '17')                                                                                                                         		                                           
                   AND b.TENDERTYPE NOT IN ('03', '12')                                                                                                                             		                                   
  	     	    AND a.POSNO    NOT LIKE '__[49]_'                                                                                                    


  		  AND a.SALEDATE  = '{saleDate}'     
          AND a.STORECD  = '{storeCd}'  

                UNION ALL                                                                                                                                                                                                 
                SELECT '선결제완료매출금권(상품권지불제외)'                                                                    AS 'TYPE',                                      		                                       
                       a.CLOSEDATE                                                                                             AS 'SALEDATE',                                  		                                       
                       a.STORECD                                                                                               AS 'STORECD',                                   		                                       
                       a.POSNO                                                                                                 AS 'POSNO',                                     		                                       
                       a.COMPANY                                                                                               AS 'COMPANY',                                     		                                       
                       a.TRANNO                                                                                                AS 'TRANNO',                                    		                                       
                       (CASE WHEN b.SALEAMTM <> 0 THEN '1' ELSE '2' END)                                                       AS 'PLUGB',                                     		                                       
                       b.TENDERTYPE                                                                                            AS 'TENDERTYPE',                                		                                       
                       (SELECT IsNull(ETC1NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E15' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2',                               		                                       
                       (SELECT IsNull(ETC2NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E15' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2NM',                             		                                       
                       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)                                        AS 'SALEAMT'                                    		                                       
                  FROM (select c.FNSHDATE AS CLOSEDATE, a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO, b.SEQ
                              , a.COMPANY, a.TRANTYPE, a.TRANKIND
                            , (case when a.STORECD IN ('013','023') then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end) SALCOMCD
                              , '1' SALEFLAG
                              , '0' PAYFLAG, b.PLUGB
                              , max(c.RESTYPE) RESTYPE
                          from IDPS..TRNRESERVATION c, IDPS..TRNSALHDR a (INDEX PK_TRNSALHDR), IDPS..TRNSALDTL b (INDEX PK_TRNSALDTL)
                        where c.SALEDATE = a.SALEDATE   and c.STORECD  = a.STORECD    and c.POSNO    = a.POSNO      and c.TRANNO   = a.TRANNO
                          and a.SALEDATE = b.SALEDATE   and a.STORECD  = b.STORECD    and a.POSNO    = b.POSNO      and a.TRANNO   = b.TRANNO
                            and a.TRANTYPE in ('00','01')
                          and a.TRANKIND = '15'
                          AND a.POSNO NOT LIKE '99%'
                          AND a.DELYN = '0'
                          and b.CANCELGB = '0'
                              and (c.FNSHDATE <> '' AND c.FNSHDATE IS NOT NULL)
                      group by c.SALEDATE, c.STORECD, c.POSNO, c.TRANNO
                              , a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO
                              , b.SALEDATE, b.STORECD, b.POSNO, b.TRANNO, b.SEQ
                              , a.COMPANY, a.TRANTYPE, a.TRANKIND, c.FNSHDATE, b.PLUGB
                            , (case when a.STORECD  IN ('013','023')  then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end)
                      ) a, IDPS..TRNSALPAY b                                                                                                                                  		                               
                 WHERE a.SALEDATE   = b.SALEDATE                                                                                                                                		                                       
                   AND a.STORECD    = b.STORECD                                                                                                                                 		                                       
                   AND a.POSNO      = b.POSNO                                                                                                                                   		                                       
                   AND a.TRANNO     = b.TRANNO                                                                                                                                                                            
                   AND a.SEQ        = 1                                                                                                                                 		                                               
                   AND a.TRANTYPE   IN ('00', '01')                                                                                                                             		                                       
                   AND a.TRANKIND  = '15' AND a.SALEFLAG  = '1' AND a.PAYFLAG   = '0'                                                                                                                         		         
                   AND b.TENDERTYPE NOT IN ('03', '12')                                                                                                                             		                                   
  	     	    AND a.POSNO    NOT LIKE '__[49]_'                                                                                                    


  		  AND a.CLOSEDATE  = '{saleDate}'     
          AND a.STORECD  = '{storeCd}'  

                UNION ALL                                                                                                                                                                                                 
                SELECT '상품권지불금권(일반상품판매, 선결제, 상품권판매 제외)'                                                 AS 'TYPE',                                      		                                       
                       a.SALEDATE                                                                                              AS 'SALEDATE',                                  		                                       
                       a.STORECD                                                                                               AS 'STORECD',                                   		                                       
                       a.POSNO                                                                                                 AS 'POSNO',                                     		                                       
                       a.COMPANY                                                                                               AS 'COMPANY',                                     		                                       
                       a.TRANNO                                                                                                AS 'TRANNO',                                    		                                       
                       (CASE WHEN b.SALEAMTM <> 0 THEN '1' ELSE '2' END)                                                       AS 'PLUGB',                                     		                                       
                       '03'                                                                                                    AS 'TENDERTYPE',                                		                                       
                       (SELECT IsNull(ETC1NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E14' AND CODE = b.GVTYPE AND DELYN = '0')     AS 'TENDERTYPE2',                               		                                       
                       (SELECT IsNull(ETC2NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E14' AND CODE = b.GVTYPE AND DELYN = '0')     AS 'TENDERTYPE2NM',                             		                                       
                       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)                                        AS 'SALEAMT'                                    		                                       
                  FROM IDPS..TRNSALHDR a, IDPS..TRNGVPAY b                                                                                                                                  		                                       
                 WHERE a.SALEDATE   = b.SALEDATE                                                                                                                                		                                       
                   AND a.STORECD    = b.STORECD                                                                                                                                 		                                       
                   AND a.POSNO      = b.POSNO                                                                                                                                   		                                       
                   AND a.TRANNO     = b.TRANNO                                                                                                                                  		                                       
                   AND a.TRANTYPE   IN ('00', '01')                                                                                                                             		                                       
                   AND a.TRANKIND   NOT IN ('10', '11','13','14', '15', '12', '17')                                                                                                                        		                       
  	     	    AND a.POSNO    NOT LIKE '__[49]_'                                                                                                    


  		  AND a.SALEDATE = '{saleDate}'     
          AND a.STORECD  = '{storeCd}'  


                UNION ALL                                                                                                                                                                                                 
                SELECT '상품권지불금권(선결제완료매출금권)'                                                                    AS 'TYPE',                                      		                                       
                       a.CLOSEDATE                                                                                             AS 'SALEDATE',                                  		                                       
                       a.STORECD                                                                                               AS 'STORECD',                                   		                                       
                       a.POSNO                                                                                                 AS 'POSNO',                                     		                                       
                       a.COMPANY                                                                                               AS 'COMPANY',                                     		                                       
                       a.TRANNO                                                                                                AS 'TRANNO',                                    		                                       
                       (CASE WHEN b.SALEAMTM <> 0 THEN '1' ELSE '2' END)                                                       AS 'PLUGB',                                     		                                       
                       '03'                                                                                                    AS 'TENDERTYPE',                                		                                       
                       (SELECT IsNull(ETC1NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E14' AND CODE = b.GVTYPE AND DELYN = '0')     AS 'TENDERTYPE2',                               		                                       
                       (SELECT IsNull(ETC2NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E14' AND CODE = b.GVTYPE AND DELYN = '0')     AS 'TENDERTYPE2NM',                             		                                       
                       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)                                        AS 'SALEAMT'                                    		                                       
                  FROM (select c.FNSHDATE AS CLOSEDATE, a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO, b.SEQ
                              , a.COMPANY, a.TRANTYPE, a.TRANKIND
                            , (case when a.STORECD IN ('013','023') then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end) SALCOMCD
                              , '1' SALEFLAG
                              , '0' PAYFLAG, b.PLUGB
                              , max(c.RESTYPE) RESTYPE
                          from IDPS..TRNRESERVATION c, IDPS..TRNSALHDR a (INDEX PK_TRNSALHDR), IDPS..TRNSALDTL b (INDEX PK_TRNSALDTL)
                        where c.SALEDATE = a.SALEDATE   and c.STORECD  = a.STORECD    and c.POSNO    = a.POSNO      and c.TRANNO   = a.TRANNO
                          and a.SALEDATE = b.SALEDATE   and a.STORECD  = b.STORECD    and a.POSNO    = b.POSNO      and a.TRANNO   = b.TRANNO
                            and a.TRANTYPE in ('00','01')
                          and a.TRANKIND = '15'
                          AND a.POSNO NOT LIKE '99%'
                          AND a.DELYN = '0'
                          and b.CANCELGB = '0'
                              and (c.FNSHDATE <> '' AND c.FNSHDATE IS NOT NULL)
                      group by c.SALEDATE, c.STORECD, c.POSNO, c.TRANNO
                              , a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO
                              , b.SALEDATE, b.STORECD, b.POSNO, b.TRANNO, b.SEQ
                              , a.COMPANY, a.TRANTYPE, a.TRANKIND, c.FNSHDATE, b.PLUGB
                            , (case when a.STORECD  IN ('013','023')  then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end)
                      ) a, IDPS..TRNGVPAY b                                                                                                                                		                                   
                 WHERE a.SALEDATE   = b.SALEDATE                                                                                                                                		                                       
                   AND a.STORECD    = b.STORECD                                                                                                                                 		                                       
                   AND a.POSNO      = b.POSNO                                                                                                                                   		                                       
                   AND a.TRANNO     = b.TRANNO                                                                                                                                                                            
                   AND a.SEQ        = 1                                                                                                                                                                                   
                   AND a.TRANTYPE   IN ('00', '01')                                                                                                                             		                                       
                   AND a.TRANKIND  = '15' AND a.SALEFLAG  = '1' AND a.PAYFLAG   = '0'                                                                                                                         		         
  	     	    AND a.POSNO    NOT LIKE '__[49]_'                                                                                                    


  		  AND a.CLOSEDATE ='{saleDate}'     
          AND a.STORECD  = '{storeCd}'  


        ) TT1                                                                                                                                                                                                             
        GROUP BY TT1.SALEDATE, TT1.STORECD, TT1.PLUGB, TT1.TENDERTYPE2, TT1.COMPANY                                                                                                                                         
        ORDER BY 1,2,3,4,5                                                                                                                             

   """,    



   "newlotte_tender_aftermergefromto":"""

        SELECT TT1.SALEDATE                                                                                                             AS 'SALEDATE',                                                                    
               TT1.STORECD + ':' + (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C03' AND CODE = TT1.STORECD AND DELYN = '0')         AS 'STORECD',                                                                         
               (CASE WHEN TT1.PLUGB = '1' THEN '교보문고' WHEN TT1.PLUGB = '2' THEN '핫트랙스' WHEN TT1.PLUGB = '3' THEN '문고상품권' WHEN TT1.PLUGB = '4' THEN '핫트랙스상품권' ELSE '' END) AS 'PLUGB',            
               TT1.TENDERTYPE2 + ':' + (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E16' AND CODE = TT1.TENDERTYPE2 AND DELYN = '0') AS 'TENDERTYPE2',                                                                 
               SUM((CASE WHEN TT1.COMPANY = '1' THEN TT1.SALEAMT ELSE 0           END))                                                 AS 'KB_SALEAMT',                              
               SUM((CASE WHEN TT1.COMPANY = '1' THEN 0           ELSE TT1.SALEAMT END))                                                 AS 'HT_SALEAMT',                              
               SUM(TT1.SALEAMT)                                                                                                         AS 'TOT_SALEAMT'                              
          FROM (                                                                                                                                                          		                                             
                SELECT '일반매출금권(상품권판매/상품권지불/선결제 제외)'                                                       AS 'TYPE',                                      		                                       
                       a.SALEDATE                                                                                              AS 'SALEDATE',                                  		                                       
                       a.STORECD                                                                                               AS 'STORECD',                                   		                                       
                       a.POSNO                                                                                                 AS 'POSNO',                                     		                                       
                       a.COMPANY                                                                                               AS 'COMPANY',                                     		                                       
                       a.TRANNO                                                                                                AS 'TRANNO',                                    		                                       
                       (CASE WHEN b.SALEAMTM <> 0 THEN '1' ELSE '2' END)                                                       AS 'PLUGB',                                     		                                       
                       b.TENDERTYPE                                                                                            AS 'TENDERTYPE',                                		                                       
                       (SELECT IsNull(ETC1NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E15' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2',                               		                                       
                       (SELECT IsNull(ETC2NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E15' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2NM',                             		                                       
                       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)                                        AS 'SALEAMT'                                    		                                       
                  FROM IDPS..TRNSALHDR a, IDPS..TRNSALPAY b                                                                                                                                  		                                     
                 WHERE a.SALEDATE   = b.SALEDATE                                                                                                                                		                                       
                   AND a.STORECD    = b.STORECD                                                                                                                                 		                                       
                   AND a.POSNO      = b.POSNO                                                                                                                                   		                                       
                   AND a.TRANNO     = b.TRANNO                                                                                                                                  		                                       
                   AND a.TRANTYPE   IN ('00', '01')                                                                                                                             		                                       
                   AND a.TRANKIND   NOT IN ('10','11','13','14','15', '12', '17')                                                                                                                         		                         
                   AND b.TENDERTYPE NOT IN ('03', '12')                                                                                                                             		                                   
  	     	    AND a.POSNO    NOT LIKE '__[49]_'                                                                                                    


  		  AND a.SALEDATE  >='{fromDate}'     
            AND a.SALEDATE  <='{toDate}'     
          AND a.STORECD  = '{storeCd}'  

                UNION ALL                                                                                                                                                                                                 
                SELECT '상품권판매매출금권(상품권지불/선결제 제외)'                                                            AS 'TYPE',                                      		                                       
                       a.SALEDATE                                                                                              AS 'SALEDATE',                                  		                                       
                       a.STORECD                                                                                               AS 'STORECD',                                   		                                       
                       a.POSNO                                                                                                 AS 'POSNO',                                     		                                       
                       a.COMPANY                                                                                               AS 'COMPANY',                                     		                                       
                       a.TRANNO                                                                                                AS 'TRANNO',                                    		                                       
                       (CASE WHEN b.SALEAMTM <> 0 THEN '3' ELSE '4' END)                                                       AS 'PLUGB',                                     		                                       
                       b.TENDERTYPE                                                                                            AS 'TENDERTYPE',                                		                                       
                       (SELECT IsNull(ETC1NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E15' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2',                               		                                       
                       (SELECT IsNull(ETC2NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E15' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2NM',                             		                                       
                       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)                                        AS 'SALEAMT'                                    		                                       
                  FROM IDPS..TRNSALHDR a, IDPS..TRNSALPAY b                                                                                                                                  		                                     
                 WHERE a.SALEDATE   = b.SALEDATE                                                                                                                                		                                       
                   AND a.STORECD    = b.STORECD                                                                                                                                 		                                       
                   AND a.POSNO      = b.POSNO                                                                                                                                   		                                       
                   AND a.TRANNO     = b.TRANNO                                                                                                                                  		                                       
                   AND a.TRANTYPE   IN ('00', '01')                                                                                                                             		                                       
                   AND a.TRANKIND   IN ('13','14', '17')                                                                                                                         		                                           
                   AND b.TENDERTYPE NOT IN ('03', '12')                                                                                                                             		                                   
  	     	    AND a.POSNO    NOT LIKE '__[49]_'                                                                                                    


  		  AND a.SALEDATE  >='{fromDate}'     
            AND a.SALEDATE  <='{toDate}'     
          AND a.STORECD  = '{storeCd}'  

                UNION ALL                                                                                                                                                                                                 
                SELECT '선결제완료매출금권(상품권지불제외)'                                                                    AS 'TYPE',                                      		                                       
                       a.CLOSEDATE                                                                                             AS 'SALEDATE',                                  		                                       
                       a.STORECD                                                                                               AS 'STORECD',                                   		                                       
                       a.POSNO                                                                                                 AS 'POSNO',                                     		                                       
                       a.COMPANY                                                                                               AS 'COMPANY',                                     		                                       
                       a.TRANNO                                                                                                AS 'TRANNO',                                    		                                       
                       (CASE WHEN b.SALEAMTM <> 0 THEN '1' ELSE '2' END)                                                       AS 'PLUGB',                                     		                                       
                       b.TENDERTYPE                                                                                            AS 'TENDERTYPE',                                		                                       
                       (SELECT IsNull(ETC1NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E15' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2',                               		                                       
                       (SELECT IsNull(ETC2NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E15' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2NM',                             		                                       
                       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)                                        AS 'SALEAMT'                                    		                                       
                  FROM (select c.FNSHDATE AS CLOSEDATE, a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO, b.SEQ
                              , a.COMPANY, a.TRANTYPE, a.TRANKIND
                            , (case when a.STORECD IN ('013','023') then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end) SALCOMCD
                              , '1' SALEFLAG
                              , '0' PAYFLAG, b.PLUGB
                              , max(c.RESTYPE) RESTYPE
                          from IDPS..TRNRESERVATION c, IDPS..TRNSALHDR a (INDEX PK_TRNSALHDR), IDPS..TRNSALDTL b (INDEX PK_TRNSALDTL)
                        where c.SALEDATE = a.SALEDATE   and c.STORECD  = a.STORECD    and c.POSNO    = a.POSNO      and c.TRANNO   = a.TRANNO
                          and a.SALEDATE = b.SALEDATE   and a.STORECD  = b.STORECD    and a.POSNO    = b.POSNO      and a.TRANNO   = b.TRANNO
                            and a.TRANTYPE in ('00','01')
                          and a.TRANKIND = '15'
                          AND a.POSNO NOT LIKE '99%'
                          AND a.DELYN = '0'
                          and b.CANCELGB = '0'
                              and (c.FNSHDATE <> '' AND c.FNSHDATE IS NOT NULL)
                      group by c.SALEDATE, c.STORECD, c.POSNO, c.TRANNO
                              , a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO
                              , b.SALEDATE, b.STORECD, b.POSNO, b.TRANNO, b.SEQ
                              , a.COMPANY, a.TRANTYPE, a.TRANKIND, c.FNSHDATE, b.PLUGB
                            , (case when a.STORECD  IN ('013','023')  then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end)
                      ) a, IDPS..TRNSALPAY b                                                                                                                                  		                               
                 WHERE a.SALEDATE   = b.SALEDATE                                                                                                                                		                                       
                   AND a.STORECD    = b.STORECD                                                                                                                                 		                                       
                   AND a.POSNO      = b.POSNO                                                                                                                                   		                                       
                   AND a.TRANNO     = b.TRANNO                                                                                                                                                                            
                   AND a.SEQ        = 1                                                                                                                                 		                                               
                   AND a.TRANTYPE   IN ('00', '01')                                                                                                                             		                                       
                   AND a.TRANKIND  = '15' AND a.SALEFLAG  = '1' AND a.PAYFLAG   = '0'                                                                                                                         		         
                   AND b.TENDERTYPE NOT IN ('03', '12')                                                                                                                             		                                   
  	     	    AND a.POSNO    NOT LIKE '__[49]_'                                                                                                    


  		  AND a.CLOSEDATE  >='{fromDate}'     
            AND a.CLOSEDATE  <='{toDate}'    
          AND a.STORECD  = '{storeCd}'  

                UNION ALL                                                                                                                                                                                                 
                SELECT '상품권지불금권(일반상품판매, 선결제, 상품권판매 제외)'                                                 AS 'TYPE',                                      		                                       
                       a.SALEDATE                                                                                              AS 'SALEDATE',                                  		                                       
                       a.STORECD                                                                                               AS 'STORECD',                                   		                                       
                       a.POSNO                                                                                                 AS 'POSNO',                                     		                                       
                       a.COMPANY                                                                                               AS 'COMPANY',                                     		                                       
                       a.TRANNO                                                                                                AS 'TRANNO',                                    		                                       
                       (CASE WHEN b.SALEAMTM <> 0 THEN '1' ELSE '2' END)                                                       AS 'PLUGB',                                     		                                       
                       '03'                                                                                                    AS 'TENDERTYPE',                                		                                       
                       (SELECT IsNull(ETC1NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E14' AND CODE = b.GVTYPE AND DELYN = '0')     AS 'TENDERTYPE2',                               		                                       
                       (SELECT IsNull(ETC2NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E14' AND CODE = b.GVTYPE AND DELYN = '0')     AS 'TENDERTYPE2NM',                             		                                       
                       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)                                        AS 'SALEAMT'                                    		                                       
                  FROM IDPS..TRNSALHDR a, IDPS..TRNGVPAY b                                                                                                                                  		                                       
                 WHERE a.SALEDATE   = b.SALEDATE                                                                                                                                		                                       
                   AND a.STORECD    = b.STORECD                                                                                                                                 		                                       
                   AND a.POSNO      = b.POSNO                                                                                                                                   		                                       
                   AND a.TRANNO     = b.TRANNO                                                                                                                                  		                                       
                   AND a.TRANTYPE   IN ('00', '01')                                                                                                                             		                                       
                   AND a.TRANKIND   NOT IN ('10', '11','13','14', '15', '12', '17')                                                                                                                        		                       
  	     	    AND a.POSNO    NOT LIKE '__[49]_'                                                                                                    


  		  AND a.SALEDATE  >='{fromDate}'     
            AND a.SALEDATE  <='{toDate}'   
          AND a.STORECD  = '{storeCd}'  


                UNION ALL                                                                                                                                                                                                 
                SELECT '상품권지불금권(선결제완료매출금권)'                                                                    AS 'TYPE',                                      		                                       
                       a.CLOSEDATE                                                                                             AS 'SALEDATE',                                  		                                       
                       a.STORECD                                                                                               AS 'STORECD',                                   		                                       
                       a.POSNO                                                                                                 AS 'POSNO',                                     		                                       
                       a.COMPANY                                                                                               AS 'COMPANY',                                     		                                       
                       a.TRANNO                                                                                                AS 'TRANNO',                                    		                                       
                       (CASE WHEN b.SALEAMTM <> 0 THEN '1' ELSE '2' END)                                                       AS 'PLUGB',                                     		                                       
                       '03'                                                                                                    AS 'TENDERTYPE',                                		                                       
                       (SELECT IsNull(ETC1NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E14' AND CODE = b.GVTYPE AND DELYN = '0')     AS 'TENDERTYPE2',                               		                                       
                       (SELECT IsNull(ETC2NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E14' AND CODE = b.GVTYPE AND DELYN = '0')     AS 'TENDERTYPE2NM',                             		                                       
                       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)                                        AS 'SALEAMT'                                    		                                       
                  FROM (select c.FNSHDATE AS CLOSEDATE, a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO, b.SEQ
                              , a.COMPANY, a.TRANTYPE, a.TRANKIND
                            , (case when a.STORECD IN ('013','023') then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end) SALCOMCD
                              , '1' SALEFLAG
                              , '0' PAYFLAG, b.PLUGB
                              , max(c.RESTYPE) RESTYPE
                          from IDPS..TRNRESERVATION c, IDPS..TRNSALHDR a (INDEX PK_TRNSALHDR), IDPS..TRNSALDTL b (INDEX PK_TRNSALDTL)
                        where c.SALEDATE = a.SALEDATE   and c.STORECD  = a.STORECD    and c.POSNO    = a.POSNO      and c.TRANNO   = a.TRANNO
                          and a.SALEDATE = b.SALEDATE   and a.STORECD  = b.STORECD    and a.POSNO    = b.POSNO      and a.TRANNO   = b.TRANNO
                            and a.TRANTYPE in ('00','01')
                          and a.TRANKIND = '15'
                          AND a.POSNO NOT LIKE '99%'
                          AND a.DELYN = '0'
                          and b.CANCELGB = '0'
                              and (c.FNSHDATE <> '' AND c.FNSHDATE IS NOT NULL)
                      group by c.SALEDATE, c.STORECD, c.POSNO, c.TRANNO
                              , a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO
                              , b.SALEDATE, b.STORECD, b.POSNO, b.TRANNO, b.SEQ
                              , a.COMPANY, a.TRANTYPE, a.TRANKIND, c.FNSHDATE, b.PLUGB
                            , (case when a.STORECD  IN ('013','023')  then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end)
                      ) a, IDPS..TRNGVPAY b                                                                                                                                		                                   
                 WHERE a.SALEDATE   = b.SALEDATE                                                                                                                                		                                       
                   AND a.STORECD    = b.STORECD                                                                                                                                 		                                       
                   AND a.POSNO      = b.POSNO                                                                                                                                   		                                       
                   AND a.TRANNO     = b.TRANNO                                                                                                                                                                            
                   AND a.SEQ        = 1                                                                                                                                                                                   
                   AND a.TRANTYPE   IN ('00', '01')                                                                                                                             		                                       
                   AND a.TRANKIND  = '15' AND a.SALEFLAG  = '1' AND a.PAYFLAG   = '0'                                                                                                                         		         
  	     	    AND a.POSNO    NOT LIKE '__[49]_'                                                                                                    


  		  AND a.CLOSEDATE  >='{fromDate}'     
            AND a.CLOSEDATE  <='{toDate}'    
          AND a.STORECD  = '{storeCd}'  


        ) TT1                                                                                                                                                                                                             
        GROUP BY TT1.SALEDATE, TT1.STORECD, TT1.PLUGB, TT1.TENDERTYPE2, TT1.COMPANY                                                                                                                                         
        ORDER BY 1,2,3,4,5                                                                                                                             

   """,    

  "hyundai_tender_aftermerge":"""
SELECT TT1.SALEDATE                                                                                                             AS 'SALEDATE',                                                                    
               TT1.STORECD + ':' + (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C03' AND CODE = TT1.STORECD AND DELYN = '0')         AS 'STORECD',                                                                       
               (CASE WHEN TT1.PLUGB = '1' THEN '교보문고' ELSE '핫트랙스' END)                                                          AS 'PLUGB',                                                                       
               TT1.TENDERTYPE + ':' + (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E17' AND CODE = TT1.TENDERTYPE AND DELYN = '0')   AS 'TENDERTYPE2', 
               (CASE WHEN TT1.TENDERTYPE IN ('80','99')  THEN COUNT(TT1.SALEDATE) ELSE 0 END)   AS 'HYUNDAICNT', 
               SUM((CASE WHEN TT1.COMPANY = '1' THEN TT1.SALEAMT ELSE 0           END))                                                 AS 'KB_SALEAMT',                              
               SUM((CASE WHEN TT1.COMPANY = '1' THEN 0           ELSE TT1.SALEAMT END))                                                 AS 'HT_SALEAMT',                              
               SUM(TT1.SALEAMT)                                                                                                         AS 'TOT_SALEAMT'                              
          FROM (                                                                                                                                                                                                          

                SELECT '일반매출금권(신용카드only)'                                                                            AS 'TYPE',                                                                                  
                       a.SALEDATE                                                                                              AS 'SALEDATE',                                                                             
                       a.STORECD                                                                                               AS 'STORECD',                                                                              
                       a.POSNO                                                                                                 AS 'POSNO',                                                                                
                       a.COMPANY                                                                                               AS 'COMPANY',                                                                                
                       a.TRANNO                                                                                                AS 'TRANNO',                                                                               
                       (CASE WHEN b.SALEAMTM <> 0 THEN '1' ELSE '2' END)                                                       AS 'PLUGB',                                                                                
                        (CASE WHEN b.CARDKIND = '30' THEN '99' ELSE '80' END)                                                 AS 'TENDERTYPE',                                                                           
                       (SELECT IsNull(ETC1NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E17' AND CODE = b.CARDKIND AND DELYN = '0')   AS 'TENDERTYPE2',                         
                       (SELECT IsNull(ETC2NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E17' AND CODE = b.CARDKIND AND DELYN = '0')   AS 'TENDERTYPE2NM',                       
                       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)                                        AS 'SALEAMT'                                                                               
                  FROM IDPS..TRNSALHDR a, IDPS..TRNCARDPAY b                                                                                                                                                                          
                 WHERE a.SALEDATE   = b.SALEDATE                                                                                                                                                                          
                   AND a.STORECD    = b.STORECD                                                                                                                                                                           
                   AND a.POSNO      = b.POSNO                                                                                                                                                                             
                   AND a.TRANNO     = b.TRANNO                                                                                                                                                                            
                   AND a.TRANTYPE   IN ('00', '01')                                                                                                                                                                       
                   AND a.TRANKIND   NOT IN ('10','11','13','14', '12','17')                                                                                                                                                    
                   AND b.PURCODE = '90'                                                                                                                                                      
         AND a.POSNO    NOT LIKE '__[49]_'                                                                                                           

       AND a.SALEDATE = '{saleDate}'     
          AND a.STORECD  = '{storeCd}'  
       
                UNION ALL                                                                                                                                                                                                 
                SELECT '상품권판매매출금권(신용카드only)'                                                                            AS 'TYPE',                                                                                 
                       a.SALEDATE                                                                                              AS 'SALEDATE',                                                                             
                       a.STORECD                                                                                               AS 'STORECD',                                                                              
                       a.POSNO                                                                                                 AS 'POSNO',                                                                                
                       a.COMPANY                                                                                               AS 'COMPANY',                                                                                
                       a.TRANNO                                                                                                AS 'TRANNO',                                                                               
                       (CASE WHEN b.SALEAMTM <> 0 THEN '1' ELSE '2' END)                                                       AS 'PLUGB',                                                                                
                       (CASE WHEN b.CARDKIND = '30' THEN '99' ELSE '80' END)                                                 AS 'TENDERTYPE',                                                                           
                       (SELECT IsNull(ETC1NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E17' AND CODE = b.CARDKIND AND DELYN = '0')   AS 'TENDERTYPE2',                         
                       (SELECT IsNull(ETC2NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E17' AND CODE = b.CARDKIND AND DELYN = '0')   AS 'TENDERTYPE2NM',                       
                       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)                                        AS 'SALEAMT'                                                                               
                  FROM IDPS..TRNSALHDR a, IDPS..TRNCARDPAY b                                                                                                          
                 WHERE a.SALEDATE   = b.SALEDATE                                                                                                                                                                          
                   AND a.STORECD    = b.STORECD                                                                                                                                                                           
                   AND a.POSNO      = b.POSNO                                                                                                                                                                             
                   AND a.TRANNO     = b.TRANNO                                                                                                                                                                            
                   AND a.TRANTYPE   IN ('00', '01')                                                                                                                                                                       
                   AND a.TRANKIND   IN ('13','14','17')                                                                                                                                                                       
                   AND b.PURCODE = '90'                                                                                                                                                        
         AND a.POSNO    NOT LIKE '__[49]_'                                                                                                           

       AND a.SALEDATE = '{saleDate}'     
          AND a.STORECD  = '{storeCd}'  
       


        ) TT1                                                                                                                                                                                                             
        GROUP BY TT1.SALEDATE, TT1.STORECD, TT1.PLUGB, TT1.TENDERTYPE, TT1.COMPANY                                                                                                                                         
        ORDER BY 1,2,3,4,5
   """,    

   "hyundai_tender_aftermergefromto":"""
SELECT TT1.SALEDATE                                                                                                             AS 'SALEDATE',                                                                    
               TT1.STORECD + ':' + (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C03' AND CODE = TT1.STORECD AND DELYN = '0')         AS 'STORECD',                                                                       
               (CASE WHEN TT1.PLUGB = '1' THEN '교보문고' ELSE '핫트랙스' END)                                                          AS 'PLUGB',                                                                       
               TT1.TENDERTYPE + ':' + (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E17' AND CODE = TT1.TENDERTYPE AND DELYN = '0')   AS 'TENDERTYPE2', 
               (CASE WHEN TT1.TENDERTYPE IN ('80','99')  THEN COUNT(TT1.SALEDATE) ELSE 0 END)   AS 'HYUNDAICNT', 
               SUM((CASE WHEN TT1.COMPANY = '1' THEN TT1.SALEAMT ELSE 0           END))                                                 AS 'KB_SALEAMT',                              
               SUM((CASE WHEN TT1.COMPANY = '1' THEN 0           ELSE TT1.SALEAMT END))                                                 AS 'HT_SALEAMT',                              
               SUM(TT1.SALEAMT)                                                                                                         AS 'TOT_SALEAMT'                              
          FROM (                                                                                                                                                                                                          

                SELECT '일반매출금권(신용카드only)'                                                                            AS 'TYPE',                                                                                  
                       a.SALEDATE                                                                                              AS 'SALEDATE',                                                                             
                       a.STORECD                                                                                               AS 'STORECD',                                                                              
                       a.POSNO                                                                                                 AS 'POSNO',                                                                                
                       a.COMPANY                                                                                               AS 'COMPANY',                                                                                
                       a.TRANNO                                                                                                AS 'TRANNO',                                                                               
                       (CASE WHEN b.SALEAMTM <> 0 THEN '1' ELSE '2' END)                                                       AS 'PLUGB',                                                                                
                        (CASE WHEN b.CARDKIND = '30' THEN '99' ELSE '80' END)                                                 AS 'TENDERTYPE',                                                                           
                       (SELECT IsNull(ETC1NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E17' AND CODE = b.CARDKIND AND DELYN = '0')   AS 'TENDERTYPE2',                         
                       (SELECT IsNull(ETC2NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E17' AND CODE = b.CARDKIND AND DELYN = '0')   AS 'TENDERTYPE2NM',                       
                       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)                                        AS 'SALEAMT'                                                                               
                  FROM IDPS..TRNSALHDR a, IDPS..TRNCARDPAY b                                                                                                                                                                          
                 WHERE a.SALEDATE   = b.SALEDATE                                                                                                                                                                          
                   AND a.STORECD    = b.STORECD                                                                                                                                                                           
                   AND a.POSNO      = b.POSNO                                                                                                                                                                             
                   AND a.TRANNO     = b.TRANNO                                                                                                                                                                            
                   AND a.TRANTYPE   IN ('00', '01')                                                                                                                                                                       
                   AND a.TRANKIND   NOT IN ('10','11','13','14', '12','17')                                                                                                                                                    
                   AND b.PURCODE = '90'                                                                                                                                                      
         AND a.POSNO    NOT LIKE '__[49]_'                                                                                                           

          AND a.SALEDATE >='{fromDate}'     
          AND a.SALEDATE <='{toDate}'      
          AND a.STORECD  = '{storeCd}'  
       
                UNION ALL                                                                                                                                                                                                 
                SELECT '상품권판매매출금권(신용카드only)'                                                                            AS 'TYPE',                                                                                 
                       a.SALEDATE                                                                                              AS 'SALEDATE',                                                                             
                       a.STORECD                                                                                               AS 'STORECD',                                                                              
                       a.POSNO                                                                                                 AS 'POSNO',                                                                                
                       a.COMPANY                                                                                               AS 'COMPANY',                                                                                
                       a.TRANNO                                                                                                AS 'TRANNO',                                                                               
                       (CASE WHEN b.SALEAMTM <> 0 THEN '1' ELSE '2' END)                                                       AS 'PLUGB',                                                                                
                       (CASE WHEN b.CARDKIND = '30' THEN '99' ELSE '80' END)                                                 AS 'TENDERTYPE',                                                                           
                       (SELECT IsNull(ETC1NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E17' AND CODE = b.CARDKIND AND DELYN = '0')   AS 'TENDERTYPE2',                         
                       (SELECT IsNull(ETC2NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E17' AND CODE = b.CARDKIND AND DELYN = '0')   AS 'TENDERTYPE2NM',                       
                       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)                                        AS 'SALEAMT'                                                                               
                  FROM IDPS..TRNSALHDR a, IDPS..TRNCARDPAY b                                                                                                          
                 WHERE a.SALEDATE   = b.SALEDATE                                                                                                                                                                          
                   AND a.STORECD    = b.STORECD                                                                                                                                                                           
                   AND a.POSNO      = b.POSNO                                                                                                                                                                             
                   AND a.TRANNO     = b.TRANNO                                                                                                                                                                            
                   AND a.TRANTYPE   IN ('00', '01')                                                                                                                                                                       
                   AND a.TRANKIND   IN ('13','14','17')                                                                                                                                                                       
                   AND b.PURCODE = '90'                                                                                                                                                        
         AND a.POSNO    NOT LIKE '__[49]_'                                                                                                           

          AND a.SALEDATE >='{fromDate}'     
          AND a.SALEDATE <='{toDate}'    
          AND a.STORECD  = '{storeCd}'  
       


        ) TT1                                                                                                                                                                                                             
        GROUP BY TT1.SALEDATE, TT1.STORECD, TT1.PLUGB, TT1.TENDERTYPE, TT1.COMPANY                                                                                                                                         
        ORDER BY 1,2,3,4,5
   """,    


   "hyundai_internal_card":"""
      SELECT A.SALEDATE AS '매출일자', 
            A.STORECD + '[' + (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C03' AND CODE = A.STORECD AND DELYN = '0') + ']' AS '점포명', 
            A.POSNO AS '포스번호', A.TRANNO AS '거래번호', A.SEQ AS '순번', A.APPTIME AS '판매시간',
            (CASE WHEN A.TRANTYPE = '10' THEN '판매' ELSE '반품' END) AS '판매구분',
            A.APPNO '승인번호',
            (CASE WHEN A.TRANTYPE = '10' THEN A.SALEAMT ELSE -A.SALEAMT END) AS '결제금액',
            
            A.ORGAPPDATE AS '원거래승인일자',
            A.ORGAPPNO AS '원거래 승인번호',
            SUBSTRING(A.ORGTRANINFO,13,4) AS '원거래포스번호',
            SUBSTRING(A.ORGTRANINFO,17,4) AS '원거래거래번호'
            
              FROM IDPS..TRNCARDPAY A WHERE A.SALEDATE >= '{fromDate}'     
              AND A.SALEDATE <= '{toDate}' 
              AND A.STORECD = '{storeCd}'  
              AND A.PURCODE ='90'
            AT ISOLATION 0
   """,    





   "getcommissionsaledatabyprod":"""
       SELECT TT1.SALEDATE                                                                                                     AS 'SALEDATE',          
              (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C03' AND CODE = TT1.STORECD AND DELYN = '0') AS 'STORECD',           
              (CASE WHEN TT1.PLUGB = '1' THEN '교보문고' ELSE '핫트랙스' END)                                                  AS 'PLUGB',             
              (CASE WHEN TT1.TAXGB = '0' THEN '과세'                                                                                                   
                    WHEN TT1.TAXGB = '1' THEN '비과세'                                                                                                 
                    WHEN TT1.TAXGB = '2' THEN '상품권' END)                                                                    AS 'TAXGB',             
            SUM((CASE WHEN TT1.COMPANY = '1' THEN TT1.QTY     ELSE 0           END))                                         AS 'KB_QTY',          
            SUM((CASE WHEN TT1.COMPANY = '1' THEN 0           ELSE TT1.QTY     END))                                         AS 'HT_QTY',          
            SUM(TT1.QTY)                                                                                                     AS 'TOT_QTY',         
            SUM((CASE WHEN TT1.COMPANY = '1' THEN TT1.SALEAMT ELSE 0           END))                                         AS 'KB_SALEAMT',      
            SUM((CASE WHEN TT1.COMPANY = '1' THEN 0           ELSE TT1.SALEAMT END))                                         AS 'HT_SALEAMT',      
            SUM(TT1.SALEAMT)                                                                                                 AS 'TOT_SALEAMT'      
         FROM (                                                                                                                                        
                   SELECT '일반매출'                                                        AS 'TYPE',                                                          
                          a.SALEDATE                                                        AS 'SALEDATE',                                                      
                          a.STORECD                                                         AS 'STORECD',                                                       
                          a.POSNO                                                           AS 'POSNO',                                                         
                           a.COMPANY                                                         AS 'COMPANY',                                                       
                          a.TRANNO                                                          AS 'TRANNO',                                                        
                          b.SEQ                                                             AS 'SEQ',                                                           
                          (CASE WHEN b.PLUGB = '7' THEN '1' ELSE '2' END)                   AS 'PLUGB',                                                         
                          (CASE WHEN b.TAXGB = '0' THEN '0' ELSE '1' END)                   AS 'TAXGB',                                                         
                          (CASE WHEN a.TRANTYPE = '00' THEN b.QTY ELSE -b.QTY END)          AS 'QTY',                                                           
                          (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)  AS 'SALEAMT'                                                        
                   FROM IDPS..TRNSALHDR a, IDPS..TRNSALDTL b                                                                                                                
                   WHERE a.SALEDATE = b.SALEDATE                                                                                                                
                     AND a.STORECD  = b.STORECD                                                                                                                 
                     AND a.POSNO    = b.POSNO                                                                                                                   
                     AND a.TRANNO   = b.TRANNO                                                                                                                  
                     AND a.TRANTYPE IN ('00', '01')                                                                                                             
                     AND a.TRANKIND NOT IN ('10', '11', '13', '14', '15', '12', '17')                                                                                            
                     AND b.CANCELGB = '0'                                                                                                                       
              AND a.POSNO    NOT LIKE '__[49]_'                                                                                                          

                     AND a.SALEDATE ='{saleDate}'     
                     AND a.STORECD  ='{storeCd}'                                                                 


                   UNION ALL                                                                                                                                    
                    SELECT '선매출'                                                          AS 'TYPE',                                                          
                          a.CLOSEDATE                                                       AS 'SALEDATE',                                                      
                          a.STORECD                                                         AS 'STORECD',                                                       
                          a.POSNO                                                           AS 'POSNO',                                                         
                           a.COMPANY                                                         AS 'COMPANY',                                                       
                          a.TRANNO                                                          AS 'TRANNO',                                                        
                          b.SEQ                                                             AS 'SEQ',                                                           
                          (CASE WHEN b.PLUGB = '7' THEN '1' ELSE '2' END)                   AS 'PLUGB',                                                         
                          (CASE WHEN b.TAXGB = '0' THEN '0' ELSE '1' END)                   AS 'TAXGB',                                                         
                          (CASE WHEN a.TRANTYPE = '00' THEN b.QTY ELSE -b.QTY END)          AS 'QTY',                                                           
                          (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)  AS 'SALEAMT'         
                      FROM (select c.FNSHDATE AS CLOSEDATE, a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO, b.SEQ
                              , a.COMPANY, a.TRANTYPE, a.TRANKIND
                            , (case when a.STORECD IN ('013','023') then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end) SALCOMCD
                              , '1' SALEFLAG
                              , '0' PAYFLAG, b.PLUGB
                              , max(c.RESTYPE) RESTYPE
                          from IDPS..TRNRESERVATION c, IDPS..TRNSALHDR a (INDEX PK_TRNSALHDR), IDPS..TRNSALDTL b (INDEX PK_TRNSALDTL)
                        where c.SALEDATE = a.SALEDATE   and c.STORECD  = a.STORECD    and c.POSNO    = a.POSNO      and c.TRANNO   = a.TRANNO
                          and a.SALEDATE = b.SALEDATE   and a.STORECD  = b.STORECD    and a.POSNO    = b.POSNO      and a.TRANNO   = b.TRANNO
                            and a.TRANTYPE in ('00','01')
                          and a.TRANKIND = '15'
                          AND a.POSNO NOT LIKE '99%'
                          AND a.DELYN = '0'
                          and b.CANCELGB = '0'
                              and (c.FNSHDATE <> '' AND c.FNSHDATE IS NOT NULL)
                      group by c.SALEDATE, c.STORECD, c.POSNO, c.TRANNO
                              , a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO
                              , b.SALEDATE, b.STORECD, b.POSNO, b.TRANNO, b.SEQ
                              , a.COMPANY, a.TRANTYPE, a.TRANKIND, c.FNSHDATE, b.PLUGB
                            , (case when a.STORECD  IN ('013','023')  then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end)
                      ) a, IDPS..TRNSALDTL b                                                                                                          
                                        WHERE a.SALEDATE  = b.SALEDATE                                                                                                               
                                          AND a.STORECD   = b.STORECD                                                                                                                
                                          AND a.POSNO     = b.POSNO                                                                                                                  
                                          AND a.TRANNO    = b.TRANNO                                                                                                                 
                                          AND a.SEQ       = b.SEQ                                                                                                                    
                                          AND a.TRANTYPE  IN ('00', '01')                                                                                                            
                                          AND a.TRANKIND  = '15' AND a.SALEFLAG  = '1' AND a.PAYFLAG   = '0'                                                                         
                                          AND b.CANCELGB  = '0'                                                                                                                      
                                    AND a.POSNO    NOT LIKE '__[49]_'                                                                                                          
                                                                                                      

                     AND a.CLOSEDATE ='{saleDate}'     
                     AND a.STORECD  = '{storeCd}'                                                         


                   UNION ALL                                                                                                                                    
                   SELECT '상품권'                                                          AS 'TYPE',                                                          
                          a.SALEDATE                                                        AS 'SALEDATE',                                                      
                          a.STORECD                                                         AS 'STORECD',                                                       
                          a.POSNO                                                           AS 'POSNO',                                                         
                           a.COMPANY                                                         AS 'COMPANY',                                                       
                          a.TRANNO                                                          AS 'TRANNO',                                                        
                          b.SEQ                                                             AS 'SEQ',                                                           
                          (CASE WHEN b.PLUGB = '7' THEN '1' ELSE '2' END)                   AS 'PLUGB',                                                         
                          '2'                                                               AS 'TAXGB',                                                         
                          (CASE WHEN a.TRANTYPE = '00' THEN b.QTY ELSE -b.QTY END)          AS 'QTY',                                                           
                          (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)  AS 'SALEAMT'                                                        
                   FROM IDPS..TRNSALHDR a, IDPS..TRNSALDTL b                                                                                                                
                   WHERE a.SALEDATE = b.SALEDATE                                                                                                                
                     AND a.STORECD  = b.STORECD                                                                                                                 
                     AND a.POSNO    = b.POSNO                                                                                                                   
                     AND a.TRANNO   = b.TRANNO                                                                                                                  
                     AND a.TRANTYPE IN ('00', '01')                                                                                                             
                     AND a.TRANKIND IN ('13', '14', '17')                                                                                                             
                     AND b.CANCELGB = '0'                                                                                                                       
              AND a.POSNO    NOT LIKE '__[49]_'                                                                                                          

                     AND a.SALEDATE ='{saleDate}'     
                     AND a.STORECD  = '{storeCd}'  
               ) TT1                                                                                                                                       
         GROUP BY TT1.SALEDATE, TT1.STORECD, TT1.PLUGB, TT1.TAXGB, TT1.COMPANY                                                                           
         ORDER BY 1,2,3,4,5                                                                                                                            
                       

   """,    

   "getcommissionsaledatabyprodfromto":"""
       SELECT TT1.SALEDATE                                                                                                     AS 'SALEDATE',          
              (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C03' AND CODE = TT1.STORECD AND DELYN = '0') AS 'STORECD',           
              (CASE WHEN TT1.PLUGB = '1' THEN '교보문고' ELSE '핫트랙스' END)                                                  AS 'PLUGB',             
              (CASE WHEN TT1.TAXGB = '0' THEN '과세'                                                                                                   
                    WHEN TT1.TAXGB = '1' THEN '비과세'                                                                                                 
                    WHEN TT1.TAXGB = '2' THEN '상품권' END)                                                                    AS 'TAXGB',             
            SUM((CASE WHEN TT1.COMPANY = '1' THEN TT1.QTY     ELSE 0           END))                                         AS 'KB_QTY',          
            SUM((CASE WHEN TT1.COMPANY = '1' THEN 0           ELSE TT1.QTY     END))                                         AS 'HT_QTY',          
            SUM(TT1.QTY)                                                                                                     AS 'TOT_QTY',         
            SUM((CASE WHEN TT1.COMPANY = '1' THEN TT1.SALEAMT ELSE 0           END))                                         AS 'KB_SALEAMT',      
            SUM((CASE WHEN TT1.COMPANY = '1' THEN 0           ELSE TT1.SALEAMT END))                                         AS 'HT_SALEAMT',      
            SUM(TT1.SALEAMT)                                                                                                 AS 'TOT_SALEAMT'      
         FROM (                                                                                                                                        
                   SELECT '일반매출'                                                        AS 'TYPE',                                                          
                          a.SALEDATE                                                        AS 'SALEDATE',                                                      
                          a.STORECD                                                         AS 'STORECD',                                                       
                          a.POSNO                                                           AS 'POSNO',                                                         
                           a.COMPANY                                                         AS 'COMPANY',                                                       
                          a.TRANNO                                                          AS 'TRANNO',                                                        
                          b.SEQ                                                             AS 'SEQ',                                                           
                          (CASE WHEN b.PLUGB = '7' THEN '1' ELSE '2' END)                   AS 'PLUGB',                                                         
                          (CASE WHEN b.TAXGB = '0' THEN '0' ELSE '1' END)                   AS 'TAXGB',                                                         
                          (CASE WHEN a.TRANTYPE = '00' THEN b.QTY ELSE -b.QTY END)          AS 'QTY',                                                           
                          (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)  AS 'SALEAMT'                                                        
                   FROM IDPS..TRNSALHDR a, IDPS..TRNSALDTL b                                                                                                                
                   WHERE a.SALEDATE = b.SALEDATE                                                                                                                
                     AND a.STORECD  = b.STORECD                                                                                                                 
                     AND a.POSNO    = b.POSNO                                                                                                                   
                     AND a.TRANNO   = b.TRANNO                                                                                                                  
                     AND a.TRANTYPE IN ('00', '01')                                                                                                             
                     AND a.TRANKIND NOT IN ('10', '11', '13', '14', '15', '12', '17')                                                                                            
                     AND b.CANCELGB = '0'                                                                                                                       
              AND a.POSNO    NOT LIKE '__[49]_'                                                                                                          

                     AND a.SALEDATE >='{fromDate}'     
                     AND a.SALEDATE <='{toDate}'     
                     AND a.STORECD  ='{storeCd}'                                                                 


                   UNION ALL                                                                                                                                    
                    SELECT '선매출'                                                          AS 'TYPE',                                                          
                          a.CLOSEDATE                                                       AS 'SALEDATE',                                                      
                          a.STORECD                                                         AS 'STORECD',                                                       
                          a.POSNO                                                           AS 'POSNO',                                                         
                           a.COMPANY                                                         AS 'COMPANY',                                                       
                          a.TRANNO                                                          AS 'TRANNO',                                                        
                          b.SEQ                                                             AS 'SEQ',                                                           
                          (CASE WHEN b.PLUGB = '7' THEN '1' ELSE '2' END)                   AS 'PLUGB',                                                         
                          (CASE WHEN b.TAXGB = '0' THEN '0' ELSE '1' END)                   AS 'TAXGB',                                                         
                          (CASE WHEN a.TRANTYPE = '00' THEN b.QTY ELSE -b.QTY END)          AS 'QTY',                                                           
                          (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)  AS 'SALEAMT'         
                      FROM (select c.FNSHDATE AS CLOSEDATE, a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO, b.SEQ
                              , a.COMPANY, a.TRANTYPE, a.TRANKIND
                            , (case when a.STORECD IN ('013','023') then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end) SALCOMCD
                              , '1' SALEFLAG
                              , '0' PAYFLAG, b.PLUGB
                              , max(c.RESTYPE) RESTYPE
                          from IDPS..TRNRESERVATION c, IDPS..TRNSALHDR a (INDEX PK_TRNSALHDR), IDPS..TRNSALDTL b (INDEX PK_TRNSALDTL)
                        where c.SALEDATE = a.SALEDATE   and c.STORECD  = a.STORECD    and c.POSNO    = a.POSNO      and c.TRANNO   = a.TRANNO
                          and a.SALEDATE = b.SALEDATE   and a.STORECD  = b.STORECD    and a.POSNO    = b.POSNO      and a.TRANNO   = b.TRANNO
                            and a.TRANTYPE in ('00','01')
                          and a.TRANKIND = '15'
                          AND a.POSNO NOT LIKE '99%'
                          AND a.DELYN = '0'
                          and b.CANCELGB = '0'
                              and (c.FNSHDATE <> '' AND c.FNSHDATE IS NOT NULL)
                      group by c.SALEDATE, c.STORECD, c.POSNO, c.TRANNO
                              , a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO
                              , b.SALEDATE, b.STORECD, b.POSNO, b.TRANNO, b.SEQ
                              , a.COMPANY, a.TRANTYPE, a.TRANKIND, c.FNSHDATE, b.PLUGB
                            , (case when a.STORECD  IN ('013','023')  then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end)
                      ) a, IDPS..TRNSALDTL b                                                                                                          
                                        WHERE a.SALEDATE  = b.SALEDATE                                                                                                               
                                          AND a.STORECD   = b.STORECD                                                                                                                
                                          AND a.POSNO     = b.POSNO                                                                                                                  
                                          AND a.TRANNO    = b.TRANNO                                                                                                                 
                                          AND a.SEQ       = b.SEQ                                                                                                                    
                                          AND a.TRANTYPE  IN ('00', '01')                                                                                                            
                                          AND a.TRANKIND  = '15' AND a.SALEFLAG  = '1' AND a.PAYFLAG   = '0'                                                                         
                                          AND b.CANCELGB  = '0'                                                                                                                      
                                    AND a.POSNO    NOT LIKE '__[49]_'              

                     AND a.CLOSEDATE >='{fromDate}'     
                     AND a.CLOSEDATE <='{toDate}'                        
                     AND a.STORECD  = '{storeCd}'                                                         


                   UNION ALL                                                                                                                                    
                   SELECT '상품권'                                                          AS 'TYPE',                                                          
                          a.SALEDATE                                                        AS 'SALEDATE',                                                      
                          a.STORECD                                                         AS 'STORECD',                                                       
                          a.POSNO                                                           AS 'POSNO',                                                         
                           a.COMPANY                                                         AS 'COMPANY',                                                       
                          a.TRANNO                                                          AS 'TRANNO',                                                        
                          b.SEQ                                                             AS 'SEQ',                                                           
                          (CASE WHEN b.PLUGB = '7' THEN '1' ELSE '2' END)                   AS 'PLUGB',                                                         
                          '2'                                                               AS 'TAXGB',                                                         
                          (CASE WHEN a.TRANTYPE = '00' THEN b.QTY ELSE -b.QTY END)          AS 'QTY',                                                           
                          (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)  AS 'SALEAMT'                                                        
                   FROM IDPS..TRNSALHDR a, IDPS..TRNSALDTL b                                                                                                                
                   WHERE a.SALEDATE = b.SALEDATE                                                                                                                
                     AND a.STORECD  = b.STORECD                                                                                                                 
                     AND a.POSNO    = b.POSNO                                                                                                                   
                     AND a.TRANNO   = b.TRANNO                                                                                                                  
                     AND a.TRANTYPE IN ('00', '01')                                                                                                             
                     AND a.TRANKIND IN ('13', '14', '17')                                                                                                             
                     AND b.CANCELGB = '0'                                                                                                                       
              AND a.POSNO    NOT LIKE '__[49]_'                                                                                                          

                     AND a.SALEDATE >='{fromDate}'     
                     AND a.SALEDATE <='{toDate}'     
                     AND a.STORECD  = '{storeCd}'  
               ) TT1                                                                                                                                       
         GROUP BY TT1.SALEDATE, TT1.STORECD, TT1.PLUGB, TT1.TAXGB, TT1.COMPANY                                                                           
         ORDER BY 1,2,3,4,5                                                                                                                            
                       

   """,    


   "getcommissionsaledatabyprodfromtobyposno":"""
       SELECT TT1.SALEDATE                                                                                                     AS 'SALEDATE',          
              (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C03' AND CODE = TT1.STORECD AND DELYN = '0') AS 'STORECD',           
               TT1.POSNO,
              (CASE WHEN TT1.PLUGB = '1' THEN '교보문고' ELSE '핫트랙스' END)                                                  AS 'PLUGB',             
              (CASE WHEN TT1.TAXGB = '0' THEN '과세'                                                                                                   
                    WHEN TT1.TAXGB = '1' THEN '비과세'                                                                                                 
                    WHEN TT1.TAXGB = '2' THEN '상품권' END)                                                                    AS 'TAXGB',             
            SUM((CASE WHEN TT1.COMPANY = '1' THEN TT1.QTY     ELSE 0           END))                                         AS 'KB_QTY',          
            SUM((CASE WHEN TT1.COMPANY = '1' THEN 0           ELSE TT1.QTY     END))                                         AS 'HT_QTY',          
            SUM(TT1.QTY)                                                                                                     AS 'TOT_QTY',         
            SUM((CASE WHEN TT1.COMPANY = '1' THEN TT1.SALEAMT ELSE 0           END))                                         AS 'KB_SALEAMT',      
            SUM((CASE WHEN TT1.COMPANY = '1' THEN 0           ELSE TT1.SALEAMT END))                                         AS 'HT_SALEAMT',      
            SUM(TT1.SALEAMT)                                                                                                 AS 'TOT_SALEAMT'      
         FROM (                                                                                                                                        
                   SELECT '일반매출'                                                        AS 'TYPE',                                                          
                          a.SALEDATE                                                        AS 'SALEDATE',                                                      
                          a.STORECD                                                         AS 'STORECD',                                                       
                          a.POSNO                                                           AS 'POSNO',                                                         
                           a.COMPANY                                                         AS 'COMPANY',                                                       
                          a.TRANNO                                                          AS 'TRANNO',                                                        
                          b.SEQ                                                             AS 'SEQ',                                                           
                          (CASE WHEN b.PLUGB = '7' THEN '1' ELSE '2' END)                   AS 'PLUGB',                                                         
                          (CASE WHEN b.TAXGB = '0' THEN '0' ELSE '1' END)                   AS 'TAXGB',                                                         
                          (CASE WHEN a.TRANTYPE = '00' THEN b.QTY ELSE -b.QTY END)          AS 'QTY',                                                           
                          (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)  AS 'SALEAMT'                                                        
                   FROM IDPS..TRNSALHDR a, IDPS..TRNSALDTL b                                                                                                                
                   WHERE a.SALEDATE = b.SALEDATE                                                                                                                
                     AND a.STORECD  = b.STORECD                                                                                                                 
                     AND a.POSNO    = b.POSNO                                                                                                                   
                     AND a.TRANNO   = b.TRANNO                                                                                                                  
                     AND a.TRANTYPE IN ('00', '01')                                                                                                             
                     AND a.TRANKIND NOT IN ('10', '11', '13', '14', '15', '12', '17')                                                                                            
                     AND b.CANCELGB = '0'                                                                                                                 

                     AND a.SALEDATE >='{fromDate}'     
                     AND a.SALEDATE <='{toDate}'     
                     AND a.STORECD  ='{storeCd}'                                                                 
                     AND a.POSNO ='{posNo}'

                   UNION ALL                                                                                                                                    
                    SELECT '선매출'                                                          AS 'TYPE',                                                          
                          a.CLOSEDATE                                                       AS 'SALEDATE',                                                      
                          a.STORECD                                                         AS 'STORECD',                                                       
                          a.POSNO                                                           AS 'POSNO',                                                         
                           a.COMPANY                                                         AS 'COMPANY',                                                       
                          a.TRANNO                                                          AS 'TRANNO',                                                        
                          b.SEQ                                                             AS 'SEQ',                                                           
                          (CASE WHEN b.PLUGB = '7' THEN '1' ELSE '2' END)                   AS 'PLUGB',                                                         
                          (CASE WHEN b.TAXGB = '0' THEN '0' ELSE '1' END)                   AS 'TAXGB',                                                         
                          (CASE WHEN a.TRANTYPE = '00' THEN b.QTY ELSE -b.QTY END)          AS 'QTY',                                                           
                          (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)  AS 'SALEAMT'         
                      FROM (select c.FNSHDATE AS CLOSEDATE, a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO, b.SEQ
                              , a.COMPANY, a.TRANTYPE, a.TRANKIND
                            , (case when a.STORECD IN ('013','023') then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end) SALCOMCD
                              , '1' SALEFLAG
                              , '0' PAYFLAG, b.PLUGB
                              , max(c.RESTYPE) RESTYPE
                          from IDPS..TRNRESERVATION c, IDPS..TRNSALHDR a (INDEX PK_TRNSALHDR), IDPS..TRNSALDTL b (INDEX PK_TRNSALDTL)
                        where c.SALEDATE = a.SALEDATE   and c.STORECD  = a.STORECD    and c.POSNO    = a.POSNO      and c.TRANNO   = a.TRANNO
                          and a.SALEDATE = b.SALEDATE   and a.STORECD  = b.STORECD    and a.POSNO    = b.POSNO      and a.TRANNO   = b.TRANNO
                            and a.TRANTYPE in ('00','01')
                          and a.TRANKIND = '15'
                          AND a.POSNO NOT LIKE '99%'
                          AND a.DELYN = '0'
                          and b.CANCELGB = '0'
                              and (c.FNSHDATE <> '' AND c.FNSHDATE IS NOT NULL)
                      group by c.SALEDATE, c.STORECD, c.POSNO, c.TRANNO
                              , a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO
                              , b.SALEDATE, b.STORECD, b.POSNO, b.TRANNO, b.SEQ
                              , a.COMPANY, a.TRANTYPE, a.TRANKIND, c.FNSHDATE, b.PLUGB
                            , (case when a.STORECD  IN ('013','023')  then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end)
                      ) a, IDPS..TRNSALDTL b                                                                                                          
                                        WHERE a.SALEDATE  = b.SALEDATE                                                                                                               
                                          AND a.STORECD   = b.STORECD                                                                                                                
                                          AND a.POSNO     = b.POSNO                                                                                                                  
                                          AND a.TRANNO    = b.TRANNO                                                                                                                 
                                          AND a.SEQ       = b.SEQ                                                                                                                    
                                          AND a.TRANTYPE  IN ('00', '01')                                                                                                            
                                          AND a.TRANKIND  = '15' AND a.SALEFLAG  = '1' AND a.PAYFLAG   = '0'                                                                         
                                          AND b.CANCELGB  = '0'                     

                     AND a.CLOSEDATE >='{fromDate}'     
                     AND a.CLOSEDATE <='{toDate}'                        
                     AND a.STORECD  = '{storeCd}'                                                         
                     AND a.POSNO ='{posNo}'

                   UNION ALL                                                                                                                                    
                   SELECT '상품권'                                                          AS 'TYPE',                                                          
                          a.SALEDATE                                                        AS 'SALEDATE',                                                      
                          a.STORECD                                                         AS 'STORECD',                                                       
                          a.POSNO                                                           AS 'POSNO',                                                         
                           a.COMPANY                                                         AS 'COMPANY',                                                       
                          a.TRANNO                                                          AS 'TRANNO',                                                        
                          b.SEQ                                                             AS 'SEQ',                                                           
                          (CASE WHEN b.PLUGB = '7' THEN '1' ELSE '2' END)                   AS 'PLUGB',                                                         
                          '2'                                                               AS 'TAXGB',                                                         
                          (CASE WHEN a.TRANTYPE = '00' THEN b.QTY ELSE -b.QTY END)          AS 'QTY',                                                           
                          (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)  AS 'SALEAMT'                                                        
                   FROM IDPS..TRNSALHDR a, IDPS..TRNSALDTL b                                                                                                                
                   WHERE a.SALEDATE = b.SALEDATE                                                                                                                
                     AND a.STORECD  = b.STORECD                                                                                                                 
                     AND a.POSNO    = b.POSNO                                                                                                                   
                     AND a.TRANNO   = b.TRANNO                                                                                                                  
                     AND a.TRANTYPE IN ('00', '01')                                                                                                             
                     AND a.TRANKIND IN ('13', '14', '17')                                                                                                             
                     AND b.CANCELGB = '0'                                                                                                                 

                     AND a.SALEDATE >='{fromDate}'     
                     AND a.SALEDATE <='{toDate}'     
                     AND a.STORECD  = '{storeCd}'  
                     AND a.POSNO ='{posNo}'
               ) TT1                                                                                                                                       
         GROUP BY TT1.SALEDATE, TT1.STORECD,  TT1.POSNO, TT1.PLUGB, TT1.TAXGB, TT1.COMPANY                                                                           
         ORDER BY 1,2,3,4,5                                                                                                                            
                       

   """,    


    "getcashlogbyvan": """
            
    SELECT 

      SUBSTRING(A.SALEDATE,1,6), SUM(CASE WHEN B.TRANTYPE ='10' AND B.HOSTID IN ( 'nice','NICE' ) THEN 1 ELSE 0 END) , SUM(CASE WHEN B.TRANTYPE ='10' AND B.HOSTID IN ( 'nice','NICE' ) THEN B.APPAMT ELSE 0 END) AS 'niceappramt'  ,
      SUM(CASE WHEN B.TRANTYPE ='11' AND B.HOSTID IN ( 'nice','NICE' ) THEN 1 ELSE 0 END) , SUM(CASE WHEN B.TRANTYPE ='11' AND B.HOSTID IN ( 'nice','NICE' ) THEN -B.APPAMT ELSE 0 END) AS 'nicecancelamt'  ,

      SUM(CASE WHEN B.TRANTYPE ='10' AND B.HOSTID IN ( 'kftc','KFTC' ) THEN 1 ELSE 0 END) , SUM(CASE WHEN B.TRANTYPE ='10' AND B.HOSTID IN ( 'kftc','KFTC' ) THEN B.APPAMT ELSE 0 END) AS 'kftcappramt'  ,
      SUM(CASE WHEN B.TRANTYPE ='11' AND B.HOSTID IN ( 'kftc','KFTC' ) THEN 1 ELSE 0 END) , SUM(CASE WHEN B.TRANTYPE ='11' AND B.HOSTID IN (  'kftc','KFTC' ) THEN -B.APPAMT ELSE 0 END) AS 'kftccancelamt'  
    FROM IDPS..TRNSALHDR A , IDPS..TRNCASHLOG B WHERE A.SALEDATE = B.SALEDATE
      AND A.STORECD  = B.STORECD
      AND A.POSNO    = B.POSNO
      AND A.TRANNO   = B.TRANNO
      AND A.SALEDATE >='{fromDate}'
    AND A.SALEDATE <='{toDate}'
    AND A.TRANTYPE IN ( '00', '01' )
    GROUP BY 
    SUBSTRING(A.SALEDATE,1,6)
    ORDER BY 1 
    AT ISOLATION 0

   """,    

    "getmonthlypayday": """
        

  SELECT TT1.STORECD + ':' + (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C03' AND CODE = TT1.STORECD AND DELYN = '0') AS 'STORECD',
         SUM(CASE WHEN TT1.PLUGB = '1' AND TT1.TAXGB ='0' THEN TT1.SALEAMT ELSE 0 END) AS 'KB_TAX_SALEAMT',              
         SUM(CASE WHEN TT1.PLUGB = '1' AND TT1.TAXGB ='1' THEN TT1.SALEAMT ELSE 0 END) AS 'KB_NONETAX_SALEAMT',              
         SUM(CASE WHEN TT1.PLUGB = '1' AND TT1.TAXGB ='2' THEN TT1.SALEAMT ELSE 0 END) AS 'KB_GIFTCARD_SALEAMT',              
         SUM(CASE WHEN TT1.PLUGB = '2' AND TT1.TAXGB ='0' THEN TT1.SALEAMT ELSE 0 END) AS 'KH_TAX_SALEAMT',              
         SUM(CASE WHEN TT1.PLUGB = '2' AND TT1.TAXGB ='1' THEN TT1.SALEAMT ELSE 0 END) AS 'KH_NONETAX_SALEAMT',              
         SUM(CASE WHEN TT1.PLUGB = '2' AND TT1.TAXGB ='2' THEN TT1.SALEAMT ELSE 0 END) AS 'KH_GIFTCARD_SALEAMT',             
         SUM(TT1.SALEAMT)                                                                                                 AS 'TOT_SALEAMT'       
    FROM (                                                                                                                                         
  		SELECT '일반매출'                                                        AS 'TYPE',                                                           
  		       a.SALEDATE                                                        AS 'SALEDATE',                                                       
  		       a.STORECD                                                         AS 'STORECD',                                                        
  		       a.POSNO                                                           AS 'POSNO',                                                          
       		   a.COMPANY                                                         AS 'COMPANY',                                                        
  		       a.TRANNO                                                          AS 'TRANNO',                                                         
  		       b.SEQ                                                             AS 'SEQ',                                                            
  		       (CASE WHEN substring(b.CMDTDVSNCD,1,2) <> '40' THEN '1' ELSE '2' END)                   AS 'PLUGB',                                                        
  		       (CASE WHEN b.TAXGB = '0' THEN '0' ELSE '1' END)                   AS 'TAXGB',                                                          
  		       (CASE WHEN a.TRANTYPE = '00' THEN b.QTY ELSE -b.QTY END)          AS 'QTY',                                                            
  		       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)  AS 'SALEAMT'                                                         
  		FROM IDPS..TRNSALHDR a, IDPS..TRNSALDTL b                                                                                                                 
  		WHERE a.SALEDATE = b.SALEDATE                                                                                                                 
  		  AND a.STORECD  = b.STORECD                                                                                                                  
  		  AND a.POSNO    = b.POSNO                                                                                                                    
  		  AND a.TRANNO   = b.TRANNO                                                                                                                   
  		  AND a.TRANTYPE IN ('00', '01')                                                                                                              
  		  AND a.TRANKIND NOT IN ('10', '11', '13', '14', '15', '12', '17')                                                                                             
  		  AND b.CANCELGB = '0'                                                                                                                        
  	     	    AND a.POSNO    NOT LIKE '__[49]_'                                                                                                    


  		  AND a.SALEDATE >= '{fromDate}' and  a.SALEDATE <= '{toDate}'                                                             

  		UNION ALL                                                                                                                                     
  		SELECT '선매출'                                                          AS 'TYPE',                                                           
  		       a.CLOSEDATE                                                       AS 'SALEDATE',                                                       
  		       a.STORECD                                                         AS 'STORECD',                                                        
  		       a.POSNO                                                           AS 'POSNO',                                                          
       		 a.COMPANY                                                         AS 'COMPANY',                                                        
  		       a.TRANNO                                                          AS 'TRANNO',                                                         
  		       b.SEQ                                                             AS 'SEQ',                                                            
  		       (CASE WHEN substring(b.CMDTDVSNCD,1,2) <> '40' THEN '1' ELSE '2' END)                   AS 'PLUGB',                                                          
  		       (CASE WHEN b.TAXGB = '0' THEN '0' ELSE '1' END)                   AS 'TAXGB',                                                          
  		       (CASE WHEN a.TRANTYPE = '00' THEN b.QTY ELSE -b.QTY END)          AS 'QTY',                                                            
  		       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)  AS 'SALEAMT'                                                         
  		FROM (select c.FNSHDATE AS CLOSEDATE, a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO, b.SEQ
                              , a.COMPANY, a.TRANTYPE, a.TRANKIND
                            , (case when a.STORECD IN ('013','023') then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end) SALCOMCD
                              , '1' SALEFLAG
                              , '0' PAYFLAG, b.PLUGB
                              , max(c.RESTYPE) RESTYPE
                          from IDPS..TRNRESERVATION c, IDPS..TRNSALHDR a (INDEX PK_TRNSALHDR), IDPS..TRNSALDTL b (INDEX PK_TRNSALDTL)
                        where c.SALEDATE = a.SALEDATE   and c.STORECD  = a.STORECD    and c.POSNO    = a.POSNO      and c.TRANNO   = a.TRANNO
                          and a.SALEDATE = b.SALEDATE   and a.STORECD  = b.STORECD    and a.POSNO    = b.POSNO      and a.TRANNO   = b.TRANNO
                            and a.TRANTYPE in ('00','01')
                          and a.TRANKIND = '15'
                          AND a.POSNO NOT LIKE '99%'
                          AND a.DELYN = '0'
                          and b.CANCELGB = '0'
                              and (c.FNSHDATE <> '' AND c.FNSHDATE IS NOT NULL)
                      group by c.SALEDATE, c.STORECD, c.POSNO, c.TRANNO
                              , a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO
                              , b.SALEDATE, b.STORECD, b.POSNO, b.TRANNO, b.SEQ
                              , a.COMPANY, a.TRANTYPE, a.TRANKIND, c.FNSHDATE, b.PLUGB
                            , (case when a.STORECD  IN ('013','023')  then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end)
                      ) a, IDPS..TRNSALDTL b                                                                                                           
  		WHERE a.SALEDATE  = b.SALEDATE                                                                                                                
  		  AND a.STORECD   = b.STORECD                                                                                                                 
  		  AND a.POSNO     = b.POSNO                                                                                                                   
  		  AND a.TRANNO    = b.TRANNO                                                                                                                  
  		  AND a.SEQ       = b.SEQ                                                                                                                     
  		  AND a.TRANTYPE  IN ('00', '01')                                                                                                             
  		  AND a.TRANKIND  = '15' AND a.SALEFLAG  = '1' AND a.PAYFLAG   = '0'                                                                          
  		  AND b.CANCELGB  = '0'                                                                                                                       
  	     	    AND a.POSNO    NOT LIKE '__[49]_'                                                                                                    


  		  AND a.CLOSEDATE  >= '{fromDate}' and  a.CLOSEDATE <= '{toDate}'                                                                
    
  		UNION ALL                                                                                                                                     
  		SELECT '상품권'                                                          AS 'TYPE',                                                           
  		       a.SALEDATE                                                        AS 'SALEDATE',                                                       
  		       a.STORECD                                                         AS 'STORECD',                                                        
  		       a.POSNO                                                           AS 'POSNO',                                                          
       		 a.COMPANY                                                         AS 'COMPANY',                                                        
  		       a.TRANNO                                                          AS 'TRANNO',                                                         
  		       b.SEQ                                                             AS 'SEQ',                                                            
  		       (CASE WHEN substring(b.CMDTDVSNCD,1,2) <> '40' THEN '1' ELSE '2' END)                   AS 'PLUGB',                                                        
  		       '2'                                                               AS 'TAXGB',                                                          
  		       (CASE WHEN a.TRANTYPE = '00' THEN b.QTY ELSE -b.QTY END)          AS 'QTY',                                                            
  		       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)  AS 'SALEAMT'                                                         
  		FROM IDPS..TRNSALHDR a, IDPS..TRNSALDTL b                                                                                                                 
  		WHERE a.SALEDATE = b.SALEDATE                                                                                                                 
  		  AND a.STORECD  = b.STORECD                                                                                                                  
  		  AND a.POSNO    = b.POSNO                                                                                                                    
  		  AND a.TRANNO   = b.TRANNO                                                                                                                   
  		  AND a.TRANTYPE IN ('00', '01')                                                                                                              
  		  AND a.TRANKIND IN ('13', '14', '17')                                                                                                              
  		  AND b.CANCELGB = '0'                                                                                                                        
        AND a.POSNO    NOT LIKE '__[49]_'                                                                                                           

        AND a.SALEDATE  >= '{fromDate}' and  a.SALEDATE <= '{toDate}'                                                             
                                                   
  	   ) TT1                                                                                                                                        
    GROUP BY TT1.STORECD
    ORDER BY 1

        AT ISOLATION 0        
    """,    

    "getempinfo": """
        SELECT rtrim(user_id) as 'user_id', user_name , sert_num, user_rgst_patr_code, isnull(rdp_code,'000') AS 'rdp_code'
          FROM IDCM..TM_USER 
         WHERE user_id = '{empNo}' AND dlt_ysno ='N'
            AT ISOLATION 0
    """,
 

    "getsubscriptionorder": """
        SELECT (CASE WHEN A.pren_sale_rcpt_mthd_code ='001' THEN 'N' ELSE 'Y' END)         AS deliver_way,
            ISNULL(A.stlm_date, '')                                                        AS pos_sale_ymd,
            ISNULL(A.pos_num, '')	                                                       AS pos_no,
            ISNULL(A.recp_num, '')                                                         AS pos_receipt_no,
            'N'		                                                                       AS bct_trans_yn,
            ISNULL(A.sbsc_pros_cdtn_code,'')                                               AS pren_pros_cdtn_code, 
            ISNULL(A.sbsc_rcpn_date,'')                                                    AS pren_sale_rcpn_date,
            CASE A.sbsc_pros_cdtn_code
                WHEN '004' THEN
                    'Y'
                ELSE 
                    'N'
            END	                                                                             AS PayComplete,
            '1'                                                                              AS RESTYPE 
   
        FROM   IDSS..TM_SBSC_SALE_M A
        WHERE A.sbsc_rcpn_id = '{resNo}'
        AND A.str_rdp_code = '{storeCd}'

    """,
 
    "subscriptapproved": """
        UPDATE  IDSS..TM_SBSC_SALE_M
        SET pren_ordr_amnt = {saleAmt}, amnr_id = 'posAPI', amnd_dttm = getdate()
        WHERE sbsc_rcpn_id = '{resNo}'

        SELECT sbsc_rcpn_id, pren_ordr_amnt 
          FROM IDSS..TM_SBSC_SALE_M T1
         WHERE  sbsc_rcpn_id = '{resNo}'
            AT ISOLATION 0
    """,

    "regcampaign": """
        IF EXISTS (SELECT 1 FROM IDPS..TM_POS_OFFER 
                  WHERE offer_nmbr = '{offer_nmbr}' AND pr_nmbr = '{pr_nmbr}' AND mmbr_nmbr = '{mmbr_nmbr}')
        BEGIN
            UPDATE IDPS..TM_POS_OFFER
            SET 
                strt_date = '{strt_date}',
                clse_date = '{clse_date}',
                crtr_id = '{crtr_id}',
                cret_dttm = GETDATE()
            WHERE offer_nmbr = '{offer_nmbr}' AND pr_nmbr = '{pr_nmbr}' AND mmbr_nmbr = '{mmbr_nmbr}'
        END
        ELSE
        BEGIN
            INSERT INTO IDPS..TM_POS_OFFER (
                mmbr_nmbr,  
                offer_nmbr,
                pr_nmbr,    
                strt_date,  
                clse_date,  
                etc1,       
                etc2,       
                crtr_id,    
                cret_dttm)
            VALUES (
                '{mmbr_nmbr}', 
                '{offer_nmbr}', 
                '{pr_nmbr}', 
                '{strt_date}', 
                '{clse_date}', 
                '', 
                '',
                '{crtr_id}', 
                GETDATE())
        END

        SELECT count(offer_nmbr) cnt FROM IDPS..TM_POS_OFFER WHERE offer_nmbr = '{offer_nmbr}' AND pr_nmbr = '{pr_nmbr}' AND mmbr_nmbr = '{mmbr_nmbr}' 
        AT ISOLATION 0
       """,   

    "checkcampaign": """

        SELECT offer_nmbr, pr_nmbr, count(offer_nmbr) as cnt FROM IDPS..TM_POS_OFFER WHERE offer_nmbr = '{offer_nmbr}' AND pr_nmbr = '{pr_nmbr}' group by offer_nmbr, pr_nmbr
        AT ISOLATION 0
       """,   

    "getCreditSaleByPurcode": """

      DECLARE @as_ymd_from CHAR(8), @as_ymd_to CHAR(8)
      SELECT @as_ymd_from = '{fromDate}'
      SELECT @as_ymd_to   = '{toDate}'

      SELECT T1.PURCODE, (SELECT X.CODENAME FROM IDPS..MSTCOMMON X WHERE X.CDTYPE ='C47' AND T1.PURCODE = X.CODE) AS 'PURNAME' , SUM(T1.SALEAMT) AS 'SALEAMT' FROM
      (
      SELECT (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C03' AND CODE = A.STORECD) AS '점포명',
          A.SALEDATE AS '승인일자',
          A.POSNO    AS '포스번호',
          A.TRANNO   AS '영수증번호',
          A.SEQ      AS '순번',
          A.CARDNO   AS '카드번호',
          A.DIVMONTH AS '할부개월',
          (CASE WHEN A.TRANTYPE = '10' THEN A.SALEAMT ELSE -A.SALEAMT END) AS 'SALEAMT',
          A.APPTIME  AS '승인시간',
          (CASE WHEN A.TRANTYPE = '10' THEN '판매' ELSE '반품' END) AS '승인구분',
          A.APPNO AS '승인번호',
          A.PURCODE,
          (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C18' AND CODE = A.PURCODE) AS '매입사명',
          '신용카드' AS '타입구분'
        FROM IDPS..TRNCARDPAY A, IDPS..TRNSALHDR B
      WHERE A.SALEDATE = B.SALEDATE
        AND A.STORECD  = B.STORECD
        AND A.POSNO    = B.POSNO
        AND A.TRANNO   = B.TRANNO
        AND A.SALEDATE >= @as_ymd_from
        AND A.SALEDATE <= @as_ymd_to
        AND A.STORECD NOT IN ( '072', '077', '063', '064')
        AND A.COMPANY = '1'
        AND B.TRANTYPE IN ('00', '01')
      GROUP BY A.STORECD, A.SALEDATE, A.POSNO, A.TRANNO, A.SEQ, A.CARDNO, A.DIVMONTH, A.TRANTYPE, A.SALEAMT, A.APPTIME, A.APPNO, A.PURCODE
      UNION ALL
      -- 복지카드(직영점)
      --SELECT 10
      SELECT (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C03' AND CODE = A.STORECD) AS '점포명',
          A.SALEDATE AS '승인일자',
          A.POSNO    AS '포스번호',
          A.TRANNO   AS '영수증번호',
          A.SEQ      AS '순번',
          A.CARDNO   AS '카드번호',
          A.DIVMONTH AS '할부개월',
          (CASE WHEN A.TRANTYPE = '10' THEN A.SALEAMT ELSE -A.SALEAMT END) AS 'SALEAMT',
          A.APPTIME  AS '승인시간',
          (CASE WHEN A.TRANTYPE = '10' THEN '판매' ELSE '반품' END) AS '승인구분',
          A.APPNO AS '승인번호',
          A.PURCODE,
          (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C18' AND CODE = A.PURCODE) AS '매입사명',
          '복지카드' AS '타입구분'
        FROM IDPS..TRNWFPAY A, IDPS..TRNSALHDR B
      WHERE A.SALEDATE = B.SALEDATE
        AND A.STORECD  = B.STORECD
        AND A.POSNO    = B.POSNO
        AND A.TRANNO   = B.TRANNO
        AND A.SALEDATE >= @as_ymd_from
        AND A.SALEDATE <= @as_ymd_to
        AND A.STORECD NOT IN ('072', '077', '063', '064')
        AND A.COMPANY = '1'
        AND B.TRANTYPE IN ('00', '01')
      GROUP BY A.STORECD, A.SALEDATE, A.POSNO, A.TRANNO, A.SEQ, A.CARDNO, A.DIVMONTH, A.TRANTYPE, A.SALEAMT, A.APPTIME, A.APPNO, A.PURCODE ) T1

      GROUP BY T1.PURCODE
      ORDER BY 1
      AT ISOLATION 0

    """,   

    "getCreditSaleFranchiseeByPurcode": """

      DECLARE @as_ymd_from CHAR(8), @as_ymd_to CHAR(8)
      SELECT @as_ymd_from = '{fromDate}'
      SELECT @as_ymd_to   = '{toDate}'

      SELECT T1.PURCODE, (SELECT X.CODENAME FROM IDPS..MSTCOMMON X WHERE X.CDTYPE ='C47' AND T1.PURCODE = X.CODE) AS 'PURNAME' , SUM(T1.SALEAMT) AS 'SALEAMT' FROM
      (
      SELECT (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C03' AND CODE = A.STORECD) AS '점포명',
          A.SALEDATE AS '승인일자',
          A.POSNO    AS '포스번호',
          A.TRANNO   AS '영수증번호',
          A.SEQ      AS '순번',
          A.CARDNO   AS '카드번호',
          A.DIVMONTH AS '할부개월',
          (CASE WHEN A.TRANTYPE = '10' THEN A.SALEAMT ELSE -A.SALEAMT END) AS 'SALEAMT',
          A.APPTIME  AS '승인시간',
          (CASE WHEN A.TRANTYPE = '10' THEN '판매' ELSE '반품' END) AS '승인구분',
          A.APPNO AS '승인번호',
          A.PURCODE,
          (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C18' AND CODE = A.PURCODE) AS '매입사명',
          '신용카드' AS '타입구분'
        FROM IDPS..TRNCARDPAY A, IDPS..TRNSALHDR B
      WHERE A.SALEDATE = B.SALEDATE
        AND A.STORECD  = B.STORECD
        AND A.POSNO    = B.POSNO
        AND A.TRANNO   = B.TRANNO
        AND A.SALEDATE >= @as_ymd_from
        AND A.SALEDATE <= @as_ymd_to
        AND A.STORECD IN ('072', '077')
        AND A.COMPANY = '1'
        AND B.TRANTYPE IN ('00', '01')
      GROUP BY A.STORECD, A.SALEDATE, A.POSNO, A.TRANNO, A.SEQ, A.CARDNO, A.DIVMONTH, A.TRANTYPE, A.SALEAMT, A.APPTIME, A.APPNO, A.PURCODE
      UNION ALL
      -- 복지카드(직영점)
      --SELECT 10
      SELECT (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C03' AND CODE = A.STORECD) AS '점포명',
          A.SALEDATE AS '승인일자',
          A.POSNO    AS '포스번호',
          A.TRANNO   AS '영수증번호',
          A.SEQ      AS '순번',
          A.CARDNO   AS '카드번호',
          A.DIVMONTH AS '할부개월',
          (CASE WHEN A.TRANTYPE = '10' THEN A.SALEAMT ELSE -A.SALEAMT END) AS 'SALEAMT',
          A.APPTIME  AS '승인시간',
          (CASE WHEN A.TRANTYPE = '10' THEN '판매' ELSE '반품' END) AS '승인구분',
          A.APPNO AS '승인번호',
          A.PURCODE,
          (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C18' AND CODE = A.PURCODE) AS '매입사명',
          '복지카드' AS '타입구분'
        FROM IDPS..TRNWFPAY A, IDPS..TRNSALHDR B
      WHERE A.SALEDATE = B.SALEDATE
        AND A.STORECD  = B.STORECD
        AND A.POSNO    = B.POSNO
        AND A.TRANNO   = B.TRANNO
        AND A.SALEDATE >= @as_ymd_from
        AND A.SALEDATE <= @as_ymd_to
        AND A.STORECD IN ('072', '077')
        AND A.COMPANY = '1'
        AND B.TRANTYPE IN ('00', '01')
      GROUP BY A.STORECD, A.SALEDATE, A.POSNO, A.TRANNO, A.SEQ, A.CARDNO, A.DIVMONTH, A.TRANTYPE, A.SALEAMT, A.APPTIME, A.APPNO, A.PURCODE ) T1

      GROUP BY T1.PURCODE
      ORDER BY 1
      AT ISOLATION 0

    """,       

    "getCreditSaleGrouping": """
        DECLARE @as_ymd_from CHAR(8), @as_ymd_to CHAR(8)
        SELECT @as_ymd_from =  '{fromDate}'
        SELECT @as_ymd_to   =  '{toDate}'


        SELECT T1.SALEDATE, 
              SUM(CASE WHEN T1.PURCODE ='02' THEN (CASE WHEN T1.TRANTYPE ='10' THEN T1.SALEAMT ELSE -T1.SALEAMT END) ELSE 0 END ) AS 'KB', 
              SUM(CASE WHEN T1.PURCODE ='45' THEN (CASE WHEN T1.TRANTYPE ='10' THEN T1.SALEAMT ELSE -T1.SALEAMT END) ELSE 0 END ) AS 'NH', 
              SUM(CASE WHEN T1.PURCODE ='13' THEN (CASE WHEN T1.TRANTYPE ='10' THEN T1.SALEAMT ELSE -T1.SALEAMT END) ELSE 0 END ) AS 'LOTTE', 
              SUM(CASE WHEN T1.PURCODE ='01' THEN (CASE WHEN T1.TRANTYPE ='10' THEN T1.SALEAMT ELSE -T1.SALEAMT END) ELSE 0 END ) AS 'BC', 
              SUM(CASE WHEN T1.PURCODE ='04' THEN (CASE WHEN T1.TRANTYPE ='10' THEN T1.SALEAMT ELSE -T1.SALEAMT END) ELSE 0 END ) AS 'SAMSUNG', 
              SUM(CASE WHEN T1.PURCODE ='05' THEN (CASE WHEN T1.TRANTYPE ='10' THEN T1.SALEAMT ELSE -T1.SALEAMT END) ELSE 0 END ) AS 'SHINHAN', 
              SUM(CASE WHEN T1.PURCODE ='06' THEN (CASE WHEN T1.TRANTYPE ='10' THEN T1.SALEAMT ELSE -T1.SALEAMT END) ELSE 0 END ) AS 'WORI', 
              SUM(CASE WHEN T1.PURCODE ='03' THEN (CASE WHEN T1.TRANTYPE ='10' THEN T1.SALEAMT ELSE -T1.SALEAMT END) ELSE 0 END ) AS 'KEB', 
              SUM(CASE WHEN T1.PURCODE ='97' THEN (CASE WHEN T1.TRANTYPE ='10' THEN T1.SALEAMT ELSE -T1.SALEAMT END) ELSE 0 END ) AS 'PAYCO',
              SUM(CASE WHEN T1.PURCODE ='90' THEN (CASE WHEN T1.TRANTYPE ='10' THEN T1.SALEAMT ELSE -T1.SALEAMT END) ELSE 0 END ) AS 'HDEPART', 
              SUM(CASE WHEN T1.PURCODE ='12' THEN (CASE WHEN T1.TRANTYPE ='10' THEN T1.SALEAMT ELSE -T1.SALEAMT END) ELSE 0 END ) AS 'HYUNDAI' 
                FROM
        (
        SELECT A.SALEDATE,
            A.PURCODE,
            A.SALEAMT,
            A.TRANTYPE,
            (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C18' AND CODE = A.PURCODE) AS '매입사명',
            '신용카드' AS '타입구분'
          FROM IDPS..TRNCARDPAY A, IDPS..TRNSALHDR B
        WHERE A.SALEDATE = B.SALEDATE
          AND A.STORECD  = B.STORECD
          AND A.POSNO    = B.POSNO
          AND A.TRANNO   = B.TRANNO
          AND A.SALEDATE >= @as_ymd_from
          AND A.SALEDATE <= @as_ymd_to
          AND A.STORECD NOT IN ( '072', '077', '063', '064')
          AND A.COMPANY = '1'
          AND B.TRANTYPE IN ('00', '01')
        UNION ALL
        -- 복지카드(직영점)
        --SELECT 10
        SELECT A.SALEDATE,
            A.PURCODE,
            A.SALEAMT,
            A.TRANTYPE,
            (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C18' AND CODE = A.PURCODE) AS '매입사명',
            '복지카드' AS '타입구분'
          FROM IDPS..TRNWFPAY A, IDPS..TRNSALHDR B
        WHERE A.SALEDATE = B.SALEDATE	
          AND A.STORECD  = B.STORECD
          AND A.POSNO    = B.POSNO
          AND A.TRANNO   = B.TRANNO
          AND A.SALEDATE >= @as_ymd_from
          AND A.SALEDATE <= @as_ymd_to
          AND A.STORECD NOT IN ('072', '077', '063', '064')
          AND A.COMPANY = '1'
          AND B.TRANTYPE IN ('00', '01')
        -- 
        
        ) T1
        
        GROUP BY T1.SALEDATE
        ORDER BY 1 
        AT ISOLATION 0

    """,


    "getCreditSaleFranchiseeGrouping": """
        DECLARE @as_ymd_from CHAR(8), @as_ymd_to CHAR(8)
        SELECT @as_ymd_from =  '{fromDate}'
        SELECT @as_ymd_to   =  '{toDate}'


        SELECT T1.SALEDATE, 
              SUM(CASE WHEN T1.PURCODE ='02' THEN (CASE WHEN T1.TRANTYPE ='10' THEN T1.SALEAMT ELSE -T1.SALEAMT END) ELSE 0 END ) AS 'KB', 
              SUM(CASE WHEN T1.PURCODE ='45' THEN (CASE WHEN T1.TRANTYPE ='10' THEN T1.SALEAMT ELSE -T1.SALEAMT END) ELSE 0 END ) AS 'NH', 
              SUM(CASE WHEN T1.PURCODE ='13' THEN (CASE WHEN T1.TRANTYPE ='10' THEN T1.SALEAMT ELSE -T1.SALEAMT END) ELSE 0 END ) AS 'LOTTE', 
              SUM(CASE WHEN T1.PURCODE ='01' THEN (CASE WHEN T1.TRANTYPE ='10' THEN T1.SALEAMT ELSE -T1.SALEAMT END) ELSE 0 END ) AS 'BC', 
              SUM(CASE WHEN T1.PURCODE ='04' THEN (CASE WHEN T1.TRANTYPE ='10' THEN T1.SALEAMT ELSE -T1.SALEAMT END) ELSE 0 END ) AS 'SAMSUNG', 
              SUM(CASE WHEN T1.PURCODE ='05' THEN (CASE WHEN T1.TRANTYPE ='10' THEN T1.SALEAMT ELSE -T1.SALEAMT END) ELSE 0 END ) AS 'SHINHAN', 
              SUM(CASE WHEN T1.PURCODE ='03' THEN (CASE WHEN T1.TRANTYPE ='10' THEN T1.SALEAMT ELSE -T1.SALEAMT END) ELSE 0 END ) AS 'KEB', 
              SUM(CASE WHEN T1.PURCODE ='97' THEN (CASE WHEN T1.TRANTYPE ='10' THEN T1.SALEAMT ELSE -T1.SALEAMT END) ELSE 0 END ) AS 'PAYCO',
              SUM(CASE WHEN T1.PURCODE ='90' THEN (CASE WHEN T1.TRANTYPE ='10' THEN T1.SALEAMT ELSE -T1.SALEAMT END) ELSE 0 END ) AS 'HDEPART', 
              SUM(CASE WHEN T1.PURCODE ='12' THEN (CASE WHEN T1.TRANTYPE ='10' THEN T1.SALEAMT ELSE -T1.SALEAMT END) ELSE 0 END ) AS 'HYUNDAI' 
                FROM
        (
        SELECT A.SALEDATE,
            A.PURCODE,
            A.SALEAMT,
            A.TRANTYPE,
            (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C18' AND CODE = A.PURCODE) AS '매입사명',
            '신용카드' AS '타입구분'
          FROM IDPS..TRNCARDPAY A, IDPS..TRNSALHDR B
        WHERE A.SALEDATE = B.SALEDATE
          AND A.STORECD  = B.STORECD
          AND A.POSNO    = B.POSNO
          AND A.TRANNO   = B.TRANNO
          AND A.SALEDATE >= @as_ymd_from
          AND A.SALEDATE <= @as_ymd_to
          AND A.STORECD IN ('072', '077')
          AND A.COMPANY = '1'
          AND B.TRANTYPE IN ('00', '01')
        UNION ALL
        -- 복지카드(직영점)
        --SELECT 10
        SELECT A.SALEDATE,
            A.PURCODE,
            A.SALEAMT,
            A.TRANTYPE,
            (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C18' AND CODE = A.PURCODE) AS '매입사명',
            '복지카드' AS '타입구분'
          FROM IDPS..TRNWFPAY A, IDPS..TRNSALHDR B
        WHERE A.SALEDATE = B.SALEDATE	
          AND A.STORECD  = B.STORECD
          AND A.POSNO    = B.POSNO
          AND A.TRANNO   = B.TRANNO
          AND A.SALEDATE >= @as_ymd_from
          AND A.SALEDATE <= @as_ymd_to
          AND A.STORECD IN ('072', '077')
          AND A.COMPANY = '1'
          AND B.TRANTYPE IN ('00', '01')
        -- 
        
        ) T1
        
        GROUP BY T1.SALEDATE
        ORDER BY 1 
        AT ISOLATION 0

    """,    

    "getCreditSaleDetail": """


        DECLARE @as_ymd_from CHAR(8), @as_ymd_to CHAR(8)
        SELECT @as_ymd_from =  '{fromDate}'
        SELECT @as_ymd_to   =  '{toDate}'


            --통합포스(직영점)
            SELECT (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C03' AND CODE = A.STORECD) AS 'STORECD',
                A.SALEDATE AS 'SALEDATE',
                A.POSNO    AS 'POSNO',
                A.TRANNO   AS 'TRANNO',
                A.SEQ      AS 'SEQ',
                A.CARDNO   AS 'CARDNO',
                A.DIVMONTH AS 'DIVMONTH',
                (CASE WHEN A.TRANTYPE = '10' THEN A.SALEAMT ELSE -A.SALEAMT END) AS 'SALEAMT',
                A.APPTIME  AS 'APPTIME',
                (CASE WHEN A.TRANTYPE = '10' THEN '판매' ELSE '반품' END) AS 'TRANTYPE',
                A.APPNO AS 'APPNO',
                (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C18' AND CODE = A.PURCODE) AS 'PURNAME',
                '신용카드' AS 'CARDTYPE'
              FROM IDPS..TRNCARDPAY A, IDPS..TRNSALHDR B
            WHERE A.SALEDATE = B.SALEDATE
              AND A.STORECD  = B.STORECD
              AND A.POSNO    = B.POSNO
              AND A.TRANNO   = B.TRANNO
              AND A.SALEDATE >= @as_ymd_from
              AND A.SALEDATE <= @as_ymd_to
              AND A.STORECD NOT IN ('072', '077', '063', '064')
              AND A.COMPANY = '1'
              AND B.TRANTYPE IN ('00', '01')
            GROUP BY A.STORECD, A.SALEDATE, A.POSNO, A.TRANNO, A.SEQ, A.CARDNO, A.DIVMONTH, A.TRANTYPE, A.SALEAMT, A.APPTIME, A.APPNO, A.PURCODE
            UNION ALL
            -- 복지카드(직영점)
            --SELECT 10
            SELECT (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C03' AND CODE = A.STORECD) AS 'STORECD',
                A.SALEDATE AS 'SALEDATE',
                A.POSNO    AS 'POSNO',
                A.TRANNO   AS 'TRANNO',
                A.SEQ      AS 'SEQ',
                A.CARDNO   AS 'CARDNO',
                A.DIVMONTH AS 'DIVMONTH',
                (CASE WHEN A.TRANTYPE = '10' THEN A.SALEAMT ELSE -A.SALEAMT END) AS 'SALEAMT',
                A.APPTIME  AS 'APPTIME',
                (CASE WHEN A.TRANTYPE = '10' THEN '판매' ELSE '반품' END) AS 'TRANTYPE',
                A.APPNO AS 'APPNO',
                (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C18' AND CODE = A.PURCODE) AS 'PURNAME',
                '복지카드' AS 'CARDTYPE'
              FROM IDPS..TRNWFPAY A, IDPS..TRNSALHDR B
            WHERE A.SALEDATE = B.SALEDATE	
              AND A.STORECD  = B.STORECD
              AND A.POSNO    = B.POSNO
              AND A.TRANNO   = B.TRANNO
              AND A.SALEDATE >= @as_ymd_from
              AND A.SALEDATE <= @as_ymd_to
              AND A.STORECD NOT IN ('072', '077', '063', '064')
              AND A.COMPANY = '1'
              AND B.TRANTYPE IN ('00', '01')
            GROUP BY A.STORECD, A.SALEDATE, A.POSNO, A.TRANNO, A.SEQ, A.CARDNO, A.DIVMONTH, A.TRANTYPE, A.SALEAMT, A.APPTIME, A.APPNO, A.PURCODE
            AT ISOLATION 0

    """,   

    "getCreditSaleFranchiseeDetail": """


        DECLARE @as_ymd_from CHAR(8), @as_ymd_to CHAR(8)
        SELECT @as_ymd_from =  '{fromDate}'
        SELECT @as_ymd_to   =  '{toDate}'


            --통합포스(직영점)
            SELECT (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C03' AND CODE = A.STORECD) AS 'STORECD',
                A.SALEDATE AS 'SALEDATE',
                A.POSNO    AS 'POSNO',
                A.TRANNO   AS 'TRANNO',
                A.SEQ      AS 'SEQ',
                A.CARDNO   AS 'CARDNO',
                A.DIVMONTH AS 'DIVMONTH',
                (CASE WHEN A.TRANTYPE = '10' THEN A.SALEAMT ELSE -A.SALEAMT END) AS 'SALEAMT',
                A.APPTIME  AS 'APPTIME',
                (CASE WHEN A.TRANTYPE = '10' THEN '판매' ELSE '반품' END) AS 'TRANTYPE',
                A.APPNO AS 'APPNO',
                (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C18' AND CODE = A.PURCODE) AS 'PURNAME',
                '신용카드' AS 'CARDTYPE'
              FROM IDPS..TRNCARDPAY A, IDPS..TRNSALHDR B
            WHERE A.SALEDATE = B.SALEDATE
              AND A.STORECD  = B.STORECD
              AND A.POSNO    = B.POSNO
              AND A.TRANNO   = B.TRANNO
              AND A.SALEDATE >= @as_ymd_from
              AND A.SALEDATE <= @as_ymd_to
              AND A.STORECD IN ('072', '077')
              AND A.COMPANY = '1'
              AND B.TRANTYPE IN ('00', '01')
            GROUP BY A.STORECD, A.SALEDATE, A.POSNO, A.TRANNO, A.SEQ, A.CARDNO, A.DIVMONTH, A.TRANTYPE, A.SALEAMT, A.APPTIME, A.APPNO, A.PURCODE
            UNION ALL
            -- 복지카드(직영점)
            --SELECT 10
            SELECT (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C03' AND CODE = A.STORECD) AS 'STORECD',
                A.SALEDATE AS 'SALEDATE',
                A.POSNO    AS 'POSNO',
                A.TRANNO   AS 'TRANNO',
                A.SEQ      AS 'SEQ',
                A.CARDNO   AS 'CARDNO',
                A.DIVMONTH AS 'DIVMONTH',
                (CASE WHEN A.TRANTYPE = '10' THEN A.SALEAMT ELSE -A.SALEAMT END) AS 'SALEAMT',
                A.APPTIME  AS 'APPTIME',
                (CASE WHEN A.TRANTYPE = '10' THEN '판매' ELSE '반품' END) AS 'TRANTYPE',
                A.APPNO AS 'APPNO',
                (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C18' AND CODE = A.PURCODE) AS 'PURNAME',
                '복지카드' AS 'CARDTYPE'
              FROM IDPS..TRNWFPAY A, IDPS..TRNSALHDR B
            WHERE A.SALEDATE = B.SALEDATE	
              AND A.STORECD  = B.STORECD
              AND A.POSNO    = B.POSNO
              AND A.TRANNO   = B.TRANNO
              AND A.SALEDATE >= @as_ymd_from
              AND A.SALEDATE <= @as_ymd_to
              AND A.STORECD IN ('072', '077')
              AND A.COMPANY = '1'
              AND B.TRANTYPE IN ('00', '01')
            GROUP BY A.STORECD, A.SALEDATE, A.POSNO, A.TRANNO, A.SEQ, A.CARDNO, A.DIVMONTH, A.TRANTYPE, A.SALEAMT, A.APPTIME, A.APPNO, A.PURCODE
            AT ISOLATION 0

    """,       

    "getmileagespend": """

      SELECT A.SALEDATE AS 'PROCDATE',
            A.STORECD  AS 'STORECD',
            (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0') AS 'STORENM',
          (CASE WHEN SUBSTRING(A.POSNO, 3, 2) <= '50' THEN '교보문고POS' ELSE '핫트랙스POS' END)     AS 'POSTYPE',
          '교보문고상품' AS 'PLUTYPE',
          (SELECT ETC1NM FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0')   AS 'ETC1NM',
          '마일리지' AS 'MILEAGETYPE',
          (CASE WHEN B.TRANTYPE = '00' THEN '사용' ELSE '사용취소' END) AS 'TRANTYPE',
          SUM((CASE WHEN B.TRANTYPE = '01' THEN A.SALEAMT ELSE A.SALEAMT * -1 END)) AS 'APPAMT',
          0 AS 'DEP1',
          0 AS 'DEP2',
          0 AS 'DEP3',
          '사용/사용취소' AS 'SAVETYPE'
        FROM IDPS..TRNSALPAY A, IDPS..TRNSALHDR B
      WHERE A.SALEDATE    = B.SALEDATE
        AND A.STORECD     = B.STORECD
        AND A.POSNO       = B.POSNO
        AND A.TRANNO      = B.TRANNO
        AND A.SALEDATE   >= '{fromDate}'
        AND A.SALEDATE   <= '{toDate}'
      --   AND A.STORECD     = '033'
        AND A.STORECD    NOT IN ('013', '023', '063') --부천 수유 제외 MOD 20211014 LJY, 건대 추가 
        AND A.TENDERTYPE  = '04'
        AND A.SALEAMTM   <> 0
        AND B.TRANTYPE   IN ('00', '01')
      GROUP BY A.SALEDATE, A.STORECD
            ,(CASE WHEN SUBSTRING(A.POSNO, 3, 2) <= '50' THEN '교보문고POS' ELSE '핫트랙스POS' END)
            ,(CASE WHEN B.TRANTYPE = '00' THEN '사용' ELSE '사용취소' END)
      UNION ALL
      SELECT A.SALEDATE AS 'PROCDATE',
            A.STORECD  AS 'STORECD',
            (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0') AS 'STORENM',
          (CASE WHEN SUBSTRING(A.POSNO, 3, 2) <= '50' THEN '교보문고POS' ELSE '핫트랙스POS' END) AS 'POSTYPE',
          '핫트랙스상품' AS 'PLUTYPE',
          (SELECT ETC1NM FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0') AS 'ETC1NM',
          '마일리지' AS 'MILEAGETYPE',
          (CASE WHEN B.TRANTYPE = '00' THEN '사용' ELSE '사용취소' END) AS 'TRANTYPE',
          SUM((CASE WHEN B.TRANTYPE = '01' THEN A.SALEAMT ELSE A.SALEAMT * -1 END)) AS 'APPAMT',
          0 AS 'DEP1',
          0 AS 'DEP2',
          0 AS 'DEP3',
          '사용/사용취소' AS 'SAVETYPE'
        FROM IDPS..TRNSALPAY A, IDPS..TRNSALHDR B
      WHERE A.SALEDATE    = B.SALEDATE
        AND A.STORECD     = B.STORECD
        AND A.POSNO       = B.POSNO
        AND A.TRANNO      = B.TRANNO
        AND A.SALEDATE   >= '{fromDate}'
        AND A.SALEDATE   <= '{toDate}'
      --   AND A.STORECD     = '033'
        AND A.STORECD    NOT IN ('013', '023', '063') --부천 수유 제외  MOD 20211014 LJY, 건대 추가 
        AND A.TENDERTYPE  = '04'
        AND A.SALEAMTH   <> 0
        AND B.TRANTYPE   IN ('00', '01')
      GROUP BY A.SALEDATE, A.STORECD
            ,(CASE WHEN SUBSTRING(A.POSNO, 3, 2) <= '50' THEN '교보문고POS' ELSE '핫트랙스POS' END)
            ,(CASE WHEN B.TRANTYPE = '00' THEN '사용' ELSE '사용취소' END)
      UNION ALL
      SELECT A.SALEDATE AS 'PROCDATE',
            A.STORECD  AS 'STORECD',
            (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0') AS 'STORENM',
          '교보문고POS'  AS 'POSTYPE',
          '교보문고상품' AS 'PLUTYPE',
          (SELECT ETC1NM FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0') AS 'ETC1NM',
          '마일리지' AS 'MILEAGETYPE',
          (CASE WHEN B.TRANTYPE = '00' THEN '사용' ELSE '사용취소' END) AS 'TRANTYPE',
          SUM((CASE WHEN B.TRANTYPE = '01' THEN A.SALEAMT ELSE A.SALEAMT * -1 END)) AS 'APPAMT',
          0 AS 'DEP1',
          0 AS 'DEP2',
          0 AS 'DEP3',
          '사용/사용취소' AS 'SAVETYPE'
        FROM IDPS..TRNSALPAY A, IDPS..TRNSALHDR B
      WHERE A.SALEDATE    = B.SALEDATE
        AND A.STORECD     = B.STORECD
        AND A.POSNO       = B.POSNO
        AND A.TRANNO      = B.TRANNO
        AND A.SALEDATE   >= '{fromDate}'
        AND A.SALEDATE   <= '{toDate}'
        AND A.STORECD     IN ('013', '023') -- 부천만   MOD 20211014 LJY, 건대 추가 
        AND A.TENDERTYPE  = '04'
        AND B.TRANTYPE   IN ('00', '01')
      GROUP BY A.SALEDATE, A.STORECD
            ,(CASE WHEN B.TRANTYPE = '00' THEN '사용' ELSE '사용취소' END)
      UNION ALL
      SELECT A.SALEDATE AS 'PROCDATE',
            A.STORECD  AS 'STORECD',
            (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0') AS 'STORENM',
          '핫트랙스POS'  AS 'POSTYPE',
          '핫트랙스상품' AS 'PLUTYPE',
          (SELECT ETC1NM FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0') AS 'ETC1NM',
          '마일리지' AS 'MILEAGETYPE',
          (CASE WHEN B.TRANTYPE = '00' THEN '사용' ELSE '사용취소' END) AS 'TRANTYPE',
          SUM((CASE WHEN B.TRANTYPE = '01' THEN A.SALEAMT ELSE A.SALEAMT * -1 END)) AS 'APPAMT',
          0 AS 'DEP1',
          0 AS 'DEP2',
          0 AS 'DEP3',
          '사용/사용취소' AS 'SAVETYPE'
        FROM IDPS..TRNSALPAY A, IDPS..TRNSALHDR B
      WHERE A.SALEDATE    = B.SALEDATE
        AND A.STORECD     = B.STORECD
        AND A.POSNO       = B.POSNO
        AND A.TRANNO      = B.TRANNO
        AND A.SALEDATE   >= '{fromDate}'
        AND A.SALEDATE   <= '{toDate}'
        AND A.STORECD     = '063' -- 수유만
        AND A.TENDERTYPE  = '04'
        AND B.TRANTYPE   IN ('00', '01')
      GROUP BY A.SALEDATE, A.STORECD
            ,(CASE WHEN B.TRANTYPE = '00' THEN '사용' ELSE '사용취소' END)
      UNION ALL 		  
      --통합포인트 사용/사용취소
      SELECT A.SALEDATE AS 'PROCDATE',
            A.STORECD  AS 'STORECD',
            (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0') AS 'STORENM',
          (CASE WHEN SUBSTRING(A.POSNO, 3, 2) <= '50' THEN '교보문고POS' ELSE '핫트랙스POS' END) AS 'POSTYPE',
          '교보문고상품' AS 'PLUTYPE',
          (SELECT ETC1NM FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0') AS 'ETC1NM',
          '통합포인트' AS 'MILEAGETYPE',
          (CASE WHEN B.TRANTYPE = '00' THEN '사용' ELSE '사용취소' END) AS 'TRANTYPE',
          SUM((CASE WHEN B.TRANTYPE = '01' THEN A.SALEAMT ELSE A.SALEAMT * -1 END)) AS 'APPAMT',
          0 AS 'DEP1',
          0 AS 'DEP2',
          0 AS 'DEP3',
          '사용/사용취소' AS 'SAVETYPE'
        FROM IDPS..TRNSALPAY A, IDPS..TRNSALHDR B
      WHERE A.SALEDATE    = B.SALEDATE
        AND A.STORECD     = B.STORECD
        AND A.POSNO       = B.POSNO
        AND A.TRANNO      = B.TRANNO
        AND A.SALEDATE   >= '{fromDate}'
        AND A.SALEDATE   <= '{toDate}'
      --   AND A.STORECD     = '033'
        AND A.STORECD    NOT IN ('013', '023', '063') -- MOD 20211014 LJY, 건대 추가 
        AND A.TENDERTYPE  = '29'
        AND A.SALEAMTM   <> 0
        AND B.TRANTYPE   IN ('00', '01')
      GROUP BY A.SALEDATE, A.STORECD
            ,(CASE WHEN SUBSTRING(A.POSNO, 3, 2) <= '50' THEN '교보문고POS' ELSE '핫트랙스POS' END)
            ,(CASE WHEN B.TRANTYPE = '00' THEN '사용' ELSE '사용취소' END)
      UNION ALL
      SELECT A.SALEDATE AS 'PROCDATE',
            A.STORECD  AS 'STORECD',
            (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0') AS 'STORENM',
          (CASE WHEN SUBSTRING(A.POSNO, 3, 2) <= '50' THEN '교보문고POS' ELSE '핫트랙스POS' END) AS 'POSTYPE',
          '핫트랙스상품' AS 'PLUTYPE',
          (SELECT ETC1NM FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0') AS 'ETC1NM',
          '통합포인트' AS 'MILEAGETYPE',
          (CASE WHEN B.TRANTYPE = '00' THEN '사용' ELSE '사용취소' END) AS 'TRANTYPE',
          SUM((CASE WHEN B.TRANTYPE = '01' THEN A.SALEAMT ELSE A.SALEAMT * -1 END)) AS 'APPAMT',
          0 AS 'DEP1',
          0 AS 'DEP2',
          0 AS 'DEP3',
          '사용/사용취소' AS 'SAVETYPE'
        FROM IDPS..TRNSALPAY A, IDPS..TRNSALHDR B
      WHERE A.SALEDATE    = B.SALEDATE
        AND A.STORECD     = B.STORECD
        AND A.POSNO       = B.POSNO
        AND A.TRANNO      = B.TRANNO
        AND A.SALEDATE   >= '{fromDate}'
        AND A.SALEDATE   <= '{toDate}'
      --   AND A.STORECD     = '033'
        AND A.STORECD    NOT IN ('013', '023', '063')
        AND A.TENDERTYPE  = '29'
        AND A.SALEAMTH   <> 0
        AND B.TRANTYPE   IN ('00', '01')
      GROUP BY A.SALEDATE, A.STORECD
            ,(CASE WHEN SUBSTRING(A.POSNO, 3, 2) <= '50' THEN '교보문고POS' ELSE '핫트랙스POS' END)
            ,(CASE WHEN B.TRANTYPE = '00' THEN '사용' ELSE '사용취소' END)
      UNION ALL
      SELECT A.SALEDATE AS 'PROCDATE',
            A.STORECD  AS 'STORECD',
            (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0') AS 'STORENM',
          '교보문고POS'  AS 'POSTYPE',
          '교보문고상품' AS 'PLUTYPE',
          (SELECT ETC1NM FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0') AS 'ETC1NM',
          '통합포인트' AS 'MILEAGETYPE',
          (CASE WHEN B.TRANTYPE = '00' THEN '사용' ELSE '사용취소' END) AS 'TRANTYPE',
          SUM((CASE WHEN B.TRANTYPE = '01' THEN A.SALEAMT ELSE A.SALEAMT * -1 END)) AS 'APPAMT',
          0 AS 'DEP1',
          0 AS 'DEP2',
          0 AS 'DEP3',
          '사용/사용취소' AS 'SAVETYPE'
        FROM IDPS..TRNSALPAY A, IDPS..TRNSALHDR B
      WHERE A.SALEDATE    = B.SALEDATE
        AND A.STORECD     = B.STORECD
        AND A.POSNO       = B.POSNO
        AND A.TRANNO      = B.TRANNO
        AND A.SALEDATE   >= '{fromDate}'
        AND A.SALEDATE   <= '{toDate}'
      --   AND A.STORECD     = '033'
        AND A.STORECD     IN ('013', '023')
        AND A.TENDERTYPE  = '29'
        AND B.TRANTYPE   IN ('00', '01')
      GROUP BY A.SALEDATE, A.STORECD
            ,(CASE WHEN B.TRANTYPE = '00' THEN '사용' ELSE '사용취소' END)
      UNION ALL
      SELECT A.SALEDATE AS 'PROCDATE',
            A.STORECD  AS 'STORECD',
            (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0') AS 'STORENM',
          '핫트랙스POS'  AS 'POSTYPE',
          '핫트랙스상품' AS 'PLUTYPE',
          (SELECT ETC1NM FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0') AS 'ETC1NM',
          '통합포인트' AS 'MILEAGETYPE',
          (CASE WHEN B.TRANTYPE = '00' THEN '사용' ELSE '사용취소' END) AS 'TRANTYPE',
          SUM((CASE WHEN B.TRANTYPE = '01' THEN A.SALEAMT ELSE A.SALEAMT * -1 END)) AS 'APPAMT',
          0 AS 'DEP1',
          0 AS 'DEP2',
          0 AS 'DEP3',
          '사용/사용취소' AS 'SAVETYPE'
        FROM IDPS..TRNSALPAY A, IDPS..TRNSALHDR B
      WHERE A.SALEDATE    = B.SALEDATE
        AND A.STORECD     = B.STORECD
        AND A.POSNO       = B.POSNO
        AND A.TRANNO      = B.TRANNO
        AND A.SALEDATE   >= '{fromDate}'
        AND A.SALEDATE   <= '{toDate}'
      --   AND A.STORECD     = '033'
        AND A.STORECD     = '063'
        AND A.TENDERTYPE  = '29'
        AND B.TRANTYPE   IN ('00', '01')
      GROUP BY A.SALEDATE, A.STORECD
            ,(CASE WHEN B.TRANTYPE = '00' THEN '사용' ELSE '사용취소' END)
      ORDER BY 1,2,4,8,5
      AT ISOLATION 0
    """,   



    

    "getmileagesave": """


      SELECT A.SALEDATE AS 'PROCDATE',
            A.STORECD  AS 'STORECD',
            (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0') AS 'STORENM',
          (CASE WHEN SUBSTRING(A.POSNO, 3, 2) <= '50' THEN '교보문고POS' ELSE '핫트랙스POS' END)     AS 'POSTYPE',
          '교보문고상품' AS 'PLUTYPE',
          (SELECT ETC1NM FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0')   AS 'ETC1NM',
          '통합포인트' AS 'MILEAGETYPE',
          (CASE WHEN B.TRANTYPE = '00' THEN '적립' ELSE '적립취소' END) AS 'TRANTYPE',
          0 AS 'APPAMT',
          SUM((CASE WHEN B.TRANTYPE = '00' THEN A.DEPMILEAGE1 ELSE A.DEPMILEAGE1 * -1 END)) AS 'DEP1',
          SUM((CASE WHEN B.TRANTYPE = '00' THEN A.DEPMILEAGE2 ELSE A.DEPMILEAGE2 * -1 END)) AS 'DEP2',
          SUM((CASE WHEN B.TRANTYPE = '00' THEN A.DEPMILEAGE3 ELSE A.DEPMILEAGE3 * -1 END)) AS 'DEP3',
          '즉시적립' AS 'SAVETYPE'
        FROM IDPS..TRNKBM A, IDPS..TRNSALHDR B
      WHERE A.SALEDATE    = B.SALEDATE
        AND A.STORECD     = B.STORECD
        AND A.POSNO       = B.POSNO
        AND A.TRANNO      = B.TRANNO
          AND A.SALEDATE   >= '{fromDate}'
          AND A.SALEDATE   <= '{toDate}'
      --   AND A.STORECD     = '033'
        AND A.STORECD     NOT IN ('013', '023', '063') --부천 수유 제외 mod 20211014 LJY , 건대추가 
        AND A.MILEAGETYPE = '2'                 --통합포인트
        AND A.TRANTYPE    IN ('20', '21')       --적립/적립취소
        AND A.COMPANY     = '1'                 --교보문고상품
        AND B.AFTSAVEDATE IS NULL                --즉시적립분 
        AND B.TRANTYPE    IN ('00', '01')
      GROUP BY A.SALEDATE, A.STORECD
            ,(CASE WHEN SUBSTRING(A.POSNO, 3, 2) <= '50' THEN '교보문고POS' ELSE '핫트랙스POS' END)
            ,(CASE WHEN B.TRANTYPE = '00' THEN '적립' ELSE '적립취소' END)
      UNION ALL
      SELECT A.SALEDATE AS 'PROCDATE',
            A.STORECD  AS 'STORECD',
            (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0') AS 'STORENM',
          (CASE WHEN SUBSTRING(A.POSNO, 3, 2) <= '50' THEN '교보문고POS' ELSE '핫트랙스POS' END)     AS 'POSTYPE',
          '핫트랙스상품' AS 'PLUTYPE',
          (SELECT ETC1NM FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0')   AS 'ETC1NM',
          '통합포인트' AS 'MILEAGETYPE',
          (CASE WHEN B.TRANTYPE = '00' THEN '적립' ELSE '적립취소' END) AS 'TRANTYPE',
          0 AS 'APPAMT',
          SUM((CASE WHEN B.TRANTYPE = '00' THEN A.DEPMILEAGE1 ELSE A.DEPMILEAGE1 * -1 END)) AS 'DEP1',
          SUM((CASE WHEN B.TRANTYPE = '00' THEN A.DEPMILEAGE2 ELSE A.DEPMILEAGE2 * -1 END)) AS 'DEP2',
          SUM((CASE WHEN B.TRANTYPE = '00' THEN A.DEPMILEAGE3 ELSE A.DEPMILEAGE3 * -1 END)) AS 'DEP3',
          '즉시적립' AS 'SAVETYPE'
        FROM IDPS..TRNKBM A, IDPS..TRNSALHDR B 
      WHERE A.SALEDATE    = B.SALEDATE
        AND A.STORECD     = B.STORECD
        AND A.POSNO       = B.POSNO
        AND A.TRANNO      = B.TRANNO
          AND A.SALEDATE   >= '{fromDate}'
          AND A.SALEDATE   <= '{toDate}'
      --   AND A.STORECD     = '033'
        AND A.STORECD     NOT IN ('013', '023', '063') --부천 수유 제외  mod 20211014 LJY , 건대추가 
        AND A.MILEAGETYPE = '2'                 --통합포인트
        AND A.TRANTYPE    IN ('20', '21')       --적립/적립취소
        AND A.COMPANY     = '2'                 --핫트랙스상품
        AND B.AFTSAVEDATE IS NULL                --즉시적립분
        AND B.TRANTYPE    IN ('00', '01')
      GROUP BY A.SALEDATE, A.STORECD
            ,(CASE WHEN SUBSTRING(A.POSNO, 3, 2) <= '50' THEN '교보문고POS' ELSE '핫트랙스POS' END)
            ,(CASE WHEN B.TRANTYPE = '00' THEN '적립' ELSE '적립취소' END)
      UNION ALL
      SELECT A.SALEDATE AS 'PROCDATE',
            A.STORECD  AS 'STORECD',
            (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0') AS 'STORENM',
          '교보문고POS'  AS 'POSTYPE',
          '교보문고상품' AS 'PLUTYPE',
          (SELECT ETC1NM FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0')   AS 'ETC1NM',
          '통합포인트' AS 'MILEAGETYPE',
          (CASE WHEN B.TRANTYPE = '00' THEN '적립' ELSE '적립취소' END) AS 'TRANTYPE',
          0 AS 'APPAMT',
          SUM((CASE WHEN B.TRANTYPE = '00' THEN A.DEPMILEAGE1 ELSE A.DEPMILEAGE1 * -1 END)) AS 'DEP1',
          SUM((CASE WHEN B.TRANTYPE = '00' THEN A.DEPMILEAGE2 ELSE A.DEPMILEAGE2 * -1 END)) AS 'DEP2',
          SUM((CASE WHEN B.TRANTYPE = '00' THEN A.DEPMILEAGE3 ELSE A.DEPMILEAGE3 * -1 END)) AS 'DEP3',
          '즉시적립' AS 'SAVETYPE'
        FROM IDPS..TRNKBM A, IDPS..TRNSALHDR B
      WHERE A.SALEDATE    = B.SALEDATE
        AND A.STORECD     = B.STORECD
        AND A.POSNO       = B.POSNO
        AND A.TRANNO      = B.TRANNO
              AND A.SALEDATE   >= '{fromDate}'
              AND A.SALEDATE   <= '{toDate}'
        AND A.STORECD     IN ( '013', '023')
        AND A.MILEAGETYPE = '2'                 --통합포인트
        AND A.TRANTYPE    IN ('20', '21')       --적립/적립취소
        AND B.AFTSAVEDATE IS NULL                --즉시적립분
        AND B.TRANTYPE    IN ('00', '01')
      GROUP BY A.SALEDATE, A.STORECD
            ,(CASE WHEN B.TRANTYPE = '00' THEN '적립' ELSE '적립취소' END)
      UNION ALL
      SELECT A.SALEDATE AS 'PROCDATE',
            A.STORECD  AS 'STORECD',
            (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0') AS 'STORENM',
          '핫트랙스POS'  AS 'POSTYPE',
          '핫트랙스상품' AS 'PLUTYPE',
          (SELECT ETC1NM FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0')   AS 'ETC1NM',
          '통합포인트' AS 'MILEAGETYPE',
          (CASE WHEN B.TRANTYPE = '00' THEN '적립' ELSE '적립취소' END) AS 'TRANTYPE',
          0 AS 'APPAMT',
          SUM((CASE WHEN B.TRANTYPE = '00' THEN A.DEPMILEAGE1 ELSE A.DEPMILEAGE1 * -1 END)) AS 'DEP1',
          SUM((CASE WHEN B.TRANTYPE = '00' THEN A.DEPMILEAGE2 ELSE A.DEPMILEAGE2 * -1 END)) AS 'DEP2',
          SUM((CASE WHEN B.TRANTYPE = '00' THEN A.DEPMILEAGE3 ELSE A.DEPMILEAGE3 * -1 END)) AS 'DEP3',
          '즉시적립' AS 'SAVETYPE'
        FROM IDPS..TRNKBM A, IDPS..TRNSALHDR B
      WHERE A.SALEDATE    = B.SALEDATE
        AND A.STORECD     = B.STORECD
        AND A.POSNO       = B.POSNO
        AND A.TRANNO      = B.TRANNO
              AND A.SALEDATE   >= '{fromDate}'
              AND A.SALEDATE   <= '{toDate}'
        AND A.STORECD     = '063'
        AND A.MILEAGETYPE = '2'                 --통합포인트
        AND A.TRANTYPE    IN ('20', '21')       --적립/적립취소
        AND B.AFTSAVEDATE IS NULL                --즉시적립분
        AND B.TRANTYPE    IN ('00', '01')
      GROUP BY A.SALEDATE, A.STORECD
            ,(CASE WHEN B.TRANTYPE = '00' THEN '적립' ELSE '적립취소' END)
      -- AT ISOLATION 0 
      UNION ALL
      SELECT B.AFTSAVEDATE AS 'PROCDATE',
            B.STORECD  AS 'STORECD',
            (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = B.STORECD AND DELYN = '0') AS 'STORENM',
          (CASE WHEN SUBSTRING(B.POSNO, 3, 2) <= '50' THEN '교보문고POS' ELSE '핫트랙스POS' END)     AS 'POSTYPE',
          '교보문고상품' AS 'PLUTYPE',
          (SELECT ETC1NM FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = B.STORECD AND DELYN = '0')   AS 'ETC1NM',
          '통합포인트' AS 'MILEAGETYPE',
          (CASE WHEN B.TRANTYPE = '00' THEN '적립' ELSE '적립취소' END) AS 'TRANTYPE',
          0 AS 'APPAMT',
          SUM((CASE WHEN B.TRANTYPE = '00' THEN A.DEPMILEAGE1 ELSE A.DEPMILEAGE1 * -1 END)) AS 'DEP1',
          SUM((CASE WHEN B.TRANTYPE = '00' THEN A.DEPMILEAGE2 ELSE A.DEPMILEAGE2 * -1 END)) AS 'DEP2',
          SUM((CASE WHEN B.TRANTYPE = '00' THEN A.DEPMILEAGE3 ELSE A.DEPMILEAGE3 * -1 END)) AS 'DEP3',
          '후적립' AS 'SAVETYPE'
        FROM IDPS..TRNKBM A, IDPS..TRNSALHDR B
      WHERE A.SALEDATE    = B.SALEDATE
        AND A.STORECD     = B.STORECD
        AND A.POSNO       = B.POSNO
        AND A.TRANNO      = B.TRANNO
        AND B.AFTSAVEDATE >= '{fromDate}' --후적립분
        AND B.AFTSAVEDATE <=  '{toDate}'
      --   AND B.STORECD     = '033'
        AND B.TRANTYPE    IN ('00', '01')
        AND B.STORECD     NOT IN ('013', '023', '063') --부천 수유 제외  mod 20211014 LJY , 건대추가 
        AND A.MILEAGETYPE = '2'                 --통합포인트
        AND A.TRANTYPE    IN ('20', '21')       --적립/적립취소
        AND A.COMPANY     = '1'                 --교보문고상품
      GROUP BY B.AFTSAVEDATE, B.STORECD
            ,(CASE WHEN SUBSTRING(B.POSNO, 3, 2) <= '50' THEN '교보문고POS' ELSE '핫트랙스POS' END)
            ,(CASE WHEN B.TRANTYPE = '00' THEN '적립' ELSE '적립취소' END)
      UNION ALL
      SELECT B.AFTSAVEDATE AS 'PROCDATE',
            B.STORECD  AS 'STORECD',
            (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = B.STORECD AND DELYN = '0') AS 'STORENM',
          (CASE WHEN SUBSTRING(B.POSNO, 3, 2) <= '50' THEN '교보문고POS' ELSE '핫트랙스POS' END)     AS 'POSTYPE',
          '핫트랙스상품' AS 'PLUTYPE',
          (SELECT ETC1NM FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = B.STORECD AND DELYN = '0')   AS 'ETC1NM',
          '통합포인트' AS 'MILEAGETYPE',
          (CASE WHEN B.TRANTYPE = '00' THEN '적립' ELSE '적립취소' END) AS 'TRANTYPE',
          0 AS 'APPAMT',
          SUM((CASE WHEN B.TRANTYPE = '00' THEN A.DEPMILEAGE1 ELSE A.DEPMILEAGE1 * -1 END)) AS 'DEP1',
          SUM((CASE WHEN B.TRANTYPE = '00' THEN A.DEPMILEAGE2 ELSE A.DEPMILEAGE2 * -1 END)) AS 'DEP2',
          SUM((CASE WHEN B.TRANTYPE = '00' THEN A.DEPMILEAGE3 ELSE A.DEPMILEAGE3 * -1 END)) AS 'DEP3',
          '후적립' AS 'SAVETYPE'
        FROM IDPS..TRNKBM A, IDPS..TRNSALHDR B
      WHERE A.SALEDATE    = B.SALEDATE
        AND A.STORECD     = B.STORECD
        AND A.POSNO       = B.POSNO
        AND A.TRANNO      = B.TRANNO
        AND B.AFTSAVEDATE >= '{fromDate}' --후적립분
        AND B.AFTSAVEDATE <=  '{toDate}'
      --   AND B.STORECD     = '033'
        AND B.TRANTYPE    IN ('00', '01')
        AND B.STORECD     NOT IN ('013', '023', '063') --부천 수유 제외  mod 20211014 LJY , 건대추가 
        AND A.MILEAGETYPE = '2'                 --통합포인트
        AND A.TRANTYPE    IN ('20', '21')       --적립/적립취소
        AND A.COMPANY     = '2'                 --핫트랙스상품
      GROUP BY B.AFTSAVEDATE, B.STORECD
            ,(CASE WHEN SUBSTRING(B.POSNO, 3, 2) <= '50' THEN '교보문고POS' ELSE '핫트랙스POS' END)
            ,(CASE WHEN B.TRANTYPE = '00' THEN '적립' ELSE '적립취소' END)
      UNION ALL
      SELECT B.AFTSAVEDATE AS 'PROCDATE',
            B.STORECD  AS 'STORECD',
            (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = B.STORECD AND DELYN = '0') AS 'STORENM',
          '교보문고POS'  AS 'POSTYPE',
          '교보문고상품' AS 'PLUTYPE',
          (SELECT ETC1NM FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = B.STORECD AND DELYN = '0')   AS 'ETC1NM',
          '통합포인트' AS 'MILEAGETYPE',
          (CASE WHEN B.TRANTYPE = '00' THEN '적립' ELSE '적립취소' END) AS 'TRANTYPE',
          0 AS 'APPAMT',
          SUM((CASE WHEN B.TRANTYPE = '00' THEN A.DEPMILEAGE1 ELSE A.DEPMILEAGE1 * -1 END)) AS 'DEP1',
          SUM((CASE WHEN B.TRANTYPE = '00' THEN A.DEPMILEAGE2 ELSE A.DEPMILEAGE2 * -1 END)) AS 'DEP2',
          SUM((CASE WHEN B.TRANTYPE = '00' THEN A.DEPMILEAGE3 ELSE A.DEPMILEAGE3 * -1 END)) AS 'DEP3',
          '후적립' AS 'SAVETYPE'
        FROM IDPS..TRNKBM A, IDPS..TRNSALHDR B
      WHERE A.SALEDATE    = B.SALEDATE
        AND A.STORECD     = B.STORECD
        AND A.POSNO       = B.POSNO
        AND A.TRANNO      = B.TRANNO
        AND B.AFTSAVEDATE >= '{fromDate}' --후적립분
        AND B.AFTSAVEDATE <=  '{toDate}'
        AND B.STORECD     IN ('013', '023')
        AND B.TRANTYPE    IN ('00', '01')
        AND A.MILEAGETYPE = '2'                 --통합포인트
        AND A.TRANTYPE    IN ('20', '21')       --적립/적립취소
      GROUP BY B.AFTSAVEDATE, B.STORECD
            ,(CASE WHEN B.TRANTYPE = '00' THEN '적립' ELSE '적립취소' END)
      UNION ALL
      SELECT B.AFTSAVEDATE AS 'PROCDATE',
            B.STORECD  AS 'STORECD',
            (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = B.STORECD AND DELYN = '0') AS 'STORENM',
          '핫트랙스POS'  AS 'POSTYPE',
          '핫트랙스상품' AS 'PLUTYPE',
          (SELECT ETC1NM FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = B.STORECD AND DELYN = '0')   AS 'ETC1NM',
          '통합포인트' AS 'MILEAGETYPE',
          (CASE WHEN B.TRANTYPE = '00' THEN '적립' ELSE '적립취소' END) AS 'TRANTYPE',
          0 AS 'APPAMT',
          SUM((CASE WHEN B.TRANTYPE = '00' THEN A.DEPMILEAGE1 ELSE A.DEPMILEAGE1 * -1 END)) AS 'DEP1',
          SUM((CASE WHEN B.TRANTYPE = '00' THEN A.DEPMILEAGE2 ELSE A.DEPMILEAGE2 * -1 END)) AS 'DEP2',
          SUM((CASE WHEN B.TRANTYPE = '00' THEN A.DEPMILEAGE3 ELSE A.DEPMILEAGE3 * -1 END)) AS 'DEP3',
          '후적립' AS 'SAVETYPE'
        FROM IDPS..TRNKBM A, IDPS..TRNSALHDR B
      WHERE A.SALEDATE    = B.SALEDATE
        AND A.STORECD     = B.STORECD
        AND A.POSNO       = B.POSNO
        AND A.TRANNO      = B.TRANNO
        AND B.AFTSAVEDATE >= '{fromDate}' --후적립분
        AND B.AFTSAVEDATE <=  '{toDate}'
        AND B.STORECD     = '063'
        AND B.TRANTYPE    IN ('00', '01')
        AND A.MILEAGETYPE = '2'                 --통합포인트
        AND A.TRANTYPE    IN ('20', '21')       --적립/적립취소
      GROUP BY B.AFTSAVEDATE, B.STORECD
            ,(CASE WHEN B.TRANTYPE = '00' THEN '적립' ELSE '적립취소' END)
      ORDER BY 1,2,4,8,5
      AT ISOLATION 0


    """,   

    "getvendorcommissiondtl": """
     SELECT (CASE WHEN t1.TRANKIND ='01' THEN '직원판매'
                  ELSE                        '일반판매'
             END)                                                                    AS slsDvsn,
            t1.SALEDATE                                                              AS slsDate,
            (CASE WHEN t4.PBCMCODE IS NOT NULL THEN t4.PBCMCODE
                  ELSE                              '미등록'
             END) AS pbcmCode,
            (CASE WHEN t4.PBCMCODE IS NOT NULL THEN (SELECT pbcm_name
                                                       FROM IDCM..TM_PBCM
                                                      WHERE pbcm_code = t4.PBCMCODE)
                  ELSE                              '미등록'
             END)                                                                    AS pbcmName,
            t3.PLUBARCD                                                              AS cmdtCode,
            t2.NAME                                                                  AS cmdtName,
            t3.NORPRICE                                                              AS cmdtPrce,
            t4.UNITPRICE                                                             AS cmdtPrceAddDsrp,
            convert(NUMERIC, t4.ETCINFO1)                                            AS byngRate,
            100 - convert(NUMERIC,t4.ETCINFO1)                                       AS beneRate,
            (CASE WHEN t1.TRANTYPE ='00' THEN t3.QTY
                  ELSE                       -t3.QTY
             END)                                                                    AS slsQntt,
            (CASE WHEN t1.TRANTYPE ='00' THEN t3.SALEAMT
                  ELSE                       -t3.SALEAMT
             END)                                                                    AS slsAmnt,
            (CASE WHEN t1.TRANTYPE ='00' THEN t4.UNITPRICE * (convert(NUMERIC, t4.ETCINFO1) / 100.0)
                  ELSE                       -(t4.UNITPRICE * (convert(NUMERIC, t4.ETCINFO1) / 100.0))
             END)                                                                    AS byngAmnt,
            (CASE WHEN t1.TRANTYPE ='00' THEN t3.SALEAMT - (t4.UNITPRICE * (convert(NUMERIC, t4.ETCINFO1) / 100.0) * t3.QTY)
                  ELSE                      -(t3.SALEAMT - (t4.UNITPRICE * (convert(NUMERIC, t4.ETCINFO1) / 100.0) * t3.QTY))
             END)                                                                    AS beneAmnt
        FROM IDPS..VW_TRNSALHDRDTL t1,
             IDPS..MSTGOODS t2,
             IDPS..TRNSALDTL t3,
             IDPS..MSTGOODSADDTDSRP t4
        WHERE t1.SALEDATE  = t3.SALEDATE
          AND t1.STORECD   = t3.STORECD
          AND t1.POSNO     = t3.POSNO
          AND t1.TRANNO    = t3.TRANNO
          AND t1.SEQ       = t3.SEQ
          AND t3.STORECD   = t2.STORECD
          AND t3.PLUBARCD  = t2.PLUBARCD
          AND t3.STORECD   *= t4.STORECD
          AND t3.PLUBARCD  *= t4.PLUBARCD
          AND t1.SALEFLAG  = '1'
          AND t1.SALCOMCD  = '001'
          AND t1.CLOSEDATE >= '{fromDate}'
          AND t1.CLOSEDATE <= '{toDate}'
          AND t1.STORECD   = '{storeCd}'
          AND t2.JOCD      = '04'

        ORDER BY 1,2,3,5
           AT ISOLATION 0
    """,   

    "getCmdtBksh": """
        SELECT T1.rdp_code                                   AS rdpCode
             , T1.bksh_num                                   AS bkshNum
             , T1.prgp_num                                   AS prgpNum
             , T1.bksh_rspb_jo_code                          AS bkshRspbJoCode
             , T1.bksh_shp_dvsn_code                         AS bkshShpDvsnCode
             , IDSS.dbo.uf_removelf(T1.bksh_name)            AS bkshName
             , T1.stir_dvsn_name                             AS stirDvsnName
             , T1.bksh_nmsr                                  AS bkshNmsr
             , T1.bksh_with                                  AS bkshWith
             , T1.bksh_hstr_name                             AS bkshHstrName
             , T1.gvrm_infm_code                             AS gvrmInfmCode
             , T1.bksh_cntt                                  AS bkshCntt
             , IDSS.dbo.uf_removelf(T1.prgp_name)            AS prgpName
             , T1.mgr_empl_num                               AS mgrEmplNum
             , (SELECT IDSS.dbo.uf_removelf(S1.user_name) 
                  FROM IDCM..TM_USER S1 
                 WHERE S1.user_id = T1.mgr_empl_num)         AS mgrEmplName
             , T3.cmdt_id                                    AS cmdtId
             , T3.cmdt_name                                  AS cmdtName
             , T3.cmdt_code                                  AS cmdtCode
          FROM IDSS..TM_STR_BKSH T1, IDSS..TM_STR_BKSH_BKS T2, IDCM..TM_CMDT T3
         WHERE T1.rdp_code     = T2.str_rdp_code
           AND T1.bksh_num     = T2.bksh_num
           AND T1.prgp_num     = T2.bksh_prgp_num
           AND T2.cmdt_id      = T3.cmdt_id
           AND T3.dlt_ysno     = 'N'
           AND T2.str_rdp_code = '{rdpCode}'
           AND T3.cmdt_code    = '{cmdtCode}'
            AT ISOLATION 0
    """,

    "getCmdtPrenCnt": """
        SELECT count(*) AS cnt
          FROM IDSS..TM_PREN_SALE_BSC T1, IDSS..TD_PREN_SALE_DTL T2, IDCM..TM_CMDT T3
         WHERE T1.pren_rcpn_num = T2.pren_rcpn_num
           AND T2.cmdt_id = T3.cmdt_id
           AND T1.dlt_ysno = 'N'
           AND T2.dlt_ysno = 'N'
           AND T3.dlt_ysno = 'N'
           AND T1.pren_sale_rcpn_date >= convert(CHAR(8), dateadd(dd, -365, getdate()), 112)
           AND T1.pren_pros_cdtn_code <= '003'
           AND T1.str_rdp_code         = '{rdpCode}'
           AND T3.cmdt_code            = '{cmdtCode}'
            AT ISOLATION 0
    """,

    "getCmdtPrenList": """
        SELECT T1.pren_sale_rcpn_date       AS prenSaleRcpnDate
             , T1.pren_rcpn_num             AS prenRcpnNum
             , T1.aord_name                 AS aordName
             , T1.ago_stlm_ysno             AS agoStlmYsno
          FROM IDSS..TM_PREN_SALE_BSC T1, IDSS..TD_PREN_SALE_DTL T2, IDCM..TM_CMDT T3
         WHERE T1.pren_rcpn_num = T2.pren_rcpn_num
           AND T2.cmdt_id = T3.cmdt_id
           AND T1.dlt_ysno = 'N'
           AND T2.dlt_ysno = 'N'
           AND T3.dlt_ysno = 'N'
           AND T1.pren_sale_rcpn_date >= convert(CHAR(8), dateadd(dd, -365, getdate()), 112)
           AND T1.pren_pros_cdtn_code <= '003'
           AND T1.str_rdp_code         = '{rdpCode}'
           AND T3.cmdt_code            = '{cmdtCode}'
            AT ISOLATION 0
    """,

    "cmd_search_emp_card_사원카드": """
        SELECT T1.GRPTYPE AS grp_type
             , T1.EMPNO   AS user_id
             , T1.IDNO    AS id_no
             , T1.CHKBIT  AS chk_bit
             , T1.EMPNAME AS user_name
             , T1.USEYN   AS use_yn
             , T1.GRPTYPE + T1.EMPNO + T1.IDNO + T1.CHKBIT               AS emp_card_no
             , SUBSTRING(T1.DRCARDNO, 1, 16) + '=' + rtrim(T1.DRCARDPIN) AS dream_card_no
          FROM IDPS..MSTEMPLOYEE T1
         WHERE T1.EMPNO = '{}'
            AT ISOLATION 0
    """,

    "inline_btn_invn_qntt_전점재고조회": """
            SELECT '1'                            AS rank
                 , T3.rdp_code                    AS rdp_code
                 , T3.rdp_name                    AS rdp_name
                 , T2.cmdt_code                   AS cmdt_code
                 , T1.rcds_invn_qntt              AS rcds_invn_qntt
                 , T1.real_invn_qntt              AS real_invn_qntt
                 , T1.str_pren_invn_qntt          AS str_pren_invn_qntt
                 , T1.real_invn_qntt - T1.str_pren_invn_qntt AS available_invn_qntt
              FROM IDSS..TM_RDP_INVN T1, IDCM..TM_CMDT T2, IDCM..TM_RDP T3
             WHERE T1.rdp_code  = T3.rdp_code
               AND T1.cmdt_id   = T2.cmdt_id
               AND T2.dlt_ysno  = 'N'
               AND T3.dlt_ysno  = 'N'
               AND T1.rdp_code  = '{0}'
               AND T2.cmdt_code = '{1}'
             UNION ALL
            SELECT '2'                            AS rank
                 , T3.rdp_code                    AS rdp_code
                 , T3.rdp_name                    AS rdp_name
                 , T2.cmdt_code                   AS cmdt_code
                 , T1.rcds_invn_qntt              AS rcds_invn_qntt
                 , T1.real_invn_qntt              AS real_invn_qntt
                 , T1.str_pren_invn_qntt          AS str_pren_invn_qntt
                 , T1.real_invn_qntt - T1.str_pren_invn_qntt AS available_invn_qntt
              FROM IDSS..TM_RDP_INVN T1, IDCM..TM_CMDT T2, IDCM..TM_RDP T3
             WHERE T1.rdp_code   = T3.rdp_code
               AND T1.cmdt_id    = T2.cmdt_id
               AND T2.dlt_ysno   = 'N'
               AND T3.dlt_ysno   = 'N'
               AND T1.rdp_code  <> '{0}'
               AND T2.cmdt_code  = '{1}'
            HAVING T1.real_invn_qntt - T1.str_pren_invn_qntt > 0
             ORDER BY 1, 2
        """,

    "cmd_search_cmdt_info_상품기본정보조회": """
        SELECT T1.cmdt_id                                                     AS cmdt_id
             , T1.cmdt_code                                                   AS cmdt_code
             , T1.cmdt_name                                                   AS cmdt_name
             , T1.jo_code                                                     AS jo_code
             , (SELECT S1.code_wrth_name                                      
                  FROM IDCM..TC_KFLW_CODE_DTL S1                              
                 WHERE S1.code_id   = '3002'                                  
                   AND S1.code_wrth = T1.jo_code                              
                   AND S1.dlt_ysno  = 'N')                                    AS jo_name
             , T1.pbcm_code                                                   AS pbcm_code
             , (SELECT S2.pbcm_name
                  FROM IDCM..TM_PBCM S2
                 WHERE S2.pbcm_code = T1.pbcm_code
                   AND S2.dlt_ysno  = 'N')                                    AS pbcm_name
             , (SELECT IsNull(S3.autr_name1, '')                              
                  FROM IDCM..TD_CMDT_AUTR S3                                  
                 WHERE S3.cmdt_id = T1.cmdt_id)                               AS autr_name
             , T1.cmdt_dvsn_code                                              AS cmdt_dvsn_code
             , (SELECT S1.code_wrth_name
                  FROM IDCM..TC_KFLW_CODE_DTL S1
                 WHERE S1.code_id   = '1257'
                   AND S1.code_wrth = T1.cmdt_dvsn_code
                   AND S1.dlt_ysno  = 'N')                                    AS cmdt_dvsn_name
             , T1.vndr_code                                                   AS vndr_code
             , (SELECT S4.vndr_name
                  FROM IDCM..TM_VNDR S4
                 WHERE S4.vndr_code = T1.vndr_code
                   AND S4.dlt_ysno  = 'N')                                    AS vndr_name
             , (CASE WHEN T1.sale_lmtt_age >= 19 THEN '19금' ELSE '일반' END) AS sale_lmtt_age
             , IsNull((SELECT S6.wncr_prce
                         FROM IDCM..TD_CMDT_PRCE S6
                        WHERE S6.cmdt_id      = T1.cmdt_id
                          AND S6.apl_end_dttm = '99991231235959'), 0)                 AS wncr_prce
             , IsNull((SELECT SUM(sale_qntt)
                         FROM IDSS..TS_STOR_SALE_SUMY S5
                        WHERE S5.sale_date    = convert(CHAR(8), getdate(), 112)
                          AND S5.cmdt_id      = T1.cmdt_id), 0)                       AS off_today_sale_qntt
             , IsNull((SELECT SUM(sale_qntt)
                         FROM IDSS..TS_STOR_SALE_SUMY S5
                        WHERE S5.sale_date    >= convert(CHAR(8), dateadd(dd, -7, getdate()), 112)
                          AND S5.sale_date    <= convert(CHAR(8), getdate(), 112)
                          AND S5.cmdt_id       = T1.cmdt_id), 0)                      AS off_last7days_sale_qntt
          FROM IDCM..TM_CMDT T1
         WHERE T1.cmdt_code = '{0}'
           AND T1.dlt_ysno  = 'N'
                 AT ISOLATION 0
    """,

    "cmd_search_cmdt_info_점포귀속정보조회": """
        SELECT (SELECT (CASE WHEN count(*) > 0 THEN 'Y' ELSE 'N' END)
                  FROM IDCM..TM_APIC_AREA_ECSL_VNDR S4
                 WHERE S4.rdp_code = T2.rdp_code
                   AND S4.cmdt_id  = T1.cmdt_code
                   AND S4.dlt_ysno = 'N')                                            AS apic_area_ysno
             , IsNull((SELECT S3.rcds_invn_qntt                                      
                         FROM IDSS..TM_RDP_INVN S3                                   
                        WHERE S3.rdp_code = T2.rdp_code                              
                          AND S3.cmdt_id  = T1.cmdt_id), 0)                          AS rcds_invn_qntt
             , IsNull((SELECT S3.real_invn_qntt                                      
                         FROM IDSS..TM_RDP_INVN S3                                   
                        WHERE S3.rdp_code = T2.rdp_code                              
                          AND S3.cmdt_id  = T1.cmdt_id), 0)                          AS real_invn_qntt
             , IsNull((SELECT S3.str_pren_invn_qntt                                  
                         FROM IDSS..TM_RDP_INVN S3                                   
                        WHERE S3.rdp_code = T2.rdp_code                              
                          AND S3.cmdt_id  = T1.cmdt_id), 0)                          AS str_pren_invn_qntt
             , IsNull((SELECT S2.bks_prce
                         FROM IDCM..TD_BKS_BSC_PRCE S2
                        WHERE S2.rdp_code = T2.rdp_code
                          AND S2.cmdt_id  = T1.cmdt_id
                          AND S2.dlt_ysno = 'N'), 0)                                 AS bks_prce
             , IsNull((SELECT S1.UNITPRICE
                         FROM IDPS..MSTGOODS S1
                        WHERE S1.STORECD  = T2.rdp_code
                          AND S1.PLUBARCD = T1.cmdt_code), 0)                        AS pos_sale_prce
             , IsNull((SELECT SUM(sale_qntt)
                         FROM IDSS..TS_STOR_SALE_SUMY S5
                        WHERE S5.str_rdp_code = T2.rdp_code
                          AND S5.sale_date    = convert(CHAR(8), getdate(), 112)
                          AND S5.cmdt_id      = T1.cmdt_id), 0)                      AS today_sale_qntt
             , IsNull((SELECT SUM(sale_qntt)
                         FROM IDSS..TS_STOR_SALE_SUMY S5
                        WHERE S5.str_rdp_code  = T2.rdp_code
                          AND S5.sale_date    >= convert(CHAR(8), dateadd(dd, -7, getdate()), 112)
                          AND S5.sale_date    <= convert(CHAR(8), getdate(), 112)
                          AND S5.cmdt_id       = T1.cmdt_id), 0)                      AS last7days_sale_qntt
             , IsNull((SELECT min(S6.bksh_num + '-' + S6.bksh_prgp_num)
                         FROM IDSS..TM_STR_BKSH_BKS S6
                        WHERE S6.str_rdp_code = T2.rdp_code
                          AND S6.cmdt_id      = T1.cmdt_id), '서가미등록')            AS bksh_bks_min
             , IsNull((SELECT max(S6.bksh_num + '-' + S6.bksh_prgp_num)
                         FROM IDSS..TM_STR_BKSH_BKS S6
                        WHERE S6.str_rdp_code = T2.rdp_code
                          AND S6.cmdt_id      = T1.cmdt_id), '서가미등록')            AS bksh_bks_max
          FROM IDCM..TM_CMDT T1, (SELECT '{0}' AS rdp_code) T2
         WHERE T1.cmdt_code = '{1}'
           AND T1.dlt_ysno  = 'N'  
    """,

    "user_auth_process_step_사용자정보조회": """
         SELECT rtrim(T1.user_id)                       AS user_id
              , T1.sert_num                             AS sert_num
              , T1.user_name                            AS user_name
              , T1.cg_inhb_rgst_num                     AS cg_inhb_rgst_num
              , T1.tnof_cdtn_code                       AS tnof_cdtn_code
              , T1.dprt_code                            AS dprt_code
              , T1.pstn_code                            AS pstn_code
              , T1.jbcl_code                            AS jbcl_code
              , T1.rprs_pstn_code                       AS rprs_pstn_code
              , T1.arbt_ysno                            AS arbt_ysno
              , T1.spr_mgr_ysno                         AS spr_mgr_ysno
              , T1.tlnm                                 AS tlnm
              , T1.prtb_tlnm                            AS prtb_tlnm
              , T1.eml_adrs                             AS eml_adrs
              , T1.jo_code                              AS jo_code
              , T1.rdp_code                             AS rdp_code
              , T1.user_rgst_patr_code                  AS user_rgst_patr_code
              , T1.crtr_id                              AS crtr_id
              , T1.cret_dttm                            AS cret_dttm
              , T1.amnr_id                              AS amnr_id
              , T1.amnd_dttm                            AS amnd_dttm
              , T1.dlt_ysno                             AS dlt_ysno
              , IsNull(T2.dprt_name, '부서코드없음') AS dprt_name
              , (SELECT S1.rdp_name
                   FROM IDCM..TM_RDP S1
                  WHERE S1.rdp_code = T1.rdp_code
                    AND S1.dlt_ysno = 'N')           AS rdp_name
              , (SELECT S2.code_wrth_name
                   FROM IDCM..TC_KFLW_CODE_DTL S2
                  WHERE S2.code_id = '1379'
                    AND S2.code_wrth = T1.pstn_code
                    AND S2.dlt_ysno = 'N') AS pstn_name
              , (SELECT S2.code_wrth_name
                   FROM IDCM..TC_KFLW_CODE_DTL S2
                  WHERE S2.code_id = '1377'
                    AND S2.code_wrth = T1.jbcl_code
                    AND S2.dlt_ysno = 'N') AS jbcl_name
              , (SELECT S2.code_wrth_name
                   FROM IDCM..TC_KFLW_CODE_DTL S2
                  WHERE S2.code_id = '3002'
                    AND S2.code_wrth = T1.jo_code
                    AND S2.dlt_ysno = 'N') AS jo_name
           FROM IDCM..TM_USER T1, IDCM..TM_UNFY_DTRB_DPRT T2
          WHERE T1.dprt_code      = T2.dprt_code
            AND T1.tnof_cdtn_code = '001'
            AND T1.arbt_ysno      = 'N'
            AND T1.prtb_tlnm     IN ('{0}', '{1}')
            AND T1.dlt_ysno       = 'N'
          ORDER BY 1
             AT ISOLATION 0
    """,

    "chk_login_로그인정보조회": """
        SELECT T1.chat_id        AS chat_id
             , rtrim(T1.user_id)        AS user_id
             , T1.sert_num       AS sert_num
             , T1.user_name      AS user_name
             , T1.rdp_code       AS rdp_code
             , T1.rdp_name       AS rdp_name
             , T1.dprt_code      AS dprt_code
             , T1.dprt_name      AS dprt_name
             , T1.pstn_code      AS pstn_code
             , T1.pstn_name      AS pstn_name
             , T1.jbcl_code      AS jbcl_code
             , T1.jbcl_name      AS jbcl_name
             , T1.rprs_pstn_code AS rprs_pstn_code
             , T1.arbt_ysno      AS arbt_ysno
             , T1.prtb_tlnm      AS prtb_tlnm
             , T1.eml_adrs       AS eml_adrs
             , T1.dlt_ysno       AS dlt_ysno
          FROM IDSS..TM_STR_BOT_USER T1
         WHERE T1.chat_id = '{0}'
           AND T1.dlt_ysno = 'N'
        """,

    "cmd_search_emp_사원번호검색": """
         SELECT T1.user_id                   AS user_id
              , T1.user_name                 AS user_name
              , T1.tlnm                      AS tlnm
              , T1.prtb_tlnm                 AS prtb_tlnm
              , T1.eml_adrs                  AS eml_adrs
              , T1.jo_code                   AS jo_code
              , T1.rdp_code                  AS rdp_code
              , T1.dprt_code                 AS dprt_code
              , T1.pstn_code                 AS pstn_code
              , T1.jbcl_code                 AS jbcl_code
              , T1.rprs_pstn_code            AS rprs_pstn_code
              , T1.arbt_ysno                 AS arbt_ysno
              , IsNull(T2.dprt_name, '부서코드없음') AS dprt_name
              , (SELECT S1.rdp_name
                   FROM IDCM..TM_RDP S1
                  WHERE S1.rdp_code = T1.rdp_code
                    AND S1.dlt_ysno = 'N')           AS rdp_name
              , (SELECT S2.code_wrth_name
                   FROM IDCM..TC_KFLW_CODE_DTL S2
                  WHERE S2.code_id = '1379'
                    AND S2.code_wrth = T1.pstn_code
                    AND S2.dlt_ysno = 'N') AS pstn_name
              , (SELECT S2.code_wrth_name
                   FROM IDCM..TC_KFLW_CODE_DTL S2
                  WHERE S2.code_id = '1377'
                    AND S2.code_wrth = T1.jbcl_code
                    AND S2.dlt_ysno = 'N') AS jbcl_name
              , (SELECT S2.code_wrth_name
                   FROM IDCM..TC_KFLW_CODE_DTL S2
                  WHERE S2.code_id = '3002'
                    AND S2.code_wrth = T1.jo_code
                    AND S2.dlt_ysno = 'N') AS jo_name
           FROM IDCM..TM_USER T1, IDCM..TM_UNFY_DTRB_DPRT T2
          WHERE T1.dprt_code           = T2.dprt_code
            AND T1.tnof_cdtn_code      = '001'
            AND T1.arbt_ysno           = 'N'
            AND T1.user_id             = '{}'
            AND T1.user_rgst_patr_code = '001'
            AND T1.dlt_ysno            = 'N'
          ORDER BY 2
    """,

    "cmd_search_emp_사원명검색": """
         SELECT T1.user_id                   AS user_id
              , T1.user_name                 AS user_name
              , T1.tlnm                      AS tlnm
              , T1.prtb_tlnm                 AS prtb_tlnm
              , T1.eml_adrs                  AS eml_adrs
              , T1.jo_code                   AS jo_code
              , T1.rdp_code                  AS rdp_code
              , T1.dprt_code                 AS dprt_code
              , T1.pstn_code                 AS pstn_code
              , T1.jbcl_code                 AS jbcl_code
              , T1.rprs_pstn_code            AS rprs_pstn_code
              , T1.arbt_ysno                 AS arbt_ysno
              , IsNull(T2.dprt_name, '부서코드없음') AS dprt_name
              , (SELECT S1.rdp_name
                   FROM IDCM..TM_RDP S1
                  WHERE S1.rdp_code = T1.rdp_code
                    AND S1.dlt_ysno = 'N')           AS rdp_name
              , (SELECT S2.code_wrth_name
                   FROM IDCM..TC_KFLW_CODE_DTL S2
                  WHERE S2.code_id = '1379'
                    AND S2.code_wrth = T1.pstn_code
                    AND S2.dlt_ysno = 'N') AS pstn_name
              , (SELECT S2.code_wrth_name
                   FROM IDCM..TC_KFLW_CODE_DTL S2
                  WHERE S2.code_id = '1377'
                    AND S2.code_wrth = T1.jbcl_code
                    AND S2.dlt_ysno = 'N') AS jbcl_name
              , (SELECT S2.code_wrth_name
                   FROM IDCM..TC_KFLW_CODE_DTL S2
                  WHERE S2.code_id = '3002'
                    AND S2.code_wrth = T1.jo_code
                    AND S2.dlt_ysno = 'N') AS jo_name
           FROM IDCM..TM_USER T1, IDCM..TM_UNFY_DTRB_DPRT T2
          WHERE T1.dprt_code           = T2.dprt_code
            AND T1.tnof_cdtn_code      = '001'
            AND T1.arbt_ysno           = 'N'
            AND T1.user_name           LIKE '{}%'
            AND T1.user_rgst_patr_code = '001'
            AND T1.dlt_ysno            = 'N'
          ORDER BY 2
    """,

    "cmd_search_emp_휴대폰번호검색": """
         SELECT T1.user_id                   AS user_id
              , T1.user_name                 AS user_name
              , T1.tlnm                      AS tlnm
              , T1.prtb_tlnm                 AS prtb_tlnm
              , T1.eml_adrs                  AS eml_adrs
              , T1.jo_code                   AS jo_code
              , T1.rdp_code                  AS rdp_code
              , T1.dprt_code                 AS dprt_code
              , T1.pstn_code                 AS pstn_code
              , T1.jbcl_code                 AS jbcl_code
              , T1.rprs_pstn_code            AS rprs_pstn_code
              , T1.arbt_ysno                 AS arbt_ysno
              , IsNull(T2.dprt_name, '부서코드없음') AS dprt_name
              , (SELECT S1.rdp_name
                   FROM IDCM..TM_RDP S1
                  WHERE S1.rdp_code = T1.rdp_code
                    AND S1.dlt_ysno = 'N')           AS rdp_name
              , (SELECT S2.code_wrth_name
                   FROM IDCM..TC_KFLW_CODE_DTL S2
                  WHERE S2.code_id = '1379'
                    AND S2.code_wrth = T1.pstn_code
                    AND S2.dlt_ysno = 'N') AS pstn_name
              , (SELECT S2.code_wrth_name
                   FROM IDCM..TC_KFLW_CODE_DTL S2
                  WHERE S2.code_id = '1377'
                    AND S2.code_wrth = T1.jbcl_code
                    AND S2.dlt_ysno = 'N') AS jbcl_name
              , (SELECT S2.code_wrth_name
                   FROM IDCM..TC_KFLW_CODE_DTL S2
                  WHERE S2.code_id = '3002'
                    AND S2.code_wrth = T1.jo_code
                    AND S2.dlt_ysno = 'N') AS jo_name
           FROM IDCM..TM_USER T1, IDCM..TM_UNFY_DTRB_DPRT T2
          WHERE T1.dprt_code           = T2.dprt_code
            AND T1.tnof_cdtn_code      = '001'
            AND T1.arbt_ysno           = 'N'
            AND T1.prtb_tlnm           IN ('{0}', '{1}')
            AND T1.user_rgst_patr_code = '001'
            AND T1.dlt_ysno            = 'N'
          ORDER BY 2
    """,

    "cmd_search_emp_이메일주소검색": """
         SELECT T1.user_id                   AS user_id
              , T1.user_name                 AS user_name
              , T1.tlnm                      AS tlnm
              , T1.prtb_tlnm                 AS prtb_tlnm
              , T1.eml_adrs                  AS eml_adrs
              , T1.jo_code                   AS jo_code
              , T1.rdp_code                  AS rdp_code
              , T1.dprt_code                 AS dprt_code
              , T1.pstn_code                 AS pstn_code
              , T1.jbcl_code                 AS jbcl_code
              , T1.rprs_pstn_code            AS rprs_pstn_code
              , T1.arbt_ysno                 AS arbt_ysno
              , IsNull(T2.dprt_name, '부서코드없음') AS dprt_name
              , (SELECT S1.rdp_name
                   FROM IDCM..TM_RDP S1
                  WHERE S1.rdp_code = T1.rdp_code
                    AND S1.dlt_ysno = 'N')           AS rdp_name
              , (SELECT S2.code_wrth_name
                   FROM IDCM..TC_KFLW_CODE_DTL S2
                  WHERE S2.code_id = '1379'
                    AND S2.code_wrth = T1.pstn_code
                    AND S2.dlt_ysno = 'N') AS pstn_name
              , (SELECT S2.code_wrth_name
                   FROM IDCM..TC_KFLW_CODE_DTL S2
                  WHERE S2.code_id = '1377'
                    AND S2.code_wrth = T1.jbcl_code
                    AND S2.dlt_ysno = 'N') AS jbcl_name
              , (SELECT S2.code_wrth_name
                   FROM IDCM..TC_KFLW_CODE_DTL S2
                  WHERE S2.code_id = '3002'
                    AND S2.code_wrth = T1.jo_code
                    AND S2.dlt_ysno = 'N') AS jo_name
           FROM IDCM..TM_USER T1, IDCM..TM_UNFY_DTRB_DPRT T2
          WHERE T1.dprt_code           = T2.dprt_code
            AND T1.tnof_cdtn_code      = '001'
            AND T1.arbt_ysno           = 'N'
            AND T1.eml_adrs            LIKE '{}%'
            AND T1.user_rgst_patr_code = '001'
            AND T1.dlt_ysno            = 'N'
          ORDER BY 2
    """,

    "get_amount_of_sale_per_hour": """
        SELECT A.code_wrth                                                        AS timeCode
             , A.code_wrth_name                                                   AS timeName
             , ISNULL(B.oldCmdtCnt , 0)                                           AS oldCmdtCnt
             , ISNULL(B.oldQntt    , 0)                                           AS oldQntt
             , ISNULL(B.oldSaleAmt , 0)                                           AS oldSaleAmt
             , ISNULL(C.strdCmdtCnt, 0)                                           AS strdCmdtCnt
             , ISNULL(C.strdQntt   , 0)                                           AS strdQntt
             , ISNULL(C.strdSaleAmt, 0)                                           AS strdSaleAmt
             , ISNULL(C.strdQntt   , 0) - ISNULL( B.oldQntt   , 0)                AS qnttIndc
             , ISNULL(C.strdSaleAmt, 0) - ISNULL( B.oldSaleAmt, 0)                AS amntIndc
             , ISNULL((CASE WHEN ISNULL(B.oldQntt   , 0 ) <> 0 THEN (CONVERT(NUMERIC(15,2), ROUND(CONVERT(NUMERIC(15,2), (C.strdQntt    - B.oldQntt   )) * 100 / B.oldQntt   , 2)))
                            ELSE                                     100 END), 0) AS qnttIndcRate
             , ISNULL((CASE WHEN ISNULL(B.oldSaleAmt, 0 ) <> 0 THEN (CONVERT(NUMERIC(15,2), ROUND(CONVERT(NUMERIC(15,2), (C.strdSaleAmt - B.oldSaleAmt)) * 100 / B.oldSaleAmt, 2)))
                            ELSE                                     100 END), 0) AS amntIndcRate
          FROM IDCM..TC_KFLW_CODE_DTL A
             , (
                 SELECT t2.SALETIME     AS oldSaleTime
                      , SUM(t2.cmdtCnt) AS oldCmdtCnt
                      , SUM(t2.QTY    ) AS oldQntt
                      , SUM(t2.SALEAMT) AS oldSaleAmt
                   FROM (
        
                          SELECT (CASE WHEN SUBSTRING(st1.SALETIME, 1, 2) < '09' THEN '09'
                                       WHEN SUBSTRING(st1.SALETIME, 1, 2) > '22' THEN '22'
                                       ELSE                                           SUBSTRING(st1.SALETIME, 1, 2) END) AS SALETIME
                               , st1.SALEDATE                                                                            AS SALEDATE
                               , st1.STORECD                                                                             AS STORECD
                               , st1.POSNO                                                                               AS POSNO
                               , st1.TRANNO                                                                              AS TRANNO
                               , (CASE WHEN st1.TRANTYPE = '00' THEN  st1.ROWCNT
                                       ELSE                          -st1.ROWCNT END)                                    AS cmdtCnt
                               , (CASE WHEN st1.TRANTYPE = '00' THEN  st1.ITEMCNT
                                       ELSE                          -st1.ITEMCNT END)                                   AS QTY
                               , (CASE WHEN st1.TRANTYPE = '00' THEN  st1.SALEAMT
                                       ELSE                          -st1.SALEAMT END)                                   AS SALEAMT
                            FROM IDPS..TRNSALHDR st1
                           WHERE st1.SALEDATE  = '{old_date}'
                             AND st1.TRANTYPE IN ('00', '01')
                             AND st1.TRANKIND <> '15'
                             and st1.STORECD = '{rdp_code}'
                             AND (st1.SALEAMTM <> 0 OR st1.STORECD = '013' )
                             AND st1.STORECD  NOT IN ('063', '064')
                          GROUP BY (CASE WHEN SUBSTRING(st1.SALETIME, 1, 2) < '09' THEN '09'
                                         WHEN SUBSTRING(st1.SALETIME, 1, 2) > '22' THEN '22'
                                         ELSE                                           SUBSTRING(st1.SALETIME, 1, 2) END)
                                 , st1.SALEDATE
                                 , st1.STORECD
                                 , st1.POSNO
                                 , st1.TRANNO
                                 , (CASE WHEN st1.TRANTYPE = '00' THEN  st1.ROWCNT
                                         ELSE                          -st1.ROWCNT END)
                                 , (CASE WHEN st1.TRANTYPE = '00' THEN  st1.ITEMCNT
                                         ELSE                          -st1.ITEMCNT END)
                                 , (CASE WHEN st1.TRANTYPE = '00' THEN  st1.SALEAMT
                                         ELSE                          -st1.SALEAMT END)
                          UNION ALL
                             SELECT (CASE WHEN SUBSTRING(CONVERT(CHAR(6), c.UPTDATE, 108), 1, 2) < '09' THEN '09'
                                          WHEN SUBSTRING(CONVERT(CHAR(6), c.UPTDATE, 108), 1, 2) > '22' THEN '22'
                                                                                                        ELSE SUBSTRING(CONVERT(CHAR(6), c.UPTDATE, 108), 1, 2) END) AS SALETIME
                                  , c.FNSHDATE                                                                                                                      AS SALEDATE
                                  , c.STORECD                                                                                                                       AS STORECD
                                  , c.POSNO                                                                                                                         AS POSNO
                                  , c.TRANNO                                                                                                                        AS TRANNO
                                  , (CASE WHEN a.TRANTYPE = '00' THEN  a.ROWCNT
                                          ELSE                        -a.ROWCNT END)                                                                                AS cmdtCnt
                                  , (CASE WHEN a.TRANTYPE = '00' THEN  a.ITEMCNT
                                          ELSE                        -a.ITEMCNT END)                                                                               AS QTY
                                  , (CASE WHEN a.TRANTYPE = '00' THEN  a.SALEAMT
                                          ELSE                        -a.SALEAMT END)                                                                               AS SALEAMT
                               FROM IDPS..TRNRESERVATION c, IDPS..TRNSALHDR a
                           WHERE c.SALEDATE      = a.SALEDATE
                             AND c.STORECD       = a.STORECD
                             AND c.POSNO         = a.POSNO
                             AND c.TRANNO        = a.TRANNO
                             AND c.FNSHDATE      = '{old_date}'
                             AND a.TRANTYPE     IN ('00','01')
                             AND a.TRANKIND      = '15'
                             and a.STORECD       = '{rdp_code}'
                             AND (a.SALEAMTM <> 0 OR a.STORECD = '013' )
                             AND a.STORECD  NOT IN ('063', '064')
                           GROUP BY (CASE WHEN SUBSTRING(CONVERT(CHAR(6), c.UPTDATE, 108), 1, 2) < '09' THEN '09'
                                          WHEN SUBSTRING(CONVERT(CHAR(6), c.UPTDATE, 108), 1, 2) > '22' THEN '22'
                                                                                                        ELSE SUBSTRING(CONVERT(CHAR(6), c.UPTDATE, 108), 1, 2) END)
                                 , c.FNSHDATE
                                 , c.STORECD
                                 , c.POSNO
                                 , c.TRANNO
                                 , (CASE WHEN a.TRANTYPE = '00' THEN  a.ROWCNT
                                         ELSE                        -a.ROWCNT  END)
                                 , (CASE WHEN a.TRANTYPE = '00' THEN  a.ITEMCNT
                                         ELSE                        -a.ITEMCNT END)
                                 , (CASE WHEN a.TRANTYPE = '00' THEN  a.SALEAMT
                                         ELSE                        -a.SALEAMT END)
                        ) t2
                 GROUP BY t2.SALETIME
               ) B
             , (
                 SELECT t2.SALETIME     AS strdSaleTime
                      , SUM(t2.cmdtCnt) AS strdCmdtCnt
                      , SUM(t2.QTY    ) AS strdQntt
                      , SUM(t2.SALEAMT) AS strdSaleAmt
                   FROM (
                          SELECT (CASE WHEN SUBSTRING(st1.SALETIME, 1, 2) < '09' THEN '09'
                                       WHEN SUBSTRING(st1.SALETIME, 1, 2) > '22' THEN '22'
                                       ELSE                                           SUBSTRING(st1.SALETIME, 1, 2) END) AS SALETIME
                               , st1.SALEDATE                                                                            AS SALEDATE
                               , st1.STORECD                                                                             AS STORECD
                               , st1.POSNO                                                                               AS POSNO
                               , st1.TRANNO                                                                              AS TRANNO
                               , (CASE WHEN st1.TRANTYPE = '00' THEN  st1.ROWCNT
                                       ELSE                          -st1.ROWCNT END)                                    AS cmdtCnt
                               , (CASE WHEN st1.TRANTYPE = '00' THEN  st1.ITEMCNT
                                       ELSE                          -st1.ITEMCNT END)                                   AS QTY
                               , (CASE WHEN st1.TRANTYPE = '00' THEN  st1.SALEAMT
                                       ELSE                          -st1.SALEAMT END)                                   AS SALEAMT
                            FROM IDPS..TRNSALHDR st1
                           WHERE st1.SALEDATE  = '{base_date}'
                             AND st1.TRANTYPE IN ( '00', '01' )
                             AND st1.TRANKIND <> '15'
                             AND st1.STORECD = '{rdp_code}'
                             AND (st1.SALEAMTM <> 0 OR st1.STORECD = '013' )
                             AND st1.STORECD  NOT IN ('063', '064')
                          GROUP BY (CASE WHEN SUBSTRING(st1.SALETIME, 1, 2) < '09' THEN '09'
                                         WHEN SUBSTRING(st1.SALETIME, 1, 2) > '22' THEN '22'
                                         ELSE                                           SUBSTRING(st1.SALETIME, 1, 2) END)
                                 , st1.SALEDATE
                                 , st1.STORECD
                                 , st1.POSNO
                                 , st1.TRANNO
                                 , (CASE WHEN st1.TRANTYPE = '00' THEN  st1.ROWCNT
                                         ELSE                          -st1.ROWCNT END)
                                 , (CASE WHEN st1.TRANTYPE = '00' THEN  st1.ITEMCNT
                                         ELSE                          -st1.ITEMCNT END)
                                 , (CASE WHEN st1.TRANTYPE = '00' THEN  st1.SALEAMT
                                         ELSE                          -st1.SALEAMT END)
                          UNION ALL
                             SELECT (CASE WHEN SUBSTRING(CONVERT(CHAR(6), c.UPTDATE, 108), 1, 2) < '09' THEN '09'
                                          WHEN SUBSTRING(CONVERT(CHAR(6), c.UPTDATE, 108), 1, 2) > '22' THEN '22'
                                                                                                        ELSE SUBSTRING(CONVERT(CHAR(6), c.UPTDATE, 108), 1, 2) END) AS SALETIME
                                  , c.FNSHDATE                                                                                                                      AS SALEDATE
                                  , c.STORECD                                                                                                                       AS STORECD
                                  , c.POSNO                                                                                                                         AS POSNO
                                  , c.TRANNO                                                                                                                        AS TRANNO
                                  , (CASE WHEN a.TRANTYPE = '00' THEN  a.ROWCNT
                                          ELSE                        -a.ROWCNT END)                                                                                AS cmdtCnt
                                  , (CASE WHEN a.TRANTYPE = '00' THEN  a.ITEMCNT
                                          ELSE                        -a.ITEMCNT END)                                                                               AS QTY
                                  , (CASE WHEN a.TRANTYPE = '00' THEN  a.SALEAMT
                                          ELSE                        -a.SALEAMT END)                                                                               AS SALEAMT
                               FROM IDPS..TRNRESERVATION c, IDPS..TRNSALHDR a
                              WHERE c.SALEDATE     = a.SALEDATE
                                AND c.STORECD      = a.STORECD
                                AND c.POSNO        = a.POSNO
                                AND c.TRANNO       = a.TRANNO
                                AND c.FNSHDATE     = '{base_date}'
                                AND a.TRANTYPE    IN ('00', '01')
                                AND a.TRANKIND     = '15'
                                AND a.STORECD      = '{rdp_code}'
                                AND (a.SALEAMTM <> 0 OR a.STORECD = '013' )
                                AND a.STORECD  NOT IN ('063', '064')
                           GROUP BY (CASE WHEN SUBSTRING(CONVERT(CHAR(6), c.UPTDATE, 108), 1, 2) < '09' THEN '09'
                                          WHEN SUBSTRING(CONVERT(CHAR(6), c.UPTDATE, 108), 1, 2) > '22' THEN '22'
                                                                                                        ELSE SUBSTRING(CONVERT(CHAR(6), c.UPTDATE, 108), 1, 2) END)
                                 , c.FNSHDATE
                                 , c.STORECD
                                 , c.POSNO
                                 , c.TRANNO
                                 , (CASE WHEN a.TRANTYPE = '00' THEN  a.ROWCNT
                                         ELSE                        -a.ROWCNT END)
                                 , (CASE WHEN a.TRANTYPE = '00' THEN  a.ITEMCNT
                                         ELSE                        -a.ITEMCNT END)
                                 , (CASE WHEN a.TRANTYPE = '00' THEN  a.SALEAMT
                                         ELSE                        -a.SALEAMT END)
                        ) t2
                 GROUP BY t2.SALETIME
               ) C
         WHERE A.code_id    = '1551'
           AND A.code_wrth *= B.oldSaleTime
           AND A.code_wrth *= C.strdSaleTime
        ORDER BY 1
           AT ISOLATION 0
    """,

    """get_rdp_dataset_점포데이터셋""": """
          SELECT rdp_code
           , rdp_name
           , rdp_adrs
           , rdp_tlnm
           , rdp_fax_num
           , rdp_dvsn_code
           , by_warh_rdp_code
           , phds_wrk_grp_code
           , frmr_rdp_code
           , stdvr_ysno
           , plor_grp_code
           , str_ysno
           , dprt_code
           , crtr_id
           , convert(CHAR(19), cret_dttm, 121) AS cret_dttm
           , amnr_id
           , convert(CHAR(19), amnd_dttm, 121) AS amnd_dttm
           , dlt_ysno
        FROM IDCM..TM_RDP
       WHERE rdp_dvsn_code = '002'
         AND phds_wrk_grp_code IN ('001', '002', '003')
         AND dlt_ysno = 'N'
       ORDER BY 1
          AT ISOLATION 0
    """,

    """cmd_search_bdcolrt_바드집책현황조회""": """
        SELECT T1.ordr_id                                                                                                                                            AS orderNo
             , T8.cmdt_code                                                                                                                                          AS barcode
             , T8.onln_cmdt_dvsn_code                                                                                                                                AS bookGb
             , Rtrim(T2.jo_code)                                                                                                                                     AS joCode
             , cti.dbo.fn_POS_STR_FILTER_100(T9.bookname) AS bookNm
             , cti.dbo.fn_POS_STR_FILTER_100(T9.pubname)  AS pubNm
             , cti.dbo.fn_POS_STR_FILTER_100(T9.author)   AS authorNm
             , T9.pubymd                                                                                                                                             AS pubYmd
             , T2.cmdt_prce_amnt                                                                                                                                     AS junga
             , T2.cmdt_sls_amnt                                                                                                                                      AS orderPrice
             , IsNull(T6.orig_drcs_qntt, 0) - IsNull(T6.ttl_cncl_qntt, 0)                                                                                            AS orderQty
             , T6.dlvr_prgs_cdtn_code                                                                                                                                AS stateGb
             , T5.collect_yn                                                                                                                                         AS collectYn
             , T5.collect_ymd                                                                                                                                        AS collectYmd
             , T5.collect_time                                                                                                                                       AS collectTime
             , T5.collect_userid                                                                                                                                     AS collectUserId
             , T5.transfer_yn                                                                                                                                        AS transferYn
             , T5.transfer_ymd                                                                                                                                       AS transferYmd
             , T5.transfer_time                                                                                                                                      AS transferTime
             , T5.transfer_userid                                                                                                                                    AS transferUserId
             , T5.pickup_yn                                                                                                                                          AS pickupYn
             , T5.pickup_ymd                                                                                                                                         AS pickupYmd
             , T5.pickup_time                                                                                                                                        AS pickupTime
             , T5.pickup_userid                                                                                                                                      AS pickupUserId
             , T1.ordr_date                                                                                                                                          AS orderYmd
             , T1.ordr_hms                                                                                                                                           AS ordrDate
             , T4.invn_bran_code                                                                                                                                     AS siteGubunCode
             , (SELECT S1.rdp_name
                  FROM ibims..TM_RDP S1
                 WHERE S1.frmr_rdp_code = T4.invn_bran_code)                                                                                                         AS siteGubunNm
             , T4.rcpt_bran_code                                                                                                                                     AS rcvSiteCode
             , (SELECT S1.rdp_name
                  FROM ibims..TM_RDP S1
                 WHERE S1.frmr_rdp_code = T4.rcpt_bran_code)                                                                                                         AS rcvSiteNm
             , EBIO.dbo.ufn_code_name('1693', T2.dlvr_rspb_code)                                                                                                     AS dlvrRspbName
             , EBIO.dbo.ufn_code_name('1692', T2.dlvr_shp_code)                                                                                                      AS dlvrShpName
             , EBIO.dbo.ufn_code_name('1731', T2.dlvr_dtl_shp_code)                                                                                                  AS dlvrDtlShpName
             , EBIO.dbo.ufn_code_name('1691', T6.dlvr_prgs_cdtn_code)                                                                                                AS dlvrPrgsCdtnName
          FROM EBIO..TM_ORDR          T1
             , EBIO..TD_ORDR_CMDT     T2
             , EBIO..TI_DLVR_REQU     T3
             , EBIO..TM_ORDR_DLPN     T4
             , EBIO..TD_ORDR_PICKUP   T5
             , EBIO..TI_DLVR_REQU_IEM T6
             , EBIO..TM_UNFY_CMDT     T8
             , cti..CTI_CD10          T9
         WHERE T1.ordr_id             = T2.ordr_id
           AND T1.ordr_srmb           = T2.ordr_srmb
           AND T2.ordr_id             = T3.ordr_id
           AND T2.ordr_srmb           = T3.ordr_srmb
    
           AND T2.dlvr_rspb_code      = T3.dlvr_rspb_code
           AND T2.dlvr_shp_code       = T3.dlvr_shp_code
           AND T2.dlvr_dtl_shp_code   = T3.dlvr_dtl_shp_code
    
           AND T3.ordr_dlpn_id        = T4.ordr_dlpn_id
           AND T3.dlvr_requ_id        = T6.dlvr_requ_id
    
           AND T2.ordr_id             = T5.ordr_id
           AND T2.ordr_srmb           = T5.ordr_srmb
           AND T2.ordr_cmdt_srmb      = T5.ordr_cmdt_srmb
           AND T2.ordr_cmdt_pros_srmb = T5.ordr_cmdt_pros_srmb
           AND T5.ordr_id             = T6.ordr_id
           AND T5.ordr_srmb           = T6.ordr_srmb
           AND T5.ordr_cmdt_srmb      = T6.ordr_cmdt_srmb
           AND T5.ordr_cmdt_pros_srmb = T6.ordr_cmdt_pros_srmb
           AND T5.unfy_cmdt_id        = T8.unfy_cmdt_id
           AND T5.ori_bookgb          = T9.bookgb
           AND T8.cmdt_code           = T9.barcode
           AND T1.pros_rslt_code      = '100'
           AND T2.dlvr_rspb_code      = '050'          /* 배송담당코드 050 영업점 */
           AND T2.dlvr_shp_code       = '130'          /* 배송형태코드 130 바로드림 */
           AND T1.ordr_dvsn_code      = '100'
           AND T1.ordr_date >= '{from_date}'
           AND T1.ordr_date <= '{to_date}'
           AND T4.invn_bran_code = '{rdp_code}'
            AT ISOLATION 0
    """,

    "proc_get_bksh_num_서가정보조회": """
        SELECT t1.rdp_code                                   AS rdp_code
             , (SELECT S1.rdp_name
                  FROM IDCM..TM_RDP S1
                 WHERE S1.rdp_code = t1.rdp_code)            AS rdp_name
             , t1.bksh_num                                   AS bksh_num
             , t1.bksh_name                                  AS bksh_name
             , t1.prgp_num                                   AS prgp_num
             , t1.prgp_name                                  AS prgp_name
             , t1.bksh_rspb_jo_code                          AS bksh_rspb_jo_code
             , (SELECT S2.code_wrth_name
                  FROM IDCM..TC_KFLW_CODE_DTL S2
                 WHERE S2.code_id   = '3002'
                   AND S2.code_wrth = t1.bksh_rspb_jo_code)  AS bksh_rspb_jo_name
             , t1.bksh_shp_dvsn_code                         AS bksh_shp_dvsn_code
             , (SELECT S2.code_wrth_name
                  FROM IDCM..TC_KFLW_CODE_DTL S2
                 WHERE S2.code_id   = '1260'
                   AND S2.code_wrth = t1.bksh_shp_dvsn_code) AS bksh_shp_dvsn_name
             , t1.gvrm_infm_code                             AS gvrm_infm_code
             , (SELECT S2.code_wrth_name
                  FROM IDCM..TC_KFLW_CODE_DTL S2
                 WHERE S2.code_id   = '1219'
                   AND S2.code_wrth = t1.bksh_shp_dvsn_code) AS gvrm_infm_name
             , t1.mgr_empl_num                               AS mgr_empl_num
             , (SELECT S3.user_name
                  FROM IDCM..TM_USER S3
                 WHERE S3.user_id = t1.mgr_empl_num)         AS user_name
          FROM IDSS..TM_STR_BKSH t1
         WHERE 1=1
           AND t1.rdp_code = '{rdp_code}'
           AND t1.bksh_num = '{bksh_num}'
           AND t1.prgp_num = '{prgp_bksh_num}'
           AND t1.dlt_ysno = 'N'
         ORDER BY t1.rdp_code, t1.bksh_num, t1.prgp_num
            AT ISOLATION 0
    """,

    "CMDT_CODE_TO_ID": """
        SELECT TOP 1 IsNull(max(cmdt_id), 'X') AS cmdt_id
          FROM IDCM..TM_CMDT
         WHERE cmdt_code = '{cmdt_code}'
           AND dlt_ysno = 'N'
         ORDER BY cmdt_code
            AT ISOLATION 0
    """,

    "cmd_search_plorrlnc_발주의뢰현황": """
        SELECT t1.plor_rlnc_date                                  AS plor_rlnc_date
             , (CASE WHEN datepart(dw, convert(DATETIME, t1.plor_rlnc_date)) = 1 THEN '일'
                     WHEN datepart(dw, convert(DATETIME, t1.plor_rlnc_date)) = 2 THEN '월'
                     WHEN datepart(dw, convert(DATETIME, t1.plor_rlnc_date)) = 3 THEN '화'
                     WHEN datepart(dw, convert(DATETIME, t1.plor_rlnc_date)) = 4 THEN '수'
                     WHEN datepart(dw, convert(DATETIME, t1.plor_rlnc_date)) = 5 THEN '목'
                     WHEN datepart(dw, convert(DATETIME, t1.plor_rlnc_date)) = 6 THEN '금'
                     WHEN datepart(dw, convert(DATETIME, t1.plor_rlnc_date)) = 7 THEN '토'
                END) AS weekday
             , count(DISTINCT t1.cmdt_id)                         AS cmdt_qntt
             , sum(t1.plor_rlnc_qntt)                             AS tot_qntt
          FROM IDPL..TM_PLOR_RLNC t1
             , IDCM..TM_CMDT t4
         WHERE 1 = 1
           AND t1.cmdt_id             = t4.cmdt_id
           AND t1.plor_rlnc_date      >= '{from_date}'
           AND t1.plor_rlnc_date      <= '{to_date}'
           AND t1.rdp_code            LIKE '{rdp_code}%'
           AND t1.rdp_code            NOT IN ('050', '009', '040')
           AND t1.isbn                IS NOT NULL
           AND t4.dlt_ysno            = 'N'
         GROUP BY t1.plor_rlnc_date
                , (CASE WHEN datepart(dw, convert(DATETIME, t1.plor_rlnc_date)) = 1 THEN '일'
                        WHEN datepart(dw, convert(DATETIME, t1.plor_rlnc_date)) = 2 THEN '월'
                        WHEN datepart(dw, convert(DATETIME, t1.plor_rlnc_date)) = 3 THEN '화'
                        WHEN datepart(dw, convert(DATETIME, t1.plor_rlnc_date)) = 4 THEN '수'
                        WHEN datepart(dw, convert(DATETIME, t1.plor_rlnc_date)) = 5 THEN '목'
                        WHEN datepart(dw, convert(DATETIME, t1.plor_rlnc_date)) = 6 THEN '금'
                        WHEN datepart(dw, convert(DATETIME, t1.plor_rlnc_date)) = 7 THEN '토'
                   END)
         ORDER BY 1
            AT ISOLATION 0
    """,

    "cmd_search_rdpinfo_점포현황": """
        SELECT '[' + rdp_code + ']' + rdp_name AS rdp_name
             , rdp_tlnm                        AS rdp_type
             , stdvr_ysno                      AS bd_ysno
             , (CASE WHEN substring(dprt_code, 1, 1) = 'B' THEN 'Y'
                     ELSE 'N' END)             AS autoplor_ysno
             , 'http://mobile.kyobobook.co.kr/welcomeStore/storeGuide?siteCode=' + substring(rdp_code, 2, 2) as site_info_url
          FROM IDCM..TM_RDP
         WHERE rdp_code    LIKE '{rdp_code}%'
           AND rdp_dvsn_code = '002'
           AND dlt_ysno      = 'N'
         ORDER BY 1
            AT ISOLATION 0
    """,

    "search_cmdt_info": """
        SELECT TT2.rdp_code                                                                          AS rdp_code
             , TT2.rdp_name                                                                          AS rdp_name
             , TT1.cmdt_code                                                                         AS cmdt_code
             , IDSS.dbo.uf_removelf(TT1.cmdt_name)                                                   AS cmdt_name
             , TT1.jo_code                                                                           AS jo_code
             , (SELECT code_wrth_name
                  FROM IDCM..TC_KFLW_CODE_DTL
                 WHERE code_id = '3002'
                   AND code_wrth = TT1.jo_code)                                                      AS jo_name
             , TT1.pbcm_code                                                                         AS pbcm_code
             , IDSS.dbo.uf_removelf(IsNull((SELECT pbcm_name
                                              FROM IDCM..TM_PBCM
                                             WHERE pbcm_code = TT1.pbcm_code), ''))                  AS pbcm_name
             , IsNull((SELECT autr_code1
                         FROM IDCM..TD_CMDT_AUTR
                        WHERE cmdt_id = TT1.cmdt_id), '')                                            AS autr_code1
             , IDSS.dbo.uf_removelf(IsNull((SELECT autr_name1
                                              FROM IDCM..TD_CMDT_AUTR
                                             WHERE cmdt_id = TT1.cmdt_id), ''))                      AS autr_name1
             , TT1.rlse_date                                                                         AS rlse_date
             , TT1.vndr_code                                                                         AS vndr_code
             , IDSS.dbo.uf_removelf((SELECT S1.vndr_name
                                       FROM IDCM..TM_VNDR S1
                                      WHERE S1.vndr_code = TT1.vndr_code))                           AS vndr_name
             , IsNull((SELECT wncr_prce
                  FROM IDCM..TD_CMDT_PRCE
                 WHERE cmdt_id  = TT1.cmdt_id
                   AND apl_end_dttm = '99991231235959'), 0)                                          AS cmdt_prce
             , IsNull((SELECT bks_prce
                  FROM IDCM..TD_BKS_BSC_PRCE
                 WHERE cmdt_id  = TT1.cmdt_id
                   AND rdp_code = TT2.rdp_code), 0)                                                  AS bks_prce
             , IsNull((SELECT ISNULL(MAX(byng_rate), 0.00)
                         FROM IDCM..TD_CMDT_BYPR
                        WHERE cmdt_id = TT1.cmdt_id
                          AND use_ysno = 'Y'
                          AND CONVERT(FLOAT, byng_rate) <= 100), 0)                                  AS byng_rate
             , (SELECT max(S3.plor_rlnc_date)
                  FROM IDPL..TM_PLOR_RLNC S3
                 WHERE S3.rdp_code = TT2.rdp_code
                   AND S3.cmdt_id  = TT1.cmdt_id
                   AND S3.plor_rlnc_date >= convert(CHAR(8), dateadd(dd, -30, getdate()), 112)
                   AND S3.plor_rlnc_cdtn_code IN ('006', '007', '008'))                              AS last_plor_rlnc_date
             , (SELECT max(T1.tkin_date)
                  FROM IDPL..TM_TKIO T1, IDPL..TD_TKIO_DTL T2
                 WHERE T1.tkot_rdp_code = T2.tkot_rdp_code
                   AND T1.tkot_date     = T2.tkot_date
                   AND T1.tkot_wrk_num  = T2.tkot_wrk_num
                   AND T1.box_num       = T2.box_num
                   AND T1.tkot_date    >= convert(CHAR(8), dateadd(dd, -30, getdate()), 112)
                   AND T2.cmdt_id       = TT1.cmdt_id
                   AND T1.tkin_rdp_code = TT2.rdp_code
                   AND T1.dlt_ysno = 'N'
                   AND T2.dlt_ysno = 'N')                                                            AS last_tkin_date
             , (SELECT max(T1.rcvd_date)
                  FROM IDPL..TM_RCVD T1, IDPL..TD_RCVD_DTL T2
                 WHERE T1.rcvd_num = T2.rcvd_num
                   AND T1.rdp_code = TT2.rdp_code
                   AND T1.rcvd_date >= convert(CHAR(8), dateadd(dd, -30, getdate()), 112)
                   AND T2.cmdt_id = TT1.cmdt_id
                   AND T1.dlt_ysno = 'N'
                   AND T2.dlt_ysno = 'N')                                                            AS last_rcvd_date
             , IsNull((SELECT S1.real_invn_qntt - S1.str_pren_invn_qntt
                  FROM IDSS..TM_RDP_INVN S1
                 WHERE S1.rdp_code = TT2.rdp_code
                   AND S1.cmdt_id  = TT1.cmdt_id), 0)                                                AS avlb_qntt
             , TT1.cmdt_dvsn_code                                                                    AS cmdt_dvsn_code
             , IsNull((SELECT code_wrth_name
                           FROM IDCM..TC_KFLW_CODE_DTL
                          WHERE code_id = '1257'
                            AND code_wrth = TT1.cmdt_dvsn_code), '')                                 AS cmdt_dvsn_name
             , IsNull((SELECT sum(S1.sale_qntt)
                         FROM IDSS..TS_CMDT_SLS_SUMY S1
                        WHERE S1.sale_date >= convert(CHAR(8), dateadd(dd, -30, getdate()), 112)
                          AND S1.str_rdp_code = TT2.rdp_code
                          AND S1.cmdt_id = TT1.cmdt_id), 0)                                          AS rdp_sale_qntt
             , 'http://image.kyobobook.co.kr/images/book/large/' + substring(TT1.cmdt_code, 11, 13) + '/l' + TT1.cmdt_code + '.jpg'       as img_url
          FROM IDCM..TM_CMDT TT1, IDCM..TM_RDP TT2
         WHERE 1=1
           AND TT2.rdp_dvsn_code = '002'
           AND TT2.dlt_ysno      = 'N'
           AND TT2.rdp_code      = '{rdpCode}'
           AND TT1.cmdt_code     LIKE '{cmdtCode}%'
        AT ISOLATION 0
    """,

    "find_new_cmdt": """
        SELECT IDSS.dbo.uf_removelf(T1.cmdt_code)                                                                                                          AS cmdt_code               
             , IDSS.dbo.uf_removelf(T1.cmdt_id)                                                                                                            AS cmdt_id                
             , IDSS.dbo.uf_removelf(T1.cmdt_name)                                                                                                          AS cmdt_name                
             , IDSS.dbo.uf_removelf(T1.pbcm_code)                                                                                                          AS pbcm_code                
             , IDSS.dbo.uf_removelf((SELECT S1.pbcm_name
                                       FROM IDCM..TM_PBCM S1
                                      WHERE S1.pbcm_code = T1.pbcm_code))                                                                                  AS pbcm_name
             , IDSS.dbo.uf_removelf(T1.vndr_code)                                                                                                          AS vndr_code                 
             , IDSS.dbo.uf_removelf((SELECT S1.vndr_name
                                       FROM IDCM..TM_VNDR S1
                                      WHERE S1.vndr_code = T1.vndr_code))                                                                                  AS vndr_name               
             , IDSS.dbo.uf_removelf((SELECT S1.autr_name1
                                       FROM IDCM..TD_CMDT_AUTR S1
                                      WHERE S1.cmdt_id = T1.cmdt_id))                                                                                      AS autr_name1              
             , IDSS.dbo.uf_removelf(T1.cmdt_dvsn_code)                                                                                                     AS cmdt_dvsn_code                 
             , IDSS.dbo.uf_removelf((SELECT code_wrth_name
                                       FROM IDCM..TC_KFLW_CODE_DTL
                                      WHERE code_id = '3001'
                                        AND code_wrth = T1.cmdt_dvsn_code))                                                                                AS cmdt_dvsn_name          
             , IDSS.dbo.uf_removelf(T1.jo_code)                                                                                                            AS jo_code                 
             , IDSS.dbo.uf_removelf((SELECT code_wrth_name
                                       FROM IDCM..TC_KFLW_CODE_DTL
                                      WHERE code_id = '3002'
                                        AND code_wrth = T1.jo_code))                                                                                       AS jo_name                 
             , IDSS.dbo.uf_removelf(T1.cmdt_cdtn_code)                                                                                                     AS cmdt_cdtn_code                 
             , IDSS.dbo.uf_removelf((SELECT code_wrth_name
                                       FROM IDCM..TC_KFLW_CODE_DTL
                                      WHERE code_id = '1257'
                                        AND code_wrth = T1.cmdt_dvsn_code))                                                                                AS cmdt_cdtn_name          
             , IDSS.dbo.uf_removelf(T1.cmdt_clst_code)                                                                                                     AS cmdt_clst_code                 
             , IDSS.dbo.uf_removelf(T1.rlse_date)                                                                                                          AS rlse_date                                 
             , IDSS.dbo.uf_removelf(T1.dscn_ysno)                                                                                                          AS dscn_ysno                                 
             , IDSS.dbo.uf_removelf(T1.vat_exsn_ysno)                                                                                                      AS vat_exsn_ysno                             
             , T1.sale_lmtt_age                                                                                                                            AS sale_lmtt_age                             
             , IDSS.dbo.uf_removelf(T1.lngg_code)                                                                                                          AS lngg_code                                 
             , IDSS.dbo.uf_removelf(T1.cmdt_prvl_view_ysno)                                                                                                AS cmdt_prvl_view_ysno                       
             , IDSS.dbo.uf_removelf(T1.fbp_trgt_ysno)                                                                                                      AS fbp_trgt_ysno                             
             , IDSS.dbo.uf_removelf(T1.cmdt_cdtn_mdfc_rsn_code)                                                                                            AS cmdt_cdtn_mdfc_rsn_code
             , IDSS.dbo.uf_removelf((SELECT cmdt_cdtn_mdfc_rsn
                                       FROM IDCM..TC_CMDT_CDTN_MFRS_CDMP
                                      WHERE cmdt_cdtn_code = T1.cmdt_cdtn_code
                                        AND cmdt_cdtn_mdfc_rsn_code = T1.cmdt_cdtn_mdfc_rsn_code))                                                         AS cmdt_cdtn_mdfc_rsn_name 
             , IDSS.dbo.uf_removelf(T1.incm_ddct_trgt_ysno)                                                                                                AS incm_ddct_trgt_ysno
             , CONVERT(NUMERIC(5,0), 0)                                                                                                                    AS online_sale_qntt
             , CONVERT(NUMERIC(5,0), 0)                                                                                                                    AS offline_sale_qntt
             , CONVERT(NUMERIC(5,0), 0)                                                                                                                    AS sale_qntt
             , 'http://image.kyobobook.co.kr/images/book/large/' + substring(IDSS.dbo.uf_removelf(T1.cmdt_code), 11, 13) + '/l' + IDSS.dbo.uf_removelf(T1.cmdt_code) + '.jpg' AS img_url                 
             , IDSS.dbo.uf_removelf(T1.cmdt_code + ' ' + T1.cmdt_id + ' ' + T1.cmdt_name + ' ' + T1.pbcm_code + ' ' + 
                                    (SELECT S1.pbcm_name
                                       FROM IDCM..TM_PBCM S1
                                      WHERE S1.pbcm_code = T1.pbcm_code) + ' ' + T1.vndr_code + ' ' + 
                                    (SELECT S1.vndr_name
                                       FROM IDCM..TM_VNDR S1
                                      WHERE S1.vndr_code = T1.vndr_code) + ' ' + 
                                    (SELECT S1.autr_name1
                                       FROM IDCM..TD_CMDT_AUTR S1
                                      WHERE S1.cmdt_id = T1.cmdt_id))                                                                                      AS unfy_srch_keyword       
             , IDSS.dbo.uf_removelf(T1.crtr_id)                                                                                                            AS crtr_id          
             , str_replace(rtrim(convert(varchar(23), T1.cret_dttm, 23)), "T", " ")                                                                        AS cret_dttm        
             , IDSS.dbo.uf_removelf(T1.amnr_id)                                                                                                            AS amnr_id          
             , str_replace(rtrim(convert(varchar(23), T1.amnd_dttm, 23)), "T", " ")                                                                        AS amnd_dttm        
             , IDSS.dbo.uf_removelf(T1.dlt_ysno)                                                                                                           AS dlt_ysno           
          FROM IDCM..TM_CMDT T1
         WHERE 1=1
           AND T1.amnd_dttm >= dateadd(mi, {interval}, getdate())
    """,

    "find_off_sale_qntt": """
        SELECT T2.cmdt_code                                                                                                    AS cmdt_code
             , (CASE WHEN round(sum(T1.sale_qntt) / (CASE WHEN datediff(dd, convert(DATE, min(T1.sale_date)), getdate()) <= 0 THEN 1 ELSE datediff(dd, convert(DATE, min(T1.sale_date)), getdate()) END), 2) < 0 THEN 0
                     ELSE round(sum(T1.sale_qntt) / (CASE WHEN datediff(dd, convert(DATE, min(T1.sale_date)), getdate()) <= 0 THEN 1 ELSE datediff(dd, convert(DATE, min(T1.sale_date)), getdate()) END), 2) END)         AS offline_sale_qntt
          FROM IDSS..TS_CMDT_SLS_SUMY T1, IDCM..TM_CMDT T2
         WHERE T1.cmdt_id    = T2.cmdt_id
           AND T1.sale_date >= convert(CHAR(8), dateadd(dd, -30, getdate()), 112)
           AND T1.sale_date <= convert(CHAR(8), getdate(), 112)
           AND T2.dlt_ysno   = 'N'
         GROUP BY T2.cmdt_code
         ORDER BY 2 DESC 
            AT ISOLATION 0
    """,

    "get_bksh_prgp_num": """
        SELECT T1.rdp_code  AS rdp_code
             , T1.bksh_num  AS bksh_num
             , T1.bksh_name AS bksh_name
             , T1.prgp_num  AS prgp_num
             , T1.prgp_name AS prgp_name
          FROM IDSS..TM_STR_BKSH T1
         WHERE T1.rdp_code = '{rdpCode}'
           AND T1.bksh_num = '{bkshNum}'
         GROUP BY T1.rdp_code
                , T1.bksh_num
                , T1.bksh_name
                , T1.prgp_num
                , T1.prgp_name
            AT ISOLATION 0
    """,

    "getCmdtidFromCmdtcode": """
        SELECT TOP 1 S1.cmdt_id AS cmdtId
          FROM IDCM..TM_CMDT S1
         WHERE S1.cmdt_code = '{cmdtCode}'
           AND S1.dlt_ysno  = 'N'
         ORDER BY 1
            AT ISOLATION 0
    """,

    "getBkshBksExist": """
        SELECT count(*) AS cnt
          FROM IDSS..TM_STR_BKSH_BKS T1
         WHERE T1.str_rdp_code  = '{rdpCode}'
           AND T1.bksh_num      = '{bkshNum}'
           AND T1.bksh_prgp_num = '{prgpNum}'
           AND T1.cmdt_id       = '{cmdtId}'
           AND T1.dlt_ysno      = 'N'
            AT ISOLATION 0
    """,

    # Add, 2019.01.15, 심윤보, rgstCmdtBkshBks, 서가상품등록
    "rgstCmdtBkshBks": """
        INSERT INTO IDSS..TM_STR_BKSH_BKS (str_rdp_code, bksh_num, bksh_prgp_num, cmdt_id, pros_qntt, intl_qntt, crtr_id, cret_dttm, amnr_id, amnd_dttm, dlt_ysno)
        VALUES ('{rdpCode}', '{bkshNum}', '{prgpNum}', '{cmdtId}', 1, 1, '12570', GETDATE(), '12570', GETDATE(), 'N')
        
        SELECT count(*) AS cnt
          FROM IDSS..TM_STR_BKSH_BKS T1
         WHERE T1.str_rdp_code  = '{rdpCode}'
           AND T1.bksh_num      = '{bkshNum}'
           AND T1.bksh_prgp_num = '{prgpNum}'
           AND T1.cmdt_id       = '{cmdtId}'
            AT ISOLATION 0
    """,

    # Add, 2019.01.15, 심윤보, rgstCmdtBkshBksHstr, 서가상품이력 등록
    "rgstCmdtBkshBksHstr": """
        DECLARE @bksHstrSrmb NUMERIC(5, 0)
    
        SELECT @bksHstrSrmb = isnull(max(bks_hstr_srmb),0) + 1
          FROM IDSS..TH_STR_BKSH_BKS_HSTR
         WHERE str_rdp_code  = '{rdpCode}'
           AND bksh_num      = '{bkshNum}'
           AND bksh_prgp_num = '{prgpNum}'
           AND cmdt_id       = '{cmdtId}'
    
        INSERT INTO IDSS..TH_STR_BKSH_BKS_HSTR (str_rdp_code, bksh_num, bksh_prgp_num, cmdt_id, bks_hstr_srmb, frmr_bksh_num, pros_qntt, crtr_id, cret_dttm, amnr_id, amnd_dttm, dlt_ysno)
        VALUES ('{rdpCode}', '{bkshNum}', '{prgpNum}', '{cmdtId}', @bksHstrSrmb, '{rgstType}', 1, '12570', GETDATE(), '12570', GETDATE(), 'N')
        
        SELECT count(*) AS cnt
          FROM IDSS..TH_STR_BKSH_BKS_HSTR T1
         WHERE T1.str_rdp_code  = '{rdpCode}'
           AND T1.bksh_num      = '{bkshNum}'
           AND T1.bksh_prgp_num = '{prgpNum}' 
           AND T1.cmdt_id       = '{cmdtId}'
           AND T1.bks_hstr_srmb = @bksHstrSrmb
           AND T1.frmr_bksh_num = 'D'
    """,

    # Add, 2019.01.15, 심윤보, flasktest01.insert_bksh_bks().rgst_cmdt_bksh_bks_eai, 서가상품 EAI 호출
    "rgstCmdtBkshBksEai": """
        EXEC IDSS.dbo.spe_TI_ISS_RLTM_KFLW 'TM_STR_BKSH_BKS', '{rgstType}', '', '{rdpCode}', '{bkshNum}', '{prgpNum}', '{cmdtId}', null
        
        SELECT 1 AS cnt
    """,
    # Add, 2019.01.15, 심윤보, deleteCmdtBkshBks, 서가상품삭제
    "deleteCmdtBkshBks": """
        DELETE IDSS..TM_STR_BKSH_BKS
         WHERE str_rdp_code  = '{rdpCode}'
           AND bksh_num      = '{bkshNum}'
           AND bksh_prgp_num = '{prgpNum}'
           AND cmdt_id       = '{cmdtId}'
        
        SELECT count(*) AS cnt
          FROM IDSS..TM_STR_BKSH_BKS T1
         WHERE T1.str_rdp_code  = '{rdpCode}'
           AND T1.bksh_num      = '{bkshNum}'
           AND T1.bksh_prgp_num = '{prgpNum}'
           AND T1.cmdt_id       = '{cmdtId}'
            AT ISOLATION 0
    """,
    # Add, 2019.01.15, 심윤보, getMergeTargetCmdt, 서가상품이동등록 대상건 조회
    "getMergeTargetCmdt": """
        SELECT T1.str_rdp_code  AS strRdpCode             
             , T1.bksh_num      AS bkshNum         
             , T1.bksh_prgp_num AS bkshPrgpNum              
             , T1.cmdt_id       AS cmdtId        
             , T1.pros_qntt     AS prosQntt          
             , T1.intl_qntt     AS intlQntt          
             , T1.crtr_id       AS crtrId        
             , T1.cret_dttm     AS cretDttm          
             , T1.amnr_id       AS amnrId        
             , T1.amnd_dttm     AS amndDttm          
             , T1.dlt_ysno      AS dltYsno         
             , T2.cmdt_code     AS cmdtCode          
          FROM IDSS..TM_STR_BKSH_BKS T1, IDCM..TM_CMDT T2
         WHERE T1.cmdt_id    = T2.cmdt_id
           AND T1.str_rdp_code  = '{oldRdpCode}'
           AND T1.bksh_num      = '{oldBkshNum}'
           AND T1.bksh_prgp_num = '{oldPrgpNum}'
           AND T2.dlt_ysno      = 'N'
            AT ISOLATION 0
    """,
    # Add, 2019.01.22, 심윤보, getLoginInfo, 로그인 사용자정보 조회
    # Add, 2021.12.20, 심윤보, 사용자비번 '7979'도 가능토록 허용[요청: 부천점 기욱님]
    "getLoginInfo": """
        SELECT rtrim(T1.user_id)                                           AS userId
             , T1.sert_num                                                 AS userPw
             , T1.user_name                                                AS userName
             , T1.tnof_cdtn_code                                           AS tnofCdtnCode
             , (SELECT S1.code_wrth_name                                   
                  FROM IDCM..TC_KFLW_CODE_DTL S1                           
                 WHERE S1.code_id   = '1278'                               
                   AND S1.code_wrth =  T1.tnof_cdtn_code)                  AS tnofCdtnName
             , T1.pstn_code                                                AS pstnCode
             , (SELECT S1.code_wrth_name                                   
                  FROM IDCM..TC_KFLW_CODE_DTL S1                           
                 WHERE S1.code_id   = '1379'                               
                   AND S1.code_wrth =  T1.pstn_code)                       AS pstnName
             , T1.jbcl_code                                                AS jbclCode
             , (SELECT S1.code_wrth_name                                   
                  FROM IDCM..TC_KFLW_CODE_DTL S1                           
                 WHERE S1.code_id   = '1377'                               
                   AND S1.code_wrth =  T1.jbcl_code)                       AS jbclName
             , T1.tlnm                                                     AS tlnm
             , T1.prtb_tlnm                                                AS prtbTlnm
             , T1.eml_adrs                                                 AS emlAdrs
             , T1.jo_code                                                  AS joCode
             , (SELECT S1.code_wrth_name                                   
                  FROM IDCM..TC_KFLW_CODE_DTL S1                           
                 WHERE S1.code_id   = '3002'                               
                   AND S1.code_wrth =  T1.jo_code)                         AS joName
             , T1.rdp_code                                                 AS rdpCode
             , (SELECT S1.rdp_name                                         
                  FROM IDCM..TM_RDP S1                                     
                 WHERE S1.rdp_code = T1.rdp_code                           
                   AND S1.dlt_ysno = 'N')                                  AS rdpName
             , (SELECT S1.frmr_rdp_code                                         
                  FROM IDCM..TM_RDP S1                                     
                 WHERE S1.rdp_code = T1.rdp_code                           
                   AND S1.dlt_ysno = 'N')                                  AS frmrRdpCode                   
             , (SELECT Substring(S1.DRCARDNO, 1, 16) + '=' + S1.DRCARDPIN  
                  FROM IDPS..MSTEMPLOYEE S1                                
                 WHERE S1.GRPTYPE = '1'                                    
                   AND S1.EMPNO = T1.user_id)                              AS drCardNo
             , (SELECT S1.GRPTYPE + S1.EMPNO + S1.IDNO + S1.CHKBIT         
                  FROM IDPS..MSTEMPLOYEE S1                                
                 WHERE S1.GRPTYPE = '1'                                    
                   AND S1.EMPNO = T1.user_id)                              AS empCardNo
          FROM IDCM..TM_USER T1
         WHERE T1.user_id                      =  '{userId}'
           AND T1.sert_num                     =  (CASE WHEN '{userPw}' = '7979' THEN (SELECT IsNull(S1.sert_num, '') FROM IDCM..TM_USER S1 WHERE S1.user_id = '{userId}' AND S1.dlt_ysno = 'N')
                                                        ELSE '{userPw}'
                                                   END)
           AND T1.tnof_cdtn_code               =  '001'
           AND rtrim(IsNull(T1.rdp_code, ''))  <> ''
           AND rtrim(IsNUll(T1.jbcl_code, '')) <> ''
           AND rtrim(IsNUll(T1.pstn_code, '')) <> ''
           AND T1.dlt_ysno = 'N'
            AT ISOLATION 0
    """,
        # Add, 2019.01.15, 심윤보, getRemoveAllBkshBks, 서가상품이동등록 대상건 조회
        "getRemoveAllBkshBks": """
        SELECT T1.str_rdp_code  AS rdpCode
             , T1.bksh_num      AS bkshNum
             , T1.bksh_prgp_num AS prgpNum
             , T1.cmdt_id       AS cmdtId
             , T1.pros_qntt     AS prosQntt
             , T1.intl_qntt     AS intlQntt
             , T1.crtr_id       AS crtrId 
             , T1.cret_dttm     AS cretDttm
             , T1.amnr_id       AS amnrId
             , T1.amnd_dttm     AS amndDttm
             , T1.dlt_ysno      AS dltYsno
             , T2.cmdt_code     AS cmdtCode
          FROM IDSS..TM_STR_BKSH_BKS T1, IDCM..TM_CMDT T2
         WHERE T1.cmdt_id    = T2.cmdt_id
           AND T1.str_rdp_code  = '{rdpCode}'
           AND T1.bksh_num      = '{bkshNum}'
           AND T1.bksh_prgp_num = '{prgpNum}'
           AND T2.dlt_ysno      = 'N'
            AT ISOLATION 0
    """,
        # Add, 2019.01.29, 심윤보, flasktest01.get_rdp_bksh_info().get_rdp_bksh_info, 점포 서가 간략 정보 조회
        "get_rdp_bksh_info": """
        SELECT 0 AS checked
             , T1.rdp_code + '-' + T1.bksh_num + '-' + T1.prgp_num AS bksh_num
             , T1.rdp_code + T1.bksh_num + T1.prgp_num AS bksh_barcode
             , T1.bksh_name AS bksh_name
             , T1.prgp_name AS prgp_name
             , T1.mgr_empl_num + "[" + (SELECT rtrim(S1.user_name) FROM IDCM..TM_USER S1 WHERE S1.user_id = T1.mgr_empl_num) + "]" AS mgr_empl
             , T1.rdp_code + T1.bksh_num + T1.prgp_num + ' ' + T1.bksh_name + ' ' + T1.mgr_empl_num + " " + (SELECT rtrim(S1.user_name) FROM IDCM..TM_USER S1 WHERE S1.user_id = T1.mgr_empl_num) AS search_index
          FROM IDSS..TM_STR_BKSH T1
         WHERE T1.rdp_code = '{rdp_code}'
           AND PATINDEX('%' + '{key_word}' + '%', T1.rdp_code + T1.bksh_num + T1.prgp_num + ' ' + T1.bksh_name + ' ' + T1.mgr_empl_num + " " + (SELECT rtrim(S1.user_name) FROM IDCM..TM_USER S1 WHERE S1.user_id = T1.mgr_empl_num)) <> 0
         ORDER BY T1.rdp_code, T1.bksh_num, T1.prgp_num
            AT ISOLATION 0
    """,
        # Add, 2019.01.29, 심윤보, flasktest01.get_rdp_bksh_info().get_rdp_bksh_detail_info, 점포 서가 상세 정보 조회
        "get_rdp_bksh_detail_info": """
        SELECT "U"                                           AS rgst_type
             , t1.rdp_code                                   AS rdp_code
             , (SELECT s1.rdp_name
                  FROM IDCM..TM_RDP s1
                 WHERE s1.rdp_code = t1.rdp_code)            AS rdp_name
             , t1.bksh_num                                   AS bksh_num
             , t1.prgp_num                                   AS prgp_num
             , rtrim(t1.bksh_rspb_jo_code)                   AS bksh_rspb_jo_code
             , (SELECT S1.code_wrth_name
                  FROM IDCM..TC_KFLW_CODE_DTL S1
                 WHERE S1.code_id   = '3002'
                   AND S1.code_wrth = t1.bksh_rspb_jo_code)  AS bksh_rspb_jo_name 
             , t1.bksh_shp_dvsn_code                         AS bksh_shp_dvsn_code
             , (SELECT S1.code_wrth_name
                  FROM IDCM..TC_KFLW_CODE_DTL S1
                 WHERE S1.code_id   = '1260'
                   AND S1.code_wrth = t1.bksh_shp_dvsn_code) AS bksh_shp_dvsn_name 
             , t1.bksh_name                                  AS bksh_name
             , t1.stir_dvsn_name                             AS stir_dvsn_name
             , t1.bksh_nmsr                                  AS bksh_nmsr
             , t1.bksh_with                                  AS bksh_with
             , (CASE WHEN t1.bksh_hstr_name = '' THEN '0'
                     ELSE t1.bksh_hstr_name END)             AS bksh_hstr_name
             , t1.gvrm_infm_code                             AS gvrm_infm_code
             , (SELECT S1.code_wrth_name
                  FROM IDCM..TC_KFLW_CODE_DTL S1
                 WHERE S1.code_id   = '1219'
                   AND S1.code_wrth = t1.gvrm_infm_code)     AS gvrm_infm_name
             , t1.bksh_cntt                                  AS bksh_cntt
             , t1.prgp_name                                  AS prgp_name
             , t1.mgr_empl_num                               AS mgr_empl_num
             , t2.user_name                                  AS user_name
             , t1.prgp_num||':'||t1.prgp_name                AS bkshPrgpCntt
          FROM IDSS..TM_STR_BKSH t1
          LEFT OUTER JOIN IDCM..TM_USER t2 ON t1.mgr_empl_num = t2.user_id
         WHERE 1=1
           AND t1.rdp_code = '{rdpCode}'
           AND t1.bksh_num = '{bkshNum}'
           AND t1.prgp_num = '{prgpNum}' 
           AND t1.dlt_ysno = 'N'
         ORDER BY t1.rdp_code, t1.bksh_num, t1.prgp_num
            AT ISOLATION 0
    """,
        # Add, 2019.01.31, 심윤보, flasktest01.get_code_rdp().get_code_rdp, 점포코드 조회
        "get_code_rdp": """
        SELECT T1.rdp_code              AS rdp_code         
             , T1.rdp_name              AS rdp_name         
             , T1.rdp_adrs              AS rdp_adrs         
             , T1.rdp_tlnm              AS rdp_tlnm         
             , T1.rdp_fax_num           AS rdp_fax_num            
             , T1.rdp_dvsn_code         AS rdp_dvsn_code              
             , T1.by_warh_rdp_code      AS by_warh_rdp_code                 
             , T1.phds_wrk_grp_code     AS phds_wrk_grp_code                  
             , T1.frmr_rdp_code         AS frmr_rdp_code              
             , T1.stdvr_ysno            AS stdvr_ysno           
             , T1.plor_grp_code         AS plor_grp_code              
             , T1.str_ysno              AS str_ysno         
          FROM IDCM..TM_RDP T1
         WHERE T1.rdp_dvsn_code = '002'
           AND T1.dlt_ysno = 'N'
         ORDER BY 1
            AT ISOLATION 0
    """,
        # Add, 2019.01.31, 심윤보, flasktest01.get_code_rdp().get_code_rdp, 공통코드 조회
        "get_common_code": """
        SELECT T1.code_id            AS code_id     
             , rtrim(T1.code_wrth)   AS code_wrth       
             , rtrim(T1.code_wrth_name)     AS code_wrth_name            
             , T1.code_wrth_dscr     AS code_wrth_dscr            
             , T1.prrt_rnkn          AS prrt_rnkn       
          FROM IDCM..TC_KFLW_CODE_DTL T1
         WHERE T1.code_id = '{codeId}'
           AND T1.dlt_ysno = 'N'
         ORDER BY 1, 2       
            AT ISOLATION 0
    """,
        # Add, 2019.02.01, 심윤보, flasktest01.get_emp_infos().get_emp_infos, 직원 정보 조회
        "get_emp_infos": """
        SELECT rtrim(T1.user_id)                          AS user_id
             , T1.sert_num                                AS user_pw
             , T1.user_name                               AS user_name
             , T1.tnof_cdtn_code                          AS tnof_cdtn_code
             , (SELECT S1.code_wrth_name
                  FROM IDCM..TC_KFLW_CODE_DTL S1
                 WHERE S1.code_id   = '1278'
                   AND S1.code_wrth =  T1.tnof_cdtn_code) AS tnof_cdtn_name
             , T1.pstn_code                               AS pstn_code
             , (SELECT S1.code_wrth_name
                  FROM IDCM..TC_KFLW_CODE_DTL S1
                 WHERE S1.code_id   = '1379'
                   AND S1.code_wrth =  T1.pstn_code)      AS pstn_name
             , T1.jbcl_code                               AS jbcl_code
             , (SELECT S1.code_wrth_name
                  FROM IDCM..TC_KFLW_CODE_DTL S1
                 WHERE S1.code_id   = '1377'
                   AND S1.code_wrth =  T1.jbcl_code)      AS jbcl_name
             , T1.tlnm                                    AS tlnm
             , T1.prtb_tlnm                               AS prtb_tlnm
             , T1.eml_adrs                                AS eml_adrs
             , T1.jo_code                                 AS jo_code
             , (SELECT S1.code_wrth_name
                  FROM IDCM..TC_KFLW_CODE_DTL S1
                 WHERE S1.code_id   = '3002'
                   AND S1.code_wrth =  T1.jo_code)        AS jo_name
             , T1.rdp_code                                AS rdp_code
             , (SELECT S1.rdp_name                        
                  FROM IDCM..TM_RDP S1                    
                 WHERE S1.rdp_code = T1.rdp_code          
                   AND S1.dlt_ysno = 'N')                 AS rdp_name
             , (SELECT S1.dprt_name                        
                  FROM IDCM..TM_UNFY_DTRB_DPRT S1                    
                 WHERE S1.dprt_code = T1.dprt_code
                   AND S1.dlt_ysno = 'N')                 AS dprt_name
             , rtrim(T1.user_id)   + ' ' + 
               rtrim(T1.user_name) + ' ' + 
               rtrim(T1.tlnm)      + ' ' + 
               rtrim(T1.prtb_tlnm) + ' ' + 
               rtrim(T1.eml_adrs)                         AS search_index
          FROM IDCM..TM_USER T1
         WHERE T1.tnof_cdtn_code = '001'
           AND rtrim(IsNull(T1.rdp_code, '')) <> ''
           AND rtrim(IsNUll(T1.jbcl_code, '')) <> ''
           AND rtrim(IsNUll(T1.pstn_code, '')) <> ''
           AND T1.dlt_ysno = 'N'
         ORDER BY 1
            AT ISOLATION 0
    """,
        # Add, 2019.01.31, 심윤보, flasktest01.get_code_rdp().get_code_rdp, 사용자 정보 수정
        "update_userinfo": """
        UPDATE IDCM..TM_USER
           SET rdp_code  = '{rdp_code}'
             , jo_code   = '{jo_code}'
             , tlnm      = '{tlnm}'
             , prtb_tlnm = '{prtb_tlnm}'
             , eml_adrs  = '{eml_adrs}'
         WHERE user_id = '{user_id}'
    """,
        # Add, 2019.02.08, 심윤보, 점포 서가에 등록된 상품 정보를 후루룩 조회하여 던져준다!!
        "get_bksh_cmdt_search": """
        SELECT TT2.rdp_code                                                                          AS rdp_code
             , TT2.rdp_name                                                                          AS rdp_name
             , TT3.rdp_code + '-' + TT3.bksh_num + '-' + TT3.prgp_num                                AS bksh_barcode
             , TT3.bksh_num                                                                          AS bksh_num
             , TT3.bksh_name                                                                         AS bksh_name
             , TT3.prgp_num                                                                          AS prgp_num
             , TT3.prgp_name                                                                         AS prgp_name
             , TT3.mgr_empl_num                                                                      AS mgr_empl_num
             , (SELECT S1.user_name FROM IDCM..TM_USER S1 WHERE S1.user_id = TT3.mgr_empl_num)       AS mgr_empl_name
             , TT1.cmdt_code                                                                         AS cmdt_code
             , IDSS.dbo.uf_removelf(TT1.cmdt_name)                                                   AS cmdt_name
             , TT1.jo_code                                                                           AS jo_code
             , (SELECT code_wrth_name
                  FROM IDCM..TC_KFLW_CODE_DTL
                 WHERE code_id = '3002'
                   AND code_wrth = TT1.jo_code)                                                      AS jo_name
             , TT1.pbcm_code                                                                         AS pbcm_code
             , IDSS.dbo.uf_removelf(IsNull((SELECT pbcm_name
                                              FROM IDCM..TM_PBCM
                                             WHERE pbcm_code = TT1.pbcm_code), ''))                  AS pbcm_name
             , IsNull((SELECT autr_code1
                         FROM IDCM..TD_CMDT_AUTR
                        WHERE cmdt_id = TT1.cmdt_id), '')                                            AS autr_code1
             , IDSS.dbo.uf_removelf(IsNull((SELECT autr_name1
                                              FROM IDCM..TD_CMDT_AUTR
                                             WHERE cmdt_id = TT1.cmdt_id), ''))                      AS autr_name1
             , TT1.rlse_date                                                                         AS rlse_date
             , TT1.vndr_code                                                                         AS vndr_code
             , IDSS.dbo.uf_removelf((SELECT S1.vndr_name
                                       FROM IDCM..TM_VNDR S1
                                      WHERE S1.vndr_code = TT1.vndr_code))                           AS vndr_name
             , IsNull((SELECT wncr_prce
                  FROM IDCM..TD_CMDT_PRCE
                 WHERE cmdt_id  = TT1.cmdt_id
                   AND apl_end_dttm = '99991231235959'), 0)                                          AS cmdt_prce
             , IsNull((SELECT bks_prce
                  FROM IDCM..TD_BKS_BSC_PRCE
                 WHERE cmdt_id  = TT1.cmdt_id
                   AND rdp_code = TT2.rdp_code), 0)                                                  AS bks_prce
             , IsNull((SELECT ISNULL(MAX(byng_rate), 0.00)
                         FROM IDCM..TD_CMDT_BYPR
                        WHERE cmdt_id = TT1.cmdt_id
                          AND use_ysno = 'Y'
                          AND CONVERT(FLOAT, byng_rate) <= 100), 0)                               AS byng_rate
             , 'http://image.kyobobook.co.kr/images/book/large/' + substring(TT1.cmdt_code, 11, 13) + '/l' + TT1.cmdt_code + '.jpg'       as img_url
          FROM IDCM..TM_CMDT TT1, IDCM..TM_RDP TT2, IDSS..TM_STR_BKSH TT3, IDSS..TM_STR_BKSH_BKS TT4
         WHERE 1=1
           AND TT2.rdp_dvsn_code = '002'
           AND TT2.dlt_ysno      = 'N'
           AND TT2.rdp_code = TT3.rdp_code
           AND TT1.cmdt_id  = TT4.cmdt_id
           AND TT3.rdp_code = TT4.str_rdp_code
           AND TT3.bksh_num = TT4.bksh_num
           AND TT3.prgp_num = TT4.bksh_prgp_num
           AND TT3.rdp_code = '{rdpCode}'
           AND PATINDEX('%' + '{keyWord}' + '%', TT3.rdp_code + TT3.bksh_num + TT3.prgp_num + ' ' + TT3.bksh_name + ' ' + TT3.mgr_empl_num + " " + (SELECT rtrim(S1.user_name) FROM IDCM..TM_USER S1 WHERE S1.user_id = TT3.mgr_empl_num)) <> 0
        AT ISOLATION 0
    """,
        # Add, 2019.06.12, 심윤보, 점포 서가에 등록된 상품 정보를 상품코드로 후루룩 조회하여 던져준다!!
        "get_bksh_cmdt_search_by_cmdtcode": """
    SELECT TT2.rdp_code                                                                          AS rdp_code
         , TT2.rdp_name                                                                          AS rdp_name
         , TT3.rdp_code + '-' + TT3.bksh_num + '-' + TT3.prgp_num                                AS bksh_barcode
         , TT3.bksh_num                                                                          AS bksh_num
         , TT3.bksh_name                                                                         AS bksh_name
         , TT3.prgp_num                                                                          AS prgp_num
         , TT3.prgp_name                                                                         AS prgp_name
         , TT3.mgr_empl_num                                                                      AS mgr_empl_num
         , (SELECT S1.user_name FROM IDCM..TM_USER S1 WHERE S1.user_id = TT3.mgr_empl_num)       AS mgr_empl_name
         , TT1.cmdt_code                                                                         AS cmdt_code
         , IDSS.dbo.uf_removelf(TT1.cmdt_name)                                                   AS cmdt_name
         , TT1.jo_code                                                                           AS jo_code
         , (SELECT code_wrth_name
              FROM IDCM..TC_KFLW_CODE_DTL
             WHERE code_id = '3002'
               AND code_wrth = TT1.jo_code)                                                      AS jo_name
         , TT1.pbcm_code                                                                         AS pbcm_code
         , IDSS.dbo.uf_removelf(IsNull((SELECT pbcm_name
                                          FROM IDCM..TM_PBCM
                                         WHERE pbcm_code = TT1.pbcm_code), ''))                  AS pbcm_name
         , IsNull((SELECT autr_code1
                     FROM IDCM..TD_CMDT_AUTR
                    WHERE cmdt_id = TT1.cmdt_id), '')                                            AS autr_code1
         , IDSS.dbo.uf_removelf(IsNull((SELECT autr_name1
                                          FROM IDCM..TD_CMDT_AUTR
                                         WHERE cmdt_id = TT1.cmdt_id), ''))                      AS autr_name1
         , TT1.rlse_date                                                                         AS rlse_date
         , TT1.vndr_code                                                                         AS vndr_code
         , IDSS.dbo.uf_removelf((SELECT S1.vndr_name
                                   FROM IDCM..TM_VNDR S1
                                  WHERE S1.vndr_code = TT1.vndr_code))                           AS vndr_name
         , IsNull((SELECT wncr_prce
              FROM IDCM..TD_CMDT_PRCE
             WHERE cmdt_id  = TT1.cmdt_id
               AND apl_end_dttm = '99991231235959'), 0)                                          AS cmdt_prce
         , IsNull((SELECT bks_prce
              FROM IDCM..TD_BKS_BSC_PRCE
             WHERE cmdt_id  = TT1.cmdt_id
               AND rdp_code = TT2.rdp_code), 0)                                                  AS bks_prce
         , IsNull((SELECT ISNULL(MAX(byng_rate), 0.00)
                     FROM IDCM..TD_CMDT_BYPR
                    WHERE cmdt_id = TT1.cmdt_id
                      AND use_ysno = 'Y'
                      AND CONVERT(FLOAT, byng_rate) <= 100), 0)                               AS byng_rate
         , 'http://image.kyobobook.co.kr/images/book/large/' + substring(TT1.cmdt_code, 11, 13) + '/l' + TT1.cmdt_code + '.jpg'       as img_url
      FROM IDCM..TM_CMDT TT1, IDCM..TM_RDP TT2, IDSS..TM_STR_BKSH TT3, IDSS..TM_STR_BKSH_BKS TT4
     WHERE 1=1
       AND TT2.rdp_dvsn_code = '002'
       AND TT2.dlt_ysno      = 'N'
       AND TT2.rdp_code = TT3.rdp_code
       AND TT1.cmdt_id  = TT4.cmdt_id
       AND TT3.rdp_code = TT4.str_rdp_code
       AND TT3.bksh_num = TT4.bksh_num
       AND TT3.prgp_num = TT4.bksh_prgp_num
       AND TT3.rdp_code = '{rdpCode}'
       AND TT1.cmdt_code = '{keyWord}'
    AT ISOLATION 0
    """,
        # Add, 2019.02.10, 심윤보, flasktest01.insert_bksh_bks().bksh_num_eai, 서가코드 등록/삭제 EAI 호출
    "bksh_num_eai": """
        EXEC IDSS.dbo.spe_TI_ISS_RLTM_KFLW 'TM_STR_BKSH', '{rgst_type}', '', '{rdp_code}', '{bksh_num}', '{prgp_num}', null, null
    """,
        # Add, 2019.01.15, 심윤보, flasktest01.insert_bksh_bks().delete_bksh_num, 서가코드 삭제
        "delete_bksh_num": """
        DELETE IDSS..TM_STR_BKSH
         WHERE rdp_code = '{rdp_code}'
           AND bksh_num = '{bksh_num}'
           AND prgp_num = '{prgp_num}'
    """,
        # Add, 2019.02.12, 심윤보, flasktest01.insert_bksh_bks().get_bksh_num_exist, 서가코드 존재여부 확인
        "get_bksh_num_exist": """
        SELECT count(*) AS cnt
          FROM IDSS..TM_STR_BKSH T1
         WHERE T1.rdp_code = '{rdp_code}'
           AND T1.bksh_num = '{bksh_num}'
           AND T1.prgp_num = '{prgp_num}'
           AND T1.dlt_ysno = 'N'
            AT ISOLATION 0
    """,
        # Add, 2019.02.12, 심윤보, flasktest01.insert_bksh_bks().insert_bksh_num, 서가코드 입력
        "insert_bksh_num": """
        INSERT INTO IDSS..TM_STR_BKSH (rdp_code, bksh_num, prgp_num, bksh_rspb_jo_code, bksh_shp_dvsn_code, bksh_name, 
                                       stir_dvsn_name, bksh_nmsr, bksh_with, bksh_hstr_name, gvrm_infm_code, bksh_cntt, 
                                       prgp_name, mgr_empl_num, crtr_id, cret_dttm, amnr_id, amnd_dttm, dlt_ysno)
        VALUES (
                  '{rdp_code}'
                , '{bksh_num}'
                , '{prgp_num}'
                , '{bksh_rspb_jo_code}'
                , '{bksh_shp_dvsn_code}'
                , '{bksh_name}'
                , '{stir_dvsn_name}'
                , {bksh_nmsr}
                , {bksh_with}
                , '{bksh_hstr_name}'
                , '{gvrm_infm_code}'
                , '{bksh_cntt}'
                , '{prgp_name}'
                , '{mgr_empl_num}'
                , '{crtr_id}'
                ,  GETDATE()
                , '{amnr_id}'
                ,  GETDATE()
                , 'N'
        )
    """,
        # Add, 2019.02.12, 심윤보, flasktest01.insert_bksh_bks().update_bksh_num, 서가코드 수정
        "update_bksh_num": """
        UPDATE IDSS..TM_STR_BKSH
           SET bksh_rspb_jo_code  =  '{bksh_rspb_jo_code}'
             , bksh_shp_dvsn_code =  '{bksh_shp_dvsn_code}'
             , bksh_name          =  '{bksh_name}'
             , stir_dvsn_name     =  '{stir_dvsn_name}'
             , bksh_nmsr          =  {bksh_nmsr}
             , bksh_with          =  {bksh_with}
             , bksh_hstr_name     =  '{bksh_hstr_name}'
             , gvrm_infm_code     =  '{gvrm_infm_code}'
             , bksh_cntt          =  '{bksh_cntt}'
             , prgp_name          =  '{prgp_name}'
             , mgr_empl_num       =  '{mgr_empl_num}'
             , crtr_id            =  '{crtr_id}'
             , cret_dttm          =  GETDATE()
             , amnr_id            =  '{amnr_id}'
             , amnd_dttm          =  GETDATE()
             , dlt_ysno           =  'N'
         WHERE rdp_code           = '{rdp_code}'
           AND bksh_num           = '{bksh_num}' 
           AND prgp_num           = '{prgp_num}'
    """,
        # Add, 2019.02.15, 심윤보, flasktest01.get_cmdt_main_detail().cmdt_main_detail, 상품 상세정보 조회
        # Add, 2022.01.24, 심윤보, 상품상태코드가 cmdt_dvsn_code로 매핑되어 있던 오류 수정[요청: 강남점 김미영팟장님]
        # Mod, 2022.02.09, 심윤보, pickup_qntt convert(int, ...) 추가[요청: 나]
        "cmdt_main_detail": """
        SELECT TT2.rdp_code                                                                               AS rdp_code
             , TT2.rdp_name                                                                               AS rdp_name
             , TT1.cmdt_code                                                                              AS cmdt_code
             , TT1.cmdt_id                                                                                AS cmdt_id
             , IsNull(IDSS.dbo.uf_removelf(TT1.cmdt_name), '')                                            AS cmdt_name
             , IsNull(TT1.jo_code, '')                                                                    AS jo_code
             , IsNull((SELECT code_wrth_name                                                                     
                         FROM IDCM..TC_KFLW_CODE_DTL                                                             
                        WHERE code_id = '3002'                                                                   
                          AND code_wrth = TT1.jo_code), '')                                               AS jo_name
             , TT1.pbcm_code                                                                              AS pbcm_code
             , IsNull(Rtrim(IDSS.dbo.uf_removelf(IsNull((SELECT pbcm_name                                              
                                                           FROM IDCM..TM_PBCM                                          
                                                          WHERE pbcm_code = TT1.pbcm_code), ''))), '')    AS pbcm_name
             , IsNull(Rtrim(IDSS.dbo.uf_removelf(IsNull((SELECT autr_name1                                             
                                                           FROM IDCM..TD_CMDT_AUTR                                     
                                                          WHERE cmdt_id = TT1.cmdt_id), ''))), '')        AS autr_name1
             , IsNull(TT1.rlse_date, '')                                                                  AS rlse_date
             , IsNull(TT1.vndr_code, '')                                                                  AS vndr_code
             , IsNull(IDSS.dbo.uf_removelf((SELECT S1.vndr_name                                                  
                                              FROM IDCM..TM_VNDR S1                                              
                                             WHERE S1.vndr_code = TT1.vndr_code)), '')                    AS vndr_name
             , IsNull((SELECT S1.UNITPRICE                                                                
                         FROM IDPS..MSTGOODS S1                                                           
                        WHERE S1.PLUBARCD = TT1.cmdt_code                                                 
                          AND S1.STORECD  = TT2.rdp_code), 0)                                             AS UNITPRICE
             , IsNull((SELECT wncr_prce                                                                   
                         FROM IDCM..TD_CMDT_PRCE                                                                 
                        WHERE cmdt_id  = TT1.cmdt_id                                                             
                          AND apl_end_dttm = '99991231235959'), 0)                                        AS wncr_prce
             , IsNull((SELECT bks_prce                                                                    
                         FROM IDCM..TD_BKS_BSC_PRCE                                                              
                        WHERE cmdt_id  = TT1.cmdt_id                                                             
                          AND rdp_code = TT2.rdp_code), 0)                                                AS bks_prce
             , IsNull((SELECT ISNULL(MAX(byng_rate), 0.00)                                                
                         FROM IDCM..TD_CMDT_BYPR                                                          
                        WHERE cmdt_id = TT1.cmdt_id                                                       
                          AND use_ysno = 'Y'                                                              
                          AND CONVERT(FLOAT, byng_rate) <= 100), 0)                                       AS byng_rate
             , IsNull((SELECT max(S3.plor_rlnc_date)
                         FROM IDPL..TM_PLOR_RLNC S3
                        WHERE S3.rdp_code = TT2.rdp_code
                          AND S3.cmdt_id  = TT1.cmdt_id
                          AND S3.plor_rlnc_date >= convert(CHAR(8), dateadd(dd, -30, getdate()), 112)
                          AND S3.plor_rlnc_cdtn_code IN ('006', '007', '008')), '')                       AS plor_rlnc_date
             , IsNull((SELECT max(T1.tkin_date)
                         FROM IDPL..TM_TKIO T1, IDPL..TD_TKIO_DTL T2
                        WHERE T1.tkot_rdp_code = T2.tkot_rdp_code
                          AND T1.tkot_date     = T2.tkot_date
                          AND T1.tkot_wrk_num  = T2.tkot_wrk_num
                          AND T1.box_num       = T2.box_num
                          AND T1.tkot_date    >= convert(CHAR(8), dateadd(dd, -30, getdate()), 112)
                          AND T2.cmdt_id       = TT1.cmdt_id
                          AND T1.tkin_rdp_code = TT2.rdp_code
                          AND T1.dlt_ysno = 'N'
                          AND T2.dlt_ysno = 'N'), '')                                                     AS tkin_date
             , IsNull((SELECT max(T1.rcvd_date)
                         FROM IDPL..TM_RCVD T1, IDPL..TD_RCVD_DTL T2
                        WHERE T1.rcvd_num = T2.rcvd_num
                          AND T1.rdp_code = TT2.rdp_code
                          AND T1.rcvd_date >= convert(CHAR(8), dateadd(dd, -30, getdate()), 112)
                          AND T2.cmdt_id = TT1.cmdt_id
                          AND T1.dlt_ysno = 'N'
                          AND T2.dlt_ysno = 'N'), '')                                                     AS rcvd_date
             , IsNull((SELECT S1.real_invn_qntt - S1.str_pren_invn_qntt
                         FROM IDSS..TM_RDP_INVN S1
                        WHERE S1.rdp_code = TT2.rdp_code
                          AND S1.cmdt_id  = TT1.cmdt_id), 0)                                              AS avlb_qntt
             , IsNull(TT1.cmdt_dvsn_code, '')                                                             AS cmdt_dvsn_code
             , IsNull((SELECT code_wrth_name                                                              
                         FROM IDCM..TC_KFLW_CODE_DTL                                                    
                        WHERE code_id = '1257'                                                          
                          AND code_wrth = TT1.cmdt_cdtn_code), '')                                        AS cmdt_dvsn_name
             , IsNull((SELECT CONVERT(int, sum(S1.sale_qntt))                                                         
                         FROM IDSS..TS_CMDT_SLS_SUMY S1                                                   
                        WHERE S1.sale_date >= convert(CHAR(8), dateadd(dd, -30, getdate()), 112)          
                          AND S1.str_rdp_code = TT2.rdp_code                                              
                          AND S1.cmdt_id = TT1.cmdt_id), 0)                                               AS sale_qntt
             , (CASE WHEN (SELECT count(*)
                             FROM IDCM..TM_APIC_AREA_ECSL_VNDR S1                                                    
                            WHERE S1.rdp_code = TT2.rdp_code                                                         
                              AND S1.cmdt_id  = TT1.cmdt_id
                              AND S1.dlt_ysno = 'N') > 0 THEN 'Y' ELSE 'N' END)                           AS area_vndr_code
             , IsNull((SELECT CONVERT(int, SUM(S1.plor_rlnc_qntt))
                         FROM IDPL..TM_PLOR_RLNC S1
                        WHERE S1.rdp_code       = '{rdpCode}'
                          AND S1.plor_rlnc_date = CONVERT(CHAR(8), GETDATE(), 112)
                          AND S1.cmdt_id        = TT1.cmdt_id
                          AND S1.plor_rlnc_cdtn_code < '009'), 0)                                         AS plor_rlnc_qntt
             , ISNULL((SELECT CONVERT(int, SUM(T2.tkio_qntt))
                         FROM IDPL..TM_TKIO T1, IDPL..TD_TKIO_DTL T2, IDCM..TM_CMDT T3
                        WHERE T1.tkot_rdp_code = T2.tkot_rdp_code
                          AND T1.tkot_date     = T2.tkot_date
                          AND T1.tkot_wrk_num  = T2.tkot_wrk_num
                          AND T1.box_num       = T2.box_num
                          AND T2.cmdt_id       = T3.cmdt_id
                          AND T3.dlt_ysno      = 'N'
                          AND T1.tkot_date    >= convert(CHAR(8), dateadd(dd, -30, getdate()), 112)          
                          AND T1.box_num       = 'ONEDAYBX'
                          AND T1.tkot_rdp_code = TT2.rdp_code
                          AND T1.tkin_rdp_code = '007'
                          AND T3.cmdt_code     = TT1.cmdt_code), 0)                                       AS pickup_qntt                          
             , 'http://image.kyobobook.co.kr/images/book/large/' + substring(IsNull(TT1.cmdt_code, ''), 11, 13) + '/l' + IsNull(TT1.cmdt_code, '') + '.jpg'       AS imgUrl
          FROM IDCM..TM_CMDT TT1, IDCM..TM_RDP TT2
         WHERE 1=1
           AND TT2.rdp_code      = '{rdpCode}'
           AND TT1.cmdt_code     = '{cmdtCode}'
           AND TT2.rdp_dvsn_code = '002'
           AND TT2.dlt_ysno      = 'N'
         ORDER BY 1
            AT ISOLATION 0
    """,
        # Add, 2019.02.15, 심윤보, flasktest01.insert_bksh_bks().cmdt_main_detail_invnqntt, (IDSS)재고수량 조회 : getInventoryQty
        "cmdt_main_detail_invnqntt": """
        SELECT B1.rdp_code                                                                       AS rdp_code
             , B1.cmdt_id                                                                        AS cmdt_id
             , CONVERT(int, coalesce(T1.rcds_invn_qntt, 0))                                      AS rcds_invn_qntt
             , CONVERT(int, coalesce(T1.real_invn_qntt, 0))                                      AS real_invn_qntt
             , CONVERT(int, coalesce(T1.str_pren_invn_qntt, 0))                                  AS str_pren_invn_qntt
             , CONVERT(int, coalesce(T1.real_invn_qntt, 0) - coalesce(T1.str_pren_invn_qntt, 0)) AS avlb_qntt
             , IsNull((SELECT CONVERT(int, coalesce(sum(coalesce(S1.real_invn_qntt,0)),0))
                         FROM IDPL..VW_TM_CNTR_PRST_BKSH_INVN_01 S1
                        WHERE S1.rdp_code = '009'
                          AND S1.cmdt_id = '{cmdtId}'), 0)                                       AS real_invn_qntt_009
             , 0                                                                                 AS rdp_sale_qntt
          FROM (SELECT '{rdpCode}' AS rdp_code 
                     , '{cmdtId}' AS cmdt_id) B1
             , IDSS..TM_RDP_INVN T1
         WHERE B1.rdp_code *= T1.rdp_code
           AND B1.cmdt_id  *= T1.cmdt_id
           AND T1.rdp_code = '{rdpCode}'
           AND T1.cmdt_id  = '{cmdtId}'
           AND T1.dlt_ysno = 'N'
            AT ISOLATION 0
    """,
        # Add, 2019.02.15, 심윤보, flasktest01.insert_bksh_bks().cmdt_main_detail_apicperarea, getApicPerAreaExclusiveSaleVendor : 공통 CommodityEntity CM_Commodity 단품별지역총판매입처
        "cmdt_main_detail_apicperarea": """
        SELECT t2.rdp_code              AS rdp_code
             , t2.cmdt_id               AS cmdt_id
             , t1.vndr_code             AS vndr_code
             , t1.vndr_name             AS vndr_name
          FROM IDCM..TM_VNDR t1,
               IDCM..TM_APIC_AREA_ECSL_VNDR t2
         WHERE t1.vndr_code = t2.vndr_code
           AND t2.rdp_code  = '{rdpCode}'  
           AND t2.cmdt_id   = '{cmdtId}'
           AND t2.dlt_ysno  = 'N'
            AT ISOLATION 0
    """,
        # Add, 2019.02.15, 심윤보, flasktest01.insert_bksh_bks().cmdt_main_detail_bksh_num, SS_BookshelfBooksManagement.findStoreBookshelfBooksInfm 점포별서가도서:상품안내Main
        "cmdt_main_detail_bksh_num": """
        /* SS_BookshelfBooksManagement.findStoreBookshelfBooksInfm 점포별서가도서:상품안내Main    */
        SELECT t2.str_rdp_code                                        AS rdp_code
             , t2.bksh_num                                            AS bksh_num
             , IsNull(t1.bksh_name, '')                               AS bksh_name
             , t2.bksh_prgp_num                                       AS bksh_prgp_num
             , IsNull(t1.prgp_name, '')                               AS prgp_name
             , t1.prgp_num||':'||t1.prgp_name                         AS bkshPrgpCntt
             , t2.cmdt_id                                             AS cmdt_id
             , ISNULL(t1.bksh_shp_dvsn_code, '')                      AS bksh_shp_dvsn_code
             , IsNull((SELECT code_wrth_name                                                              
                         FROM IDCM..TC_KFLW_CODE_DTL                                                    
                        WHERE code_id = '1047'                                                          
                          AND code_wrth = t1.bksh_shp_dvsn_code), '') AS bksh_shp_dvsn_name
             , ISNULL(t1.gvrm_infm_code, '')                          AS gvrm_infm_code
             , IsNull((SELECT code_wrth_name                                                              
                         FROM IDCM..TC_KFLW_CODE_DTL                                                    
                        WHERE code_id = '1219'                                                          
                          AND code_wrth = t1.bksh_shp_dvsn_code), '') AS gvrm_infm_name
          FROM IDSS..TM_STR_BKSH t1,
               IDSS..TM_STR_BKSH_BKS t2
         WHERE 1=1
           AND t1.rdp_code     =* t2.str_rdp_code
           AND t1.bksh_num     =* t2.bksh_num
           AND t1.prgp_num     =* t2.bksh_prgp_num
           AND t2.str_rdp_code = '{rdpCode}'
           AND t2.cmdt_id      = '{cmdtId}'
         ORDER BY t1.bksh_num, t1.prgp_num
            AT ISOLATION 0
    """,

    # Add, 2019.02.15, 심윤보, flasktest01.insert_bksh_bks().cmdt_main_detail_bksh_num, SS_BookshelfBooksManagement.findStoreBookshelfBooksInfm 점포별서가도서:상품안내Main
    "cmdt_main_detail_pren": """
    /* SS_BookshelfBooksManagement.cmdt_main_detail_pren 점포별서가도서:상품안내Main    */
    SELECT IsNull(T1.pren_rcpn_num, '')                                    AS pren_rcpn_num
         , IsNull(T1.pren_sale_rcpn_date, '')                              AS pren_sale_rcpn_date  
         , IsNull(T1.aord_name, '')                                        AS aord_name
         , IsNull(T1.pren_pros_cdtn_code, '')                              AS pren_pros_cdtn_code
         , IsNull((SELECT code_wrth_name                                                              
                     FROM IDCM..TC_KFLW_CODE_DTL                                                    
                    WHERE code_id = '1151'                                                          
                      AND code_wrth = IsNull(T1.pren_pros_cdtn_code, '')), '코드없음') AS pren_pros_cdtn_name
         , SUM(COUNT(*))                                                   AS cnt
      FROM IDSS..TM_PREN_SALE_BSC T1, IDSS..TD_PREN_SALE_DTL T2
     WHERE T1.pren_rcpn_num = T2.pren_rcpn_num
       AND T1.pren_sale_rcpn_date >= CONVERT(CHAR(8), DATEADD(dd, -180, GETDATE()), 112)
       AND T1.str_rdp_code         = '{rdpCode}'
       AND T2.cmdt_id              = '{cmdtId}'
       AND T1.pren_pros_cdtn_code NOT IN ('004', '005')
     GROUP BY IsNull(T1.pren_rcpn_num, '')
            , IsNull(T1.pren_sale_rcpn_date, '')
            , IsNull(T1.aord_name, '')
            , IsNull(T1.pren_pros_cdtn_code, '')
        AT ISOLATION 0
    """,

    "getScanList": """
        SELECT COUNT(*) AS cnt
          FROM IDSS..TM_CMDT_SCAN_LIST T1
         WHERE T1.rgst_date = '{rgstDate}'
           AND T1.user_id  = '{crtrId}'
           AND T1.scan_wrk_name = '{scanWrkName}'
           AND T1.cmdt_code = '{cmdtCode}'
           AND T1.dlt_ysno = 'N'
            AT ISOLATION 0
    """,

    "updateScanList": """
        DECLARE @arngSqnc NUMERIC(5, 0)
    
        SELECT @arngSqnc = isnull(max(T1.arng_sqnc), 0) + 1
          FROM IDSS..TM_CMDT_SCAN_LIST T1
         WHERE T1.rgst_date = '{rgstDate}'
           AND T1.user_id  = '{crtrId}'
           AND T1.scan_wrk_name = '{scanWrkName}'
           AND T1.rdp_code = '{rdpCode}'
           AND T1.dlt_ysno = 'N'
            AT ISOLATION 0
    
        UPDATE IDSS..TM_CMDT_SCAN_LIST
           SET scan_qntt  = scan_qntt + {scanQntt}
             , arng_sqnc = @arngSqnc
             , amnr_id   = 'P{crtrId}'
             , amnd_dttm = GETDATE()
         WHERE rgst_date = '{rgstDate}'
           AND user_id  = '{crtrId}'
           AND scan_wrk_name = '{scanWrkName}'
           AND rdp_code = '{rdpCode}'
           AND cmdt_id  = '{cmdtId}'
           
        SELECT IsNull(T1.scan_qntt, 0) AS scanQntt
             , IsNull(T1.arng_sqnc, 0) AS arngSqnc
             , IsNull(T1.cmdt_code, '') AS cmdtCode
          FROM IDSS..TM_CMDT_SCAN_LIST T1
         WHERE T1.rgst_date = '{rgstDate}'
           AND T1.user_id  = '{crtrId}'
           AND T1.scan_wrk_name = '{scanWrkName}'
           AND T1.rdp_code = '{rdpCode}'
           AND T1.cmdt_id  = '{cmdtId}'
            AT ISOLATION 0
    """,

    "insertScanList": """
        DECLARE @arngSqnc NUMERIC(5, 0)
    
        SELECT @arngSqnc = isnull(max(T1.arng_sqnc), 0) + 1
          FROM IDSS..TM_CMDT_SCAN_LIST T1
         WHERE T1.rgst_date = '{rgstDate}'
           AND T1.user_id  = '{crtrId}'
           AND T1.scan_wrk_name = '{scanWrkName}'
           AND T1.rdp_code = '{rdpCode}'
           AND T1.dlt_ysno = 'N'
            AT ISOLATION 0
    
        INSERT INTO IDSS..TM_CMDT_SCAN_LIST (rgst_date, user_id, scan_wrk_name, rdp_code, cmdt_id, cmdt_code, bksh_num, bksh_prgp_num, scan_qntt, arng_sqnc, crtr_id, cret_dttm, amnr_id, amnd_dttm, dlt_ysno)
             VALUES ('{rgstDate}', '{crtrId}', '{scanWrkName}', '{rdpCode}', '{cmdtId}', '{cmdtCode}', '{bkshNum}', '{prgpNum}', {scanQntt}, @arngSqnc, 'P{crtrId}', GETDATE(), 'P{crtrId}', GETDATE(), 'N')
             
        SELECT IsNull(T1.scan_qntt, 0) AS scanQntt
             , IsNull(T1.arng_sqnc, 0) AS arngSqnc
             , IsNull(T1.cmdt_code, '') AS cmdtCode
          FROM IDSS..TM_CMDT_SCAN_LIST T1
         WHERE T1.rgst_date = '{rgstDate}'
           AND T1.user_id  = '{crtrId}'
           AND T1.scan_wrk_name = '{scanWrkName}'
           AND T1.rdp_code = '{rdpCode}'
           AND T1.cmdt_id  = '{cmdtId}'
           AND T1.dlt_ysno = 'N'
            AT ISOLATION 0
    """,

    "findDuplicationTakeOut": """
        SELECT t1.tkot_rdp_code           AS tkotRdpCode
             , (SELECT S1.rdp_name 
                  FROM IDCM..TM_RDP S1 
                 WHERE S1.rdp_code = t1.tkot_rdp_code) AS tkotRdpName
             , t1.tkot_date               AS tkotDate
             , t1.tkot_wrk_num            AS tkotWrkNum
             , t1.box_num                 AS boxNum
             , t1.tkin_rdp_code           AS tkinRdpCode
             , (SELECT S1.rdp_name 
                  FROM IDCM..TM_RDP S1 
                 WHERE S1.rdp_code = t1.tkin_rdp_code) AS tkinRdpName
             , count(distinct t2.cmdt_id) AS qty
             , sum(t2.tkio_qntt)          AS ttalQty
             , t1.tkio_dvsn_code          AS tkioDvsnCode
          FROM IDPL..TM_TKIO t1
             , IDPL..TD_TKIO_DTL t2
         WHERE 1=1
           AND t1.tkot_rdp_code = t2.tkot_rdp_code
           AND t1.tkot_date     = t2.tkot_date
           AND t1.tkot_wrk_num  = t2.tkot_wrk_num
           AND t1.box_num       = t2.box_num
           AND t1.box_num       = '{boxNum}'
           --AND t1.tkin_rdp_code = '{tkinRdpCode}'
           AND t1.tkio_dvsn_code NOT IN ('004', '005', '006', '007', '009')
           AND (t1.tkin_date = '' OR t1.tkin_date IS NULL)
           AND t1.dlt_ysno = 'N'
           AND t2.dlt_ysno = 'N'
         GROUP BY t1.tkot_rdp_code , t1.tkot_date , t1.tkot_wrk_num
             , t1.box_num , t1.tkin_rdp_code , t1.tkio_dvsn_code
            AT ISOLATION 0
    """,

    "findDuplicationTakeOutDetail": """
        SELECT t1.tkot_rdp_code           AS tkotRdpCode
             , (SELECT S1.rdp_name 
                  FROM IDCM..TM_RDP S1 
                 WHERE S1.rdp_code = t1.tkot_rdp_code) AS tkotRdpName
             , t1.tkot_date               AS tkotDate
             , t1.tkot_wrk_num            AS tkotWrkNum
             , t1.box_num                 AS boxNum
             , t1.tkin_rdp_code           AS tkinRdpCode
             , (SELECT S1.rdp_name 
                  FROM IDCM..TM_RDP S1 
                 WHERE S1.rdp_code = t1.tkin_rdp_code) AS tkinRdpName
             , count(distinct t2.cmdt_id) AS qty
             , sum(t2.tkio_qntt)          AS ttalQty
             , t1.tkio_dvsn_code          AS tkioDvsnCode
          FROM IDPL..TM_TKIO t1
             , IDPL..TD_TKIO_DTL t2
         WHERE 1=1
           AND t1.tkot_rdp_code = t2.tkot_rdp_code
           AND t1.tkot_date     = t2.tkot_date
           AND t1.tkot_wrk_num  = t2.tkot_wrk_num
           AND t1.box_num       = t2.box_num
           AND t1.tkot_rdp_code = '{tkotRdpCode}'
           and t1.tkot_date     = '{tkotDate}'
           and t1.tkot_wrk_num  = '{tkotWrkNum}'
           AND t1.box_num       = '{boxNum}'
           AND t1.tkin_rdp_code = '{tkinRdpCode}'
           AND t1.tkio_dvsn_code NOT IN ('004', '005', '006', '007', '009')
           AND (t1.tkin_date = '' OR t1.tkin_date IS NULL)
           AND t1.dlt_ysno = 'N'
           AND t2.dlt_ysno = 'N'
         GROUP BY t1.tkot_rdp_code , t1.tkot_date , t1.tkot_wrk_num
             , t1.box_num , t1.tkin_rdp_code , t1.tkio_dvsn_code
            AT ISOLATION 0
    """,

    "updateTkinMaster": """
        UPDATE IDPL..TM_TKIO
           SET tkin_date               = convert(char(8),getdate(),112)
             , tkin_wrk_num            = '00002'
             , tkin_ip_adrs            = '10.101.10.55'
             , dta_mdfc_dvsn_code      = 'U'
             , snms_ysno               = 'N'
             , tkin_cnfr_pers_id       = '12570'
             , tkin_cnfr_dttm          = '20210311132915'
             , amnr_id                 = '12570'
             , amnd_dttm               = GETDATE()
         WHERE 1=1
           AND tkot_rdp_code  = '{tkotRdpCode}'
           AND tkot_date      = '{tkotDate}'
           AND tkot_wrk_num   = '{tkotWrkNum}'
           AND box_num        = '{boxNum}'
           
        SELECT COUNT(*) AS cnt
          FROM IDPL..TM_TKIO T1
         WHERE T1.tkot_rdp_code  = '{tkotRdpCode}'
           AND T1.tkot_date      = '{tkotDate}'
           AND T1.tkot_wrk_num   = '{tkotWrkNum}'
           AND T1.box_num        = '{boxNum}'
           AND T1.tkin_date      = '{tkinDate}'
           AND T1.tkin_wrk_num   = '{tkinWrkNum}'
            AT ISOLATION 0
    """,

    "getTkinDetailList": """
        SELECT t1.box_num             AS boxNum                  
             , t1.tkot_rdp_code       AS tkotRdpCode                        
             , t1.tkin_rdp_code       AS tkinRdpCode                        
             , t1.tkot_date           AS tkotDate                    
             , t1.tkot_wrk_num        AS tkotWrkNum                       
             , t1.tkio_dvsn_code      AS tkioDvsnCode                         
             , t2.cmdt_id             AS cmdtId                  
             , t3.cmdt_code           AS cmdtCode                    
             , t3.cmdt_name           AS cmdtName                    
             , t3.jo_code             AS joCode                  
             , t2.cmdt_prce           AS cmdtPrce                    
             , t2.tkio_qntt           AS tkioQntt                    
             , t1.brmv_phds_cnfr_date AS brmvPhdsCnfrDate                              
             , t3.vat_exsn_ysno       AS vatExsnYsno                        
             , t3.rlse_date           AS rlseDate                    
             , t2.byng_srmb           AS byngSrmb                    
             , t1.tkin_date           AS tkinDate                    
          FROM IDPL..TM_TKIO t1
             , IDPL..TD_TKIO_DTL t2
             , IDCM..TM_CMDT t3
         WHERE 1=1
           AND t1.tkot_rdp_code = t2.tkot_rdp_code
           AND t1.tkot_date     = t2.tkot_date
           AND t1.tkot_wrk_num  = t2.tkot_wrk_num
           AND t1.box_num       = t2.box_num
           AND t2.cmdt_id       = t3.cmdt_id
           AND t1.box_num       = '{boxNum}'
           AND t1.tkot_rdp_code = '{tkotRdpCode}'
           AND t1.tkot_date     = '{tkotDate}'
           AND t1.tkot_wrk_num  = '{tkotWrkNum}'
           AND t1.dlt_ysno      = 'N'
           AND t2.dlt_ysno      = 'N'
            AT ISOLATION 0
    """,

    "getRdpInvnCnt": """
    
    """,

    "modifyTakeInRegistration": """
        UPDATE IDPL..TM_TKIO
           SET tkin_date               = convert(char(8),getdate(),112)
             , tkin_wrk_num            = '{tkinWrkNum}'
             , tkin_ip_adrs            = '{tkinIpAdrs}'
             , dta_mdfc_dvsn_code      = 'U'
             , snms_ysno               = 'N'
             , tkin_cnfr_pers_id       = '{crtrId}'
             , tkin_cnfr_dttm          = '{tkinCnfrDttm}'
             , amnr_id                 = '{crtrId}'
             , amnd_dttm               = GETDATE()
         WHERE 1=1
           AND tkot_rdp_code  = '{tkotRdpCode}'
           AND tkot_date      = '{tkotDate}'
           AND tkot_wrk_num   = '{tkotWrkNum}'
           AND box_num        = '{boxNum}'
        
        SELECT count(*) AS cnt
          FROM IDPL..TM_TKIO T1
         WHERE 1=1
           AND tkot_rdp_code  = '{tkotRdpCode}'
           AND tkot_date      = '{tkotDate}'
           AND tkot_wrk_num   = '{tkotWrkNum}'
           AND box_num        = '{boxNum}'
           AND tkin_wrk_num   = '{tkinWrkNum}'
            AT ISOLATION 0
    """,

    "findTakeInRegistrationDetl": """
        SELECT t1.box_num             AS boxNum          
             , t1.tkot_rdp_code       AS tkotRdpCode     
             , t1.tkin_rdp_code       AS tkinRdpCode     
             , t1.tkot_date           AS tkotDate        
             , t1.tkot_wrk_num        AS tkotWrkNum      
             , t1.tkio_dvsn_code      AS tkioDvsnCode    
             , t2.cmdt_id             AS cmdtId          
             , t3.cmdt_code           AS cmdtCode        
             , t3.cmdt_name           AS cmdtName        
             , t3.jo_code             AS joCode          
             , t2.cmdt_prce           AS cmdtPrce
             , t2.cmdt_prce           AS oriCmdtPrce        
             , t2.tkio_qntt           AS tkioQntt        
             , t1.brmv_phds_cnfr_date AS brmvPhdsCnfrDate
             , t3.vat_exsn_ysno       AS vatExsnYsno     
             , t3.rlse_date           AS rlseDate        
             , t2.byng_srmb           AS byngSrmb        
             , t1.tkin_date           AS tkinDate
             , IsNull((SELECT wncr_prce 
                         FROM IDCM..TD_CMDT_PRCE 
                        WHERE cmdt_id      = '5800004678770' 
                          AND apl_end_dttm = '99991231235959'), 0) AS wncrPrce        
          FROM IDPL..TM_TKIO t1
             , IDPL..TD_TKIO_DTL t2
             , IDCM..TM_CMDT t3
         WHERE 1=1
           AND t1.tkot_rdp_code = t2.tkot_rdp_code
           AND t1.tkot_date     = t2.tkot_date
           AND t1.tkot_wrk_num  = t2.tkot_wrk_num
           AND t1.box_num       = t2.box_num
           AND t2.cmdt_id       = t3.cmdt_id
           AND t1.box_num       = '{boxNum}'
           AND t1.tkot_rdp_code = '{tkotRdpCode}'
           AND t1.tkot_date     = '{tkotDate}'
           AND t1.tkot_wrk_num  = '{tkotWrkNum}'
           AND t1.dlt_ysno      = 'N'
           AND t2.dlt_ysno      = 'N'
            AT ISOLATION 0
    """,

    # Add, 2021.03.12, 심윤보, 반입예약 재고 생성
    "registerTakeInPrenInvn": """
        /* Add, 2021.03.12, 심윤보, 반입예약 재고 생성[registerTakeInPrenInvn] */
        exec IDSS..spe_TM_STR_PREN_INVN '{tkotRdpCode}', '{tkotDate}', '{tkotWrkNum}', '{boxNum}', 'P{crtrId}'
        
        SELECT 0 AS retCode
    """,

    "findCommodityBuyingPriceForPos": """
        SELECT TOP 1 cmdt_id        AS cmdtId      
                   , byng_srmb      AS byngSrmb    
                   , byng_rate      AS byngRate    
                   , apl_wncr_cost  AS aplWncrCost 
                   , wncr_prce      AS wncrPrce    
                   , last_trnc_ysno AS lastTrncYsno
                   , use_ysno       AS useYsno     
          FROM IDCM..TD_CMDT_BYPR
         WHERE cmdt_id        = '{cmdtId}'
           AND whrt_dvsn_code = '002'
           AND last_trnc_ysno = 'Y'
         ORDER BY byng_srmb DESC
            AT ISOLATION 0
    """,

    "findHubRdpCode": """
        SELECT isNull(rtrim(T1.hub_rdp_code), '') AS hubRdpCode
          FROM IDSS..TM_HUBNSPOKE T1
         WHERE T1.spoke_rdp_code = '{tkinRdpCode}'
            AT ISOLATION 0
    """,

    "findHubMstgoodsInfo": """
        SELECT IsNull(T1.COST, 0)       AS COST
             , IsNull(T1.UNITPRICE, 0)  AS UNITPRICE
             , IsNull(T1.MAXDCRATE1, 0) AS MAXDCRATE1
             , IsNull(T1.MAXDCRATE2, 0) AS MAXDCRATE2
             , IsNull(T1.ETCINFO1, '')  AS ETCINFO1
          FROM IDPS..MSTGOODS T1
         WHERE T1.STORECD  = '{rdpCode}'
           AND T1.PLUBARCD = (SELECT IsNull(S1.cmdt_code, '')
                                FROM IDCM..TM_CMDT S1
                               WHERE S1.cmdt_id = '{cmdtId}')
            AT ISOLATION 0
    """,

    "getMstGoods": """
        SELECT STORECD      AS STORECD      
             , PLUBARCD     AS PLUBARCD     
             , NAME         AS NAME         
             , SYSTYPE      AS SYSTYPE      
             , DTLGB        AS DTLGB        
             , COST         AS COST         
             , UNITPRICE    AS UNITPRICE    
             , MAXDCRATE1   AS MAXDCRATE1   
             , MAXDCRATE2   AS MAXDCRATE2   
             , TAXTYPE      AS TAXTYPE      
             , DCYN         AS DCYN         
             , DCRATE       AS DCRATE       
             , NONMEMDC     AS NONMEMDC     
             , ETCINFO1     AS ETCINFO1     
             , ETCINFO2     AS ETCINFO2     
             , CMDTDVSNCODE AS CMDTDVSNCODE 
             , JOCD         AS JOCD         
             , FERCD        AS FERCD        
             , KDC1CD       AS KDC1CD       
             , ACCRATE      AS ACCRATE      
             , DCNO         AS DCNO         
             , PUBYMD       AS PUBYMD       
             , SALEYN       AS SALEYN       
             , SAVEYN       AS SAVEYN       
             , DELYN        AS DELYN        
             , REGDATE      AS REGDATE      
             , REGEMP       AS REGEMP       
             , UPTDATE      AS UPTDATE      
             , UPTEMP       AS UPTEMP       
          FROM IDPS..MSTGOODS
         WHERE 1=1
           AND STORECD  = '{tkinRdpCode}'
           AND PLUBARCD = '{cmdtCode}'
            AT ISOLATION 0
    """,

    "modifyMstGoodsPrice": """
        UPDATE IDPS..MSTGOODS
           SET COST         = {COST}
             , UNITPRICE    = {UNITPRICE}
             , MAXDCRATE1   = {MAXDCRATE1}
             , MAXDCRATE2   = {MAXDCRATE2}
             , ETCINFO1     = '{ETCINFO1}'
             , UPTDATE      = GETDATE()
             , UPTEMP       = 'P{crtrId}'
         WHERE 1=1
           AND STORECD  = '{STORECD}'
           AND PLUBARCD = '{PLUBARCD}'
        
        SELECT count(*) AS cnt
          FROM IDPS..MSTGOODS T1
         WHERE STORECD  = '{STORECD}'
           AND PLUBARCD = '{PLUBARCD}'
           AND UNITPRICE = {UNITPRICE}
            AT ISOLATION 0
    """,

    "getBookPriceFromBookstoreBasicPrice": """
        SELECT rdp_code  AS rdpCode 
             , cmdt_id   AS cmdtId  
             , bks_prce  AS bksPrce 
             , byng_srmb AS byngSrmb
          FROM IDCM..TD_BKS_BSC_PRCE
         WHERE rdp_code = '{tkinRdpCode}'
           AND cmdt_id  = '{cmdtId}'
           AND dlt_ysno = 'N'
            AT ISOLATION 0
    """,

    "registerCommodityBasicPrice": """
        DECLARE @byngSrmb NUMERIC(5, 0)
        
        SELECT @byngSrmb = IsNull(max(byng_srmb), 0)
          FROM IDCM..TD_CMDT_BYPR
         WHERE 1=1
           AND cmdt_id        = '{cmdtId}'
           AND wncr_prce      = convert(numeric, {cmdtPrce})
           AND whrt_dvsn_code = '002'
           AND dlt_ysno       = 'N'
            AT ISOLATION 0

        INSERT INTO IDCM..TD_BKS_BSC_PRCE (rdp_code, cmdt_id, bks_prce, byng_srmb, crtr_id, cret_dttm, amnr_id, amnd_dttm, dlt_ysno)
        VALUES ('{tkinRdpCode}', '{cmdtId}', {cmdtPrce}, @byngSrmb, 'P{crtrId}', GETDATE(), 'P{crtrId}', GETDATE(), 'N')
        
        SELECT count(*) AS cnt
          FROM IDCM..TD_BKS_BSC_PRCE T1
         WHERE T1.rdp_code  = '{tkinRdpCode}'
           AND T1.cmdt_id   = '{cmdtId}'
           AND T1.bks_prce  = {cmdtPrce}
           AND T1.byng_srmb = @byngSrmb
            AT ISOLATION 0
    """,

    "modifyCommodityBasicPrice": """
        DECLARE @byngSrmb NUMERIC(5, 0)
        
        SELECT @byngSrmb = IsNull(max(byng_srmb), 0)
          FROM IDCM..TD_CMDT_BYPR
         WHERE 1=1
           AND cmdt_id        = '{cmdtId}'
           AND wncr_prce      = convert(numeric, {cmdtPrce})
           AND whrt_dvsn_code = '002'
           AND dlt_ysno       = 'N'
            AT ISOLATION 0
    
        UPDATE IDCM..TD_BKS_BSC_PRCE
           SET bks_prce  = {cmdtPrce}
             , byng_srmb = @byngSrmb
             , amnr_id   = 'P{crtrId}'
             , amnd_dttm = GETDATE()
         WHERE rdp_code  = '{tkinRdpCode}'
           AND cmdt_id   = '{cmdtId}'
           
        SELECT count(*) AS cnt
          FROM IDCM..TD_BKS_BSC_PRCE
         WHERE rdp_code  = '{tkinRdpCode}'
           AND cmdt_id   = '{cmdtId}'
           AND byng_srmb = @byngSrmb
           AND bks_prce  = {cmdtPrce}
            AT ISOLATION 0
    """,

    "registerEaiParams": """
        exec {procName} '{tableName}','{cudGubun}','{cmdtClstCode}','{rdpCode}','{cmdtId}', NULL, NULL, NULL
        
        SELECT 0 AS retCode
    """,

    "getUserInfo": """
        SELECT IsNull(T1.user_id            , '')  AS userId
             , IsNull(T1.sert_num           , '')  AS sertNum
             , IsNull(T1.user_name          , '')  AS userName
             , IsNull(T1.cg_inhb_rgst_num   , '')  AS cgInhbRgstNum
             , IsNull(T1.tnof_cdtn_code     , '')  AS tnofCdtnCode
             , IsNull(T1.dprt_code          , '')  AS dprtCode
             , IsNull(T1.pstn_code          , '')  AS pstnCode
             , IsNull(T1.jbcl_code          , '')  AS jbclCode
             , IsNull(T1.rprs_pstn_code     , '')  AS rprsPstnCode
             , IsNull(T1.arbt_ysno          , '')  AS arbtYsno
             , IsNull(T1.spr_mgr_ysno       , '')  AS sprMgrYsno
             , IsNull(T1.tlnm               , '')  AS tlnm
             , IsNull(T1.prtb_tlnm          , '')  AS prtbTlnm
             , IsNull(T1.eml_adrs           , '')  AS emlAdrs
             , IsNull(T1.jo_code            , '')  AS joCode
             , IsNull(T1.rdp_code           , '')  AS rdpCode
             , IsNull(T2.rdp_name           , '')  AS rdpName
             , IsNull(T1.user_rgst_patr_code, '')  AS userRgstPatrCode
             , T1.crtr_id              AS crtrId
             , T1.cret_dttm            AS cretDttm
             , T1.amnr_id              AS amnrId
             , T1.amnd_dttm            AS amndDttm
             , T1.dlt_ysno             AS dltYsno
          FROM IDCM..TM_USER T1, IDCM..TM_RDP T2
         WHERE T1.rdp_code = T2.rdp_code
           AND T1.user_id = '{userId}'
            AT ISOLATION 0
    """,

    "getCmdtInfo": """
        SELECT IsNull(T1.cmdt_id                , '') AS cmdtId              
             , IsNull(T1.cmdt_name              , '') AS cmdtName            
             , IsNull(T1.sbtt_name1             , '') AS sbttName1           
             , IsNull(T1.sbtt_name2             , '') AS sbttName2           
             , IsNull(T1.cndn_cmdt_name         , '') AS cndnCmdtName        
             , IsNull(T1.cmdt_foln_name         , '') AS cmdtFolnName        
             , IsNull(T1.cmdt_hngl_mark_name    , '') AS cmdtHnglMarkName    
             , IsNull(T1.cmdt_smlr_name         , '') AS cmdtSmlrName        
             , IsNull(T1.cmdt_clst_code         , '') AS cmdtClstCode        
             , IsNull(T1.hgrn_cmdt_id           , '') AS hgrnCmdtId          
             , IsNull(T1.cmdt_code              , '') AS cmdtCode            
             , IsNull(T1.trnf_cmdt_code         , '') AS trnfCmdtCode        
             , IsNull(T1.cmdt_code_cdtn_code    , '') AS cmdtCodeCdtnCode    
             , IsNull(T1.pbcm_code              , '') AS pbcmCode            
             , IDSS.dbo.uf_removelf(IsNull((SELECT pbcm_name                                              
                                              FROM IDCM..TM_PBCM                                          
                                             WHERE pbcm_code = IsNull(T1.pbcm_code, '')), '')) AS pbcmName
             , IDSS.dbo.uf_removelf(IsNull((SELECT autr_name1                                             
                                              FROM IDCM..TD_CMDT_AUTR                                     
                                             WHERE cmdt_id = IsNull(T1.cmdt_id, '')), ''))     AS autrName
             , IsNull(T1.vndr_code              , '') AS vndrCode            
             , IsNull(T1.dscn_ysno              , '') AS dscnYsno            
             , IsNull(T1.vat_exsn_ysno          , '') AS vatExsnYsno         
             , IsNull(T1.rlse_date              , '') AS rlseDate            
             , IsNull(T1.sale_lmtt_age          , 0 ) AS saleLmttAge         
             , IsNull(T1.cmdt_set_qntt          , 0 ) AS cmdtSetQntt         
             , IsNull(T1.lngg_code              , '') AS lnggCode            
             , IsNull(T1.cmdt_cdtn_code         , '') AS cmdtCdtnCode        
             , IsNull(T1.cmdt_kind_code         , '') AS cmdtKindCode        
             , IsNull(T1.dstb_cdtn_code         , '') AS dstbCdtnCode        
             , IsNull(T1.sale_psbl_ysno         , '') AS salePsblYsno        
             , IsNull(T1.cmdt_patr_code         , '') AS cmdtPatrCode        
             , IsNull(T1.cver_pbcm_name         , '') AS cverPbcmName        
             , IsNull(T1.cver_img_exsn_ysno     , '') AS cverImgExsnYsno     
             , IsNull(T1.cver_lrim_exsn_ysno    , '') AS cverLrimExsnYsno    
             , IsNull(T1.cmdt_dvsn_code         , '') AS cmdtDvsnCode        
             , IsNull(T1.jo_code                , '') AS joCode              
             , IsNull(T1.crtr_id                , '') AS crtrId              
             , IsNull(T1.cret_dttm              , '') AS cretDttm            
             , IsNull(T1.amnr_id                , '') AS amnrId              
             , IsNull(T1.amnd_dttm              , '') AS amndDttm            
             , IsNull(T1.dlt_ysno               , '') AS dltYsno             
             , IsNull(T1.cmdt_prvl_view_ysno    , '') AS cmdtPrvlViewYsno    
             , IsNull(T1.fbp_trgt_ysno          , '') AS fbpTrgtYsno         
             , IsNull(T1.cmdt_cdtn_mdfc_rsn_code, '') AS cmdtCdtnMdfcRsnCode 
             , IsNull(T1.incm_ddct_trgt_ysno    , '') AS incmDdctTrgtYsno    
             , IsNull(rtrim((SELECT min(S1.vndr_code)
                         FROM IDCM..TM_APIC_AREA_ECSL_VNDR S1
                        WHERE S1.rdp_code = '{rdpCode}'
                          AND S1.cmdt_id  = T1.cmdt_id
                          AND S1.dlt_ysno = 'N')), '') AS minApicAreaVndrCode
             , IsNull(rtrim((SELECT max(S1.vndr_code)
                         FROM IDCM..TM_APIC_AREA_ECSL_VNDR S1
                        WHERE S1.rdp_code = '{rdpCode}'
                          AND S1.cmdt_id  = T1.cmdt_id
                          AND S1.dlt_ysno = 'N')), '') AS maxApicAreaVndrCode
             , IsNull((SELECT count(S2.cmdt_id)
                         FROM IDCM..TD_SMST_UNVR_RE S2
                        WHERE S2.smst_yr   = SUBSTRING(CONVERT(CHAR(8), GETDATE(), 112), 1, 4)
                          AND S2.smst_code = (CASE WHEN CONVERT(INT, SUBSTRING(CONVERT(CHAR(8), GETDATE(), 112), 5, 2)) <= 6 THEN '1' ELSE '2' END)
                          AND S2.rdp_code  = '{rdpCode}'
                          AND S2.cmdt_id   = T1.cmdt_id
                          AND S2.use_ysno  = 'Y'), 0) AS smstUnvrCnt
             , IsNull((SELECT count(*)
                         FROM IDCM..TM_APIC_AREA_ECSL_VNDR S1
                        WHERE S1.rdp_code = (SELECT SS1.hub_rdp_code
                                               FROM IDSS..TM_HUBNSPOKE SS1
                                              WHERE SS1.spoke_rdp_code = '{rdpCode}'
                                                AND SS1.dlt_ysno       = 'N')
                          AND S1.cmdt_id  = T1.cmdt_id
                          AND S1.dlt_ysno = 'N'), 0) AS hubApicAreaVndrYsno
             , IsNull((SELECT COUNT(*)
                         FROM IDPL..TM_CHT_STUP  t1
                        INNER JOIN IDCM..TM_RDP t2 ON SUBSTRING(t1.drcs_num,3,3) = t2.rdp_code
                          AND t2.dlt_ysno       =    'N'
                        WHERE t1.rdp_code       =    '009'
                          AND t1.drcs_num       LIKE '88%'
                          AND t1.drcs_dvsn_code =    '077'
                          AND t2.rdp_code       =    '{rdpCode}'), 0)  AS plorYsno028
             , IsNull((SELECT COUNT(*)
                         FROM IDPL..TM_CHT_STUP  t1
                        INNER JOIN IDCM..TM_RDP t2 ON SUBSTRING(t1.drcs_num,3,3) = t2.rdp_code
                          AND t2.dlt_ysno       =    'N'
                        WHERE t1.rdp_code       =    '009'
                          AND t1.drcs_num       LIKE '89%'
                          AND t1.drcs_dvsn_code =    '077'
                          AND t2.rdp_code       =    '{rdpCode}'), 0)  AS plorYsno029
             , 'http://image.kyobobook.co.kr/images/book/large/' + substring(IsNull(T1.cmdt_code, ''), 11, 13) + '/l' + IsNull(T1.cmdt_code, '') + '.jpg'       as imgUrl
          FROM IDCM..TM_CMDT T1
         WHERE T1.cmdt_code like '{cmdtCode}'
           AND T1.dlt_ysno = 'N'
            AT ISOLATION 0
    """,

    "getRdpInfoAll": """
        SELECT IsNull(rdp_code         , '') AS rdpCode       
             , IsNull(rdp_name         , '') AS rdpName       
             , IsNull(rdp_adrs         , '') AS rdpAdrs       
             , IsNull(rdp_tlnm         , '') AS rdpTlnm       
             , IsNull(rdp_fax_num      , '') AS rdpFaxNum     
             , IsNull(rdp_dvsn_code    , '') AS rdpDvsnCode   
             , IsNull(by_warh_rdp_code , '') AS byWarhRdpCode 
             , IsNull(phds_wrk_grp_code, '') AS phdsWrkGrpCode
             , IsNull(frmr_rdp_code    , '') AS frmrRdpCode   
             , IsNull(stdvr_ysno       , '') AS stdvrYsno     
             , IsNull(plor_grp_code    , '') AS plorGrpCode   
             , IsNull(str_ysno         , '') AS strYsno       
             , IsNull(dprt_code        , '') AS dprtCode      
             , IsNull(crtr_id          , '') AS crtrId        
             , IsNull(cret_dttm        , '') AS cretDttm      
             , IsNull(amnr_id          , '') AS amnrId        
             , IsNull(amnd_dttm        , '') AS amndDttm      
             , IsNull(dlt_ysno         , '') AS dltYsno       
             , IsNull(bstm_dvsn_code   , '') AS bstmDvsnCode
          FROM IDCM..TM_RDP T1
         WHERE T1.rdp_dvsn_code = '002'
           AND T1.dlt_ysno      = 'N'
         ORDER BY 1 
            AT ISOLATION 0
    """,

    "getRdpInfo": """
        SELECT IsNull(rdp_code         , '') AS rdpCode       
             , IsNull(rdp_name         , '') AS rdpName       
             , IsNull(rdp_adrs         , '') AS rdpAdrs       
             , IsNull(rdp_tlnm         , '') AS rdpTlnm       
             , IsNull(rdp_fax_num      , '') AS rdpFaxNum     
             , IsNull(rdp_dvsn_code    , '') AS rdpDvsnCode   
             , IsNull(by_warh_rdp_code , '') AS byWarhRdpCode 
             , IsNull(phds_wrk_grp_code, '') AS phdsWrkGrpCode
             , IsNull(frmr_rdp_code    , '') AS frmrRdpCode   
             , IsNull(stdvr_ysno       , '') AS stdvrYsno     
             , IsNull(plor_grp_code    , '') AS plorGrpCode   
             , IsNull(str_ysno         , '') AS strYsno       
             , IsNull(dprt_code        , '') AS dprtCode      
             , IsNull(crtr_id          , '') AS crtrId        
             , IsNull(cret_dttm        , '') AS cretDttm      
             , IsNull(amnr_id          , '') AS amnrId        
             , IsNull(amnd_dttm        , '') AS amndDttm      
             , IsNull(dlt_ysno         , '') AS dltYsno       
             , IsNull(bstm_dvsn_code   , '') AS bstmDvsnCode
          FROM IDCM..TM_RDP T1
         WHERE T1.rdp_code = '{tkotRdpCode}'
           AND T1.dlt_ysno = 'N'
         ORDER BY 1
            AT ISOLATION 0
    """,

    "getRePrceTrgtYsno": """
        SELECT IsNull(COUNT(*), 0) AS cnt
          FROM IDPL..TM_CMDT_PRCE_ADJM T1
         WHERE 1=1
           AND T1.rdp_code   = '009'
           AND T1.cmdt_id    = '{cmdtId}'
           AND T1.sttg_date  > convert(CHAR(8), getdate(), 112)
           AND T1.sttg_date <= convert(CHAR(8), dateadd(dd, +6, getdate()), 112)
           AND T1.mdfc_srmb  = (SELECT Max(TT1.mdfc_srmb)
                                  FROM IDPL..TM_CMDT_PRCE_ADJM TT1
                                 WHERE 1=1
                                   AND TT1.rdp_code   = '009'
                                   AND TT1.cmdt_id    = '{cmdtId}'
                                   AND TT1.sttg_date  > convert(CHAR(8), getdate(), 112)
                                   AND TT1.sttg_date <= convert(CHAR(8), dateadd(dd, +6, getdate()), 112)
                                   AND TT1.dlt_ysno   = 'N')
           AND T1.dlt_ysno   = 'N'
            AT ISOLATION 0
    """,

    "getPlorRlncExist": """
        SELECT IsNull(T1.plor_rlnc_num, '')       AS plorRlncNum
             , IsNull(T1.plor_rlnc_cdtn_code, '') AS plorRlncCdtnCode
          FROM IDPL..TM_PLOR_RLNC T1
         WHERE T1.rdp_code           = '{rdpCode}'
           AND T1.cmdt_id            = '{cmdtId}'
           AND T1.plor_rlnc_date     = '{plorRlncDate}'
           AND T1.plor_rler_id       = '{userId}'
           AND T1.plor_rlnc_rsn_code = '{plorRlncRsnCode}'
           AND T1.plor_rlnc_rsn_code NOT IN ('028', '029')
            AT ISOLATION 0 
    """,

    "getPlorRlncExistWithOrdrNum": """
        SELECT IsNull(T1.plor_rlnc_num, '')       AS plorRlncNum
             , IsNull(T1.plor_rlnc_cdtn_code, '') AS plorRlncCdtnCode
          FROM IDPL..TM_PLOR_RLNC T1
         WHERE T1.ordr_num           = '{ordrNum}'
           AND T1.cmdt_id            = '{cmdtId}'
            AT ISOLATION 0 
    """,

    "findOrderRequestBaseCheck": """
        SELECT S.wrhs_rdp_code                                                                                                              AS wrhsRdpCode         
             , S.plor_wkpt_code1                                                                                                            AS plorWkptCode1         
             , S.plor_wkpt_code2                                                                                                            AS plorWkptCode2         
             , (SELECT C.cmdt_cdtn_code FROM IDCM..TM_CMDT C WHERE C.cmdt_id = '{cmdtId}')                                                  AS cmdtCdtnCode
             , (SELECT C.jo_code FROM IDCM..TM_CMDT C WHERE C.cmdt_id = '{cmdtId}')                                                         AS joCode
             , (CASE WHEN '{cmdtDvsnCode}' IN (SELECT code_wrth FROM IDCM..TC_KFLW_CODE_DTL WHERE code_id = '1594') THEN 'Y' ELSE 'N' END ) AS checkCmdtDvsnCode
             , (CASE WHEN convert(CHAR(5), getdate(), 8) < C.buy_code_stnr_wrth THEN 'Y' ELSE 'N' END)                                      AS checkTodayPlorRlnc
          FROM IDCM..TM_RDP R
             , IDPL..TM_PLOR_GRP_WRK_STNR S
             , (SELECT buy_code_stnr_wrth
                  FROM IDPL..TC_BUY_STR_CODE C
                 WHERE C.code_id       = 'P201'
                   AND C.buy_code_wrth = '001') C
         WHERE R.plor_grp_code       = S.plor_grp_code
           AND R.rdp_code            = '{rdpCode}'
           AND S.plor_rlnc_dvsn_code = '{plorRlncDvsnCode}'
            AT ISOLATION 0
    """,

    "registerOrderRequestBase": """
        INSERT INTO IDPL..TM_PLOR_RLNC (plor_rlnc_num, rdp_code, plor_rlnc_date, cmdt_id, isbn, cmdt_dvsn_code, plor_rlnc_dvsn_code, wrhs_rdp_code, vndr_code, byng_dvsn_code, byng_dtl_dvsn_code, byng_rate, byng_cost, evnt_ysno, plor_rlnc_qntt, orig_plor_rlnc_qntt, plor_wkpt_code, ccbk_cnvs_qntt, plor_rlnc_cdtn_code, plor_rlnc_rsn_code, plor_rlnc_rsn, jo_code, trnp_dvsn_code, dlgd_schd_date, slpc_code, ordr_num, ordr_srmb, pclr_mtr, bkct_bks_grd_code, cmdt_name, pbcm_name, autr_name, cmdt_prce, pbcm_code, decr_id, decs_dttm, plor_ntyt_pros_rsn_code, plor_rler_id, plor_rlnc_dprt_code, orig_plor_rlnc_num, crtr_id, cret_dttm, amnr_id, amnd_dttm, use_ysno, lrqn_dlgd_dlvr_dvsn_code)
        VALUES ('{plorRlncNum}', '{rdpCode}', '{plorRlncDate}', '{cmdtId}', '{isbn}', '{cmdtDvsnCode}', '{plorRlncDvsnCode}', '{wrhsRdpCode}', '{vndrCode}', '{byngDvsnCode}', '{byngDtlDvsnCode}', {byngRate}, {byngCost}, '{evntYsno}', {plorRlncQntt}, {origPlorRlncQntt}, '{plorWkptCode}', {ccbkCnvsQntt}, '{plorRlncCdtnCode}', '{plorRlncRsnCode}', '{plorRlncRsn}', '{joCode}', '{trnpDvsnCode}', '{dlgdSchdDate}', '{slpcCode}', '{ordrNum}', {ordrSrmb}, '{pclrMtr}', '{bkctBksGrdCode}', '{cmdtName}', '{pbcmName}', '{autrName}', {cmdtPrce}, '{pbcmCode}', '{decrId}', '{decsDttm}', '{plorNtytProsRsnCode}', '{plorRlerId}', '{plorRlncDprtCode}', '{origPlorRlncNum}', 'P{crtrId}', GETDATE(), 'P{crtrId}', GETDATE(), '{useYsno}', '{lrqnDlgdDlvrDvsnCode}')
        
        SELECT plor_rlnc_num  AS plorRlncNum
             , isbn           AS cmdtCode
             , plor_rlnc_qntt AS plorRlncQntt
          FROM IDPL..TM_PLOR_RLNC T1
         WHERE T1.plor_rlnc_num = '{plorRlncNum}'
            AT ISOLATION 0
    """,

    "modifyOrderRequestBase": """
        UPDATE IDPL..TM_PLOR_RLNC
           SET plor_rlnc_qntt = {plorRlncQntt}
             , plor_rler_id   = '{plorRlerId}'
             , amnr_id        = '{amnrId}'
             , amnd_dttm      = GETDATE()
         WHERE plor_rlnc_num  = '{plorRlncNum}'
         
        SELECT plor_rlnc_num  AS plorRlncNum
             , isbn           AS cmdtCode
             , plor_rlnc_qntt AS plorRlncQntt
          FROM IDPL..TM_PLOR_RLNC T1
         WHERE T1.plor_rlnc_num = '{plorRlncNum}'
            AT ISOLATION 0
    """,

    "findTakeOutListForIsTakeInNew": """
        SELECT t1.tkot_rdp_code                 AS tkotRdpCode
             , t1.tkot_date                     AS tkotDate
             , t1.tkot_wrk_num                  AS tkotWrkNum
             , t1.box_num                       AS boxNum
             , ISNULL(RTRIM(t1.tkin_date), '')  AS tkinDate
             , t1.tkin_rdp_code                 AS tkinRdpCode
             , t1.tkio_dvsn_code                AS tkioDvsnCode
             , RTRIM(ISNULL(t1.decs_ysno, 'N')) AS decsYsno
          FROM IDPL..TM_TKIO t1
         WHERE t1.tkot_rdp_code = '{tkotRdpCode}'
           AND t1.tkot_date     = '{tkotDate}'
           AND t1.tkot_wrk_num  = '{tkotWrkNum}'
           AND t1.box_num       = '{boxNum}'
           AND t1.box_num       NOT LIKE 'ONEDAYBX%'
           AND t1.dlt_ysno      = 'N'
            AT ISOLATION 0
    """,

    "getHubSpoke": """
        SELECT hub_rdp_code   AS hubRdpCode
             , spoke_rdp_code AS spokeRdpCode
          FROM IDSS..TM_HUBNSPOKE
         WHERE hub_rdp_code = '{rdpCode}'
        UNION ALL
        SELECT hub_rdp_code   AS hubRdpCode
             , spoke_rdp_code AS spokeRdpCode
          FROM IDSS..TM_HUBNSPOKE
         WHERE spoke_rdp_code = '{rdpCode}'
            AT ISOLATION 0
    """,

    "getAreaVendorCount": """
        SELECT COUNT(*) AS cnt
          FROM IDCM..TM_APIC_AREA_ECSL_VNDR
         WHERE rdp_code = '{rdpCode}'
           AND cmdt_id  = '{cmdtId}'
           AND dlt_ysno = 'N'
            AT ISOLATION 0
    """,

    "getCommodidyReturnedGoods": """
       select t1.cmdt_id   AS cmdtId
            , t1.cmdt_code AS cmdtCode
            , (case when t2.rtgd_impb_ysno = '' or t2.rtgd_impb_ysno is null then 'N' else t2.rtgd_impb_ysno end) as pbcmRtgdImpbYsno
            , (case when t3.rtgd_impb_ysno = '' or t3.rtgd_impb_ysno is null then 'N' else t3.rtgd_impb_ysno end) as vndrRtgdImpbYsno
            , (CASE WHEN (t5.apl_code_wrth = '' or t5.apl_code_wrth IS NULL) AND
                         (t7.apl_code_wrth = '' or t7.apl_code_wrth IS NULL) AND
                         (t8.apl_code_wrth = '' or t8.apl_code_wrth IS NULL) 
                    THEN 'N' ELSE 'Y' END) as cmdtRtgdImpbYsno
         from IDCM..TM_CMDT t1
              left outer join IDCM..TM_PBCM t2 on t1.pbcm_code = t2.pbcm_code
              left outer join IDCM..TM_VNDR t3 on t1.vndr_code = t3.vndr_code
              left outer join IDPL..TM_ATMT_RTGD_EXCT t5 ON t1.cmdt_id = t5.apl_code_wrth
                                                         AND t5.sttg_date <= convert(CHAR(8), getdate(), 112)
                                                         AND t5.end_date  >= convert(CHAR(8), getdate(), 112)
                                                         AND t5.rdp_code = '009'                                    
                                                         AND t5.atmt_rtgd_exct_dvsn_code='003'                      
                                                         AND t5.atmt_rtgd_exct_rsn_code IN ('000','104','105','106') 
             left outer join IDPL..TM_ATMT_RTGD_EXCT t7 ON t7.apl_code_wrth = t1.vndr_code
                                                         AND t7.sttg_date <= convert(CHAR(8), getdate(), 112)
                                                         AND t7.end_date  >= convert(CHAR(8), getdate(), 112)
                                                         AND t7.rdp_code = '009'                                    
                                                         AND t7.atmt_rtgd_exct_dvsn_code='001'                      
                                                         AND t7.atmt_rtgd_exct_rsn_code IN ('000','101','102','103')
             LEFT outer join IDPL..TM_ATMT_RTGD_EXCT t8 ON t8.apl_code_wrth = t1.vndr_code
                                                         AND t8.sttg_date <= convert(CHAR(8), getdate(), 112)
                                                         AND t8.end_date  >= convert(CHAR(8), getdate(), 112)
                                                         AND t8.rdp_code = '000'                               
                                                         AND t8.atmt_rtgd_exct_dvsn_code='001'                 
                                                         AND t8.atmt_rtgd_exct_rsn_code IN ('101','102','103') 
             WHERE t1.dlt_ysno ='N' 
               AND t1.cmdt_id = '{cmdtId}'
                AT ISOLATION 0
    """,

    "getBoxNum": """
        DECLARE @getBoxNum CHAR(8)
        
        SELECT @getBoxNum = SUBSTRING(MAX(t1.box_num), 1, 1) + RIGHT('0000000' + CONVERT(varchar, ISNULL(CONVERT(NUMERIC, MAX(t1.box_num)), 0) + 1), 7)
          FROM IDPL..TA_BOX_STS t1
         WHERE t1.box_num BETWEEN '{startBoxNum}' AND '{endBoxNum}'
            AT ISOLATION 0
        
        IF (@getBoxNum > '{endBoxNum}')
            BEGIN
                SELECT '99999999' AS boxNum
            END
        ELSE
            BEGIN
                INSERT INTO IDPL..TA_BOX_STS (box_num, box_dvsn_code, crtr_id, cret_dttm, amnr_id, amnd_dttm, dlt_ysno)
                     VALUES (@getBoxNum, '2', 'P{crtrId}', GETDATE(), 'P{crtrId}', GETDATE(), 'N')
                
                SELECT ISNULL(T1.box_num, '00000000') AS boxNum
                  FROM IDPL..TA_BOX_STS T1
                 WHERE box_num       = @getBoxNum
                   AND box_dvsn_code = '2'
            END
    """,

    "getTakeInOutMasterCount": """
        SELECT ISNULL(COUNT(CONVERT(INT, tkot_rdp_code)), 0) AS cnt
          FROM IDPL..TM_TKIO
         WHERE 1=1
           AND tkot_date     = '{tkotDate}'
           AND box_num       = '{boxNum}'
           AND (tkin_date ='' OR tkin_date IS NULL)
           AND dlt_ysno = 'N'
            AT ISOLATION 0
    """,

    "getCmdtIdByCmdtCode": """
        SELECT TOP 1 IsNull(max(cmdt_id), '') AS cmdtId
          FROM IDCM..TM_CMDT
         WHERE cmdt_code = '{cmdtCode}'
           AND dlt_ysno = 'N'
         ORDER BY cmdt_code
            AT ISOLATION 0
    """,

    "getKflowCode": """
        SELECT t2.code_id        AS codeId                              
             , t2.code_wrth      AS codeWrth                              
             , t2.code_wrth_name AS codeWrthName                               
             , t2.prrt_rnkn      AS prrtRnkn                                    
          FROM IDCM..TC_KFLW_CODE_GRP t1
          LEFT OUTER JOIN IDCM..TC_KFLW_CODE_DTL t2
            ON t1.code_id = t2.code_id
         WHERE t1.code_id  = '{codeId}'
           AND t2.dlt_ysno = 'N'
            AT ISOLATION 0
    """,

    "rgstBkshCretDlt": """
        INSERT INTO IDSS..TM_STR_BKSH (rdp_code, bksh_num, prgp_num, bksh_rspb_jo_code, bksh_shp_dvsn_code, bksh_name, stir_dvsn_name, bksh_nmsr, bksh_with, bksh_hstr_name, gvrm_infm_code, bksh_cntt, prgp_name, mgr_empl_num, crtr_id, cret_dttm, amnr_id, amnd_dttm, dlt_ysno)
             VALUES ('{rdpCode}', '{bkshNum}', '{prgpNum}', '{bkshRspbJoCode}', '{bkshShpDvsnCode}', '{bkshName}', '', 0, 0, '', '{gvrmInfmCode}', '{bkshCntt}', '{prgpName}', '{mgrEmplNum}', 'P{crtrId}', GETDATE(), 'P{crtrId}', GETDATE(), 'N')
        
        EXEC IDSS.dbo.spe_TI_ISS_RLTM_KFLW 'TM_STR_BKSH', 'C', '', '{rdpCode}', '{bkshNum}', '{prgpNum}', null, null
        
        SELECT COUNT(*) AS cnt
          FROM IDSS..TM_STR_BKSH T1
         WHERE T1.rdp_code = '{rdpCode}'
           AND T1.bksh_num = '{bkshNum}'
           AND T1.prgp_num = '{prgpNum}'
            AT ISOLATION 0
    """,

    "getBkshExists": """
        SELECT COUNT(*) AS cnt
          FROM IDSS..TM_STR_BKSH T1
         WHERE T1.rdp_code = '{rdpCode}'
           AND T1.bksh_num = '{bkshNum}'
           AND T1.prgp_num = '{prgpNum}'
            AT ISOLATION 0
    """,

    "updateBkshCretDlt": """
        UPDATE IDSS..TM_STR_BKSH
           SET bksh_rspb_jo_code  = '{bkshRspbJoCode}'
             , bksh_shp_dvsn_code = '{bkshShpDvsnCode}'
             , bksh_name          = '{bkshName}'
             , gvrm_infm_code     = '{gvrmInfmCode}'
             , bksh_cntt          = '{bkshCntt}'
             , prgp_name          = '{prgpName}'
             , mgr_empl_num       = '{mgrEmplNum}'
             , amnr_id            = '{crtrId}'
             , amnd_dttm          = GETDATE()
             , dlt_ysno           = 'N'
         WHERE 1=1
           AND rdp_code = '{rdpCode}'
           AND bksh_num = '{bkshNum}'
           AND prgp_num = '{prgpNum}'

        EXEC IDSS.dbo.spe_TI_ISS_RLTM_KFLW 'TM_STR_BKSH', 'U', '', '{rdpCode}', '{bkshNum}', '{prgpNum}', null, null
        
        SELECT COUNT(*) AS cnt
          FROM IDSS..TM_STR_BKSH T1
         WHERE T1.rdp_code = '{rdpCode}'
           AND T1.bksh_num = '{bkshNum}'
           AND T1.prgp_num = '{prgpNum}'
            AT ISOLATION 0
    """,

    "deleteBkshCretDlt": """
        EXEC IDSS.dbo.spe_TI_ISS_RLTM_KFLW 'TM_STR_BKSH', 'D', '', '{rdpCode}', '{bkshNum}', '{prgpNum}', null, null
        
        DELETE IDSS..TM_STR_BKSH
         WHERE 1=1
           AND rdp_code = '{rdpCode}'
           AND bksh_num = '{bkshNum}'
           AND prgp_num = '{prgpNum}'

        SELECT COUNT(*) AS cnt
          FROM IDSS..TM_STR_BKSH T1
         WHERE T1.rdp_code = '{rdpCode}'
           AND T1.bksh_num = '{bkshNum}'
           AND T1.prgp_num = '{prgpNum}'
            AT ISOLATION 0
    """,

    "getExistsScanList": """
        SELECT COUNT(*) AS cnt
          FROM IDSS..TM_CMDT_SCAN_LIST T1
         WHERE T1.rgst_date = '{rgstDate}'
           AND T1.user_id  = '{crtrId}'
           AND T1.scan_wrk_name = '{scanWrkName}'
           AND T1.rdp_code = '{rdpCode}'
           AND T1.cmdt_code  = '{cmdtCode}'
           AND T1.dlt_ysno = 'N'
            AT ISOLATION 0
    """,

    "removeScanList": """
        DELETE IDSS..TM_CMDT_SCAN_LIST
         WHERE rgst_date  = '{rgstDate}'
           AND user_id   = '{crtrId}'
           AND scan_wrk_name  = '{scanWrkName}'
           AND rdp_code  = '{rdpCode}'
           AND cmdt_code = '{cmdtCode}'
           AND dlt_ysno  = 'N'
            
        SELECT COUNT(*) AS cnt
          FROM IDSS..TM_CMDT_SCAN_LIST T1
         WHERE T1.rgst_date = '{rgstDate}'
           AND T1.user_id  = '{crtrId}'
           AND T1.scan_wrk_name = '{scanWrkName}'
           AND T1.rdp_code = '{rdpCode}'
           AND T1.cmdt_code  = '{cmdtCode}'
           AND T1.dlt_ysno = 'N'
            AT ISOLATION 0
    """,

    "echoCond": """
        SELECT '{rgstDate}'  AS rgstDate
             , '{crtrId}'   AS crtrId
             , '{scanWrkName}'  AS scanWrkName
             , '{rdpCode}'  AS rdpCode
             , '{cmdtCode}' AS cmdtCode
            AT ISOLATION  0
    """,

    "getPlorRlncExistWithRlncNum": """
        SELECT IsNull(T1.plor_rlnc_num, '')       AS plorRlncNum
             , IsNull(T1.plor_rlnc_cdtn_code, '') AS plorRlncCdtnCode
          FROM IDPL..TM_PLOR_RLNC T1
         WHERE T1.plor_rlnc_num      = '{plorRlncNum}'
            AT ISOLATION 0 
    """,

    "deletePlorRlncNumWithRlncNum": """
        DELETE IDPL..TM_PLOR_RLNC
         WHERE plor_rlnc_num = '{plorRlncNum}'
    
        SELECT COUNT(*) AS cnt
          FROM IDPL..TM_PLOR_RLNC T1
         WHERE T1.plor_rlnc_num = '{plorRlncNum}'
            AT ISOLATION 0
    """,

    "getCmdtInfoFromPDA": """
        SELECT IsNull(T1.cmdt_id                , '') AS cmdtId              
             , IsNull(T1.cmdt_name              , '') AS cmdtName            
             , IsNull(T1.sbtt_name1             , '') AS sbttName1           
             , IsNull(T1.sbtt_name2             , '') AS sbttName2           
             , IsNull(T1.cndn_cmdt_name         , '') AS cndnCmdtName        
             , IsNull(T1.cmdt_foln_name         , '') AS cmdtFolnName        
             , IsNull(T1.cmdt_hngl_mark_name    , '') AS cmdtHnglMarkName    
             , IsNull(T1.cmdt_smlr_name         , '') AS cmdtSmlrName        
             , IsNull(T1.cmdt_clst_code         , '') AS cmdtClstCode        
             , IsNull(T1.hgrn_cmdt_id           , '') AS hgrnCmdtId          
             , IsNull(T1.cmdt_code              , '') AS cmdtCode            
             , IsNull(T1.trnf_cmdt_code         , '') AS trnfCmdtCode        
             , IsNull(T1.cmdt_code_cdtn_code    , '') AS cmdtCodeCdtnCode    
             , IsNull(T1.pbcm_code              , '') AS pbcmCode            
             , IDSS.dbo.uf_removelf(IsNull((SELECT pbcm_name                                              
                                              FROM IDCM..TM_PBCM                                          
                                             WHERE pbcm_code = IsNull(T1.pbcm_code, '')), '')) AS pbcmName
             , IDSS.dbo.uf_removelf(IsNull((SELECT autr_name1                                             
                                              FROM IDCM..TD_CMDT_AUTR                                     
                                             WHERE cmdt_id = IsNull(T1.cmdt_id, '')), ''))     AS autrName
             , IsNull(T1.vndr_code              , '') AS vndrCode            
             , IsNull(T1.dscn_ysno              , '') AS dscnYsno            
             , IsNull(T1.vat_exsn_ysno          , '') AS vatExsnYsno         
             , IsNull(T1.rlse_date              , '') AS rlseDate            
             , IsNull(T1.sale_lmtt_age          , 0 ) AS saleLmttAge         
             , IsNull(T1.cmdt_set_qntt          , 0 ) AS cmdtSetQntt         
             , IsNull(T1.lngg_code              , '') AS lnggCode            
             , IsNull(T1.cmdt_cdtn_code         , '') AS cmdtCdtnCode        
             , IsNull(T1.cmdt_kind_code         , '') AS cmdtKindCode        
             , IsNull(T1.dstb_cdtn_code         , '') AS dstbCdtnCode        
             , IsNull(T1.sale_psbl_ysno         , '') AS salePsblYsno        
             , IsNull(T1.cmdt_patr_code         , '') AS cmdtPatrCode        
             , IsNull(T1.cver_pbcm_name         , '') AS cverPbcmName        
             , IsNull(T1.cver_img_exsn_ysno     , '') AS cverImgExsnYsno     
             , IsNull(T1.cver_lrim_exsn_ysno    , '') AS cverLrimExsnYsno    
             , IsNull(T1.cmdt_dvsn_code         , '') AS cmdtDvsnCode        
             , IsNull(T1.jo_code                , '') AS joCode              
             , IsNull(T1.cmdt_prvl_view_ysno    , '') AS cmdtPrvlViewYsno    
             , IsNull(T1.fbp_trgt_ysno          , '') AS fbpTrgtYsno         
             , IsNull(T1.cmdt_cdtn_mdfc_rsn_code, '') AS cmdtCdtnMdfcRsnCode 
             , IsNull(T1.incm_ddct_trgt_ysno    , '') AS incmDdctTrgtYsno    
             , IsNull((SELECT min(S1.vndr_code)
                         FROM IDCM..TM_APIC_AREA_ECSL_VNDR S1
                        WHERE S1.rdp_code = '{rdpCode}'
                          AND S1.cmdt_id  = T1.cmdt_id
                          AND S1.dlt_ysno = 'N'), '') AS minApicAreaVndrCode
             , IsNull((SELECT max(S1.vndr_code)
                         FROM IDCM..TM_APIC_AREA_ECSL_VNDR S1
                        WHERE S1.rdp_code = '{rdpCode}'
                          AND S1.cmdt_id  = T1.cmdt_id
                          AND S1.dlt_ysno = 'N'), '') AS maxApicAreaVndrCode
             , IsNull((SELECT count(S2.cmdt_id)
                         FROM IDCM..TD_SMST_UNVR_RE S2
                        WHERE S2.smst_yr   = SUBSTRING(CONVERT(CHAR(8), GETDATE(), 112), 1, 4)
                          AND S2.smst_code = (CASE WHEN CONVERT(INT, SUBSTRING(CONVERT(CHAR(8), GETDATE(), 112), 5, 2)) <= 6 THEN '1' ELSE '2' END)
                          AND S2.rdp_code  = '{rdpCode}'
                          AND S2.cmdt_id   = T1.cmdt_id
                          AND S2.use_ysno  = 'Y'), 0) AS smstUnvrCnt
             , IsNull((SELECT count(*)
                         FROM IDCM..TM_APIC_AREA_ECSL_VNDR S1
                        WHERE S1.rdp_code = (SELECT SS1.hub_rdp_code
                                               FROM IDSS..TM_HUBNSPOKE SS1
                                              WHERE SS1.spoke_rdp_code = '{rdpCode}'
                                                AND SS1.dlt_ysno       = 'N')
                          AND S1.cmdt_id  = T1.cmdt_id
                          AND S1.dlt_ysno = 'N'), 0) AS hubApicAreaVndrYsno
             , 'http://image.kyobobook.co.kr/images/book/large/' + substring(IsNull(T1.cmdt_code, ''), 11, 13) + '/l' + IsNull(T1.cmdt_code, '') + '.jpg'       as imgUrl
          FROM IDCM..TM_CMDT T1
         WHERE T1.cmdt_code like '{cmdtCode}'
           AND T1.dlt_ysno = 'N'
            AT ISOLATION 0
    """,

    "cmdt_main_detail_ink_prce": """
        DECLARE @exchangeRate    NUMERIC(10,2)
        DECLARE @exchangeRateJnt NUMERIC(10,2)
        DECLARE @cmdt_code       CHAR(13)
    
        SELECT @cmdt_code       = '{cmdtCode}'
        SELECT @exchangeRate    = CONVERT(NUMERIC(10,2), rate) FROM cti..CTI_CD09 WHERE yulgb='U' and gb='1'
        SELECT @exchangeRateJnt = CONVERT(NUMERIC(10,2), rate) FROM cti..CTI_CD09 WHERE yulgb='Y' and gb='1'
    
        SELECT TOP 1 TT1.prce_pric AS ink_prce
             , TT1.dscnRate        AS ink_dscn_rate
          FROM (
                SELECT round(CONVERT(numeric, A.list_pri * @exchangeRate), -1) AS prce_pric
                     , round(CONVERT(numeric, (CASE WHEN A.dis_event_yn = 1 THEN A.sale_pri WHEN A.dis_event_yn = 0 THEN A.list_pri * (100 - CONVERT(TINYINT, ISNULL(B.code_des, '0'))) * 0.01 END * @exchangeRate)), -1) AS dscn_pric
                     , (CASE WHEN A.dis_event_yn = 1 THEN 0 WHEN A.dis_event_yn = 0 THEN CONVERT(TINYINT, ISNULL(B.code_des, '0')) END) AS dscnRate
                  FROM eshv4..EST_BNT A, eshv4..EST_BNT_CODE AS B
                 WHERE barcode = @cmdt_code
                 AND A.discount_key = rtrim(B.eng_code)
                 AND B.code_gb = '120'
                UNION ALL
                SELECT round(CONVERT(numeric, (CASE WHEN A.dis_event_yn = 1 THEN A.list_pri WHEN A.dis_event_yn = 0 THEN A.list_pri END * @exchangeRateJnt)), -1) AS prce_pric
                     , round(CONVERT(numeric, (CASE WHEN A.dis_event_yn = 1 THEN A.sale_pri WHEN A.dis_event_yn = 0 THEN A.list_pri * (100 - CONVERT(TINYINT, ISNULL(@exchangeRateJnt, 0))) * 0.01 END * @exchangeRateJnt)), -1) AS dscn_pric
                     , 0 AS dscnRate
                 FROM eshv4..EST_JNT A
                 WHERE barcode = @cmdt_code
        ) TT1
         ORDER BY 1 DESC
         at isolation 0
    """,

    "getLastBuildNumber": """
        SELECT ISNULL(code_wrth_name, '') AS buildNum
          FROM IDSS..TC_STR_CODE
         WHERE code_id = '0021'
           AND code_wrth = '001'
            AT ISOLATION 0
    """,

    "getMonthSaleReport": """
        SELECT T1.SALEDATE                                                         AS SALEDATE
             , T1.STORECD                                                          AS STORECD
             , ISNULL((SELECT IDSS.dbo.uf_removelf(CODENAME)
                         FROM IDPS..MSTCOMMON S1
                        WHERE S1.STORECD = '000'
                          AND S1.CDTYPE  = 'C03'
                          AND S1.CODE    = T1.STORECD), '')                        AS STORENM
             , ISNULL(T4.PBCMCODE, '')                                             AS PBCMCODE
             , ISNULL((SELECT IDSS.dbo.uf_removelf(S1.pbcm_name)
                         FROM IDCM..TM_PBCM S1
                        WHERE S1.pbcm_code = ISNULL(T4.PBCMCODE, '')), '')         AS PBCMNAME
             , ISNULL(T4.VNDRCODE, '')                                             AS VNDRCODE
             , ISNULL((SELECT IDSS.dbo.uf_removelf(S1.vndr_name)
                         FROM IDCM..TM_VNDR S1
                        WHERE S1.vndr_code = ISNULL(T4.VNDRCODE, '')), '')         AS VNDRNAME
             , ISNULL(T1.TRANTYPE, '')                                             AS TRANTYPE
             , ISNULL((SELECT IDSS.dbo.uf_removelf(S1.CODENAME)
                         FROM IDPS..MSTCOMMON S1
                        WHERE S1.STORECD = '000'
                          AND S1.CDTYPE  = 'PS208'
                          AND S1.CODE    = ISNULL(T1.TRANTYPE, '')), '')           AS TRANTYPENAME
             , ISNULL(T1.TRANKIND, '')                                             AS TRANKIND
             , ISNULL((SELECT IDSS.dbo.uf_removelf(S1.CODENAME)
                         FROM IDPS..MSTCOMMON S1
                        WHERE S1.STORECD = '000'
                          AND S1.CDTYPE  = 'PS309'
                          AND S1.CODE    = ISNULL(T1.TRANKIND, '')), '')           AS TRANKINDNAME
             , ISNULL(T2.PLUBARCD, '')                                             AS PLUBARCD
             , ISNULL(IDSS.dbo.uf_removelf(T3.NAME), '')                           AS NAME
             , ISNULL(T2.JOCD, '')                                                 AS JOCD
             , ISNULL((SELECT IDSS.dbo.uf_removelf(S1.CODENAME)
                         FROM IDPS..MSTCOMMON S1
                        WHERE S1.STORECD = '000'
                          AND S1.CDTYPE  = 'C04'
                          AND S1.CODE    = ISNULL(T2.JOCD, '')), '')               AS JONAME
             , ISNULL(T2.NORPRICE, 0)                                              AS NORPRICE
             , CONVERT(FLOAT, ISNULL(T2.ETCINFO1, '0'))                            AS ETCINFO1
             , 100 - CONVERT(FLOAT, ISNULL(T2.ETCINFO1, '0'))                      AS BENERATE
             , ISNULL(T2.COST, 0)                                                  AS COST
             , SUM((CASE WHEN ISNULL(T1.TRANTYPE, '') = '00' THEN T2.QTY
                         ELSE                                     T2.QTY * -1
                    END))                                                          AS QTY
             , SUM((CASE WHEN ISNULL(T1.TRANTYPE, '') = '00' THEN T2.SALEAMT
                         ELSE                                     T2.SALEAMT * -1
                    END))                                                          AS SALEAMT
             , SUM((CASE WHEN ISNULL(T1.TRANTYPE, '') = '00' THEN ISNULL(T2.NORPRICE, 0) - ISNULL(T2.COST, 0)
                         ELSE                                     (ISNULL(T2.NORPRICE, 0) - ISNULL(T2.COST, 0)) * -1
                    END))                                                          AS SALEBENEAMT
          FROM IDPS..TRNSALHDR T1, IDPS..TRNSALDTL T2, IDPS..MSTGOODS T3, IDPS..MSTGOODSADDTDSRP T4
         WHERE T1.SALEDATE  = T2.SALEDATE
           AND T1.STORECD   = T2.STORECD
           AND T1.POSNO     = T2.POSNO
           AND T1.TRANNO    = T2.TRANNO
           AND T2.STORECD   = T3.STORECD
           AND T2.PLUBARCD  = T3.PLUBARCD
           AND T3.STORECD   = T4.STORECD
           AND T3.PLUBARCD  = T4.PLUBARCD
           AND T1.SALEDATE >= '{fromDate}'
           AND T1.SALEDATE <= '{toDate}'
           AND T1.STORECD   = '{rdpCode}'
           AND T1.TRANTYPE IN ('00', '01')
           AND T2.CANCELGB  = '0'
        GROUP BY T1.SALEDATE
             , T1.STORECD
             , ISNULL(T4.PBCMCODE, '')
             , ISNULL(T4.VNDRCODE, '')
             , ISNULL(T1.TRANTYPE, '')
             , ISNULL(T1.TRANKIND, '')
             , ISNULL(T2.PLUBARCD, '')
             , ISNULL(IDSS.dbo.uf_removelf(T3.NAME), '')
             , ISNULL(T2.JOCD, '')
             , ISNULL(T2.NORPRICE, 0)
             , CONVERT(FLOAT, ISNULL(T2.ETCINFO1, '0'))
             , ISNULL(T2.COST, 0)
        ORDER BY 1, 2, 4, 8, 10, 12
            AT ISOLATION 0
    """,

"findsalespercmdt": """ 

        DECLARE 
        @fromYmd CHAR(8), 
        @toYmd  CHAR(8),
        @rdpCode  CHAR(10),
        @cmdtCode  CHAR(13),
        @vndrCode  CHAR(10),
        @posNo  CHAR(10),
        @tranNo  CHAR(10),
        @str_sql VARCHAR(16000)
        
        SELECT @fromYmd ='{fromYmd}'
        SELECT @toYmd ='{toYmd}'
        SELECT @rdpCode ='{rdpCode}'
        SELECT @cmdtCode ='{cmdtCode}'
        SELECT @vndrCode ='{vndrCode}'
        SELECT @posNo ='{posNo}'
        SELECT @tranNo ='{tranNo}'
        SELECT @str_sql = 
                '
         SELECT t1.STORECD, t1.SALEDATE, t1.POSNO, t1.TRANNO, t2.SEQ, t1.SALETIME, t3.cmdt_name AS NAME, t2.PLUBARCD
       , (CASE WHEN t1.TRANTYPE = ''00'' THEN t2.QTY ELSE (-1) * t2.QTY END) as QTY
       , (CASE WHEN t1.TRANTYPE = ''00'' THEN t2.SALEAMT ELSE (-1) * t2.SALEAMT END) as SALEAMT
    FROM IDPS..TRNSALHDR t1, IDPS..TRNSALDTL t2, IDCM..TM_CMDT t3
   WHERE t1.SALEDATE = t2.SALEDATE
     AND t1.STORECD = t2.STORECD
     AND t1.POSNO = t2.POSNO
     AND t1.TRANNO = t2.TRANNO
     AND t2.PLUBARCD = t3.cmdt_code
     AND t3.dlt_ysno = ''N''
     AND t3.cmdt_id  = (SELECT MAX(S1.cmdt_id)
                          FROM IDCM..TM_CMDT S1
                         WHERE S1.cmdt_code = t2.PLUBARCD
                           AND S1.dlt_ysno  = ''N'')  ' 
         
         
        IF @fromYmd <> 'fromYmd'
        	SELECT @str_sql = @str_sql +  ' and t1.SALEDATE >= '''+ @fromYmd + ''' '
        	
        IF @toYmd <> 'toYmd'
        	SELECT @str_sql = @str_sql +  ' and t1.SALEDATE <= '''+ @toYmd + ''' '
        		        	

        	
        IF @rdpCode <> 'rdpCode'
        	SELECT @str_sql = @str_sql +  ' and t2.STORECD = '''+ @rdpCode + ''' '
        	
        IF @cmdtCode <> 'cmdtCode'
        	SELECT @str_sql = @str_sql +  ' and t2.PLUBARCD = '''+ @cmdtCode + ''' '

        IF @vndrCode <> 'vndrCode'
        	SELECT @str_sql = @str_sql +  ' and t2.PURCHCODE = '''+ @vndrCode + ''' '
        	
        IF @posNo <> 'posNo'
        	SELECT @str_sql = @str_sql +  ' and t2.POSNO = '''+ @posNo + ''' '

        IF @tranNo <> 'tranNo'
        	SELECT @str_sql = @str_sql +  ' and t2.TRANNO = '''+ @tranNo + ''' '

        EXECUTE (@str_sql)

    """,     

    "gethyundaideptcardsale": """



SELECT A.SALEDATE AS "매출일자", 
       A.STORECD + "[" + (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = "C03" AND CODE = A.STORECD AND DELYN = "0") + "]" AS "점포명", 
       A.POSNO AS "포스번호", A.TRANNO AS "거래번호", A.SEQ AS "순번", A.APPTIME AS "판매시간",
       (CASE WHEN A.TRANTYPE = "10" THEN "판매" ELSE "반품" END) AS "판매구분",
       A.APPNO "승인번호",
       A.CARDNO,
       (CASE WHEN A.TRANTYPE = "10" THEN A.SALEAMT ELSE -A.SALEAMT END) AS "결제금액",
       
       A.ORGAPPDATE AS "원거래승인일자",
       A.ORGAPPNO AS "원거래 승인번호",
       SUBSTRING(A.ORGTRANINFO,13,4) AS "원거래포스번호",
       SUBSTRING(A.ORGTRANINFO,17,4) AS "원거래거래번호"
       
        FROM IDPS..TRNCARDPAY A 
        WHERE A.SALEDATE >='{fromDate}'
         AND A.SALEDATE <= '{toDate}'
         AND A.PURCODE ="90"
         AND A.STORECD='{storeCd}'
      AT ISOLATION 0


    """,

    "getnexthealthcheck": """

       SELECT SERVICEID, SERVICENM , RUNYN FROM IDPS..MSTNEXTENABLE WHERE SERVICEID ='{serviceId}'

    """,
}

