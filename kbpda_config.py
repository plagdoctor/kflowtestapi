# -*- coding: utf-8 -*-

"""
    Title:
    Author: helloong@gmail.com
    DateTime: 0000.00.00 First created
    History:
        -
"""
from dotenv import load_dotenv
import os

# Credentials
load_dotenv('.env')

CHAT_ID_LIST = ['287456840', ]

TOKEN = os.getenv('TOKEN')

CIPHER_KEY = os.getenv('CIPHER_KEY')

# KFLOW_TEST
# SERVICE_NAME = 'kflowapitest'
# KFLOW = os.getenv('KFLOW_TEST')
# # TPSYB2 = os.getenv('TPSYB2_TEST')
# KFLOW_URL = os.getenv('KFLOW_URL_TEST')

# KFLOW_REAL
SERVICE_NAME = 'kflowapi'
KFLOW = os.getenv('KFLOW_REAL')
TPSYB2 = os.getenv('TPSYB2_REAL')
KFLOW_URL = os.getenv('KFLOW_URL_REAL')

TIMEOUT = 30

if __name__ == '__main__':
    print('helloong rents miumiu World!!')
