
  SELECT TT1.STORECD + ':' + (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C03' AND CODE = TT1.STORECD AND DELYN = '0') AS 'STORECD',
         SUM(CASE WHEN TT1.PLUGB = '1' AND TT1.TAXGB ='0' THEN TT1.SALEAMT ELSE 0 END) AS 'KB_TAX_SALEAMT',              
         SUM(CASE WHEN TT1.PLUGB = '1' AND TT1.TAXGB ='1' THEN TT1.SALEAMT ELSE 0 END) AS 'KB_NONETAX_SALEAMT',              
         SUM(CASE WHEN TT1.PLUGB = '1' AND TT1.TAXGB ='2' THEN TT1.SALEAMT ELSE 0 END) AS 'KB_GIFTCARD_SALEAMT',              
         SUM(CASE WHEN TT1.PLUGB = '2' AND TT1.TAXGB ='0' THEN TT1.SALEAMT ELSE 0 END) AS 'KH_TAX_SALEAMT',              
         SUM(CASE WHEN TT1.PLUGB = '2' AND TT1.TAXGB ='1' THEN TT1.SALEAMT ELSE 0 END) AS 'KH_NONETAX_SALEAMT',              
         SUM(CASE WHEN TT1.PLUGB = '2' AND TT1.TAXGB ='2' THEN TT1.SALEAMT ELSE 0 END) AS 'KH_GIFTCARD_SALEAMT',             
         SUM(TT1.SALEAMT)                                                                                                 AS 'TOT_SALEAMT'       
    FROM (                                                                                                                                         
  		SELECT '일반매출'                                                        AS 'TYPE',                                                           
  		       a.SALEDATE                                                        AS 'SALEDATE',                                                       
  		       a.STORECD                                                         AS 'STORECD',                                                        
  		       a.POSNO                                                           AS 'POSNO',                                                          
       		   a.COMPANY                                                         AS 'COMPANY',                                                        
  		       a.TRANNO                                                          AS 'TRANNO',                                                         
  		       b.SEQ                                                             AS 'SEQ',                                                            
  		       (CASE WHEN substring(b.CMDTDVSNCD,1,2) <> '40' THEN '1' ELSE '2' END)                   AS 'PLUGB',                                                        
  		       (CASE WHEN b.TAXGB = '0' THEN '0' ELSE '1' END)                   AS 'TAXGB',                                                          
  		       (CASE WHEN a.TRANTYPE = '00' THEN b.QTY ELSE -b.QTY END)          AS 'QTY',                                                            
  		       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)  AS 'SALEAMT'                                                         
  		FROM IDPS..TRNSALHDR a, IDPS..TRNSALDTL b                                                                                                                 
  		WHERE a.SALEDATE = b.SALEDATE                                                                                                                 
  		  AND a.STORECD  = b.STORECD                                                                                                                  
  		  AND a.POSNO    = b.POSNO                                                                                                                    
  		  AND a.TRANNO   = b.TRANNO                                                                                                                   
  		  AND a.TRANTYPE IN ('00', '01')                                                                                                              
  		  AND a.TRANKIND NOT IN ('10', '11', '13', '14', '15', '12', '17')                                                                                             
  		  AND b.CANCELGB = '0'                                                                                                                        
  	     	    AND a.POSNO    NOT LIKE '__[49]_'                                                                                                    


  		  AND a.SALEDATE >= '20230801' and  a.SALEDATE <= '20230831'            
  		  AND a.STORECD ='059'                                                 

  		UNION ALL                                                                                                                                     
  		SELECT '선매출'                                                          AS 'TYPE',                                                           
  		       a.CLOSEDATE                                                       AS 'SALEDATE',                                                       
  		       a.STORECD                                                         AS 'STORECD',                                                        
  		       a.POSNO                                                           AS 'POSNO',                                                          
       		 a.COMPANY                                                         AS 'COMPANY',                                                        
  		       a.TRANNO                                                          AS 'TRANNO',                                                         
  		       b.SEQ                                                             AS 'SEQ',                                                            
  		       (CASE WHEN substring(b.CMDTDVSNCD,1,2) <> '40' THEN '1' ELSE '2' END)                   AS 'PLUGB',                                                          
  		       (CASE WHEN b.TAXGB = '0' THEN '0' ELSE '1' END)                   AS 'TAXGB',                                                          
  		       (CASE WHEN a.TRANTYPE = '00' THEN b.QTY ELSE -b.QTY END)          AS 'QTY',                                                            
  		       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)  AS 'SALEAMT'                                                         
  		FROM (select c.FNSHDATE AS CLOSEDATE, a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO, b.SEQ
                              , a.COMPANY, a.TRANTYPE, a.TRANKIND
                            , (case when a.STORECD IN ('013','023') then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end) SALCOMCD
                              , '1' SALEFLAG
                              , '0' PAYFLAG, b.PLUGB
                              , max(c.RESTYPE) RESTYPE
                          from IDPS..TRNRESERVATION c, IDPS..TRNSALHDR a (INDEX PK_TRNSALHDR), IDPS..TRNSALDTL b (INDEX PK_TRNSALDTL)
                        where c.SALEDATE = a.SALEDATE   and c.STORECD  = a.STORECD    and c.POSNO    = a.POSNO      and c.TRANNO   = a.TRANNO
                          and a.SALEDATE = b.SALEDATE   and a.STORECD  = b.STORECD    and a.POSNO    = b.POSNO      and a.TRANNO   = b.TRANNO
                            and a.TRANTYPE in ('00','01')
                          and a.TRANKIND = '15'
                          AND a.POSNO NOT LIKE '99%'
                          AND a.DELYN = '0'
                          and b.CANCELGB = '0'
                              and (c.FNSHDATE <> '' AND c.FNSHDATE IS NOT NULL)
                      group by c.SALEDATE, c.STORECD, c.POSNO, c.TRANNO
                              , a.SALEDATE, a.STORECD, a.POSNO, a.TRANNO
                              , b.SALEDATE, b.STORECD, b.POSNO, b.TRANNO, b.SEQ
                              , a.COMPANY, a.TRANTYPE, a.TRANKIND, c.FNSHDATE, b.PLUGB
                            , (case when a.STORECD  IN ('013','023')  then '001' when a.STORECD IN ('063', '064') then '002' when b.PLUGB = '7' then '001' else '002' end)
                      ) a, IDPS..TRNSALDTL b                                                                                                           
  		WHERE a.SALEDATE  = b.SALEDATE                                                                                                                
  		  AND a.STORECD   = b.STORECD                                                                                                                 
  		  AND a.POSNO     = b.POSNO                                                                                                                   
  		  AND a.TRANNO    = b.TRANNO                                                                                                                  
  		  AND a.SEQ       = b.SEQ                                                                                                                     
  		  AND a.TRANTYPE  IN ('00', '01')                                                                                                             
  		  AND a.TRANKIND  = '15' AND a.SALEFLAG  = '1' AND a.PAYFLAG   = '0'                                                                          
  		  AND b.CANCELGB  = '0'                                                                                                                       
  	     	    AND a.POSNO    NOT LIKE '__[49]_'                                                                                                    


  		  AND a.CLOSEDATE  >= '20230801' and  a.CLOSEDATE <= '20230831'                     
  		  AND a.STORECD ='059'                                                                                            
    
  		UNION ALL                                                                                                                                     
  		SELECT '상품권'                                                          AS 'TYPE',                                                           
  		       a.SALEDATE                                                        AS 'SALEDATE',                                                       
  		       a.STORECD                                                         AS 'STORECD',                                                        
  		       a.POSNO                                                           AS 'POSNO',                                                          
       		 a.COMPANY                                                         AS 'COMPANY',                                                        
  		       a.TRANNO                                                          AS 'TRANNO',                                                         
  		       b.SEQ                                                             AS 'SEQ',                                                            
  		       (CASE WHEN substring(b.CMDTDVSNCD,1,2) <> '40' THEN '1' ELSE '2' END)                   AS 'PLUGB',                                                        
  		       '2'                                                               AS 'TAXGB',                                                          
  		       (CASE WHEN a.TRANTYPE = '00' THEN b.QTY ELSE -b.QTY END)          AS 'QTY',                                                            
  		       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)  AS 'SALEAMT'                                                         
  		FROM IDPS..TRNSALHDR a, IDPS..TRNSALDTL b                                                                                                                 
  		WHERE a.SALEDATE = b.SALEDATE                                                                                                                 
  		  AND a.STORECD  = b.STORECD                                                                                                                  
  		  AND a.POSNO    = b.POSNO                                                                                                                    
  		  AND a.TRANNO   = b.TRANNO                                                                                                                   
  		  AND a.TRANTYPE IN ('00', '01')                                                                                                              
  		  AND a.TRANKIND IN ('13', '14', '17')                                                                                                              
  		  AND b.CANCELGB = '0'                                                                                                                        
        AND a.POSNO    NOT LIKE '__[49]_'                                                                                                           

        AND a.SALEDATE  >= '20230801' and  a.SALEDATE <= '20230831'       
  		  AND a.STORECD ='059'                                                                                                       
                                                   
  	   ) TT1                                                                                                                                        
    GROUP BY TT1.STORECD
    ORDER BY 1

        AT ISOLATION 0        
--'052:광교월드스퀘어점', 2498790, 45948770, 0, 26648540, 74240, 0, 75170340