# -*- coding: utf-8 -*-

import base64
import hashlib
from Cryptodome import Random
from Cryptodome.Cipher import AES
import string
import random

# 아마 특정한 블록 사이즈를 채우기 위해서 입력된 값을 임의의 값으로 패딩(채워주기) 하는 코드 인 듯...
BS = 16
pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS).encode()
unpad = lambda s: s[:-ord(s[len(s) - 1:])]


def iv():
    """
    The initialization vector to use for encryption or decryption.
    It is ignored for MODE_ECB and MODE_CTR.
    """
    return chr(0) * 16


class AESCipher(object):
    """
    https://github.com/dlitz/pycrypto
    """
    def __init__(self, key):
        self.key = key
        # self.key = hashlib.sha256(key.encode()).digest()

    # 메시지를 암호화 하는 함수
    def encrypt(self, plaintext):
        """
        It is assumed that you use Python 3.0+
        , so plaintext's type must be str type(== unicode).
        """
        plaintext = plaintext.encode()
        raw = pad(plaintext)
        cipher = AES.new(self.key.encode('utf-8'), AES.MODE_CBC, iv().encode('utf-8'))
        enc_message = cipher.encrypt(raw)
        return base64.b64encode(enc_message).decode('utf-8')

    def decrypt(self, enc_message):
        enc_message = base64.b64decode(enc_message)
        cipher = AES.new(self.key.encode('utf-8'), AES.MODE_CBC, iv().encode('utf-8'))
        dec = cipher.decrypt(enc_message)
        return unpad(dec).decode('utf-8')


if __name__ == "__main__":
    # 암호화 키
    key = "KBSSPDA&HELLOONG"

    # 테스트 용 문자열
    message = ""
    for idx, x in enumerate(range(0, 51)):
        char_set = string.ascii_uppercase + string.digits
        message = ''.join(random.sample(char_set*6, x))

        # 테스트용 문자열을 암호화 하고, 미리 암호화 된 값을 복호화 하기
        enc = AESCipher(key).encrypt(message)
        print(message, len(message))
        print(enc, len(enc))

