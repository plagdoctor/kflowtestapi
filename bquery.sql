        SELECT A.SALEDATE,
              A.STORECD, 
              A.POSNO, 
              A.TRANNO, 
              ISNULL(A.MEMNO,'') AS 'MEMNO',
              A.SALETIME AS 'SALETIME',
              '1' AS 'SALETYPE',
              (CASE WHEN B.PLUGB = '7' THEN 'KB' ELSE 'KH' END) AS 'PLUGB', 
              A.TRANTYPE,
              B.PLUBARCD AS 'PLUBARCD',
              B.JOCD,
              sum(CASE WHEN A.TRANTYPE = '00' THEN B.QTY ELSE -B.QTY END) AS 'QTY', 
              sum(CASE WHEN A.TRANTYPE = '00' THEN (B.DCAMT1+ B.DCAMT2)/B.QTY ELSE -((B.DCAMT1+ B.DCAMT2)/B.QTY) END) AS 'DCAMT', 
              sum(CASE WHEN A.TRANTYPE = '00' THEN B.SALEAMT/B.QTY ELSE -(B.SALEAMT/B.QTY) END) AS 'SALEAMT',
              sum(CASE WHEN A.TRANTYPE = '00' THEN B.DEPVAL1 ELSE -B.DEPVAL1 END) AS 'DEP1',
              sum(CASE WHEN A.TRANTYPE = '00' THEN B.DEPVAL2 ELSE -B.DEPVAL2 END) AS 'DEP2'
        FROM IDPS..TRNSALHDR A, 
              IDPS..TRNSALDTL B
        WHERE A.SALEDATE = B.SALEDATE
          AND A.STORECD  = B.STORECD
          AND A.POSNO    = B.POSNO
          AND A.TRANNO   = B.TRANNO
          AND A.TRANTYPE IN ('00','01')
          AND B.CANCELGB ='0'  
          AND A.SALEDATE =  '20221201'
          AND A.SALETIME >= '100000'
          AND A.SALETIME <= '110000'
          AND A.STORECD = '001'
          AND (A.SALEAMTM <> 0 OR A.STORECD = '013' OR A.STORECD ='023')
GROUP BY A.SALEDATE,
              A.STORECD, 
              A.POSNO, 
              A.TRANNO, 
              ISNULL(A.MEMNO,''),
              A.SALETIME ,        
              B.PLUGB, 
              A.TRANTYPE ,
              B.PLUBARCD ,
              B.JOCD
        UNION ALL  
        SELECT A.SALEDATE,
              A.STORECD, 
              A.POSNO, 
              A.TRANNO, 
              ISNULL(A.MEMNO,'') AS 'MEMNO',
              A.SALETIME AS 'SALETIME',
              '2' AS 'SALETYPE',
              (CASE WHEN B.PLUGB = '7' THEN 'KB' ELSE 'KH' END) AS 'PLUGB', 
              A.TRANTYPE AS 'TRANTYPE',
              B.PLUBARCD AS 'PLUBARCD',
              B.JOCD,       
              sum(CASE WHEN A.TRANTYPE = '00' THEN B.QTY ELSE -B.QTY END) AS 'QTY', 
              sum(CASE WHEN A.TRANTYPE = '00' THEN (B.DCAMT1+ B.DCAMT2)/B.QTY ELSE -((B.DCAMT1+ B.DCAMT2)/B.QTY) END) AS 'DCAMT', 
              sum(CASE WHEN A.TRANTYPE = '00' THEN B.SALEAMT/B.QTY ELSE -(B.SALEAMT/B.QTY) END) AS 'SALEAMT',
              sum(CASE WHEN A.TRANTYPE = '00' THEN B.DEPVAL1 ELSE -B.DEPVAL1 END) AS 'DEP1',
              sum(CASE WHEN A.TRANTYPE = '00' THEN B.DEPVAL2 ELSE -B.DEPVAL2 END) AS 'DEP2'
        FROM IDPS..TRNSALHDR A, 
              IDPS..TRNSALDTL B
        WHERE A.SALEDATE = B.SALEDATE
          AND A.STORECD  = B.STORECD
          AND A.POSNO    = B.POSNO
          AND A.TRANNO   = B.TRANNO
          AND A.TRANTYPE IN ('00','01')
          AND B.CANCELGB ='0'
          AND A.SALEDATE =  '20221201'
          AND A.SALETIME >= '100000'
          AND A.SALETIME <= '110000'
          AND A.STORECD = '001'
          AND A.STORECD  NOT in ('013', '023')
          AND (A.SALEAMTH <> 0 OR A.STORECD = '064')
GROUP BY A.SALEDATE,
              A.STORECD, 
              A.POSNO, 
              A.TRANNO, 
              ISNULL(A.MEMNO,''),
              A.SALETIME ,        
              B.PLUGB, 
              A.TRANTYPE ,
              B.PLUBARCD ,
              B.JOCD
        ORDER BY 1,2,3,4
        AT ISOLATION 0                            

