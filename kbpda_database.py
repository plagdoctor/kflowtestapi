# -*- coding: utf-8 -*-

"""
*************************************************************************
  1. Title   : DataBase connection for KB Store PDA Server Daemon
  2. Author  : hElLoOnG
  3. Date    : 2021.02.26
  4. ETC     :
  5. Build   :
  6. History
    - 2021.02.26, helloong, First Created
*************************************************************************
"""
import kbpda_config

import sqlalchemy
from sqlalchemy import event
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session

KFLOW = kbpda_config.KFLOW
# TPSYB2 = kbpda_config.TPSYB2
TIMEOUT = kbpda_config.TIMEOUT

KFLOW_ENGINE = sqlalchemy.create_engine(KFLOW, encoding='utf8', echo=True, pool_size=20, max_overflow=50, connect_args={'CommandTimeOut': TIMEOUT})
# TP2_ENGINE = sqlalchemy.create_engine(TPSYB2, encoding='utf8', echo=True, pool_size=20, max_overflow=50, connect_args={'CommandTimeOut': TIMEOUT})

KFLOW_SESSION = scoped_session(sessionmaker(bind=KFLOW_ENGINE, autocommit=False))
# TPS2_SESSION = scoped_session(sessionmaker(bind=TP2_ENGINE, autocommit=False))


def get_db_session(server='KFLOW'):
    if server.upper() == 'KFLOW':
        temp_db_engine = sqlalchemy.create_engine(KFLOW, encoding='utf8', echo=True, pool_size=20, max_overflow=50,
                                                connect_args={'CommandTimeOut': TIMEOUT})
        temp_session = sessionmaker(bind=temp_db_engine, autocommit=False)
        return temp_db_engine, temp_session
    else:
        # temp_db_engine = sqlalchemy.create_engine(TPSYB2, encoding='utf8', echo=True, pool_size=20, max_overflow=50,
        #                                       connect_args={'CommandTimeOut': TIMEOUT})

        temp_db_engine = sqlalchemy.create_engine(KFLOW, encoding='utf8', echo=True, pool_size=20, max_overflow=50,
                                                connect_args={'CommandTimeOut': TIMEOUT})
        temp_session = sessionmaker(bind=temp_db_engine, autocommit=False)
        return temp_db_engine, temp_session
