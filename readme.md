## 1. Python 설치

> 💡 Python 3.9.2 버전 , pip 20.2.3 버전에서 설치 권장

``` bash
# git clone
git clone https://plagdoctor@bitbucket.org/plagdoctor/kflowtestapi.git
```

- git clone 
- .env 파일 공유 요청

---

## 2. ODBC 설정
> 💡 kflow_dev 기준

![odbc setting](https://www.notion.so/image/https%3A%2F%2Fs3-us-west-2.amazonaws.com%2Fsecure.notion-static.com%2F9663fc0d-9710-4acf-9cca-46d22f553b9c%2FUntitled.png?table=block&id=6e1f91af-1c99-4885-b90e-b499287b9a52&spaceId=07268960-0cc1-40f1-8528-112a589a48d8&width=2000&userId=4449deea-1ec5-4db8-a654-18707cae51e4&cache=v2)


---

## 3. python3 venv 생성

``` bash
# 가상환경 생성
python -m venv venv

# venv 실행 
cd venv
cd Scripts

#activate는 cmd 면 bat 파워쉘은 ps1 나는 파워쉘 
activate.bat , activate.ps1

# 가상환경 실행여부 확인(venv) 프롬프트 앞에 가상환경명 확인

# 원래 최초 설치 폴더 복귀
cd ..
cd ..

# 구성요소 설치
pip install -r req.txt

# 인스톨에 문제 있으면 resolver 옵션 추가
pip install -r req.txt --use-feature=2020-resolver

# 인스톨한 패키지 req.txt 파일로 생성
pip freeze > req.txt
```

---


## 4. 기동 

``` bash
# 7070 포트 
uvicorn kbpda_main:APP --host 0.0.0.0 --port 7070

```

## 끝.
