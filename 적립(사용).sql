

      SELECT A.SALEDATE AS 'PROCDATE',
            A.STORECD  AS 'STORECD',
            (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0') AS 'STORENM',
          (CASE WHEN SUBSTRING(A.POSNO, 3, 2) <= '50' THEN '교보문고POS' ELSE '핫트랙스POS' END)     AS 'POSTYPE',
          '교보문고상품' AS 'PLUTYPE',
          (SELECT ETC1NM FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0')   AS 'ETC1NM',
          '마일리지' AS 'MILEAGETYPE',
          (CASE WHEN B.TRANTYPE = '00' THEN '사용' ELSE '사용취소' END) AS 'TRANTYPE',
          SUM((CASE WHEN B.TRANTYPE = '01' THEN A.SALEAMT ELSE A.SALEAMT * -1 END)) AS 'APPAMT',
          0 AS 'DEP1',
          0 AS 'DEP2',
          0 AS 'DEP3',
          '사용/사용취소' AS 'SAVETYPE'
        FROM IDPS..TRNSALPAY A, IDPS..TRNSALHDR B
      WHERE A.SALEDATE    = B.SALEDATE
        AND A.STORECD     = B.STORECD
        AND A.POSNO       = B.POSNO
        AND A.TRANNO      = B.TRANNO
        AND A.SALEDATE   >= '{fromDate}'
        AND A.SALEDATE   <= '{toDate}'
      --   AND A.STORECD     = '033'
        AND A.STORECD    NOT IN ('013', '023', '063') --부천 수유 제외 MOD 20211014 LJY, 건대 추가 
        AND A.TENDERTYPE  = '04'
        AND A.SALEAMTM   <> 0
        AND B.TRANTYPE   IN ('00', '01')
      GROUP BY A.SALEDATE, A.STORECD
            ,(CASE WHEN SUBSTRING(A.POSNO, 3, 2) <= '50' THEN '교보문고POS' ELSE '핫트랙스POS' END)
            ,(CASE WHEN B.TRANTYPE = '00' THEN '사용' ELSE '사용취소' END)
      UNION ALL
      SELECT A.SALEDATE AS 'PROCDATE',
            A.STORECD  AS 'STORECD',
            (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0') AS 'STORENM',
          (CASE WHEN SUBSTRING(A.POSNO, 3, 2) <= '50' THEN '교보문고POS' ELSE '핫트랙스POS' END) AS 'POSTYPE',
          '핫트랙스상품' AS 'PLUTYPE',
          (SELECT ETC1NM FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0') AS 'ETC1NM',
          '마일리지' AS 'MILEAGETYPE',
          (CASE WHEN B.TRANTYPE = '00' THEN '사용' ELSE '사용취소' END) AS 'TRANTYPE',
          SUM((CASE WHEN B.TRANTYPE = '01' THEN A.SALEAMT ELSE A.SALEAMT * -1 END)) AS 'APPAMT',
          0 AS 'DEP1',
          0 AS 'DEP2',
          0 AS 'DEP3',
          '사용/사용취소' AS 'SAVETYPE'
        FROM IDPS..TRNSALPAY A, IDPS..TRNSALHDR B
      WHERE A.SALEDATE    = B.SALEDATE
        AND A.STORECD     = B.STORECD
        AND A.POSNO       = B.POSNO
        AND A.TRANNO      = B.TRANNO
        AND A.SALEDATE   >= '{fromDate}'
        AND A.SALEDATE   <= '{toDate}'
      --   AND A.STORECD     = '033'
        AND A.STORECD    NOT IN ('013', '023', '063') --부천 수유 제외  MOD 20211014 LJY, 건대 추가 
        AND A.TENDERTYPE  = '04'
        AND A.SALEAMTH   <> 0
        AND B.TRANTYPE   IN ('00', '01')
      GROUP BY A.SALEDATE, A.STORECD
            ,(CASE WHEN SUBSTRING(A.POSNO, 3, 2) <= '50' THEN '교보문고POS' ELSE '핫트랙스POS' END)
            ,(CASE WHEN B.TRANTYPE = '00' THEN '사용' ELSE '사용취소' END)
      UNION ALL
      SELECT A.SALEDATE AS 'PROCDATE',
            A.STORECD  AS 'STORECD',
            (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0') AS 'STORENM',
          '교보문고POS'  AS 'POSTYPE',
          '교보문고상품' AS 'PLUTYPE',
          (SELECT ETC1NM FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0') AS 'ETC1NM',
          '마일리지' AS 'MILEAGETYPE',
          (CASE WHEN B.TRANTYPE = '00' THEN '사용' ELSE '사용취소' END) AS 'TRANTYPE',
          SUM((CASE WHEN B.TRANTYPE = '01' THEN A.SALEAMT ELSE A.SALEAMT * -1 END)) AS 'APPAMT',
          0 AS 'DEP1',
          0 AS 'DEP2',
          0 AS 'DEP3',
          '사용/사용취소' AS 'SAVETYPE'
        FROM IDPS..TRNSALPAY A, IDPS..TRNSALHDR B
      WHERE A.SALEDATE    = B.SALEDATE
        AND A.STORECD     = B.STORECD
        AND A.POSNO       = B.POSNO
        AND A.TRANNO      = B.TRANNO
        AND A.SALEDATE   >= '{fromDate}'
        AND A.SALEDATE   <= '{toDate}'
        AND A.STORECD     IN ('013', '023') -- 부천만   MOD 20211014 LJY, 건대 추가 
        AND A.TENDERTYPE  = '04'
        AND B.TRANTYPE   IN ('00', '01')
      GROUP BY A.SALEDATE, A.STORECD
            ,(CASE WHEN B.TRANTYPE = '00' THEN '사용' ELSE '사용취소' END)
      UNION ALL
      SELECT A.SALEDATE AS 'PROCDATE',
            A.STORECD  AS 'STORECD',
            (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0') AS 'STORENM',
          '핫트랙스POS'  AS 'POSTYPE',
          '핫트랙스상품' AS 'PLUTYPE',
          (SELECT ETC1NM FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0') AS 'ETC1NM',
          '마일리지' AS 'MILEAGETYPE',
          (CASE WHEN B.TRANTYPE = '00' THEN '사용' ELSE '사용취소' END) AS 'TRANTYPE',
          SUM((CASE WHEN B.TRANTYPE = '01' THEN A.SALEAMT ELSE A.SALEAMT * -1 END)) AS 'APPAMT',
          0 AS 'DEP1',
          0 AS 'DEP2',
          0 AS 'DEP3',
          '사용/사용취소' AS 'SAVETYPE'
        FROM IDPS..TRNSALPAY A, IDPS..TRNSALHDR B
      WHERE A.SALEDATE    = B.SALEDATE
        AND A.STORECD     = B.STORECD
        AND A.POSNO       = B.POSNO
        AND A.TRANNO      = B.TRANNO
        AND A.SALEDATE   >= '{fromDate}'
        AND A.SALEDATE   <= '{toDate}'
        AND A.STORECD     = '063' -- 수유만
        AND A.TENDERTYPE  = '04'
        AND B.TRANTYPE   IN ('00', '01')
      GROUP BY A.SALEDATE, A.STORECD
            ,(CASE WHEN B.TRANTYPE = '00' THEN '사용' ELSE '사용취소' END)
      UNION ALL 		  
      --통합포인트 사용/사용취소
      SELECT A.SALEDATE AS 'PROCDATE',
            A.STORECD  AS 'STORECD',
            (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0') AS 'STORENM',
          (CASE WHEN SUBSTRING(A.POSNO, 3, 2) <= '50' THEN '교보문고POS' ELSE '핫트랙스POS' END) AS 'POSTYPE',
          '교보문고상품' AS 'PLUTYPE',
          (SELECT ETC1NM FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0') AS 'ETC1NM',
          '통합포인트' AS 'MILEAGETYPE',
          (CASE WHEN B.TRANTYPE = '00' THEN '사용' ELSE '사용취소' END) AS 'TRANTYPE',
          SUM((CASE WHEN B.TRANTYPE = '01' THEN A.SALEAMT ELSE A.SALEAMT * -1 END)) AS 'APPAMT',
          0 AS 'DEP1',
          0 AS 'DEP2',
          0 AS 'DEP3',
          '사용/사용취소' AS 'SAVETYPE'
        FROM IDPS..TRNSALPAY A, IDPS..TRNSALHDR B
      WHERE A.SALEDATE    = B.SALEDATE
        AND A.STORECD     = B.STORECD
        AND A.POSNO       = B.POSNO
        AND A.TRANNO      = B.TRANNO
        AND A.SALEDATE   >= '{fromDate}'
        AND A.SALEDATE   <= '{toDate}'
      --   AND A.STORECD     = '033'
        AND A.STORECD    NOT IN ('013', '023', '063') -- MOD 20211014 LJY, 건대 추가 
        AND A.TENDERTYPE  = '29'
        AND A.SALEAMTM   <> 0
        AND B.TRANTYPE   IN ('00', '01')
      GROUP BY A.SALEDATE, A.STORECD
            ,(CASE WHEN SUBSTRING(A.POSNO, 3, 2) <= '50' THEN '교보문고POS' ELSE '핫트랙스POS' END)
            ,(CASE WHEN B.TRANTYPE = '00' THEN '사용' ELSE '사용취소' END)
      UNION ALL
      SELECT A.SALEDATE AS 'PROCDATE',
            A.STORECD  AS 'STORECD',
            (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0') AS 'STORENM',
          (CASE WHEN SUBSTRING(A.POSNO, 3, 2) <= '50' THEN '교보문고POS' ELSE '핫트랙스POS' END) AS 'POSTYPE',
          '핫트랙스상품' AS 'PLUTYPE',
          (SELECT ETC1NM FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0') AS 'ETC1NM',
          '통합포인트' AS 'MILEAGETYPE',
          (CASE WHEN B.TRANTYPE = '00' THEN '사용' ELSE '사용취소' END) AS 'TRANTYPE',
          SUM((CASE WHEN B.TRANTYPE = '01' THEN A.SALEAMT ELSE A.SALEAMT * -1 END)) AS 'APPAMT',
          0 AS 'DEP1',
          0 AS 'DEP2',
          0 AS 'DEP3',
          '사용/사용취소' AS 'SAVETYPE'
        FROM IDPS..TRNSALPAY A, IDPS..TRNSALHDR B
      WHERE A.SALEDATE    = B.SALEDATE
        AND A.STORECD     = B.STORECD
        AND A.POSNO       = B.POSNO
        AND A.TRANNO      = B.TRANNO
        AND A.SALEDATE   >= '{fromDate}'
        AND A.SALEDATE   <= '{toDate}'
      --   AND A.STORECD     = '033'
        AND A.STORECD    NOT IN ('013', '023', '063')
        AND A.TENDERTYPE  = '29'
        AND A.SALEAMTH   <> 0
        AND B.TRANTYPE   IN ('00', '01')
      GROUP BY A.SALEDATE, A.STORECD
            ,(CASE WHEN SUBSTRING(A.POSNO, 3, 2) <= '50' THEN '교보문고POS' ELSE '핫트랙스POS' END)
            ,(CASE WHEN B.TRANTYPE = '00' THEN '사용' ELSE '사용취소' END)
      UNION ALL
      SELECT A.SALEDATE AS 'PROCDATE',
            A.STORECD  AS 'STORECD',
            (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0') AS 'STORENM',
          '교보문고POS'  AS 'POSTYPE',
          '교보문고상품' AS 'PLUTYPE',
          (SELECT ETC1NM FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0') AS 'ETC1NM',
          '통합포인트' AS 'MILEAGETYPE',
          (CASE WHEN B.TRANTYPE = '00' THEN '사용' ELSE '사용취소' END) AS 'TRANTYPE',
          SUM((CASE WHEN B.TRANTYPE = '01' THEN A.SALEAMT ELSE A.SALEAMT * -1 END)) AS 'APPAMT',
          0 AS 'DEP1',
          0 AS 'DEP2',
          0 AS 'DEP3',
          '사용/사용취소' AS 'SAVETYPE'
        FROM IDPS..TRNSALPAY A, IDPS..TRNSALHDR B
      WHERE A.SALEDATE    = B.SALEDATE
        AND A.STORECD     = B.STORECD
        AND A.POSNO       = B.POSNO
        AND A.TRANNO      = B.TRANNO
        AND A.SALEDATE   >= '{fromDate}'
        AND A.SALEDATE   <= '{toDate}'
      --   AND A.STORECD     = '033'
        AND A.STORECD     IN ('013', '023')
        AND A.TENDERTYPE  = '29'
        AND B.TRANTYPE   IN ('00', '01')
      GROUP BY A.SALEDATE, A.STORECD
            ,(CASE WHEN B.TRANTYPE = '00' THEN '사용' ELSE '사용취소' END)
      UNION ALL
      SELECT A.SALEDATE AS 'PROCDATE',
            A.STORECD  AS 'STORECD',
            (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0') AS 'STORENM',
          '핫트랙스POS'  AS 'POSTYPE',
          '핫트랙스상품' AS 'PLUTYPE',
          (SELECT ETC1NM FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C22' AND CODE = A.STORECD AND DELYN = '0') AS 'ETC1NM',
          '통합포인트' AS 'MILEAGETYPE',
          (CASE WHEN B.TRANTYPE = '00' THEN '사용' ELSE '사용취소' END) AS 'TRANTYPE',
          SUM((CASE WHEN B.TRANTYPE = '01' THEN A.SALEAMT ELSE A.SALEAMT * -1 END)) AS 'APPAMT',
          0 AS 'DEP1',
          0 AS 'DEP2',
          0 AS 'DEP3',
          '사용/사용취소' AS 'SAVETYPE'
        FROM IDPS..TRNSALPAY A, IDPS..TRNSALHDR B
      WHERE A.SALEDATE    = B.SALEDATE
        AND A.STORECD     = B.STORECD
        AND A.POSNO       = B.POSNO
        AND A.TRANNO      = B.TRANNO
        AND A.SALEDATE   >= '{fromDate}'
        AND A.SALEDATE   <= '{toDate}'
      --   AND A.STORECD     = '033'
        AND A.STORECD     = '063'
        AND A.TENDERTYPE  = '29'
        AND B.TRANTYPE   IN ('00', '01')
      GROUP BY A.SALEDATE, A.STORECD
            ,(CASE WHEN B.TRANTYPE = '00' THEN '사용' ELSE '사용취소' END)
      ORDER BY 1,2,4,8,5
      AT ISOLATION 0