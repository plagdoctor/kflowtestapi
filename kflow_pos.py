# -*- coding: utf-8 -*-

"""
*************************************************************************
  1. Title   : KB Store PDA Server Daemon
  2. Author  : hElLoOnG
  3. Date    : 2021.02.26
  4. ETC     :
  5. Build   : pyinstaller -c -F --uac-admin -i=kbpda.ico kflow_pos.py
  6. Run     : uvicorn kflow_pos:APP --host 0.0.0.0 --port 7070
  7. History
    - 2021.02.26, helloong, First Created
*************************************************************************
"""
import json
from xmljson import parker, Parker
from xml.etree.ElementTree import fromstring
import requests
from typing import Optional
from fastapi import FastAPI, Form, Header, Request, Depends, HTTPException
from fastapi.responses import HTMLResponse
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
from typing import List  

from sqlalchemy.orm import Session

import telebot
from telebot import util

import simplejson

import kbpda_config
import kbpda_logger
from kbpda_query_map import QUERY_MAPS
from kbpda_database import KFLOW_SESSION, get_db_session
from kbpda_cipher import AESCipher
from kbpda_util import get_cur_date_time

tags_metadata = [
    {
        "name": "KFLOW_API",
        "description": "현재 라이브 서버입니다.",
        "externalDocs": {
            "description": "접속 url : ",
            "url": "http://kbbs.kyobobook.co.kr:7070/docs",
            },
    },

    {    "name": "IDPS",
         "description": "오프라인 매출관련 API",
    },
    {    "name": "CTI",
         "description": "CTI 레거시 API(현재 캠페인쪽만 있음)",
    },    
    {    "name": "LotteJamsil",
         "description": "오프라인 잠실점 수수료 매출관련 API",
    },    
    {    "name": "NewLotte",
         "description": "잠실점 제외 롯데 수수료 매출관련 API",
    },      
    {    "name": "Hyundai",
         "description": "현대백화점 매출관련 API",
    },    
    {    "name": "Merge",
         "description": "법인 합병관련 교차매출 확인 자료 API",
    },    
    {    "name": "Invn",
         "description": "재고조사 API",
    },              
    {    "name": "Subscription",
         "description": "구독 API"
    }
]

origins = ["*"]

 

# GLOBAL VARIABLES: USE CAPS IF POSSIBLE #
APP = FastAPI(openapi_tags=tags_metadata)
# cors 설정을 위한 미들웨어
APP.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

BOT = telebot.TeleBot(kbpda_config.TOKEN)
CHAT_ID_LIST = kbpda_config.CHAT_ID_LIST
KFLOW_URL = kbpda_config.KFLOW_URL
LOGGER = kbpda_logger.MyLogger('kflow_pos', 'kflow_pos').make_logger('debug')
SERVICE_NAME = kbpda_config.SERVICE_NAME
CIPHER_KEY = kbpda_config.CIPHER_KEY
TIMEOUT = kbpda_config.TIMEOUT

# CODE AND FUNCTION #
def exec_db(db: Session = KFLOW_SESSION(), query_map_name=None, cond=dict()):
    """
    Add, 2020.05.12, 심윤보, DB 쿼리를 실행하고 그 결과를 이쁘게 알려준다.
    :param db: DB세션
    :param query_map_name: 쿼리스트링
    :param cond:
    :return: 리스트형 처리결과 + 쿼리
    """
    query_str = QUERY_MAPS[query_map_name].format(**cond)
    result = db.execute(query_str)
    result_set = []
    for row in result:
        result_set.append(dict(zip(result.keys(), row)))

    return result_set, query_str


def write_db(db: Session = KFLOW_SESSION(), write_query_map_name=None, check_query_map_name=None, cond=dict()):
    query_str = QUERY_MAPS[write_query_map_name].format(**cond)
    db.execute(query_str)
    result_set = exec_db(db, check_query_map_name, cond)
    return result_set[0], query_str


def sendmsg(chat_id, msg, file_name=None):
    """
    Add, 2020.05.12, 심윤보, 텔레그램 메세지를 이쁘게 보내준다.
    :param chat_id: 리스트형 채팅 아이디
    :param msg: 보낼메세지
    :param query_str: 쿼리가 있다면 쿼리[파일로 전송]
    :return: 그른거 음따.
    """
    try:
        if type(msg) == dict:
            msg_str = simplejson.dumps(msg, sort_keys=True, indent=4, ensure_ascii=False)
        elif type(msg) == str:
            msg_str = msg

        if len(msg_str) > 3000:
            spt_msg_str = util.split_string(msg_str, 3000)
        else:
            spt_msg_str = msg_str

        if type(spt_msg_str) != list:
            spt_msg_str = [spt_msg_str, ]

        for id in chat_id:
            for msg_item in spt_msg_str:
                BOT.send_message(id, msg_item)
                if file_name:
                    with open(file_name, 'rb') as fr:
                        doc = fr
                        BOT.send_document(id, doc)

    except Exception as e:
        LOGGER.exception(f'메세지 발송 에러: \nchat_id: {chat_id}\nmsg: {msg}\nerrmsg: {e}')
        pass


def make_ret_value(ret_code, ret_msg, err_code, ret_data_count, ret_data, sess_list, query_str='', ip_adrs=''):
    """
    Add, 2020.05.12, 심윤보, 응답 와꾸에 맞게 데이터를 이쁘게 배열한 뒤 제이슨 형태로 만들어가지구능 더 이쁘게 리턴한다.
    :param ret_code: 응답코드
    :param ret_msg: 응답메세지
    :param err_code: 에러코드
    :param ret_data_count: ret_data의 길이
    :param ret_data: 응답데이터
    :param sess_list: 세션 리스트
    :param query_str: 쿼리스트링
    :param ip_adrs: 요청아이피
    :return:
    """
    result_set = dict()
    result_set['retCode'] = ret_code
    result_set['retMsg'] = ret_msg
    result_set['errCode'] = err_code
    result_set['retDataCount'] = ret_data_count
    if type(ret_data) != list:
        ret_data = [ret_data]
    result_set['retData'] = ret_data
    result_set['reqIpAdrs'] = ip_adrs

    if ret_code != 0:
        # 01. 에러 메세지 전송
        # sendmsg(CHAT_ID_LIST, result_set)
        LOGGER.error(f'{ret_msg}')
        LOGGER.error(f'[QUERY]: {query_str}')

        # 02. 세션 롤백
        for sess_item in sess_list:
            LOGGER.error('세션 롤백 후 종료')
            sess_item.rollback()
            sess_item.close()
    else:
        LOGGER.debug('전체 세션 커밋 후 종료')
        for sess_item in sess_list:
            sess_item.commit()
            sess_item.close()

    return result_set


def make_ret_value(ret_code, ret_msg, err_code, ret_data_count, ret_data, sess_list, query_str='', ip_adrs=''):
    """
    Add, 2020.05.12, 심윤보, 응답 와꾸에 맞게 데이터를 이쁘게 배열한 뒤 제이슨 형태로 만들어가지구능 더 이쁘게 리턴한다.
    :param ret_code: 응답코드
    :param ret_msg: 응답메세지
    :param err_code: 에러코드
    :param ret_data_count: ret_data의 길이
    :param ret_data: 응답데이터
    :param sess_list: 세션 리스트
    :param query_str: 쿼리스트링
    :param ip_adrs: 요청아이피
    :return:
    """
    result_set = dict()
    result_set['retCode'] = ret_code
    result_set['retMsg'] = ret_msg
    result_set['errCode'] = err_code
    result_set['retDataCount'] = ret_data_count
    if type(ret_data) != list:
        ret_data = [ret_data]
    result_set['retData'] = ret_data
    result_set['reqIpAdrs'] = ip_adrs

    if ret_code != 0:
        # 01. 에러 메세지 전송
        # sendmsg(CHAT_ID_LIST, result_set)
        LOGGER.error(f'{ret_msg}')
        LOGGER.error(f'[QUERY]: {query_str}')

        # 02. 세션 롤백
        for sess_item in sess_list:
            LOGGER.error('세션 롤백 후 종료')
            sess_item.rollback()
            sess_item.close()
    else:
        LOGGER.debug('전체 세션 커밋 후 종료')
        for sess_item in sess_list:
            sess_item.commit()
            sess_item.close()

    return result_set    


def get_kflow_seqnum(tableName='', columnName='', today='', crtrId='kb_pda', seqLen=5):
    cond = {
        'tableName': tableName,
        'columnName': columnName,
        'today': today,
        'crtrId': crtrId,
    }

    temp_kflow_engine, temp_kflow_session = get_db_session('KFLOW')
    temp_session_kflow_w = temp_kflow_session()

    try:
        # 01. 채번 테이블 데이터 유무 검사
        result_set, query_str = exec_db(temp_session_kflow_w, 'checkExistsTodaySeqNum', cond)

        if len(result_set) <= 0:
            # '[{tableName}, {columnName}, {today}]조건으로 채번에 실패하였습니다.'
            LOGGER.error(f"[{cond['tableName']}, {cond['columnName']}, {cond['today']}]조건으로 채번에 실패 하였습니다.")
            return ''

        existCnt = result_set[0]['cnt']
        # 02. 없으면 인서트
        LOGGER.debug(f"result_set[0]['cnt']: {result_set[0]['cnt']}")
        if existCnt == 0:
            result_set, query_str = exec_db(temp_session_kflow_w, 'registerSeqNumToKflow', cond)

            if len(result_set) <= 0:
                # '[{table_name}, {column_name}, {datetime.now()}]조건으로 채번에 실패하였습니다.'
                LOGGER.error(f"[{cond['tableName']}, {cond['columnName']}, {cond['today']}]조건으로 채번 내역 생성에 실패 하였습니다.")
                return ''

        # 03. 채번 테이블 업데이트 후 채번
        result_set, query_str = exec_db(temp_session_kflow_w, 'getSeqNumFromKflow', cond)
        LOGGER.debug(f'{str(result_set)}')
        if len(result_set) <= 0:
            LOGGER.error(f"[{cond['tableName']}, {cond['columnName']}, {cond['today']}]조건으로 채번 내역 업데이트에 실패 하였습니다.")
            return ''

        for temp_item in result_set:
            if type(seqLen) != int:
                seqLen = int(seqLen)
            temp_item['seqNum'] = str(temp_item['seqNum']).zfill(seqLen)

        LOGGER.debug(f"result_set: {result_set}")
        return result_set[0]['seqNum']
    except Exception as e:
        LOGGER.exception('채번도중 예외발생!!')
    finally:
        LOGGER.exception('무조건 커밋!!')
        temp_session_kflow_w.commit()
        try:
            temp_session_kflow_w.close()
        except Exception as e1:
            LOGGER.exception('temp_session_kflow_w.close() 도중 예외발생')
        try:
            temp_kflow_engine.dispose()
        except Exception as e2:
            LOGGER.exception('temp_kflow_engine.dispose() 도중 예외발생')


def get_box_num():
    temp_kflow_engine, temp_kflow_session = get_db_session('KFLOW')
    temp_session_kflow_w = temp_kflow_session()

    cond = {
        'startBoxNum': '70000001',
        'endBoxNum': '79999999',
        'boxDvsnCode': '2'
    }

    try:
        # 01. 박스번호 채번
        LOGGER.debug(f"cond: {cond}")
        box_num_info, query_str = exec_db(temp_session_kflow_w, 'getBoxNum', cond)
        LOGGER.debug(f"box_num_info: {box_num_info}")
        if len(box_num_info) <= 0:
            LOGGER.error(f"[{box_num_info}]조건으로 채번에 실패 하였습니다.")
            return ''

        return box_num_info[0]['boxNum']
    except Exception as e:
        LOGGER.exception('채번도중 예외발생!!')
    finally:
        LOGGER.exception('무조건 커밋!!')
        temp_session_kflow_w.commit()
        try:
            temp_session_kflow_w.close()
        except Exception as e1:
            LOGGER.exception('temp_session_kflow_w.close() 도중 예외발생')
        try:
            temp_kflow_engine.dispose()
        except Exception as e2:
            LOGGER.exception('temp_kflow_engine.dispose() 도중 예외발생')

def isCondContainProperly(cond=''):
    get_cond = cond

    try:
        get_cond
    except Exception as e:
        LOGGER.exception('get_cond 확인중 예외발생!!')
    finally:
        LOGGER.exception('get_cond finally!!')




@APP.get(f"/{SERVICE_NAME}/ping/")
def read_root():
    return {"Hello": "World"}


@APP.get(f"/{SERVICE_NAME}/getstorecd/")
def getstorecd():
    
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]


    try:
      
        result_set, query_str = exec_db(kflow_sess, 'getstorecd')
        LOGGER.debug(f"result_set: {result_set}")
        # LOGGER.debug(f"query_str: {query_str}")
        
        if not result_set:
            return make_ret_value(-1, f"STORECD 내역이 존재하지 않습니다.", 100, len(result_set), result_set,
                                  session_list, query_str)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, session_list, query_str)


@APP.get(f"/{SERVICE_NAME}/getstorecdinclall/")
def getstorecdinclall():
    
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]


    try:
      
        result_set, query_str = exec_db(kflow_sess, 'getstorecdinclall')
        LOGGER.debug(f"result_set: {result_set}")
        # LOGGER.debug(f"query_str: {query_str}")
        
        if not result_set:
            return make_ret_value(-1, f"STORECD 내역이 존재하지 않습니다.", 100, len(result_set), result_set,
                                  session_list, query_str)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, session_list, query_str)


@APP.get(f"/{SERVICE_NAME}/getposnofromstorecd/{{storecd}}")
def getposnofromstorecd(r: Request,                 
                         storecd: str,                
                         ):    
    cond = {
    'storecd': storecd,
    }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
      
        result_set, query_str = exec_db(kflow_sess, 'getposnofromstorecd',cond)
        LOGGER.debug(f"result_set: {result_set}")
        # LOGGER.debug(f"query_str: {query_str}")
        
        if not result_set:
            return make_ret_value(-1, f"STORECD 내역이 존재하지 않습니다.", 100, len(result_set), result_set,
                                  session_list, query_str)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, session_list, query_str)


@APP.get(f"/{SERVICE_NAME}/getsubscriptionorder/{{storeCd}}/{{resNo}}", tags=["Subscription"])
def getsubscriptionorder(r: Request,                 
                         storeCd: str,           
                         resNo: str,     
                         ):    
    cond = {
    'storeCd': storeCd,
    'resNo': resNo,
    }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
      
        result_set, query_str = exec_db(kflow_sess, 'getsubscriptionorder',cond)
        LOGGER.debug(f"result_set: {result_set}")
        # LOGGER.debug(f"query_str: {query_str}")
        
        if not result_set:
            return make_ret_value(-1, f"STORECD 내역이 존재하지 않습니다.", 100, len(result_set), result_set,
                                  session_list, query_str)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, session_list, query_str)
    
@APP.post(f"/{SERVICE_NAME}/subscriptapproved", tags=["Subscription"])
def subscriptapproved(r: Request, resNo: str = Form('resNo'), saleAmt: int = Form('saleAmt')):
    """
    ADD, 20241220, ljy, 구독 결제 완료시 업데이트용
    """
    cond = {
        'resNo': resNo,
        'saleAmt': saleAmt,
    }
    kflow_sess = KFLOW_SESSION()

    session_list = [kflow_sess, ]

    try:
        #구독 결제 업데이트
        result_set, query_str = exec_db(kflow_sess, 'subscriptapproved', cond)

        LOGGER.debug(f"result_set: {result_set}")
        
        if not result_set:
            return make_ret_value(-1, f" update 실패", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)

    except Exception as e:
        err_msg = f"구독 관련 DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, session_list, query_str)

@APP.get(f"/{SERVICE_NAME}/getstocknorolling/{{storecd}}/{{monthdiv}}",  tags=["Invn"])
def getstocknorolling(r: Request,                 
                         storecd: str,                
                         monthdiv: str,  
                         ):    
    cond = {
    'storecd': storecd,
    'monthdiv': monthdiv,
    }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        
        if storecd == '000':    
                
            result_set, query_str = exec_db(kflow_sess, 'getstocknorollingbyall',cond)
            # LOGGER.debug(f"query_str: {query_str}")
        
        else :
            result_set, query_str = exec_db(kflow_sess, 'getstocknorolling',cond)
            # LOGGER.debug(f"query_str: {query_str}")
        
        if not result_set:
            return make_ret_value(-1, f"STORECD 내역이 존재하지 않습니다.", 100, len(result_set), result_set,
                                  session_list, query_str)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, session_list, query_str)

@APP.get(f"/{SERVICE_NAME}/getinfoforinvninvs/{{storecd}}",  tags=["Invn"])
def getinfoforinvninvs(r: Request,                 
                         storecd: str,        
                         ):    
    cond = {
    'storecd': storecd,
    }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        
        result_set, query_str = exec_db(kflow_sess, 'getinfoforinvninvs',cond)
        # LOGGER.debug(f"query_str: {query_str}")
    
        if not result_set:
            return make_ret_value(-1, f"STORECD 내역이 존재하지 않습니다.", 100, len(result_set), result_set,
                                  session_list, query_str)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, session_list, query_str)


@APP.get(f"/{SERVICE_NAME}/getoffsaleamtbymem/{{memno}}")
def getoffsaleamtbymem(r: Request,                 
                         memno: str,                
                         ):    
    """
    ## Add, 20230919 LJY, 오프라인 회원 매출 존재 여부
    ## input:
    ### :param r:
    ### :param **memNo**: `회원번호` (str:11)
        
    ## return: retData[]    
    ### :output **SALEBYMEMNO** :`매출존재유무`
    """     
    cond = {
    'memno': memno,
    }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]


    try:
      
        result_set, query_str = exec_db(kflow_sess, 'getoffsaleamtbymem',cond)
        LOGGER.debug(f"result_set: {result_set}")
        # LOGGER.debug(f"query_str: {query_str}")
        
        if not result_set:
            return make_ret_value(-1, f"memno 내역이 존재하지 않습니다.", 100, len(result_set), result_set,
                                  session_list, query_str)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, session_list, query_str)



@APP.get("/items/{item_id}")
def read_item(item_id: int, q: Optional[str] = None):
    return {"item_id": item_id, "q": q}


@APP.get("/items/hoho/", response_class=HTMLResponse)
async def read_items():
    html_content =  """
    <html>
        <head>
            <title>Some HTML in here</title>
        </head>
        <body>
            <h1>Look ma! HTML!</h1>
        </body>
    </html>
    """
    return HTMLResponse(content=html_content, status_code=200)


@APP.post(f"/{SERVICE_NAME}/getcmdtmain/")
def getCmdtMain(r: Request, rdpCode: str = Form('rdpCode'), cmdtCode: str = Form('cmdtCode')):
    """
    Add, 2021.03.04, 심윤보, 상품안내메인 상품기본정보 조회
    :param r:
    :param rdpCode:
    :param cmdtCode:
    :return:
    """
    cond = {
        'rdpCode': rdpCode,
        'cmdtCode': cmdtCode,
    }

    kflow_sess = KFLOW_SESSION()

    session_list = [kflow_sess, ]


    try:
        if not str.isdigit(cond['rdpCode']):
            return make_ret_value(-1, f"[{rdpCode}] rdpCode 에 숫자외 string은 허용하지 않습니다.", 100, len(cond), cond,
                                    session_list, '', r.client.host)
        if not str.isdigit(cond['cmdtCode']):
            return make_ret_value(-1, f"[{cmdtCode}] cmdtCode 에 숫자외 string은 허용하지 않습니다.", 100, len(cond), cond,
                                    session_list, '', r.client.host)                                    

        result_set, query_str = exec_db(kflow_sess, 'findCmdtMain', cond)
        LOGGER.debug(f"result_set: {result_set}")
        # LOGGER.debug(f"query_str: {query_str}")

        if result_set:
            # 서가정보 조회
            bksh_result_set, query_str = exec_db(kflow_sess, 'getCmdtBksh', cond)
            LOGGER.debug(f"bksh_result_set: {bksh_result_set}")
            result_set[0]['bksh_list'] = bksh_result_set

            # 고객예약 건수 조회
            pren_cnt, query_str = exec_db(kflow_sess, 'getCmdtPrenCnt', cond)
            LOGGER.debug(f"pren_cnt: {pren_cnt}")
            result_set[0]['pren_cnt'] = pren_cnt[0]['cnt']

            # 고객예약 내역 조회
            if pren_cnt[0]['cnt'] > 0:
                pren_list, query_str = exec_db(kflow_sess, 'getCmdtPrenList', cond)
                LOGGER.debug(f"pren_list: {pren_list}")
                result_set[0]['pren_list'] = pren_list
            else:
                result_set[0]['pren_list'] = []

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)


# @APP.get("/ctiapi/getcampaign/{memNo}" , tags=["CTI"])
# def getempsale(r: Request, 
#                 memNo: str,
#                 ):
#     """
#     # Add, 20220727, LJY, 회원 캠페인 조회 getcampaign
#     ## input:
#     ### :param r:
#     ### :param **memNo**: `회원번호` (str:11)
        
#     ## return: retData[]    
#     ### :output **MEMNO** :`회원번호`
#     ### :output **PRCD** :`프로모션코드`
#     ### :output **MEMLEVEL** :`프로모션코드`
#     ### :output **JOINTYPE** :`가입유형`
#     """                
#     cond = {
#         'memNo': memNo,
#         }
#     tp2_sess = TPS2_SESSION()
#     session_list = [tp2_sess, ]

#     try:
#         #오프라인 매출내역 조회
#         result_set, query_str = exec_db(tp2_sess, 'getcampaign', cond)
        
#         LOGGER.debug(f"result_set: {len(result_set)}")
        
#         if not result_set:
#             return make_ret_value(-1, f"[{memNo}] 캠페인 프로모션 내역이 존재하지 않습니다.", 100, len(result_set), result_set,
#                                   session_list, query_str, r.client.host)

#         return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
#     except Exception as e:
#         err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
#         LOGGER.exception(err_msg)
#         return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)


@APP.get( f"/{SERVICE_NAME}"+"/getnexthealthcheck/{serviceId}" , tags=["IDPS"])
def getnexthealthcheck(r: Request, serviceId: str, 
                ):
    """
    # Add, 20220921, LJY, next 상태 조회 getnexthealthcheck
    ## input:
    ### :param r:
    ### :param **serviceId**: `전체:000` (str:3)
        
    ## return: retData[]    
    ### :output **RUNYN** :`처리가능유무`
    """                
    cond = {
        'serviceId': serviceId,
        }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        #오프라인 매출내역 조회
        result_set, query_str = exec_db(kflow_sess, 'getnexthealthcheck', cond)
        
        LOGGER.debug(f"result_set: {len(result_set)}")
        
        if not result_set:
            return make_ret_value(-1, f"[{serviceId}] 상태가 확인불가합니다.", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)


@APP.get(f"/{SERVICE_NAME}"+"/getempsale/{grpType}/{empNo}" , tags=["IDPS"])
def getempsale(r: Request, grpType: str,
                empNo: str,
                ):
    """
    # Add, 20220303, LJY, 직원구매내역 조회 getempsale
    ## input:
    ### :param r:
    ### :param **grpType**: `회사구분 1:교보문고, 2:핫트랙스` (str:1)
    ### :param **empNo**: `사원번호` (str:5)
        
    ## return: retData[]    
    ### :output **EMPNO** :`사번`
    ### :output **EMPNAME** :`사원명`
    ### :output **KBTOTSALEAMT** :`교보문고 구매내역`
    ### :output **KHTOTSALEAMT** :`핫트랙스 구매내역`
    """                
    cond = {
        'grpType': grpType,
        'empNo': empNo,
        }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        #오프라인 매출내역 조회
        result_set, query_str = exec_db(kflow_sess, 'getempsale', cond)
        
        LOGGER.debug(f"result_set: {len(result_set)}")
        
        if not result_set:
            return make_ret_value(0, f"[{empNo}] 사번 직원의 1달간 구매내역이 존재하지 않습니다.", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)


@APP.post(f"/{SERVICE_NAME}/getkbgiftcardpay", tags=["IDPS"])
def getkbgiftcardpay(r: Request,
                saleDate: str = Form('saleDate'),
                startSaleTime: str = Form('startSaleTime'),
                endSaleTime: str = Form('endSaleTime'),
                ):
    """
    # Add, 20220216, LJY, 교보문고기프트카드(드림카드) 사용내역 조회 getkbgiftcardpay
    ## input:
    ### :param r:
    ### :param **saleDate**: `매출일자` (str:8)
    ### :param **startSaleTime**: `판매조회 시작시간` (str:6)
    ### :param **endSaleTime**: `판매조회 종료시간` (str:6)
    
    ## return: retData[]    
    ### :output **SALEDATE** :`매출일자`
    ### :output **STORECD** :`점포수불처코드`
    ### :output **POSNO** :`포스번호`
    ### :output **TRANNO** :`거래번호`
    ### :output **SEQ** :`순번`
    ### :output **SALETIME** :`거래시각`
    ### :output **MEMNO** :`회원번호`
    ### :output **CARDNO** :`기프트카드번호`
    ### :output **SALEAMT** :`결제금액`
    ### :output **TRANTYPE** :`결제승인유형`
    ### :output **AFTSAVEDATE** :`후적립일자`


    """
    cond = {
        'saleDate': saleDate,
        'startSaleTime': startSaleTime,
        'endSaleTime': endSaleTime,
    }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        #기프트카드 매출내역 조회
        result_set, query_str = exec_db(kflow_sess, 'getKbGiftcardPay', cond)

        LOGGER.debug(f"result_set: {result_set}")
        LOGGER.debug(f"{saleDate}")
        LOGGER.debug(f"[saleDate]: {saleDate}")
        
        if not result_set:
            return make_ret_value(-1, f"[{saleDate}] 일자에 매출내역이 없습니다.", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)


@APP.post(f"/{SERVICE_NAME}/getsmartreceipt", tags=["IDPS"])
def getsmartreceipt(r: Request,
                saleDate: str = Form('saleDate'),
                startSaleTime: str = Form('startSaleTime'),
                endSaleTime: str = Form('endSaleTime'),
                memNo: str = Form('memNo'),
                ):
    """
    # Add, 20220216, LJY, 교보문고 오프라인매출 영수증 조회 getsmartreceipt
    ## input:
    ### :param r:
    ### :param **saleDate**: `매출일자` (str:8)
    ### :param **startSaleTime**: `판매조회 시작시간` (str:6)
    ### :param **endSaleTime**: `판매조회 종료시간` (str:6)
    ### :param **memNo**: `회원번호` (str:11)
   
    ## return: retData[]    
    ### :output **SALEDATE**:`매출일자`
    ### :output **STORECD**:`점포수불처코드`
    ### :output **POSNO**:`포스번호`
    ### :output **TRANNO**:`포스거래번호`
    ### :output **COMPANY**:`POS소유구분코드(1:교보문고, 2:핫트랙스)`
    ### :output **MEMNO**:`회원번호`
    ### :output **SALETIME**:`판매시각`
    ### :output **JRDATA**:`POS영수증 내용`
    """
    cond = {
        'saleDate': saleDate,
        'startSaleTime': startSaleTime,
        'endSaleTime': endSaleTime,
        'memNo': memNo,
    }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        #기프트카드 매출내역 조회
        result_set, query_str = exec_db(kflow_sess, 'getSmartReceipt', cond)

        LOGGER.debug(f"result_set: {result_set}")
        LOGGER.debug(f"{saleDate}")
        LOGGER.debug(f"[saleDate]: {saleDate}")
        
        if not result_set:
            return make_ret_value(-1, f"[{saleDate}] 일자에 매출내역이 없습니다.", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)


@APP.post(f"/{SERVICE_NAME}/getofflinesale", tags=["IDPS"])
def getofflinesale(r: Request,
                saleDate: str = Form('saleDate'),
                startSaleTime: str = Form('startSaleTime'),
                endSaleTime: str = Form('endSaleTime'),
                storeCd: str = Form('storeCd'),
                ):
    """
    # Add, 20221027, LJY, 교보문고 오프라인매출 매출자료조회 getofflinesale
    
    ## input:
    ### :param r:
    ### :param **saleDate**: `매출일자` (str:8)
    ### :param **startSaleTime**: `판매조회 시작시간` (str:6)
    ### :param **endSaleTime**: `판매조회 종료시간` (str:6)
    ### :param **storeCd**: `점포코드` (str:3)
   
    ## return: retData[]    
    ### :output **SALEDATE**:`매출일자`
    ### :output **STORECD**:`점포수불처코드`
    ### :output **POSNO**:`포스번호`
    ### :output **TRANNO**:`포스거래번호`
    ### :output **COMPANY**:`POS소유구분코드(1:교보문고, 2:핫트랙스)`
    ### :output **MEMNO**:`회원번호`
    ### :output **SALETIME**:`판매시각`
    ### :output **JRDATA**:`POS영수증 내용`
    """
    cond = {
        'saleDate': saleDate,
        'startSaleTime': startSaleTime,
        'endSaleTime': endSaleTime,
        'storeCd': storeCd,
    }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        #기프트카드 매출내역 조회
        result_set, query_str = exec_db(kflow_sess, 'getofflinesale', cond)

        LOGGER.debug(f"result_set: {result_set}")
        LOGGER.debug(f"{saleDate}")
        LOGGER.debug(f"[saleDate]: {saleDate} , [storeCd]: {storeCd}")
        
        if not result_set:
            return make_ret_value(-1, f"[{saleDate}] 일자에 매출내역이 없습니다.", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)


@APP.post(f"/{SERVICE_NAME}/getempdreamcardno", tags=["IDPS"])
def getempdreamcardno(r: Request,
                comCd: str = Form('comCd'),
                empNo: str = Form('empNo'),
                ):
    """
    # Add, 20220216, LJY, 교보문고 사원드림카드 조회 getempdreamcardno
    # Mod, 20221128, LJY, pin 정보 추가
    ## input:
    ### :param r:
    ### :param **comCd**: `회사구분 1:교보문고, 2:핫트랙스` (str:1)
    ### :param **empNo**: `사원번호 5자리` (str:5)

    ## return: retCode `처리코드`, retMsg `처리결과`, retData[]
    ### :output **GRPNAME**: `회사구분`
    ### :output **EMPNO**: `사원번호`
    ### :output **DRCARDNO**: `카드번호`
    """
    cond = {
        'comCd': comCd,
        'empNo': empNo,
    }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]
    

    try:
        
        # if cond['empno'] == '12975': 
        #     return make_ret_value(-1, f"[ {empno}] 내 사번이네?", 100, len(result_set), result_set, session_list, query_str, r.client.host)
        #기프트카드 매출내역 조회
        result_set, query_str = exec_db(kflow_sess, 'getempdreamcardno', cond)

        LOGGER.debug(f"result_set: {result_set}")
        LOGGER.debug(f"{empNo}")
        LOGGER.debug(f"[empNo]: {empNo}")
        
        if not result_set:
            return make_ret_value(-1, f"[{empNo}] 사번과 일치하는 사번이 존재하지 않습니다.", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)

@APP.post(f"/{SERVICE_NAME}/calcaftersavepoint", tags=["IDPS"])
def calcaftersavepoint(r: Request,
                memNo: str      = Form('memNo'),
                inputType: str  = Form('inputType'),
                receiptNo: str  = Form('receiptNo'),
                saleAmt: int    = Form('saleAmt'),  
                kbGrade: str    = Form('kbGrade'),
                khGrade: str    = Form('khGrade'),
                joinPath: str   = Form('joinPath'),
                additionalSaveRate: int   = Form('additionalSaveRate'),
                additionalDcRate: int     = Form('additionalDcRate'),
                exeId:str       = Form('exeId'),
                ):
    """
    # Add, 20220217, LJY, 오프라인 결제 분 후적립 금액 처리 calcaftersavepoint
    ## input:
    ### :param r:
    ### :param **memNo**: `회원번호` (str:11)
    ### :param **inputType**: `후적립 입력유형` (str:3)
    ### :param **receiptNo**: `영수증 바코드번호` (str:20)
    ### :param **saleAmt**: ` 영수증 총 판매금액` (int)
    ### :param **kbGrade**: `교보문고 회원등급` (str:3)
    ### :param **khGrade**: `핫트랙스 회원등급` (str:3)
    ### :param **joinPath**: `회원가입경로` (str:3)
    ### :param **additionalSaveRate**: `추가혜택(적립율)` (int)
    ### :param **additionalDcRate**: `추가혜택(할인율)` (int)
    ### :param **exeId**: `요청자` (str:5)

         
    ## return: retData[]    
    ### :output **COMCD**: `회사코드`
    ### :output **STORECD**: `영업점코드`
    ### :output **SALEDATE**: `판매일자`
    ### :output **POSNO**: `POS번호`
    ### :output **TRANNO**: `영수증번호`
    ### :output **SALEAMT**: `총 판매금액`
    ### :output **SAVEPOINTAMT**: `구매적립 금액`
    ### :output **ADDSAVEPOINTAMT**: `추가적립 금액`
    ### :output **RETCD**: `처리코드`
    ### :output **RETMSG**: `처리결과`

    # :Query
        EXEC IDPS..usp_cmn_receipt_point_acml
                    @is_mmbr_nmbr                      -- O `회원번호`                                   memno
                    @is_input_data_type                -- O `후적립 데이터 입력 유형 현재 '101'만 씀       inputtype
                    @is_receipt_barcode                -- O `영수증 바코드번호`                           receiptno
                    @is_receipt_cmpy_dvcd              -- X 영수증 회사코드 (POS 귀속 회사 코드)          
                    @is_receipt_stor_dvcd              -- X 영수증 영업점코드                            
                    @is_receipt_date                   -- X 영수증 판매일자                         
                    @is_receipt_pos_nmbr               -- X 영수증 POS번호                          
                    @il_receipt_nmbr                   -- X 영수증번호                              
                    @il_receipt_sls_amt                -- O `영수증 총 판매금액`                    	  saleamt
                    @is_kybk_grade                     -- O `교보문고 통합등급`                           kbgrade
                    @is_httr_grade                     -- O `핫트랙스 통합등급`                           khgrade
                    @is_join_path                      -- O `회원가입경로`                                joinpath
                    @il_addt_acml_rate                 -- O `추가혜택(적립율) 사실상 0`                    additionalsaverate
                    @il_addt_dscn_rate                 -- O `추가혜택(할인율) 사실상 0`                    additionaldcrate
                    @is_emnm                           -- O `처리자`                                      exeid
			    @os_cmpy_dvcd       OUTPUT ,       -- 회사코드 (적립금을 지급하는 회사 코드)
			    @os_stor_dvcd       OUTPUT ,       -- 영업점코드                            
			    @os_sls_date        OUTPUT ,       -- 판매일자                              
			    @os_pos_nmbr        OUTPUT ,       -- POS번호                               
			    @ol_recp_nmbr       OUTPUT ,       -- 영수증번호                            
			    @ol_sls_amt         OUTPUT ,       -- 총 판매금액                           
			    @ol_bsc_acml_amt    OUTPUT ,       -- 구매적립 금액                         
			    @ol_addt_acml_amt   OUTPUT ,       -- 추가적립 금액                         
			    @ol_result          OUTPUT ,       -- 처리코드                              
			    @os_result_msg      OUTPUT         -- 처리결과   
    
    """
    cond = {
        'memNo': memNo,
        'inputType': inputType,
        'receiptNo': receiptNo,
        'saleAmt': saleAmt,        
        'kbGrade': kbGrade,
        'khGrade': khGrade,
        'joinPath': joinPath,
        'additionalSaveRate': additionalSaveRate,
        'additionalDcRate': additionalDcRate,
        'exeId': exeId,       


    }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        #기프트카드 매출내역 조회
        result_set, query_str = exec_db(kflow_sess, 'calcaftersavepoint', cond)

        LOGGER.debug(f"result_set: {result_set}")
        LOGGER.debug(f"{exeId}")
        LOGGER.debug(f"[exeId]: {exeId}")
        
        if not result_set:
            return make_ret_value(-1, f"[{exeId}] 알 수 없는 이유로 리턴불가 ", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(result_set), result_set, session_list, query_str, r.client.host)


@APP.post(f"/{SERVICE_NAME}/getoffsaldtl", tags=["IDPS"])
def getoffsaldtl(r: Request,
                saleDate: str = Form('saleDate'),
                storeCd: str = Form('storeCd'),
                startSaleTime: str = Form('startSaleTime'),
                endSaleTime: str = Form('endSaleTime'),
                ):
    """
    # Add, 20220217, LJY, 교보문고 오프라인 단건 매출 조회 getoffsaldtl
    ## input:
    ### :param r:
    ### :param **saleDate**: `매출일자` (str:8)
    ### :param **storeCd**: `점포코드` (str:3)
    ### :param **startSaleTime**: `판매조회 시작시간` (str:6)
    ### :param **endSaleTime**: `판매조회 종료시간` (str:6)

    ## return: retData[]
    ### :output **SALEDATE**: `매출일자`
    ### :output **STORECD**: `점포명`
    ### :output **POSNO**: `포스번호`
    ### :output **TRANNO**: `거래번호`
    ### :output **SEQ**: `순번`
    ### :output **MEMNO**: `회원번호`
    ### :output **SALETIME**: `판매시간`
    ### :output **SALETYPE**: `매출구분` (1:교보문고매출, 2:핫트랙스매출)
    ### :output **PLUGB**: `상품구분` (KB:교보문고상품군, KH:핫트랙스상품군)
    ### :output **TRANTYPE**: `판매구분` (00:판매, 01:반품)
    ### :output **PLUBARCD**: `상품코드`
    ### :output **JOCD**: `조코드`
    ### :output **QTY**: `판매수량`
    ### :output **DCAMT**: `할인금액`
    ### :output **SALEAMT**: `판매금액`
    ### :output **DEP1**: `통합포인트적립금액`
    ### :output **DEP2**: `통합포인트추가적립금액`

    """
    cond = {
        'saleDate': saleDate,
        'storeCd': storeCd,
        'startSaleTime': startSaleTime,
        'endSaleTime': endSaleTime,
    }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]
    

    try:
        #오프라인 매출내역 조회
        result_set, query_str = exec_db(kflow_sess, 'getoffsaldtl', cond)
        
        LOGGER.debug(f"result_set: {len(result_set)}")
        
        if not result_set:
            return make_ret_value(-1, f"[{saleDate}] 매출일자에 매출이 존재하지 않습니다.", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)

@APP.get(f"/{SERVICE_NAME}/getstorenm", tags=["IDPS"])
def getstorenm(r: Request, 
                ):
    """
    # Add, 20221205, LJY, 오프라인 점포명 가져오기
    """                
    cond = {
        }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        #오프라인 매출내역 조회
        result_set, query_str = exec_db(kflow_sess, 'getstorenm', cond)
        
        LOGGER.debug(f"result_set: {len(result_set)}")
        
        if not result_set:
            return make_ret_value(-1, f"점포코드가 조회되지 않습니다.", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)


@APP.get(f"/{SERVICE_NAME}/getcommissionsaledata", tags=["LotteJamsil"])
def getcommissionsaledata(r: Request, storeCd: str,
                saleDate: str,
                ):
    """
    # Add, 20221205, LJY, 커미션
    """                
    cond = {
        'storeCd': storeCd,
        'saleDate': saleDate,
        }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        #오프라인 매출내역 조회
        result_set, query_str = exec_db(kflow_sess, 'getcommissionsaledata', cond)
        
        LOGGER.debug(f"result_set: {len(result_set)}")
        
        if not result_set:
            return make_ret_value(-1, f"[{saleDate}] 일자에 매출내역이 없습니다.", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)



@APP.get(f"/{SERVICE_NAME}/getnontaxamtbrieflyfromto", tags=["COMMISSION"])
def getnontaxamtbrieflyfromto(r: Request, storeCd: str,
                fromDate: str,
                toDate: str,
                ):
    """
    # Add, 20231016, LJY, 과세비과세일별데이터간소화 커미션 (from ~ to)
    """                
    cond = {
        'storeCd': storeCd,
        'fromDate': fromDate,
        'toDate': toDate,
        }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        #과세비과세일별데이터간소화
        result_set, query_str = exec_db(kflow_sess, 'getnontaxamtbrieflyfromto', cond)
        
        LOGGER.debug(f"result_set: {len(result_set)}")
        
        if not result_set:
            return make_ret_value(-1, f"[{toDate}] 일자에 매출내역이 없습니다.", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)


@APP.get(f"/{SERVICE_NAME}/getcommissionsaledatafromto", tags=["LotteJamsil"])
def getcommissionsaledatafromto(r: Request, storeCd: str,
                fromDate: str,
                toDate: str,
                ):
    """
    # Add, 20230905, LJY, 커미션 (from ~ to)
    """                
    cond = {
        'storeCd': storeCd,
        'fromDate': fromDate,
        'toDate': toDate,
        }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        #오프라인 매출내역 조회
        result_set, query_str = exec_db(kflow_sess, 'getcommissionsaledatafromto', cond)
        
        LOGGER.debug(f"result_set: {len(result_set)}")
        
        if not result_set:
            return make_ret_value(-1, f"[{toDate}] 일자에 매출내역이 없습니다.", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)


@APP.get(f"/{SERVICE_NAME}/getcommissionsale_aftermerge", tags=["LotteJamsil"])
def getcommissionsale_aftermerge(r: Request, storeCd: str,
                saleDate: str,
                ):
    """
    # Add, 20230531, LJY, 합병후 롯데(잠실) 재무 정산용
    """                
    cond = {
        'storeCd': storeCd,
        'saleDate': saleDate,
        }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        #오프라인 매출내역 조회
        result_set, query_str = exec_db(kflow_sess, 'getcommissionsale_aftermerge', cond)
        
        LOGGER.debug(f"result_set: {len(result_set)}")
        
        if not result_set:
            return make_ret_value(-1, f"[{saleDate}] 일자에 매출내역이 없습니다.", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)

@APP.post(f"/{SERVICE_NAME}/postcommissionsaledata", tags=["LotteJamsil"])
def getcommissionsaledata(r: Request,
                saleDate: str = Form('saleDate'),
                storeCd: str = Form('storeCd'),
                ):
    """
    ## Add, 20221128, LJY, 교보문고 점포별 수수료 매출자료 getcommissionsaledata
    
    ## input:
    ### :param r:
    ### :param **saleDate**: `매출일자` (str:8)
    ### :param **storeCd**: `점포코드` (str:3)
   
    ## return: retData[]    
    ### :output **SALEDATE**:`매출일자`
    """
    cond = {
        'saleDate': saleDate,
        'storeCd': storeCd,
    }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        #수수료 매출내역 조회
        result_set, query_str = exec_db(kflow_sess, 'getcommissionsaledata', cond)

        LOGGER.debug(f"result_set: {result_set}")
        LOGGER.debug(f"{saleDate}")
        LOGGER.debug(f"[saleDate]: {saleDate} , [storeCd]: {storeCd}")
        
        if not result_set:
            return make_ret_value(-1, f"[{saleDate}] 일자에 매출내역이 없습니다.", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)


@APP.get(f"/{SERVICE_NAME}/newlottebyprod_dvsn_aftermerge", tags=["NewLotte"])
def newlottebyprod_dvsn_aftermerge(r: Request, storeCd: str,
                saleDate: str,
                ):
    """
    # Add, 20230613, LJY, 커미션 합병후 신점포 모델(해운대) 상품구분별 매출 (재무용)
    """                
    cond = {
        'storeCd': storeCd,
        'saleDate': saleDate,
        }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        #오프라인 매출내역 조회
        result_set, query_str = exec_db(kflow_sess, 'newlottebyprod_dvsn_aftermerge', cond)
        
        LOGGER.debug(f"result_set: {len(result_set)}")
        
        if not result_set:
            return make_ret_value(-1, f"[{saleDate}] 일자에 매출내역이 없습니다.", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)

@APP.get(f"/{SERVICE_NAME}/newlotte_tender_aftermerge", tags=["NewLotte"])
def newlotte_tender_aftermerge(r: Request, storeCd: str,
                saleDate: str,
                ):
    """
    # Add, 20230613, LJY, 커미션 합병후 신점포 모델(해운대) 롯데금권별 매출 (재무용)
    """                
    cond = {
        'storeCd': storeCd,
        'saleDate': saleDate,
        }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        #오프라인 매출내역 조회
        result_set, query_str = exec_db(kflow_sess, 'newlotte_tender_aftermerge', cond)
        
        LOGGER.debug(f"result_set: {len(result_set)}")
        
        if not result_set:
            return make_ret_value(-1, f"[{saleDate}] 일자에 매출내역이 없습니다.", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)


@APP.get(f"/{SERVICE_NAME}/newlotte_tender_aftermergefromto", tags=["NewLotte"])
def newlotte_tender_aftermergefromto(r: Request, storeCd: str,
                fromDate: str,
                toDate: str,
                ):
    """
    # Add, 20230905, LJY, 커미션 합병후 신점포 모델(해운대) 롯데금권별 매출 (재무용) (from ~ to)
    """                
    cond = {
        'storeCd': storeCd,
        'fromDate': fromDate,
        'toDate': toDate,
        }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        #오프라인 매출내역 조회
        result_set, query_str = exec_db(kflow_sess, 'newlotte_tender_aftermergefromto', cond)
        
        LOGGER.debug(f"result_set: {len(result_set)}")
        
        if not result_set:
            return make_ret_value(-1, f"[{fromDate}] 일자에 매출내역이 없습니다.", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)


@APP.get(f"/{SERVICE_NAME}/hyundai_tender_aftermerge", tags=["Hyundai"])
def hyundai_tender_aftermerge(r: Request, storeCd: str,
                saleDate: str,
                ):
    """
    # Add, 20230630, LJY, 커미션 합병후 현대백화점 모델(판교등) 현대 금권별 매출 
    """                
    cond = {
        'storeCd': storeCd,
        'saleDate': saleDate,
        }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        #오프라인 매출내역 조회
        result_set, query_str = exec_db(kflow_sess, 'hyundai_tender_aftermerge', cond)
        
        LOGGER.debug(f"result_set: {len(result_set)}")
        
        if not result_set:
            return make_ret_value(-1, f"[{saleDate}] 일자에 매출내역이 없습니다.", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)


@APP.get(f"/{SERVICE_NAME}/hyundai_tender_aftermergefromto", tags=["Hyundai"])
def hyundai_tender_aftermergefromto(r: Request, storeCd: str,
                fromDate: str,
                toDate: str,
                ):
    """
    # Add, 20230905, LJY, 커미션 합병후 현대백화점 모델(판교등) 현대 금권별 매출 (from ~to)
    """                
    cond = {
        'storeCd': storeCd,
        'fromDate': fromDate,
        'toDate': toDate,
        }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        #오프라인 매출내역 조회
        result_set, query_str = exec_db(kflow_sess, 'hyundai_tender_aftermergefromto', cond)
        
        LOGGER.debug(f"result_set: {len(result_set)}")
        
        if not result_set:
            return make_ret_value(-1, f"[{fromDate}] - [{toDate}] 일자에 매출내역이 없습니다.", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)



@APP.get(f"/{SERVICE_NAME}/hyundai_internal_card", tags=["Hyundai"])
def hyundai_internal_card(r: Request, storeCd: str,
                fromDate: str,
                toDate: str,
                ):
    """
    # Add, 20240115 ljy , 현대백화점자사카드 매출확인
    """                
    cond = {
        'storeCd': storeCd,
        'fromDate': fromDate,
        'toDate': toDate,
        }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        #오프라인 매출내역 조회
        result_set, query_str = exec_db(kflow_sess, 'hyundai_internal_card', cond)
        
        LOGGER.debug(f"result_set: {len(result_set)}")
        
        if not result_set:
            return make_ret_value(-1, f"[{fromDate}] - [{toDate}] 일자에 매출내역이 없습니다.", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)


@APP.get(f"/{SERVICE_NAME}/getcommissionbyprod_dvsn_aftermerge", tags=["COMMISSION"])
def getcommissionbyprod_dvsn_aftermerge(r: Request, storeCd: str,
                toDate: str,
                fromDate: str,
                ):
    """
    ## Add, 20221205, LJY, 롯데 잠실 수수료 재무팀용(상품구분)
    """                
    cond = {
        'storeCd': storeCd,
        'fromDate': fromDate,
        'toDate': toDate,
        }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        #오프라인 매출내역 조회
        result_set, query_str = exec_db(kflow_sess, 'getcommissionbyprod_dvsn_aftermerge', cond)
        
        LOGGER.debug(f"result_set: {len(result_set)}")
        
        if not result_set:
            return make_ret_value(-1, f"[{fromDate}] -[{toDate}] 일자에 매출내역이 없습니다.", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)


@APP.get(f"/{SERVICE_NAME}/getcommissionsaledatabyprodfromto", tags=["COMMISSION"])
def getcommissionsaledatabyprodfromto(r: Request, storeCd: str,
                fromDate: str,
                toDate: str,
                ):
    """
    # Add, 20230905, LJY, 커미션 (from ~ to )
    """                
    cond = {
        'storeCd': storeCd,
        'fromDate': fromDate,
        'toDate': toDate,
        }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        #오프라인 매출내역 조회
        result_set, query_str = exec_db(kflow_sess, 'getcommissionsaledatabyprodfromto', cond)
        
        LOGGER.debug(f"result_set: {len(result_set)}")
        
        if not result_set:
            return make_ret_value(-1, f"[{fromDate}] - [{toDate}] 일자에 매출내역이 없습니다.", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)

@APP.get(f"/{SERVICE_NAME}/getcommissionsaledatabyprodfromtobyposno", tags=["COMMISSION"])
def getcommissionsaledatabyprodfromtobyposno(r: Request, storeCd: str,
                fromDate: str,
                toDate: str,
                posNo: str,
                ):
    """
    # Add, 20221205, LJY, 커미션
    """                
    cond = {
        'storeCd': storeCd,
        'fromDate': fromDate,
        'toDate': toDate,
        'posNo': posNo,
        }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        #오프라인 매출내역 조회
        result_set, query_str = exec_db(kflow_sess, 'getcommissionsaledatabyprodfromtobyposno', cond)
        
        LOGGER.debug(f"result_set: {len(result_set)}")
        
        if not result_set:
            return make_ret_value(-1, f"[{fromDate}] - [{toDate}] 일자에 매출내역이 없습니다.", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)


@APP.get(f"/{SERVICE_NAME}/getcommissionsaledatabyprod", tags=["COMMISSION"])
def getcommissionsaledatabyprod(r: Request, storeCd: str,
                saleDate: str,
                ):
    """
    # Add, 20221205, LJY, 커미션
    """                
    cond = {
        'storeCd': storeCd,
        'saleDate': saleDate,
        }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        #오프라인 매출내역 조회
        result_set, query_str = exec_db(kflow_sess, 'getcommissionsaledatabyprod', cond)
        
        LOGGER.debug(f"result_set: {len(result_set)}")
        
        if not result_set:
            return make_ret_value(-1, f"[{saleDate}] 일자에 매출내역이 없습니다.", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)

@APP.post(f"/{SERVICE_NAME}/postcommissionsaledatabyprod", tags=["COMMISSION"])
def postcommissionsaledatabyprod(r: Request,
                saleDate: str = Form('saleDate'),
                storeCd: str = Form('storeCd'),
                ):
    """
    ## Add, 20221207, LJY, 교보문고 점포별 상품 과세비과세 수수료 매출자료 postcommissionsaledatabyprod
    
    ## input:
    ### :param r:
    ### :param **saleDate**: `매출일자` (str:8)
    ### :param **storeCd**: `점포코드` (str:3)
   
    ## return: retData[]    
    ### :output **SALEDATE**:`매출일자`
    """
    cond = {
        'saleDate': saleDate,
        'storeCd': storeCd,
    }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        #수수료 매출내역 조회
        result_set, query_str = exec_db(kflow_sess, 'getcommissionsaledatabyprod', cond)

        LOGGER.debug(f"result_set: {result_set}")
        LOGGER.debug(f"{saleDate}")
        LOGGER.debug(f"[saleDate]: {saleDate} , [storeCd]: {storeCd}")
        
        if not result_set:
            return make_ret_value(-1, f"[{saleDate}] 일자에 매출내역이 없습니다.", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)



@APP.post(f"/{SERVICE_NAME}/getcommissionsaledata_dtl", tags=["IDPS"])
def getcommissionsaledata_dtl(r: Request,
                saleDate: str = Form('saleDate'),
                storeCd: str = Form('storeCd'),
                ):
    """
    ## Add, 20221128, LJY, 수수료 매출자료 검증용 getcommissionsaledata_dtl
    
    ## input:
    ### :param r:
    ### :param **saleDate**: `매출일자` (str:8)
    ### :param **storeCd**: `점포코드` (str:3)
   
    ## return: retData[]    
    ### :output **SALEDATE**:`매출일자`
    """
    cond = {
        'saleDate': saleDate,
        'storeCd': storeCd,
    }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        #수수료 매출내역 조회
        result_set, query_str = exec_db(kflow_sess, 'getcommissionsaledata_dtl', cond)

        LOGGER.debug(f"result_set: {result_set}")
        LOGGER.debug(f"{saleDate}")
        LOGGER.debug(f"[saleDate]: {saleDate} , [storeCd]: {storeCd}")
        
        if not result_set:
            return make_ret_value(-1, f"[{saleDate}] 일자에 매출내역이 없습니다.", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)


@APP.post(f"/{SERVICE_NAME}/getcashlogbyvan", tags=["IDPS"])
def getcashlogbyvan(r: Request,
                fromDate: str      = Form('fromDate'),
                toDate: str        = Form('toDate'),
                ):
    """
    ## Add, 20231205 LJY van사별 현금영수증 월별집계내역 getcashlogbyvan

    ## input:
    ### :param r:
    ### :param **fromDate**: `시작일자` (str:8)
    ### :param **toDate**: `종료일자` (str:8)
   
    ## return: retData[]    
    """
    cond = {
        'fromDate': fromDate,
        'toDate': toDate,
    }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        #현금영수증처리 집계
        result_set, query_str = exec_db(kflow_sess, 'getcashlogbyvan', cond)

        LOGGER.debug(f"result_set: {result_set}")
        
        if not result_set:
            return make_ret_value(-1, f"[{fromDate}]-[{toDate}] 기간일자에 매출내역이 없습니다.", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)

@APP.get(f"/{SERVICE_NAME}/getmonthlypayday", tags=["Merge"])
def getmonthlypayday(r: Request, 
                fromDate: str,
                toDate: str,
                ):
    """
    ## Add, 20230717, LJY 월별 대급금 확인
    """                
    cond = {
        'fromDate': fromDate,
        'toDate': toDate,
        }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        #대급금 월별 점포별 매출금액 조회 
        result_set, query_str = exec_db(kflow_sess, 'getmonthlypayday', cond)
        
        LOGGER.debug(f"result_set: {len(result_set)}")
        
        if not result_set:
            return make_ret_value(-1, f"[{fromDate}] ~ [{toDate}] 기간내 매출내역이 없습니다.", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)



@APP.post(f"/{SERVICE_NAME}/getempinfo", tags=["IDPS"])
def getempinfo(r: Request,
                empNo: str = Form('empNo'),
                empPassword: str = Form('empPassword'),
                ):
    """
    # Add, 20221209, LJY

    """
    cond = {
        'empNo': empNo,
        'empPassword': empPassword,
    }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        #사원정보조회
        result_set, query_str = exec_db(kflow_sess, 'getempinfo', cond)
        
        LOGGER.debug(f"result_set: {len(result_set)}")
        
        if not result_set:
            return make_ret_value(-1, f"[{empNo}] 에 매칭되는 사용자가 존재하지 않습니다.", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)


@APP.get(f"/{SERVICE_NAME}/getcampaignfromidps/{{memNo}}", tags=["IDPS"])
def getcampaignfromidps(r: Request, 
                memNo: str,
                ):
    """
    # Add, 20240424, LJY, 회원 캠페인 정보 IDPS 이관된 내용으로 조회 getcampaignfromidps
    ## input:
    ### :param r:
    ### :param **memNo**: `회원번호` (str:11)
        
    ## return: retData[]    
    ### :output **MEMNO** :`회원번호`
    ### :output **PRCD** :`프로모션코드`
    ### :output **MEMLEVEL** :`프로모션코드`
    ### :output **JOINTYPE** :`가입유형`
    """                
    cond = {
        'memNo': memNo,
        }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        #오프라인 매출내역 조회
        result_set, query_str = exec_db(kflow_sess, 'getcampaignfromidps', cond)
        
        LOGGER.debug(f"result_set: {len(result_set)}")
        
        if not result_set:
            return make_ret_value(-1, f"[{memNo}] 캠페인 프로모션 내역이 존재하지 않습니다.", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)


class campaignItem(BaseModel):
    mmbr_nmbr: str 
    offer_nmbr: str 
    pr_nmbr: str 
    strt_date: str
    clse_date: str
    crtr_id: str

@APP.put(f"/{SERVICE_NAME}/regcampaignlist", tags=["IDPS"])
def regcampaignlist(r: Request, items: List[campaignItem]):
    """
    # Add, 20240424, LJY 캠페인 인터페이스 용 리스트 등록/변경 regcampaign 리스트형태로 전달필요 

    ```json
        # json body 정보 exapmle
        [
            {"mmbr_nmbr": "10001063460","offer_nmbr": "56201", "pr_nmbr": "202212290007", "strt_date": "20240101", 
            "clse_date": "20240131", "crtr_id":"12432" },
        ]
    ```

    ### :param **mmbr_nmbr**: `회원번호` (str:11)
    ### :param **offer_nmbr**: `요청번호` (str:9)
    ### :param **pr_nmbr**: `프로모션코드` (str:14)
    ### :param **strt_date**: `시작일자` (str:8)
    ### :param **clse_date**: `종료일자` (str:8)
    ### :param **crtr_id**: `생성자ID` (str:5)

    """
    
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]
    requestList = [item.dict() for item in items]             
    for requestData in requestList:
        # =============> 리스트 처리 시작 <============= #
        LOGGER.debug(f"requestData: {requestData}")
        if len(requestData['mmbr_nmbr']) > 11 or len(requestData['mmbr_nmbr']) <= 0:
            LOGGER.debug(f"회원번호가 없습니다.")
            return make_ret_value(-1, f"회원번호 확인이 필요합니다.", 100,
                                    len(requestData['mmbr_nmbr']), requestData['mmbr_nmbr'], session_list, query_str, r.client.host)
        
        if len(requestData['offer_nmbr']) > 9 or len(requestData['offer_nmbr']) <= 0:
            LOGGER.debug(f"요청번호가 없습니다.")
            return make_ret_value(-1, f"offer_nmbr 확인이 필요합니다.", 100,
                                    len(requestData['offer_nmbr']), requestData['offer_nmbr'], session_list, query_str, r.client.host)
        
        if  len(requestData['pr_nmbr']) > 14 or len(requestData['pr_nmbr']) <= 0:
            LOGGER.debug(f"pr_nmbr가 없습니다.")
            return make_ret_value(-1, f"pr_nmbr 확인이 필요합니다.", 100,
                                    len(requestData['pr_nmbr']), requestData['pr_nmbr'], session_list, query_str, r.client.host)

        campaign_cond = {
            'mmbr_nmbr': requestData['mmbr_nmbr'],
            'offer_nmbr': requestData['offer_nmbr'],
            'pr_nmbr': requestData['pr_nmbr'],
            'strt_date': requestData['strt_date'],
            'clse_date': requestData['clse_date'],
            'crtr_id': requestData['crtr_id'],

        }

        result, query_str = exec_db(kflow_sess, 'regcampaign', campaign_cond)
        if result == 0:
            return make_ret_value(-1, f"캠페인리스트 등록 처리에 실패하였습니다!!", 100,
                                    len(campaign_cond), campaign_cond, session_list, query_str, r.client.host)

        LOGGER.debug(f"캠페인 정보 반영 완료: [{result}]")

    cond ={
        'offer_nmbr': requestList[0]['offer_nmbr'],
        'pr_nmbr': requestList[0]['pr_nmbr'],
    }

    result_set, query_str = exec_db(kflow_sess, 'checkcampaign', cond)


    return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)

@APP.post(f"/{SERVICE_NAME}/regcampaign", tags=["IDPS"])
def regcampaign(r: Request,
                mmbr_nmbr: str = Form('mmbr_nmbr'),
                offer_nmbr: str = Form('offer_nmbr'),
                pr_nmbr: str = Form('pr_nmbr'),
                strt_date: str = Form('strt_date'),
                clse_date: str = Form('clse_date'),
                crtr_id: str = Form('crtr_id'),

                ):
    """
    # Add, 20240423, LJY 캠페인 인터페이스 용 단건 등록/변경 regcampaign
          mmbr_nmbr 
        , offer_nmbr 
        , pr_nmbr 
        , strt_date 
        , clse_date 
        , crtr_id 


    ### :param r:
    ### :param **mmbr_nmbr**: `회원번호` (str:11)
    ### :param **offer_nmbr**: `요청번호` (str:9)
    ### :param **pr_nmbr**: `프로모션코드` (str:14)
    ### :param **strt_date**: `시작일자` (str:8)
    ### :param **clse_date**: `종료일자` (str:8)
    ### :param **crtr_id**: `생성자ID` (str:5)

    """
    cond = {
        'mmbr_nmbr': mmbr_nmbr,
        'offer_nmbr': offer_nmbr,
        'pr_nmbr': pr_nmbr,
        'strt_date': strt_date,
        'clse_date': clse_date,
        'crtr_id': crtr_id,

    }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        #캠페인 인터페이스 
        result_set, query_str = exec_db(kflow_sess, 'regcampaign', cond)
        
        LOGGER.debug(f"result_set: {len(result_set)}")
        
        if len(result_set) <= 0:
            return make_ret_value(-1, f"캠페인정보 생성에 실패 하였습니다.!!", 100,
                                    len(cond), cond, session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)



@APP.get(f"/{SERVICE_NAME}/getCreditSaleByPurcode", tags=["CREDIT"])
def getCreditSaleByPurcode(r: Request, fromDate: str,
                toDate: str,
                tranType: str,
                ):
    """
    # Add, 20230303, LJY, 신용카드매출 조회 집계 (카드사별)
    """                
    cond = {
        'fromDate': fromDate,
        'toDate': toDate,
        'tranType': tranType,
        }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        if tranType == 'direct':
            #신용카드 매입사 합계
            result_set, query_str = exec_db(kflow_sess, 'getCreditSaleByPurcode', cond)
        else:
            result_set, query_str = exec_db(kflow_sess, 'getCreditSaleFranchiseeByPurcode', cond)
        
        LOGGER.debug(f"result_set: {len(result_set)}")
        
        if not result_set:
            return make_ret_value(-1, f"[{fromDate}] ~ [{toDate}] 일자에 매출내역이 없습니다.", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)

@APP.get(f"/{SERVICE_NAME}/getCreditSaleGrouping", tags=["CREDIT"])
def getCreditSaleGrouping(r: Request, fromDate: str,
                toDate: str,
                tranType: str,
                ):
    """
    # Add, 20230303, LJY, 신용카드매출 매입사별 일자조회
    """                
    cond = {
        'fromDate': fromDate,
        'toDate': toDate,
        'tranType': tranType,
        }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        if tranType == 'direct':
            #신용카드 매입사 합계 일자별
            result_set, query_str = exec_db(kflow_sess, 'getCreditSaleGrouping', cond)
        else:
            result_set, query_str = exec_db(kflow_sess, 'getCreditSaleFranchiseeGrouping', cond)
                
        
        LOGGER.debug(f"result_set: {len(result_set)}")
        
        if not result_set:
            return make_ret_value(-1, f"[{fromDate}] ~ [{toDate}] 일자에 매출내역이 없습니다.", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)


@APP.get(f"/{SERVICE_NAME}/getCreditSaleDetail", tags=["CREDIT"])
def getCreditSaleDetail(r: Request, fromDate: str,
                toDate: str,
                tranType: str,
                ):
    """
    # Add, 20230303, LJY, 신용카드매출 조회 집계 (디테일)
    """                
    cond = {
        'fromDate': fromDate,
        'toDate': toDate,
        'tranType': tranType,
        }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        if tranType == 'direct':
            #신용카드 매출 상세
            result_set, query_str = exec_db(kflow_sess, 'getCreditSaleDetail', cond)
        else:
            #신용카드 매출 상세 비직영점
            result_set, query_str = exec_db(kflow_sess, 'getCreditSaleFranchiseeDetail', cond)        
        
        LOGGER.debug(f"result_set: {len(result_set)}")
        
        if not result_set:
            return make_ret_value(-1, f"[{fromDate}] ~ [{toDate}] 일자에 매출내역이 없습니다.", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)



@APP.get(f"/{SERVICE_NAME}/getmileage", tags=["COMMISSION"])
def getmileage(r: Request, fromDate: str,
                toDate: str,
                tranType: str
                ):
    """
    # Add, 20221205, LJY, 커미션
    """                
    cond = {
        'fromDate': fromDate,
        'toDate': toDate,
        'tranType' : tranType,
        }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        
        if tranType == 'a':
            #마일리지 사용내역 조회
            result_set, query_str = exec_db(kflow_sess, 'getmileagespend', cond)
            # LOGGER.debug(f"result_set: {result_set}")
            LOGGER.debug(f"tranType: {tranType}")
        else:
            #마일리지 적립내역 조회
            result_set, query_str = exec_db(kflow_sess, 'getmileagesave', cond)
            # LOGGER.debug(f"result_set: {result_set}")        
            LOGGER.debug(f"tranType: {tranType}")
        
        LOGGER.debug(f"result_set: {len(result_set)}")
        
        if not result_set:
            return make_ret_value(-1, f"[{fromDate}] ~ [{toDate}] 일자에 매출내역이 없습니다.", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)


@APP.get(f"/{SERVICE_NAME}/getvendorcommissiondtl", tags=["COMMISSION"])
def getvendorcommissiondtl(r: Request, 
                storeCd: str, 
                fromDate: str,
                toDate: str,
                ):
    """
    # Add, 20221205, LJY, 부천 건대 비도서 총판 매출 조회 커미션
    """                
    cond = {
        'storeCd': storeCd,
        'fromDate': fromDate,
        'toDate': toDate,
        }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        #마일리지 적립/사용내역 조회
        result_set, query_str = exec_db(kflow_sess, 'getvendorcommissiondtl', cond)
        
        LOGGER.debug(f"result_set: {len(result_set)}")
        
        if not result_set:
            return make_ret_value(-1, f"[{fromDate}] ~ [{toDate}] 일자에 매출내역이 없습니다.", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)

@APP.post(f"/{SERVICE_NAME}/findsalespercmdt", tags=["IDPL"])
def findsalespercmdt(r: Request,
                fromYmd: str      = Form('fromYmd'),
                toYmd: str        = Form('toYmd'),
                rdpCode: str      = Form('rdpCode'),
                cmdtCode: str     = Form('cmdtCode'),  
                vndrCode: str     = Form('vndrCode'),
                posNo: str        = Form('posNo'),
                tranNo: str       = Form('tranNo'),
                ):
    """
    # Add, 20220322, LJY, 상품별 판매내역 조회 findsalespercmdt
    ## input:
    ### :param r:
    ### :param fromYmd: str   
    ### :param toYmd: str     
    ### :param rdpCode: str   
    ### :param cmdtCode: str  
    ### :param vndrCode: str  
    ### :param posNo: str     
    ### :param tranNo: str    

         
    ## return: retData[]    
    ### :output **STORECD**: `영업점코드`
    ### :output **SALEDATE**: `판매일자`
    ### :output **POSNO**: `POS번호`
    ### :output **TRANNO**: `영수증번호`
    ### :output **SEQ**: `순번`
    ### :output **SALETIME**: `판매시간`
    ### :output **NAME**: `상품명`
    ### :output **PLUBARCD**: `상품코드`
    ### :output **QTY**: `수량`
    ### :output **SALEAMT**: `매출금액`

    # :Query


    
    """
    cond = {
        'fromYmd': fromYmd,
        'toYmd': toYmd,
        'rdpCode': rdpCode,
        'cmdtCode': cmdtCode,        
        'vndrCode': vndrCode,
        'posNo': posNo,
        'tranNo': tranNo,
               

    }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        #상품별 판매내역 조회 
        result_set, query_str = exec_db(kflow_sess, 'findsalespercmdt', cond)

        LOGGER.debug(f"result_set: {result_set}")
        
        if not result_set:
            return make_ret_value(-1, f"리턴불가 ", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(result_set), result_set, session_list, query_str, r.client.host)

@APP.get(f"/{SERVICE_NAME}/gethyundaideptcardsale", tags=["Hyundai"])
def gethyundaideptcardsale(r: Request, storeCd: str,
                fromDate: str,
                toDate: str,
                ):
    """
    # Add, 20240214, LJY, 현대백화점전용카드매출조회
    """                
    cond = {
        'storeCd': storeCd,
        'fromDate': fromDate,
        'toDate': toDate,
        }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        #오프라인 매출내역 조회
        result_set, query_str = exec_db(kflow_sess, 'gethyundaideptcardsale', cond)
        
        LOGGER.debug(f"result_set: {len(result_set)}")
        
        if not result_set:
            return make_ret_value(-1, f"[{fromDate}] -[{toDate}]일자에 매출내역이 없습니다.", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리 중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)





# @APP.post(f"/{SERVICE_NAME}/tkinrgst")
def tkinrgst(r: Request,
                boxNum: str = Form('boxNum'),
                tkinRdpCode: str = Form('tkinRdpCode'),
                tkotRdpCode: str = Form('tkotRdpCode'),
                tkotDate: str = Form('tkotDate'),
                tkotWrkNum: str = Form('tkotWrkNum'),
                targetType: str = Form('targetType'),
                crtrId: str = Form('crtrId'),
                ):
    """
    Add, 2021.03.12, 심윤보, 반입등록
    :param r:
    :param boxNum:
    :param tkinRdpCode:
    :return:
    """
    cond = {
        'boxNum': boxNum,
        'tkinRdpCode': tkinRdpCode,
        'tkotRdpCode': tkotRdpCode,
        'tkotDate': tkotDate,
        'tkotWrkNum': tkotWrkNum,
        'targetType': targetType,
        'tkinCnfrDttm': get_cur_date_time('%Y%m%d%H%M%S'),
        'tkinIpAdrs': r.client.host,
        'crtrId': crtrId,
    }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    try:
        result_set = None
        if targetType == 'D':
            result_set, query_str = exec_db(kflow_sess, 'findDuplicationTakeOutDetail', cond)
            LOGGER.debug(f"result_set: {result_set}")
        else:
            result_set, query_str = exec_db(kflow_sess, 'findDuplicationTakeOut', cond)
            LOGGER.debug(f"result_set: {result_set}")

        if not result_set:
            return make_ret_value(-1, f"[{tkinRdpCode}/{boxNum}]해당 박스로 반입할 내역이 없습니다.", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        if len(result_set) > 1:
            return make_ret_value(-2601, f"[{tkinRdpCode}/{boxNum}]중복박스 내역 존재", 100, len(result_set), result_set,
                                  session_list, query_str, r.client.host)

        # Validation Check
        if len(result_set[0]['boxNum'].strip()) != 8:
            err_msg = f"박스번호가 올바르지 않습니다!!"
            LOGGER.error(err_msg)
            return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, '', r.client.host)

        if result_set[0]['tkinRdpCode'] == '010':
            err_msg = f"대학영업으로는 해당작업이 불가능합니다."
            LOGGER.error(err_msg)
            return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, '', r.client.host)

        if result_set[0]['tkinRdpCode'] in ['073', ]:
            err_msg = f"서울대BY는 PDA 반입 작업이 불가능합니다."
            LOGGER.error(err_msg)
            return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, '', r.client.host)

        if result_set[0]['boxNum'].startswith('BCT'):
            err_msg = f"BCT로 시작하는 박스는 반입처리가 불가능합니다."
            LOGGER.error(err_msg)
            return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, '', r.client.host)

        for tkin_item in result_set:
            cond.update(tkin_item)
            LOGGER.debug(f"cond: {cond}")
            """
                    {
                        "tkotRdpCode": "009",
                        "tkotDate": "20210118",
                        "tkotWrkNum": "11022",
                        "boxNum": "10217254",
                        "tkinRdpCode": "001",
                        "qty": 5,
                        "ttalQty": 15.0,
                        "tkioDvsnCode": "001"
                    }
            """
            if tkin_item['tkotRdpCode'] == '022':
                if tkin_item['tkinRdpCode'] != '001' and tkin_item['tkinRdpCode'] != '009':
                    return make_ret_value(-1, f"신간정보에서 반출된 BOX는 광화문, 파주에서만 반입이 가능합니다.", 100,
                                          len(result_set), result_set, session_list, query_str, r.client.host)

            if tkin_item['tkinRdpCode'] == '022':
                if tkin_item['tkotRdpCode'] != '001' and tkin_item['tkotRdpCode'] != '009' and tkin_item['tkotRdpCode'] != '050':
                    return make_ret_value(-1, f"신간정보에서는 광화문, 파주, 북시티에서 반출된 BOX만 반입이 가능합니다.", 100,
                                          len(result_set), result_set, session_list, query_str, r.client.host)

            # 반입 작업번호 채번
            cond['tkinWrkNum'] = get_kflow_seqnum('TM_TKIO', 'tkin_wrk_num', get_cur_date_time('%Y%m%d'), cond['crtrId'], 5)
            LOGGER.debug(f"cond['tkinWrkNum']: {cond['tkinWrkNum']}")
            if cond['tkinWrkNum'] == '':
                return make_ret_value(-1, f"반입 작업번호 채번에 실패 하였습니다.", 100,
                                      len(result_set), result_set, session_list, query_str, r.client.host)

            # 반입등록
            update_result, query_str = exec_db(kflow_sess, 'modifyTakeInRegistration', cond)
            LOGGER.debug(f"update_result: {update_result}")

            if update_result[0]['cnt'] != 1:
                return make_ret_value(-1, f"반출입 마스터 업데이트에 실패 하였습니다.", 100,
                                      len(result_set), result_set, session_list, query_str, r.client.host)

            # 반입 상세 내역 조회
            tkin_detail, query_str = exec_db(kflow_sess, 'findTakeInRegistrationDetl', cond)
            # LOGGER.debug(f"tkin_detail: {tkin_detail}")

            if len(tkin_detail) <= 0:
                return make_ret_value(-1, f"조회된 반출입 상세내역이 없습니다.", 100,
                                      len(cond), cond, session_list, query_str, r.client.host)

            for detail_item in tkin_detail:
                # =============> 상품단위 처리 시작 <============= #
                LOGGER.debug(f"detail_item: {detail_item}")
                # 반출정보의 상품가격이 0인 경우 T_상품정가의 가격을 세팅한다.(반영: 2011.07.06)
                if detail_item['cmdtPrce'] == 0:
                    LOGGER.debug(f"반출정보의 상품가격이 0인 경우 T_상품정가의 가격을 세팅한다.(반영: 2011.07.06): cmdtPrce: {detail_item['cmdtPrce']} <= wncrPrce: {detail_item['wncrPrce']}")
                    detail_item['cmdtPrce'] = detail_item['wncrPrce']

                invn_cond = {
                    'rdpCode': detail_item['tkinRdpCode'],
                    'cmdtId': detail_item['cmdtId'],
                    'rcdsInvnQntt': detail_item['tkioQntt'],
                    'realInvnQntt': detail_item['tkioQntt'],
                    'strPrenInvnQntt': 0,
                    'crtrId': cond['crtrId'],
                }

                rdp_invn_result, query_str = exec_db(kflow_sess, 'modifyRdpInventory', invn_cond)
                if len(rdp_invn_result) <= 0:
                    return make_ret_value(-1, f"점포 재고 변경 처리에 실패하였습니다!!", 100,
                                          len(invn_cond), invn_cond, session_list, query_str, r.client.host)

                LOGGER.debug(f"점포 재고 반영 완료: [{rdp_invn_result}]")

                # 05-06. 점포 재고처리 EAI 호출
                eai_result, query_str = exec_db(kflow_sess, 'registerEaiParamsInvn', invn_cond)

                if len(eai_result) <= 0:
                    # 'EAI 호출에 실패하였습니다.'
                    return make_ret_value(-1, f"EAI 호출에 실패하였습니다!!", 100,
                                          len(invn_cond), invn_cond, session_list, query_str, r.client.host)

                LOGGER.debug(f"점포 재고처리 EAI 호출 완료: [{eai_result}]")
                # =============> 잡지, 첨단 가격정보 처리 시작 <============= #
                if cond['tkinRdpCode'] not in ['022', '063', '040', ]:
                    if detail_item['joCode'] in ['18', '38', ]:
                        LOGGER.debug(f"detail_item: {detail_item}")
                        byngprce_result, query_str = exec_db(kflow_sess, 'findCommodityBuyingPriceForPos', detail_item)
                        LOGGER.debug(f"byngprce_result: {byngprce_result}")
                        if len(byngprce_result) <= 0:
                            # 'EAI 호출에 실패하였습니다.'
                            return make_ret_value(-1, f"상품의 최종 입하 정가 정보가 없습니다.", 100,
                                                  len(detail_item), detail_item, session_list, query_str, r.client.host)

                        LOGGER.debug(f"detail_item: {detail_item}")
                        hubspoke_result, query_str = exec_db(kflow_sess, 'findHubRdpCode', detail_item)
                        LOGGER.debug(f"hubspoke_result: {hubspoke_result}")

                        if len(hubspoke_result) <= 0:
                            # 'EAI 호출에 실패하였습니다.'
                            return make_ret_value(-1, f"상품의 최종 입하 정가 정보가 없습니다.", 100,
                                                  len(detail_item), detail_item, session_list, query_str, r.client.host)

                        detail_item['hubRdpCode'] = hubspoke_result[0]['hubRdpCode']
                        if detail_item['hubRdpCode'] == '':
                            detail_item['isSpoke'] = False
                        else:
                            detail_item['isSpoke'] = True
                            temp_cond = {
                                'rdpCode': detail_item['hubRdpCode'],
                                'cmdtId': detail_item['cmdtId'],
                            }
                            LOGGER.debug(f"temp_cond: {temp_cond}")
                            hub_mstgoods_result, query_str = exec_db(kflow_sess, 'findHubMstgoodsInfo', temp_cond)
                            LOGGER.debug(f"hub_mstgoods_result: {hub_mstgoods_result}")
                            if len(hub_mstgoods_result) <= 0:
                                # 허브점포의 상품 가격정보 조회에 실패하였습니다.
                                return make_ret_value(-1, f"허브점포의 상품 가격정보 조회에 실패하였습니다.", 100,
                                                      len(detail_item), detail_item, session_list, query_str,
                                                      r.client.host)
                            detail_item['hubByngCost'] = hub_mstgoods_result[0]['COST']
                            detail_item['hubWncrPrce'] = hub_mstgoods_result[0]['UNITPRICE']
                            detail_item['hubMaxDcRate1'] = hub_mstgoods_result[0]['MAXDCRATE1']
                            detail_item['hubMaxDcRate2'] = hub_mstgoods_result[0]['MAXDCRATE2']
                            detail_item['hubEtcInfo1'] = hub_mstgoods_result[0]['ETCINFO1']

                        # POS 상품정보 조회
                        LOGGER.debug(f"detail_item: {detail_item}")
                        mstgoods_result, query_str = exec_db(kflow_sess, 'getMstGoods', detail_item)
                        LOGGER.debug(f"mstgoods_result: {mstgoods_result}")
                        if len(mstgoods_result) <= 0:
                            # POS 상품정보 조회에 실패하였습니다.
                            LOGGER.debug(f"[{detail_item['tkinRdpCode']}/{detail_item['cmdtCode']}] MSTGOODS에 해당 상품 정보가 없습니다!!")
                            detail_item['existMstGoods'] = 0
                        else:
                            # POS 상품정보가 존재하면 현재 반입상품의 가격정보와 비교하여 업데이트 결정
                            detail_item['existMstGoods'] = 1

                            mstgoods_for_save_cond = {
                                'STORECD': detail_item['tkinRdpCode'],
                                'PLUBARCD': detail_item['cmdtCode'],
                                'COST': byngprce_result['aplWncrCost'],
                                'UNITPRICE': byngprce_result['wncrPrce'],
                                'MAXDCRATE1': 0,
                                'MAXDCRATE2': 0,
                                'ETCINFO1': str(byngprce_result['byngRate']),
                            }

                            # 최대할인율1
                            if 95.0 - float(byngprce_result['byngRate']) <= 0:
                                mstgoods_for_save_cond['MAXDCRATE1'] = 0
                            else:
                                mstgoods_for_save_cond['MAXDCRATE1'] = round(95.0 - float(byngprce_result['byngRate']))

                            # 최대할인율2
                            if 90.0 - float(byngprce_result['byngRate']) <= 0:
                                mstgoods_for_save_cond['MAXDCRATE2'] = 0
                            else:
                                mstgoods_for_save_cond['MAXDCRATE2'] = round(90.0 - float(byngprce_result['byngRate']))

                            if detail_item['isSpoke']:
                                LOGGER.debug(f"스포크점이면 상품정보를 허브점의 정보로 교체, 단 허브점의 상품가격이 0이 아닌 경우에만!!")
                                if detail_item['hubWncrPrce'] <= 0:
                                    mstgoods_for_save_cond['COST'] = detail_item['hubByngCost']
                                    mstgoods_for_save_cond['UNITPRICE'] = detail_item['hubWncrPrce']
                                    mstgoods_for_save_cond['MAXDCRATE1'] = detail_item['hubMaxDcRate1']
                                    mstgoods_for_save_cond['MAXDCRATE2'] = detail_item['hubMaxDcRate2']
                                    mstgoods_for_save_cond['ETCINFO1'] = detail_item['hubEtcInfo1']

                            # 현재 포스 정가정보와 반입된 정가정보가 다르면 업데이트, 참고로 인서트는 안함!!
                            LOGGER.debug(f"현재 포스정가정보[{mstgoods_result['UNITPRICE']}]와 반입정가정보[{mstgoods_for_save_cond['UNITPRICE']}]가 다르면 업데이트")
                            if mstgoods_result['UNITPRICE'] != mstgoods_for_save_cond['UNITPRICE']:
                                LOGGER.debug(f"mstgoods_for_save_cond: {mstgoods_for_save_cond}")
                                mstgoods_update_result, query_str = exec_db(kflow_sess, 'modifyMstGoodsPrice', mstgoods_for_save_cond)
                                LOGGER.debug(f"mstgoods_update_result: {mstgoods_update_result}")
                                if len(mstgoods_update_result) <= 0:
                                    # 허브점포의 상품 가격정보 조회에 실패하였습니다.
                                    return make_ret_value(-1, f"포스 상품 정보 수정에 실패하였습니다.", 100,
                                                          len(mstgoods_update_result), mstgoods_update_result, session_list, query_str,
                                                          r.client.host)

                        # 도서기본정가 조회
                        LOGGER.debug(f"detail_item: {detail_item}")
                        basic_price_result, query_str = exec_db(kflow_sess, 'getBookPriceFromBookstoreBasicPrice', detail_item)
                        LOGGER.debug(f"basic_price_result: {basic_price_result}")
                        if len(basic_price_result) <= 0:
                            LOGGER.debug(f"도서기본정가가 없으면 생성[registerCommodityBasicPrice]")
                            LOGGER.debug(f"detail_item: {detail_item}")
                            rgst_basic_price_result, query_str = exec_db(kflow_sess, 'registerCommodityBasicPrice', detail_item)
                            LOGGER.debug(f"rgst_basic_price_result: {rgst_basic_price_result}")
                            if rgst_basic_price_result[0]['cnt'] > 0:
                                eai_cond = {
                                    'procName': 'IDCM..spe_TI_ICM_RLTM_KFLW',
                                    'tableName': 'TD_BKS_BSC_PRCE',
                                    'cudGubun': 'C',
                                    'cmdtClstCode': '',
                                    'rdpCode': detail_item['tkinRdpCode'],
                                    'cmdtId': detail_item['cmdtId'],
                                }
                                eai_result, query_str = exec_db(kflow_sess, 'registerEaiParams', detail_item)
                                LOGGER.debug(f"eai_result: {eai_result}")

                        else:
                            LOGGER.debug(f"도서기본정가가 있고, 반입정보상 상품정가[{detail_item['cmdtPrce']}]와 도서기본정가[{basic_price_result['bksPrce']}]가 다르면 업데이트[modifyCommodityBasicPrice]")
                            if detail_item['cmdtPrce'] != basic_price_result['bksPrce']:
                                LOGGER.debug(f"detail_item: {detail_item}")
                                update_basic_price_result, query_str = exec_db(kflow_sess, 'modifyCommodityBasicPrice', detail_item)
                                LOGGER.debug(f"update_basic_price_result: {update_basic_price_result}")
                                if update_basic_price_result[0]['cnt'] > 0:
                                    eai_cond = {
                                        'procName': 'IDCM..spe_TI_ICM_RLTM_KFLW',
                                        'tableName': 'TD_BKS_BSC_PRCE',
                                        'cudGubun': 'U',
                                        'cmdtClstCode': '',
                                        'rdpCode': detail_item['tkinRdpCode'],
                                        'cmdtId': detail_item['cmdtId'],
                                    }
                                    eai_result, query_str = exec_db(kflow_sess, 'registerEaiParams', detail_item)
                                    LOGGER.debug(f"eai_result: {eai_result}")

                # =============> 잡지, 첨단 가격정보 처리 끝 <============= #
                # =============> 상품단위 처리 끝 <============= #

            # 6. 반입 예약 재고 생성 (반영: 2011.05.06)
            LOGGER.debug(f"cond: {cond}")
            takeinpren_result, query_str = exec_db(kflow_sess, 'registerTakeInPrenInvn', cond)
            LOGGER.debug(f"takeinpren_result: {takeinpren_result}")
            if len(takeinpren_result) <= 0:
                LOGGER.error(f"반입 예약 재고 생성에 실패 하였습니다.!!")
                return make_ret_value(-1, f"반입 예약 재고 생성에 실패 하였습니다.!!", 100,
                                      len(cond), cond, session_list, query_str, r.client.host)

            LOGGER.debug(f"점포 재고처리 EAI 호출 완료: [{eai_result}]")

            # /* 7. 반출처:파주센터, 반입처:서울대BY */
            # /*    반입처리 후 서울대BY(반출처) -> 서울대교내서점(반입처) 로 반출입 생성한다.(반영: 2011.06.29) */
            # 윗단에서 반입 불가 처리!![BY는 무조건 통유에서 처리토록!!]

        LOGGER.debug(f"result_set: {result_set}")

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"박스 반입 등록 작업중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, '', r.client.host)


# @APP.post(f"/{SERVICE_NAME}/saveplorrlnc")
def saveplorrlnc(r: Request,
                 rdpCode: str = Form('rdpCode'),
                 plorRlncRsnCode: str = Form('plorRlncRsnCode'),
                 plorQntt: int = Form('plorQntt'),
                 cmdtCode: str = Form('cmdtCode'),
                 userId: str = Form('crtrId'),
                 plorRlncNum: str = Form('plorRlncNum'),
                 ordrNum: str = Form('ordrNum'),
                 crtrId: str = Form('crtrId'),
                 ):
    """
    Add, 2021.03.13, 심윤보, 발주의뢰
    :param r:
    :param rdpCode:
    :param plorRlncRsnCode:
    :param cmdtCode:
    :param plorQntt:
    :param userId:
    :param plorRlncNum:
    :param ordrNum:
    :return:
    """
    cond = {
        'plorRlncDate': get_cur_date_time('%Y%m%d'),
        'rdpCode': rdpCode,
        'plorRlncRsnCode': plorRlncRsnCode,
        'cmdtCode': cmdtCode,
        'plorQntt': plorQntt,
        'userId': crtrId,
        'plorRlncNum': plorRlncNum,
        'ordrNum': ordrNum,
        'crtrId': crtrId,
    }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    LOGGER.debug(f"cond: {cond}")

    try:
        # Validation Check
        if rdpCode in ['017', '016', '026', '030',]:
            err_msg = f"백야드(BY)는 PDA에서 발주가 불가능합니다."
            LOGGER.error(err_msg)
            return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, '', r.client.host)

        # 발주의뢰 사유에 따른 발주의뢰구분코드
        if cond['plorRlncRsnCode'] == '003':
            cond['plorRlncDvsnCode'] = '008'

        elif cond['plorRlncRsnCode'] == '030':
            cond['plorRlncDvsnCode'] = '019'

        elif cond['plorRlncRsnCode'] == '029':
            cond['plorRlncDvsnCode'] = '018'

        elif cond['plorRlncRsnCode'] == '028':
            cond['plorRlncDvsnCode'] = '014'

        elif cond['plorRlncRsnCode'] == '014':
            cond['plorRlncDvsnCode'] = '010'

        else:
            cond['plorRlncDvsnCode'] = '001'

        # 사용자정보 수집
        LOGGER.debug(f"cond: {cond}")
        user_info, query_str = exec_db(kflow_sess, 'getUserInfo', cond)
        LOGGER.debug(f"user_info: {user_info}")
        if len(user_info) <= 0:
            return make_ret_value(-1, f"[{cond['userId']}]사용자 정보가 없습니다!!", 100, len(cond), cond,
                                  session_list, query_str, r.client.host)
        cond['plorRlncDprtCode'] = user_info[0]['dprtCode']

        # 상품정보 수집
        cond['smstYr'] = get_cur_date_time('%Y')
        cond['smstCode'] = '1' if int(get_cur_date_time('%m')) <= 6 else '2'
        cmdt_info, query_str = exec_db(kflow_sess, 'getCmdtInfo', cond)
        LOGGER.debug(f"cmdt_info: {cmdt_info}")
        if len(cmdt_info) <= 0:
            return make_ret_value(-1, f"[{cond['cmdtCode']}]상품 정보가 없습니다!!", 100, len(cond), cond,
                                  session_list, query_str, r.client.host)
        cond['cmdtId'] = cmdt_info[0]['cmdtId']
        cond['cmdtDvsnCode'] = cmdt_info[0]['cmdtDvsnCode']
        cond['cmdtCode'] = cmdt_info[0]['cmdtCode']
        cond['cmdtName'] = cmdt_info[0]['cmdtName']
        cond['joCode'] = cmdt_info[0]['joCode']
        cond['autrName'] = cmdt_info[0]['autrName']
        cond['pbcmName'] = cmdt_info[0]['pbcmName']
        cond['vndrCode'] = cmdt_info[0]['vndrCode']
        cond['cmdtCdtnCode'] = cmdt_info[0]['cmdtCdtnCode']
        cond['minApicAreaVndrCode'] = cmdt_info[0]['minApicAreaVndrCode']
        cond['maxApicAreaVndrCode'] = cmdt_info[0]['maxApicAreaVndrCode']

        # 점포정보 수집
        rdp_info, query_str = exec_db(kflow_sess, 'getRdpInfoAll', cond)
        # LOGGER.debug(f"rdp_info: {rdp_info}")
        if len(rdp_info) <= 0:
            return make_ret_value(-1, f"점포 정보가 없습니다!!", 100, len(cond), cond,
                                  session_list, query_str, r.client.host)

        # 재정가정보 수집
        reprice_cnt, query_str = exec_db(kflow_sess, 'getRePrceTrgtYsno', cond)
        LOGGER.debug(f"reprice_info: {reprice_cnt}")
        if len(reprice_cnt) <= 0:
            LOGGER.debug(f"[{cond['cmdtCode']}]재정가 대상 아님!!")
            cond['rePriceYsno'] = False
        else:
            if reprice_cnt[0]['cnt'] <= 0:
                LOGGER.debug(f"[{cond['cmdtCode']}]재정가 대상 아님!!")
                cond['rePriceYsno'] = False
                # return make_ret_value(-1, f"[{cond['cmdtCode']}]재정가 정보가 없습니다!!", 100, len(cond), cond,
                #                       session_list, query_str, r.client.host)
            else:
                LOGGER.debug(f"[{cond['cmdtCode']}]재정가 대상!![reprice_cnt: {reprice_cnt[0]['cnt']}]")
                cond['rePriceYsno'] = True

        # 총판매입처 유일여부
        if cond['minApicAreaVndrCode'] != cond['maxApicAreaVndrCode']:
            return make_ret_value(-1, f"지역총판 매입처 코드가 여러개 존재합니다.", 100, len(cond), cond,
                                  session_list, query_str, r.client.host)

        # 기발주의뢰 여부 조회
        sql_name = ''
        LOGGER.debug(f"cond: {cond}]")
        if cond['ordrNum'] != '' and cond['ordrNum'] != 'ordrNum':
            sql_name = 'getPlorRlncExistWithOrdrNum'
        else:
            sql_name = 'getPlorRlncExist'
        LOGGER.debug(f"sql_name: {sql_name}]")
        plor_rlnc_exist, query_str = exec_db(kflow_sess, sql_name, cond)
        LOGGER.debug(f"plor_rlnc_exist: {plor_rlnc_exist}")
        if len(plor_rlnc_exist) <= 0:
            cond['plorRlncNum'] = ''
            cond['plorRlncCdtnCode'] = ''
            # plor_rlnc_exist = [{
            #     'plorRlncNum': '',
            #     'plorRlncCdtnCode': '',
            # }]
            # LOGGER.debug(f"plor_rlnc_exist: {plor_rlnc_exist}")
        else:
            cond['plorRlncNum'] = plor_rlnc_exist[0]['plorRlncNum']
            cond['plorRlncCdtnCode'] = plor_rlnc_exist[0]['plorRlncCdtnCode']

        # 발주의뢰 기본등록 정보 조회
        ordr_request_base_check, query_str = exec_db(kflow_sess, 'findOrderRequestBaseCheck', cond)
        LOGGER.debug(f"ordr_request_base_check: {ordr_request_base_check}")
        if len(ordr_request_base_check) <= 0:
            return make_ret_value(-1, f"발주의뢰 등록 기본 정보가 없습니다.", 100, len(cond), cond,
                                  session_list, query_str, r.client.host)
        cond['wrhsRdpCode'] = ordr_request_base_check[0]['wrhsRdpCode']
        cond['wrhsRdpCode'] = ordr_request_base_check[0]['wrhsRdpCode']

        # Validation Check
        # 해당 수불처가 아니면 예외처리
        if cond['rdpCode'] != user_info[0]['rdpCode']:
            return make_ret_value(-1, f"사용자의 수불처[{user_info[0]['rdpCode']}]와 발주 의뢰 수불처[{cond['rdpCode']}]가 다릅니다", 100, len(cond), cond,
                                  session_list, query_str, r.client.host)
        # 점포제한 다음 점포는 발주의뢰 할 수 없음
        if cond['rdpCode'] in ['050', '020', '009', ]:
            return make_ret_value(-1, f"요청하신 수불처[{cond['rdpCode']}/{rdp_info[cond['rdpCode']]['rdpName']}]는 발주 의뢰 불가 수불처 입니다.", 100,
                                  len(cond), cond, session_list, query_str, r.client.host)

        # [%1$s](은)는 재정가 대상상품이므로 발주의뢰를 할 수 없습니다. 확인후 재시도해주십시오.
        if reprice_cnt[0]['cnt'] > 0:
            return make_ret_value(-1, f"요청 상품[{cond['cmdtCode']}]은 재정가 대상상품으로 발주 의뢰가 불가능합니다.", 100,
                                  len(cond), cond, session_list, query_str, r.client.host)
        # 2-2. 조코드가  음반이면 발주의뢰할 수 없음.
        if cond['joCode'] == '93':
            return make_ret_value(-1, f"음반[jocode: {cond['joCode']}]은 발주 의뢰가 불가능합니다.", 100,
                                  len(cond), cond, session_list, query_str, r.client.host)

        # 2-3. 상품코드가  21로 시작하면 발주의뢰할 수 없음.
        # [수정 2013-06-07 채명훈][요청 2013-05-28 김효영]일부 매입처는 21바코드 발주 허용
        # Add, 2019.04.09, 심윤보, 신규매입처 추가 0130853[요청: 상품전략팀 종화]
        if cond['cmdtCode'][0:2] == '21':
            if cond['vndrCode'] not in ['0116933', '0125189', '0125187', '0125186', '0125188', '0130853']:
                return make_ret_value(-1, f"21코드는 발주 의뢰가 불가능합니다.", 100,
                                      len(cond), cond, session_list, query_str, r.client.host)

        # 2-4. 학기별대학교재이면 발주의뢰할 수 없음.
        if cmdt_info[0]['smstUnvrCnt'] > 0:
            return make_ret_value(-1, f"[{cond['cmdtCode']}] 학기별 대학교재는 발주 의뢰할 수 없습니다.", 100,
                                  len(cond), cond, session_list, query_str, r.client.host)

        # 2-5. 광화문만 발주의뢰가능한 상품매입처
        if cond['vndrCode'] in ['0114490', '0114956', ]:
            if cond['rdpCode'] != '001':
                return make_ret_value(-1, f"[{cond['vndrCode']}]발주 의뢰할 수 없는 매입처의 상품입니다.", 100,
                                      len(cond), cond, session_list, query_str, r.client.host)

        # 상품상태 체크(참고:UI단에도 동일한 조건 적용되어 있음.)
        if cond['cmdtCdtnCode'] not in ['001', '002', '003', '010', '015', '017', '019', '011', '047', '014', '040', '054', '013', ]:
            return make_ret_value(-1, f"[{cmdt_info[0]['cmdtCdtnCode']}]해당 상품의 상품상태가 발주 의뢰 불가 상태입니다.", 100,
                                  len(cond), cond, session_list, query_str, r.client.host)

        # Add, 2015.09.15, 심윤보, 허브앤스포크 점포의 경우 스포크점포에서는 허브 점포의 지역총판 발주 상품을 발주의뢰 할 수 없다.[요청:상품전략팀 남수선배]
        if cmdt_info[0]['hubApicAreaVndrYsno'] > 0:
            return make_ret_value(-1, f"[{cond['cmdtCode']}]해당 상품이 허브점의 지역총판상품으로 등록되어 있어 스포크점에서의 발주가 불가합니다.", 100,
                                  len(cond), cond, session_list, query_str, r.client.host)

        # Add, 2016.05.02, 심윤보, 발주의뢰사유 아웃바운드(028) 제한 점포 조회
        if cond['plorRlncRsnCode'] == '028':
            return make_ret_value(-1, f"[{cond['rdpCode']}]해당 점포는 물류 슈트미배정으로 아웃바운드 발주의뢰가 불가능합니다. ", 100,
                                  len(cond), cond, session_list, query_str, r.client.host)

        # Add, 2019.02.19, 심윤보, 아웃바운드2 개발 관련 추가, 발주의뢰사유 아웃바운드2(029)이 가능한 점포인지 체크!![요청: 원가TF 박정현팀장님]
        if cond['plorRlncRsnCode'] == '029':
            return make_ret_value(-1, f"[{cond['rdpCode']}]해당 점포는 물류 슈트미배정으로 아웃바운드2 발주의뢰가 불가능합니다. ", 100,
                                  len(cond), cond, session_list, query_str, r.client.host)

        # Add, 2016.01.12, 심윤보, 광화문점을 제외한 타점포 방통대교재(01조) 추가발주의뢰 불가처리[요청:상품전략팀 남수선배, 점포지원팀 범열과장님]
        if cond['rdpCode'] not in ['001', '017', ]:
            if cmdt_info[0]['joCode'] == '01':
                return make_ret_value(-1, f"방통대교재(01조)는 광화문점만 발주의뢰 가능합니다.", 100,
                                      len(cond), cond, session_list, query_str, r.client.host)

        # 발주의뢰 업데이트 또는 신규생성
        if len(plor_rlnc_exist) > 0 and plor_rlnc_exist[0]['plorRlncNum']:
            LOGGER.debug(f"업데이트!!")
            if plor_rlnc_exist[0]['plorRlncCdtnCode'] != '002':
                return make_ret_value(-1, f"해당 상품은 이미[{plor_rlnc_exist[0]['plorRlncNum']}]발주 처리중이므로  수정(삭제)할 수 없습니다.", 100,
                                      len(cond), cond, session_list, query_str, r.client.host)
            if plor_rlnc_exist[0]['plorRlncCdtnCode'] in ['003', '013', ]:
                if cond['ordrNum'] == '' or cond['ordrNum'] == 'ordrNum':
                    return make_ret_value(-1, f"발주의뢰상품 [{cond['cmdtCode']}] 발주의뢰사유가 고객주문인 경우에는 고객예약번호를 입력해야 합니다.",
                                          100, len(cond), cond, session_list, query_str, r.client.host)
                if len(plor_rlnc_exist) > 0:
                    return make_ret_value(-1, f"[{cond['cmdtCode']}]은 고객예약으로 이미 발주의뢰 되었습니다. 해당 건을 삭제하시고 추가발주로 발주의뢰 하십시오.",
                                          100, len(cond), cond, session_list, query_str, r.client.host)

            # 발주의뢰 수정
            plor_rlnc_cond = {
                'plorRlncNum': cond['plorRlncNum'].strip(),
                'plorRlncQntt': cond['plorQntt'],
                'plorRlerId': cond['userId'].strip(),
                'amnrId': cond['userId'],
            }

            # 발주의뢰 수량 수정
            LOGGER.debug(f"plor_rlnc_cond: {plor_rlnc_cond}")
            rgst_ordr_requ, query_str = exec_db(kflow_sess, 'modifyOrderRequestBase', plor_rlnc_cond)
            LOGGER.debug(f"modify_ordr_requ: {rgst_ordr_requ}")
            if len(rgst_ordr_requ) <= 0:
                return make_ret_value(-1, f"발주의뢰 수정에 실패 하였습니다.", 100, len(rgst_ordr_requ), rgst_ordr_requ,
                                      session_list, query_str, r.client.host)


        else:
            LOGGER.debug(f"신규생성!!")

            if cond['plorRlncRsnCode'] == '002':
                # 발주의뢰 사유가 행사[002]면
                cond['evntYsno'] = 'Y'
            else:
                cond['evntYsno'] = 'N'

            if len(plor_rlnc_exist) > 0:
                if plor_rlnc_exist[0]['plorRlncCdtnCode'] in ['003', '013', ]:
                    if cond['ordrNum'] in ['', None, 'ordrNum']:
                        return make_ret_value(-1, f"[{cond['cmdtCode']}] 발주의뢰사유가 고객주문인 경우에는 고객예약번호를 입력해야 합니다.",
                                              100, len(cond), cond, session_list, query_str, r.client.host)
                    if len(plor_rlnc_exist) > 0:
                        return make_ret_value(-1, f"[{cond['cmdtCode']}]은 고객예약으로 이미 발주의뢰 되었습니다. 해당 건을 삭제하시고 추가발주로 발주의뢰 하십시오.",
                                              100, len(cond), cond, session_list, query_str, r.client.host)
            else:
                if cond['ordrNum'] in ['', None, 'ordrNum']:
                    cond['ordrNum'] = ''

            LOGGER.debug(f"cond['minApicAreaVndrCode']: [{cond['minApicAreaVndrCode']}], LEN cond['minApicAreaVndrCode']: {len(cond['minApicAreaVndrCode'])}")
            LOGGER.debug(f"cond['vndrCode']: [{cond['vndrCode']}]")
            if cond['minApicAreaVndrCode'].strip() != '':
                # 지역총판 상품일 경우
                cond['wrhsRdpCode'] = cond['rdpCode']
                cond['vndrCode'] = cond['minApicAreaVndrCode']
                cond['plorWkptCode'] = 'B****'
            else:
                cond['wrhsRdpCode'] = ordr_request_base_check[0]['wrhsRdpCode']
                cond['plorWkptCode'] = ordr_request_base_check[0]['plorWkptCode1']

            # 구매도메인 필터
            # 상품ID와 ISBN이 둘다 없는 경우
            if cond['cmdtId'] == '' and cond['cmdtCode'] == '':
                return make_ret_value(-1, f"상품ID와 상품코드가 존재하지 않습니다.",
                                      100, len(cond), cond, session_list, query_str, r.client.host)

            if cond['plorQntt'] == 0:
                return make_ret_value(-1, f"발주의뢰 수량이 0입니다.",
                                      100, len(cond), cond, session_list, query_str, r.client.host)

            if cond['cmdtId'] == '':
                if cond['cmdtCode'] == '' or cond['cmdtName'] == '' or cond['joCode'] == '' or cond['autrName'] == '' or cond['pbcmName'] == '':
                    return make_ret_value(-1, f"상품ID가 없을경우 상품코드, 조콛, 출판사명, 저자명의 모든 값이 필수로 필요합니다.",
                                          100, len(cond), cond, session_list, query_str, r.client.host)

            if cond['cmdtDvsnCode'] == '':
                return make_ret_value(-1, f"상품구분코드가 없습니다.",
                                      100, len(cond), cond, session_list, query_str, r.client.host)

            if cond['userId'] == '':
                return make_ret_value(-1, f"로그인 사번이 없습니다.",
                                      100, len(cond), cond, session_list, query_str, r.client.host)

            if cond['plorRlncDprtCode'] == '':
                return make_ret_value(-1, f"발주의뢰 부서코드가 없습니다.",
                                      100, len(cond), cond, session_list, query_str, r.client.host)

            # 집책전환수량
            cond['ccbkCnvsQntt'] = 0

            # 발주의뢰번호 채번
            plor_rlnc_prifix = 'R' + get_cur_date_time('%Y%m%d')
            temp_seq_num = get_kflow_seqnum('TM_PLOR_RLNC', 'plor_rlnc_num', plor_rlnc_prifix, cond['crtrId'], 6)
            cond['plorRlncNum'] = plor_rlnc_prifix + temp_seq_num
            LOGGER.debug(f"cond['plorRlncNum']: {cond['plorRlncNum']}")
            if cond['plorRlncNum'] == '':
                return make_ret_value(-1, f"발주의뢰 번호 채번에 실패 하였습니다.", 100,
                                      len(cond), cond, session_list, query_str, r.client.host)

            # 발주의뢰 신규생성
            plor_rlnc_cond = {
                'plorRlncNum': cond['plorRlncNum'].strip(),
                'rdpCode': cond['rdpCode'].strip(),
                'plorRlncDate': cond['plorRlncDate'].strip(),
                'cmdtId': cond['cmdtId'].strip(),
                'isbn': cond['cmdtCode'].strip(),
                'cmdtDvsnCode': cond['cmdtDvsnCode'].strip(),
                'plorRlncDvsnCode': cond['plorRlncDvsnCode'].strip(),
                # Mod, 2021.10.06, 심윤보, 입하수불처 오류 수정[요청: 상품관리팀 소정]
                'wrhsRdpCode': cond['wrhsRdpCode'].strip(),
                'vndrCode': cond['vndrCode'].strip(),
                'byngDvsnCode': '',
                'byngDtlDvsnCode': '',
                'byngRate': 0,
                'byngCost': 0,
                'evntYsno': cond['evntYsno'].strip(),
                'plorRlncQntt': cond['plorQntt'],
                'origPlorRlncQntt': cond['plorQntt'],
                'plorWkptCode': cond['plorWkptCode'].strip(),
                'ccbkCnvsQntt': cond['ccbkCnvsQntt'],
                'plorRlncCdtnCode': '002',
                'plorRlncRsnCode': cond['plorRlncRsnCode'].strip(),
                'plorRlncRsn': ' ',
                'joCode': '',
                'trnpDvsnCode': '',
                'dlgdSchdDate': '',
                'slpcCode': '',
                'ordrNum': '',
                'ordrSrmb': 0,
                'pclrMtr': ' ',
                'bkctBksGrdCode': None,
                'cmdtName': ' ',
                'pbcmName': ' ',
                'autrName': ' ',
                'cmdtPrce': 0,
                'pbcmCode': '',
                'decrId': '',
                'decsDttm': '',
                'plorNtytProsRsnCode': '',
                'plorRlerId': cond['userId'].strip(),
                'plorRlncDprtCode': cond['plorRlncDprtCode'].strip(),
                'origPlorRlncNum': '',
                'crtrId': cond['userId'].strip(),
                # 'cretDttm': '2021-03-13 22:06:21.93',
                'amnrId': cond['userId'].strip(),
                # 'amndDttm': '2021-03-13 22:06:21.93',
                'useYsno': None,
                'lrqnDlgdDlvrDvsnCode': '',
            }

            # 발주의뢰 생성
            LOGGER.debug(f"plor_rlnc_cond: {plor_rlnc_cond}")
            rgst_ordr_requ, query_str = exec_db(kflow_sess, 'registerOrderRequestBase', plor_rlnc_cond)
            LOGGER.debug(f"rgst_ordr_requ: {rgst_ordr_requ}")
            if len(rgst_ordr_requ) <= 0:
                return make_ret_value(-1, f"발주의뢰 생성에 실패 하였습니다.", 100, len(plor_rlnc_cond), plor_rlnc_cond,
                                      session_list, query_str, r.client.host)

        return make_ret_value(0, f"정상처리 완료", 100, len(rgst_ordr_requ), rgst_ordr_requ, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"발주의뢰 처리중 오류가 발생하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, '', r.client.host)


# @APP.post(f"/{SERVICE_NAME}/deleteplorrlnc")
def deleteplorrlnc(r: Request,
                 rdpCode: str = Form('rdpCode'),
                 plorRlncRsnCode: str = Form('plorRlncRsnCode'),
                 plorQntt: int = Form('plorQntt'),
                 cmdtCode: str = Form('cmdtCode'),
                 userId: str = Form('crtrId'),
                 plorRlncNum: str = Form('plorRlncNum'),
                 ordrNum: str = Form('ordrNum'),
                 crtrId: str = Form('crtrId'),
                 ):
    """
    Add, 2021.03.13, 심윤보, 발주의뢰삭제
    :param r:
    :param rdpCode:
    :param plorRlncRsnCode:
    :param cmdtCode:
    :param plorQntt:
    :param userId:
    :param plorRlncNum:
    :param ordrNum:
    :return:
    """
    cond = {
        'plorRlncDate': get_cur_date_time('%Y%m%d'),
        'rdpCode': rdpCode,
        'plorRlncRsnCode': plorRlncRsnCode,
        'cmdtCode': cmdtCode,
        'plorQntt': plorQntt,
        'userId': crtrId,
        'plorRlncNum': plorRlncNum,
        'ordrNum': ordrNum,
        'crtrId': crtrId,
    }
    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    LOGGER.debug(f"cond: {cond}")

    try:
        # 삭제 대상건 조회
        plor_rlnc_list, query_str = exec_db(kflow_sess, 'getPlorRlncExistWithRlncNum', cond)
        LOGGER.debug(f"plor_rlnc_list: {plor_rlnc_list}")
        if len(plor_rlnc_list) <= 0:
            return make_ret_value(-1, f"[{cond['plorRlncNum']}]발주의뢰 정보가 없습니다!!", 100, len(cond), cond,
                                  session_list, query_str, r.client.host)

        if plor_rlnc_list[0]['plorRlncCdtnCode'] != '002':
            return make_ret_value(-1, f"[{cond['plorRlncNum']}]해당 발주의뢰가 이미 처리되어 삭제가 불가능합니다!!", 100, len(cond), cond,
                                  session_list, query_str, r.client.host)

        # 발주의뢰 삭제
        LOGGER.debug(f"plor_rlnc_list: {plor_rlnc_list}")
        dlt_rlnc_list, query_str = exec_db(kflow_sess, 'deletePlorRlncNumWithRlncNum', cond)
        LOGGER.debug(f"dlt_rlnc_list: {dlt_rlnc_list}")
        if len(dlt_rlnc_list) <= 0:
            return make_ret_value(-1, f"발주의뢰 삭제에 실패 하였습니다.", 100, len(dlt_rlnc_list), dlt_rlnc_list,
                                  session_list, query_str, r.client.host)

        dlt_rlnc_list[0].update(cond)
        LOGGER.debug(f"dlt_rlnc_list: {dlt_rlnc_list}")

        return make_ret_value(0, f"정상처리 완료", 100, len(dlt_rlnc_list), dlt_rlnc_list, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"발주의뢰 삭제중 오류가 발생하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, '', r.client.host)


# @APP.post(f"/{SERVICE_NAME}/saveRequestTkoutFromPDA/")
def rgsttakeout(r: Request,
                tkotRdpCode: str = Form('tkotRdpCode'),
                tkotDate: str = Form('tkotDate'),
                tkotWrkNum: str = Form('tkotWrkNum'),
                boxNum: str = Form('boxNum'),
                tkioDvsnCode: str = Form('tkioDvsnCode'),
                tkinRdpCode: str = Form('tkinRdpCode'),
                cmdtCode: str = Form('cmdtCode'),
                tkioQntt: int = Form('tkioQntt'),
                rtgdRsnCode: str = Form('rtgdRsnCode'),
                cmdtPrce: int = Form('cmdtPrce'),
                tkotScreenId: str = Form('tkotScreenId'),
                cmdtJoCode: str = Form('cmdtJoCode'),
                isGridEvent: str = Form('isGridEvent'),
                ):
    """
    Add, 2021.03.15, 심윤보, 반출등록
    :param r:
    :param tkotRdpCode:
    :param tkotDate:
    :param tkotWrkNum:
    :param boxNum:
    :param tkioDvsnCode:
    :param tkinRdpCode:
    :param cmdtCode:
    :param tkioQntt:
    :param rtgdRsnCode:
    :param cmdtPrce:
    :param tkotScreenId:
    :param cmdtJoCode:
    :return:
    """
    cond = {
        'rdpCode': tkotRdpCode,
        'tkotRdpCode': tkotRdpCode,
        'tkotDate': tkotDate,
        'tkotWrkNum': tkotWrkNum,
        'boxNum': boxNum,
        'tkioDvsnCode': tkioDvsnCode,
        'tkinRdpCode': tkinRdpCode,
        'cmdtCode': cmdtCode,
        'tkioQntt': tkioQntt,
        'rtgdRsnCode': rtgdRsnCode,
        'cmdtPrce': cmdtPrce,
        'tkotScreenId': tkotScreenId,
        'cmdtJoCode': cmdtJoCode,
        'isGridEvent': isGridEvent,
    }

    kflow_sess = KFLOW_SESSION()
    session_list = [kflow_sess, ]

    LOGGER.debug(f"cond: {cond}")

    try:
        # 상품ID 수집
        result_set, query_str = exec_db(kflow_sess, 'getCmdtIdByCmdtCode', cond)
        LOGGER.debug(f"result_set: {result_set}")
        if len(result_set) <= 0:
            return make_ret_value(-1, f"상품 정보가 없습니다!!", 100, len(cond), cond,
                                  session_list, query_str, r.client.host)

        if result_set[0]['cmdtId'] == '':
            return make_ret_value(-1, f"상품 정보가 없습니다!!", 100, len(cond), cond,
                                  session_list, query_str, r.client.host)

        cond['cmdtId'] = result_set[0]['cmdtId']

        request_uri = '/kbpda/saveRequestTkoutFromPDA.action'
        retval_json = call_kflow_api(r, request_uri, cond)
        LOGGER.debug(f"retval_json: {retval_json['l_workNum']} / {retval_json['l_boxNum']}")
        cond['tkotWrkNum'] = retval_json['l_workNum']
        cond['boxNum'] = retval_json['l_boxNum']

        return make_ret_value(0, f"정상처리 완료", 100, len(cond), cond, session_list, '', r.client.host)
    except Exception as e:
        err_msg = f"반출입 처리중 오류가 발생하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, '', r.client.host)


def call_kflow_api(r: Request, request_uri: str, cond: dict) -> str:
    global KFLOW_URL
    LOGGER.debug(f"cond: {cond}")
    json_str = json.dumps(cond)
    url = ''.join([KFLOW_URL, request_uri, '?', 'params=', json_str])
    try:
        r = requests.get(url, timeout=TIMEOUT)
        LOGGER.debug(f"req_result: {r.status_code}")
        if r.status_code == 200:
            result_str = r.content.decode('utf-8')
            result = json.loads(result_str)
            LOGGER.debug(result)
            return result
        else:
            result = ''
            return result
    except Exception as e:
        err_msg = f"KFLOW API 호출도중 오류가 발생 하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, [], '', r.client.host)


# @APP.post(f"/{SERVICE_NAME}/getstoreinfo")
async def getstoreinfo(r: Request, rdpcode: str = Form('rdpCode')):
    form_data = await r.form()
    LOGGER.debug(f"type: {type(form_data)}")
    LOGGER.debug(f"form_data[0]: {form_data}")
    for key in form_data:
        LOGGER.debug(f"key: {key}")
        LOGGER.debug(f"value: {form_data[key]}")

    cond = {
        'ip': r.client.host,
        'rdpCode': form_data['rdpCode'],
    }

    kflow_sess = KFLOW_SESSION()

    session_list = [kflow_sess,  ]

    try:
        result_set, query_str = exec_db(kflow_sess, 'findRdpCode', cond)
        LOGGER.debug(f"result_set: {result_set}")
        # LOGGER.debug(f"query_str: {query_str}")

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리중 오류가 발생하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)


# @APP.post(f"/{SERVICE_NAME}/getKflowCode")
async def getKflowCode(r: Request, codeId: str = Form('codeId')):
    form_data = await r.form()
    LOGGER.debug(f"type: {type(form_data)}")
    LOGGER.debug(f"form_data[0]: {form_data}")
    for key in form_data:
        LOGGER.debug(f"key: {key}")
        LOGGER.debug(f"value: {form_data[key]}")

    cond = {
        'ip': r.client.host,
        'codeId': form_data['codeId'],
    }

    kflow_sess = KFLOW_SESSION()

    session_list = [kflow_sess,  ]

    try:
        result_set, query_str = exec_db(kflow_sess, 'getKflowCode', cond)
        LOGGER.debug(f"result_set: {result_set}")
        # LOGGER.debug(f"query_str: {query_str}")

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리중 오류가 발생하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)


# @APP.post(f"/{SERVICE_NAME}/getRdpInfoAll")
async def getRdpInfoAll(r: Request):
    form_data = await r.form()
    for key in form_data:
        LOGGER.debug(f"key: {key}")
        LOGGER.debug(f"value: {form_data[key]}")

    cond = {
        'ip': r.client.host,
        'codeId': form_data['codeId'],
    }

    kflow_sess = KFLOW_SESSION()

    session_list = [kflow_sess, ]

    try:
        result_set, query_str = exec_db(kflow_sess, 'getRdpInfoAll', cond)
        LOGGER.debug(f"result_set: {result_set}")

        return make_ret_value(0, f"정상처리 완료", 100, len(result_set), result_set, session_list, query_str, r.client.host)
    except Exception as e:
        err_msg = f"DB 처리중 오류가 발생하였습니다!!"
        LOGGER.exception(err_msg)
        return make_ret_value(-1, err_msg, 100, len(cond), cond, session_list, query_str, r.client.host)

#
# if __name__ == '__main__':
#     request_uri = '/kbpda/saveRequestTkoutFromPDA.action'
#     cond = {
#         "tkotScreenId": "Default",
#         "tkotRdpCode": "001",
#         "tkinRdpCode": "004",
#         "isGridEvent": "false",
#         "tkotDate": "20210323",
#         "tkioDvsnCode": "003",
#         "tkioQntt": 1,
#         "cmdtJoCode": "15",
#         "cmdtId": "5800006284916",
#         "cmdtPrce": 61600,
#         "handlingUser": {}
#     }
#     retval_json = call_kflow_api(request_uri, cond)
#     LOGGER.debug(f"retval_json: {retval_json['l_workNum']} / {retval_json['l_boxNum']}")
