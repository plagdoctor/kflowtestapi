#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
*************************************************************************
  1. Title   : My public logger class
  2. Writer  : hElLoOnG
  3. Date    : 2020.05.12
  4. ETC     :
  5. History
     - 2020.05.12, helloong, First Created
     - 2022.02.09, helloong, Log file size up!
*************************************************************************
"""
import os
import platform
import logging.handlers
import logging
import msvcrt
import win32api
import win32con


class MyLogger(object):
    def __init__(self, logger_name, file_name, level='debug'):
        self.logger_name = logger_name
        self.file_name = file_name
        self.level = logging.DEBUG

    def flush(self):
        pass

    def get_root_dir(self, file_path):
        return os.path.sep.join(file_path.split(os.path.sep)[:-1])

    def make_dir(self, dir_path):
        if not os.path.exists(dir_path):
            os.makedirs(dir_path)

    def make_logger(self, level='debug'):
        MAX_FILE_SIZE = 100 * 1024 * 1024

        logger = logging.getLogger(self.logger_name)
        if len(logger.handlers) > 0:
            logger.debug(f"Already exists: {self.logger_name}")
            return logger
        # DEBUG < INFO < WARNING < ERROR < CRITICAL
        if level.lower().strip() == 'debug':
            self.level = logging.DEBUG
        elif level.lower().strip() == 'info':
            self.level = logging.INFO
        elif level.lower().strip() == 'warning':
            self.level = logging.WARNING
        elif level.lower().strip() == 'error':
            self.level = logging.ERROR
        elif level.lower().strip() == 'critical':
            self.level = logging.CRITICAL
        logger.setLevel(self.level)
        logger.propagate = 0

        streamFormatter = logging.Formatter('[%(levelname)s] %(asctime)s> %(message)s')
        fileFormatter = logging.Formatter('[%(levelname)s|_|%(processName)s|_|%(funcName)s|_|%(lineno)s] %(asctime)s> %(message)s')

        logPath = os.path.join(os.path.realpath(""), "logs", self.file_name + ".log")
        self.make_dir(self.get_root_dir(logPath))

        fileHandler = logging.handlers.RotatingFileHandler(filename=logPath, maxBytes=MAX_FILE_SIZE, backupCount=100, encoding='utf-8')
        streamHandler = logging.StreamHandler()

        fileHandler.setFormatter(fileFormatter)
        streamHandler.setFormatter(streamFormatter)

        if platform.system() == 'Windows':
            win32api.SetHandleInformation(msvcrt.get_osfhandle(fileHandler.stream.fileno()), win32con.HANDLE_FLAG_INHERIT, 0)

        logger.addHandler(fileHandler)
        logger.addHandler(streamHandler)

        return logger