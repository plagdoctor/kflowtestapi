        SELECT TT1.SALEDATE                                                                                                             AS 'SALEDATE',
               TT1.STORECD + ':' + (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'C03' AND CODE = TT1.STORECD AND DELYN = '0')         AS 'STORECD',

               (CASE WHEN TT1.PLUGB = '1' THEN '교보문고' ELSE '핫트랙스' END)                                                          AS 'PLUGB',
               TT1.TENDERTYPE2 + ':' + (SELECT CODENAME FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E03' AND CODE = TT1.TENDERTYPE2 AND DELYN = '0') AS 'TENDERTYPE2',
               SUM((CASE WHEN TT1.COMPANY = '1' THEN TT1.SALEAMT ELSE 0           END))                                                 AS 'KB_SALEAMT',
               SUM((CASE WHEN TT1.COMPANY = '1' THEN 0           ELSE TT1.SALEAMT END))                                                 AS 'HT_SALEAMT',
               SUM(TT1.SALEAMT)                                                                                                         AS 'TOT_SALEAMT'
          FROM (
                SELECT 'a'                                                       AS 'TYPE',
                       a.SALEDATE                                                                                              AS 'SALEDATE',
                       a.STORECD                                                                                               AS 'STORECD',
                       a.POSNO                                                                                                 AS 'POSNO',
                       a.COMPANY                                                                                               AS 'COMPANY',
                       a.TRANNO                                                                                                AS 'TRANNO',
                       (CASE WHEN b.SALEAMTM <> 0 THEN '1' ELSE '2' END)                                                       AS 'PLUGB',
                       b.TENDERTYPE                                                                                            AS 'TENDERTYPE',
                       (SELECT IsNull(ETC1NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E02' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2',
                       (SELECT IsNull(ETC2NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E02' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2NM',
                       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)                                        AS 'SALEAMT'
                  FROM IDPS..TRNSALHDR a, IDPS..TRNSALPAY b
                 WHERE a.SALEDATE   = b.SALEDATE
                   AND a.STORECD    = b.STORECD
                   AND a.POSNO      = b.POSNO
                   AND a.TRANNO     = b.TRANNO
                   AND a.TRANTYPE   IN ('00', '01')
                   AND a.TRANKIND   NOT IN ('10','11','13','14','15', '12', '17')
                   AND b.TENDERTYPE NOT IN ('03', '12')

                AND a.SALEDATE='20210101'
                       AND a.STORECD  = '029'

                UNION ALL
                SELECT 'b'                                                            AS 'TYPE',
                       a.SALEDATE                                                                                              AS 'SALEDATE',
                       a.STORECD                                                                                               AS 'STORECD',
                       a.POSNO                                                                                                 AS 'POSNO',
                       a.COMPANY                                                                                               AS 'COMPANY',
                       a.TRANNO                                                                                                AS 'TRANNO',
                       (CASE WHEN b.SALEAMTM <> 0 THEN '1' ELSE '2' END)                                                       AS 'PLUGB',
                       b.TENDERTYPE                                                                                            AS 'TENDERTYPE',
                       (SELECT IsNull(ETC1NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E02' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2',
                       (SELECT IsNull(ETC2NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E02' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2NM',
                       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)                                        AS 'SALEAMT'
                  FROM IDPS..TRNSALHDR a, IDPS..TRNSALPAY b
                 WHERE a.SALEDATE   = b.SALEDATE
                   AND a.STORECD    = b.STORECD
                   AND a.POSNO      = b.POSNO
                   AND a.TRANNO     = b.TRANNO
                   AND a.TRANTYPE   IN ('00', '01')
                   AND a.TRANKIND   IN ('13','14', '17')
                   AND b.TENDERTYPE NOT IN ('03', '12')

                AND a.SALEDATE='20210101'
                       AND a.STORECD  = '029'

                UNION ALL
                SELECT 'c'                                                                    AS 'TYPE',
                       a.CLOSEDATE                                                                                             AS 'SALEDATE',
                       a.STORECD                                                                                               AS 'STORECD',
                       a.POSNO                                                                                                 AS 'POSNO',
                       a.COMPANY                                                                                               AS 'COMPANY',
                       a.TRANNO                                                                                                AS 'TRANNO',
                       (CASE WHEN b.SALEAMTM <> 0 THEN '1' ELSE '2' END)                                                       AS 'PLUGB',
                       b.TENDERTYPE                                                                                            AS 'TENDERTYPE',
                       (SELECT IsNull(ETC1NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E02' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2',
                       (SELECT IsNull(ETC2NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E02' AND CODE = b.TENDERTYPE AND DELYN = '0') AS 'TENDERTYPE2NM',
                       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)                                        AS 'SALEAMT'
                  FROM IDPS..VW_TRNSALHDRDTL a, IDPS..TRNSALPAY b
                 WHERE a.SALEDATE   = b.SALEDATE
                   AND a.STORECD    = b.STORECD
                   AND a.POSNO      = b.POSNO
                   AND a.TRANNO     = b.TRANNO
                   AND a.SEQ        = 1
                   AND a.TRANTYPE   IN ('00', '01')
                   AND a.TRANKIND  = '15' AND a.SALEFLAG  = '1' AND a.PAYFLAG   = '0'
                   AND b.TENDERTYPE NOT IN ('03', '12')

                AND a.CLOSEDATE='20210101'
                       AND a.STORECD  = '029'


                UNION ALL
                SELECT 'd'                                                 AS 'TYPE',
                       a.SALEDATE                                                                                              AS 'SALEDATE',
                       a.STORECD                                                                                               AS 'STORECD',
                       a.POSNO                                                                                                 AS 'POSNO',
                       a.COMPANY                                                                                               AS 'COMPANY',
                       a.TRANNO                                                                                                AS 'TRANNO',
                       (CASE WHEN b.SALEAMTM <> 0 THEN '1' ELSE '2' END)                                                       AS 'PLUGB',
                       '03'                                                                                                    AS 'TENDERTYPE',
                       (SELECT IsNull(ETC1NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E01' AND CODE = b.GVTYPE AND DELYN = '0')     AS 'TENDERTYPE2',
                       (SELECT IsNull(ETC2NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E01' AND CODE = b.GVTYPE AND DELYN = '0')     AS 'TENDERTYPE2NM',
                       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)                                        AS 'SALEAMT'
                  FROM IDPS..TRNSALHDR a, IDPS..TRNGVPAY b
                 WHERE a.SALEDATE   = b.SALEDATE
                   AND a.STORECD    = b.STORECD
                   AND a.POSNO      = b.POSNO
                   AND a.TRANNO     = b.TRANNO
                   AND a.TRANTYPE   IN ('00', '01')
                   AND a.TRANKIND   NOT IN ('10', '11','13','14', '15', '12', '17')

                AND a.SALEDATE='20210101'
                       AND a.STORECD  = '029'




                UNION ALL
                SELECT 'e'                                                                    AS 'TYPE',
                       a.CLOSEDATE                                                                                             AS 'SALEDATE',
                       a.STORECD                                                                                               AS 'STORECD',
                       a.POSNO                                                                                                 AS 'POSNO',
                       a.COMPANY                                                                                               AS 'COMPANY',
                       a.TRANNO                                                                                                AS 'TRANNO',
                       (CASE WHEN b.SALEAMTM <> 0 THEN '1' ELSE '2' END)                                                       AS 'PLUGB',
                       '03'                                                                                                    AS 'TENDERTYPE',
                       (SELECT IsNull(ETC1NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E01' AND CODE = b.GVTYPE AND DELYN = '0')     AS 'TENDERTYPE2',
                       (SELECT IsNull(ETC2NM, '') FROM IDPS..MSTCOMMON WHERE CDTYPE = 'E01' AND CODE = b.GVTYPE AND DELYN = '0')     AS 'TENDERTYPE2NM',
                       (CASE WHEN a.TRANTYPE = '00' THEN b.SALEAMT ELSE -b.SALEAMT END)                                        AS 'SALEAMT'
                  FROM IDPS..VW_TRNSALHDRDTL a, IDPS..TRNGVPAY b
                 WHERE a.SALEDATE   = b.SALEDATE
                   AND a.STORECD    = b.STORECD
                   AND a.POSNO      = b.POSNO
                   AND a.TRANNO     = b.TRANNO
                   AND a.SEQ        = 1
                   AND a.TRANTYPE   IN ('00', '01')
                   AND a.TRANKIND  = '15' AND a.SALEFLAG  = '1' AND a.PAYFLAG   = '0'

                AND a.CLOSEDATE='20210101'
                       AND a.STORECD  = '029'



        ) TT1
        GROUP BY TT1.SALEDATE, TT1.STORECD, TT1.PLUGB, TT1.TENDERTYPE2, TT1.COMPANY
        ORDER BY 1,2,3,4,5

        AT ISOLATION 0
