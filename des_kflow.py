"""
Title: KFLOW Crypt module of PBEWithMD5AndDES for kyobobook center
Ref: https://gist.github.com/jpralves/505e653fd1c7358ad2c540e25e1ee80a
"""
from Crypto.Hash import MD5
from Crypto.Cipher import DES
import base64
import random


class PBEWithMD5AndDES:
    def __init__(self, bs=8, iterations=0):
        self.bs = bs
        self._iterations = iterations

    def randomBytes(self, n):
        return bytearray(random.getrandbits(8) for i in range(n))

    def encrypt(self, password, plaintext):
        bs = 8
        _iterations = 0

        data = bytes(plaintext.encode())
        padding = bs - (len(data) % bs)
        data += bytes((chr(padding) * padding).encode())

        salt = self.randomBytes(bs)

        hasher = MD5.new()
        hasher.update(bytearray(password))
        hasher.update(salt)
        result = hasher.digest()

        for i in range(1, _iterations):
            hasher = MD5.new()
            hasher.update(result)
            result = hasher.digest()

        encoder = DES.new(result[:bs], DES.MODE_CBC, result[bs:bs*2])
        encrypted = encoder.encrypt(data)

        salt = base64.b64encode(salt)
        encrypted = base64.b64encode(encrypted)

        return salt + encrypted

    def decrypt(self, password, data_to_decrypt):
        bs = 8
        _iterations = 0

        salt = base64.b64decode(data_to_decrypt[:12])
        data = base64.b64decode(data_to_decrypt[12:])

        hasher = MD5.new()
        hasher.update(bytearray(password))
        hasher.update(bytearray(salt))
        result = hasher.digest()

        for i in range(1, _iterations):
            hasher = MD5.new()
            hasher.update(result)
            result = hasher.digest()

        encoder = DES.new(result[:bs], DES.MODE_CBC, result[bs:bs*2])
        decrypted = encoder.decrypt(bytes(data))

        length = len(decrypted)
        if len(decrypted) > 0:
            unpadding = int(decrypted[length-1])
        else:
            unpadding = 0

        if length - unpadding > 0:
            return decrypted[:(length - unpadding)].decode()
        else:
            return decrypted.decode()


if "__main__" == __name__:
    des = PBEWithMD5AndDES(8, 0)

    enstr = des.encrypt(b'kyobobook', 'haebook@hanmail.net')
    print(f"enstr: {enstr}")
    destr = des.decrypt(b'kyobobook', 'o+eo/M0WLYQ=B3W3dRBAwQRHIiSm85T2C895f+BcWkdD')
    print(f"destr: {destr}")
